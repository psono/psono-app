import 'package:flutter/material.dart';
import 'package:psono/protected_screen.dart';
import 'package:psono/screens/account/account_screen.dart';
import 'package:psono/screens/app_home/app_home_screen.dart';
import 'package:psono/screens/autofill_onboarding/autofill_onboarding.dart';
import 'package:psono/screens/datastore/datastore_screen.dart';
import 'package:psono/screens/enable_biometrics/enable_biometrics.dart';
import 'package:psono/screens/home/home_screen.dart';
import 'package:psono/screens/key_transfer/key_transfer_screen.dart';
import 'package:psono/screens/lost_password/lost_password_screen.dart';
import 'package:psono/screens/onboarding/app_onboarding.dart';
import 'package:psono/screens/pin/pin_screen.dart';
import 'package:psono/screens/pin_configuration/pin_configuration_screen.dart';
import 'package:psono/screens/privacy_policy/index.dart';
import 'package:psono/screens/register/register_screen.dart';
import 'package:psono/screens/scan_config/index.dart';
import 'package:psono/screens/settings/settings_screen.dart';
import 'package:psono/screens/signin/signin_screen.dart';

class AppRoutes {
  static const String initial = '/';
  static const String appOnboarding = '/app_onboarding/';
  static const String account = '/account/';
  static const String autofill = '/autofill/';
  static const String datastore = '/datastore/';
  static const String autofillOnboarding = '/autofill_onboarding/';
  static const String lostPassword = '/lost_password/';
  static const String passphrase = '/passphrase/';
  static const String privacyPolicy = '/privacy_policy/';
  static const String settings = '/settings/';
  static const String signin = '/signin/';
  static const String keyTransferScreen = '/key_transfer/';
  static const String pinConfigurationScreen = '/pin_configuration_screen/';
  static const String register = '/register/';
  static const String enableBioMetrics = '/enable_biometrics/';
  static const String scanConfig = '/scan_config/';

  static Route<dynamic>? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case initial:
        return MaterialPageRoute(builder: (context) => HomeScreen());
      case appOnboarding:
        return MaterialPageRoute(builder: (context) => const AppOnboarding());
      case account:
        return MaterialPageRoute(
            builder: (context) => ProtectedScreen(child: AccountScreen()));
      case autofill:
        return MaterialPageRoute(
          builder: (context) => ProtectedScreen(
            child: const DatastoreScreen(),
          ),
        );
      case datastore:
        return MaterialPageRoute(
          builder: (context) => ProtectedScreen(
            child: const AppHome(),
          ),
        );
      case autofillOnboarding:
        return MaterialPageRoute(
            builder: (context) => AutofillOnboardingScreen());
      case lostPassword:
        return MaterialPageRoute(builder: (context) => LostPasswordScreen());
      case passphrase:
        return MaterialPageRoute(builder: (context) => PinScreen());
      case privacyPolicy:
        return MaterialPageRoute(builder: (context) => PrivacyPolicyScreen());
      case settings:
        return MaterialPageRoute(
            builder: (context) => ProtectedScreen(child: SettingsScreen()));
      case signin:
        return MaterialPageRoute(builder: (context) => SigninScreen());
      case pinConfigurationScreen:
        return MaterialPageRoute(
            builder: (context) => PinConfigurationScreen());
      case keyTransferScreen:
        return MaterialPageRoute(builder: (context) => KeyTransferScreen());
      case register:
        return MaterialPageRoute(builder: (context) => RegisterScreen());
      case scanConfig:
        return MaterialPageRoute(builder: (context) => ScanConfigScreen());
      case enableBioMetrics:
        return MaterialPageRoute(
            builder: (context) => const EnableBioMetrics());
      default:
        return null;
    }
  }
}
