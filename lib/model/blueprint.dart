import 'package:flutter/widgets.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class DropDownMenuItem {
  DropDownMenuItem({
    this.icon,
    this.text,
    this.hideOffline,
    this.hideOnNotWrite,
    this.onclick,
  });

  final IconData? icon;
  final String? text;
  final bool? hideOffline;
  final bool? hideOnNotWrite;
  final Function? onclick;
}

class BlueprintField {
  BlueprintField({
    this.name,
    this.field,
    this.type,
    this.title,
    this.validationType,
    this.placeholder,
    this.required = false,
    this.position,
    this.onChange,
    this.onAdd,
    this.onDelete,
    this.hidden = false,
    this.readonly = false,
    this.onClick,
    this.classname,
    this.errorMessageRequired,
    this.inputFormatter,
  });

  final String? name;
  final String? field;
  final String? type;
  final String? title;
  final String? validationType;
  final String? placeholder;
  bool required;
  final String? position;
  final String? onChange;
  final String? onAdd;
  final String? onDelete;
  bool hidden;
  bool readonly;
  final String? onClick;
  final String? classname;
  final String? errorMessageRequired;
  final MaskTextInputFormatter? inputFormatter;
}

class Blueprint {
  Blueprint({
    required this.id,
    required this.name,
    required this.icon,
    required this.settingField,
    required this.settingFieldDefault,
    required this.titleField,
    this.descriptionField,
    required this.imagePath,
    this.urlfilterField,
    this.autosubmitField,
    this.search,
    this.fields,
  });

  final String id;
  final String name;
  final IconData icon;
  final String imagePath;
  final String settingField;
  final bool settingFieldDefault;
  final String titleField;
  final String? descriptionField;
  final String? urlfilterField;
  final String? autosubmitField;
  final List<String>? search;
  final List<BlueprintField>? fields;
}
