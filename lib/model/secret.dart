import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';
import 'package:psono/services/manager_secret.dart' as managerSecret;

part 'secret.g.dart';

@JsonSerializable()
class KeyValue {
  KeyValue({
    this.key,
    this.value,
  });

  final String? key;
  final String? value;

  factory KeyValue.fromJson(Map<String, dynamic> json) =>
      _$KeyValueFromJson(json);

  Map<String, dynamic> toJson() => _$KeyValueToJson(this);
}

@JsonSerializable()
class CustomFields {
  CustomFields({
    this.name,
    this.type,
    this.value,
  });

  final String? name;
  final String? type;
  final String? value;

  factory CustomFields.fromJson(Map<String, dynamic> json) =>
      _$CustomFieldsFromJson(json);

  Map<String, dynamic> toJson() => _$CustomFieldsToJson(this);
}

@JsonSerializable()
class SecretData {
  SecretData({
    this.type,
    this.customFields,
    // application password
    this.applicationPasswordTitle,
    this.applicationPasswordUsername,
    this.applicationPasswordPassword,
    this.applicationPasswordNotes,
    // totp:
    this.totpTitle,
    this.totpCode,
    this.totpPeriod,
    this.totpDigits,
    this.totpAlgorithm,
    this.totpNotes,
    this.totpUrlFilter,
    // website password:
    this.websitePasswordTitle,
    this.websitePasswordUrl,
    this.websitePasswordUsername,
    this.websitePasswordPassword,
    this.websitePasswordNotes,
    this.websitePasswordAutoSubmit,
    this.websitePasswordUrlFilter,
    this.websitePasswordTotpCode,
    this.websitePasswordTotpPeriod,
    this.websitePasswordTotpDigits,
    this.websitePasswordTotpAlgorithm,
    // notes:
    this.noteTitle,
    this.noteNotes,
    // environment variables:
    this.environmentVariablesTitle,
    this.environmentVariablesVariables,
    this.environmentVariablesNotes,
    // files
    this.fileTitle,
    this.file,
    this.fileId,
    this.fileShardId,
    this.fileRepositoryId,
    this.fileDestination,
    this.fileSecretKey,
    this.fileSize,
    this.fileChunks,
    // credit card
    this.creditCardTitle,
    this.creditCardNumber,
    this.creditCardCVC,
    this.creditCardPIN,
    this.creditCardName,
    this.creditCardValidThrough,
    this.creditCardNotes,
    // ssh key
    this.sshOwnKeyTitle,
    this.sshOwnKeyPublic,
    this.sshOwnKeyPrivate,
    this.sshOwnKeyNotes,
    // elster certificate
    this.elsterCertificateTitle,
    this.elsterCertificateFileContent,
    this.elsterCertificatePassword,
    this.elsterCertificateRetrievalCode,
    this.elsterCertificateNotes,
    // mail gpg key
    this.mailGPGOwnKeyTitle,
    this.mailGPGOwnKeyEmail,
    this.mailGPGOwnKeyName,
    this.mailGPGOwnKeyPublic,
    this.mailGPGOwnKeyPrivate,
    this.mailGPGOwnKeyGenerateNew,
    this.mailGPGOwnKeyGenerateImportText,
    this.mailGPGOwnKeyEncryptMessaget,
    this.mailGPGOwnKeyDecryptMessaget,
    // bookmark
    this.bookmarkTitle,
    this.bookmarkUrl,
    this.bookmarkNotes,
    this.bookmarkUrlFilter,
  });

  final String? type;

  @JsonKey(name: 'custom_fields')
  final List<CustomFields>? customFields;

  @JsonKey(name: 'application_password_title')
  final String? applicationPasswordTitle;
  @JsonKey(name: 'application_password_username')
  final String? applicationPasswordUsername;
  @JsonKey(name: 'application_password_password')
  final String? applicationPasswordPassword;
  @JsonKey(name: 'application_password_notes')
  final String? applicationPasswordNotes;

  @JsonKey(name: 'totp_title')
  final String? totpTitle;
  @JsonKey(name: 'totp_code')
  final String? totpCode;
  @JsonKey(name: 'totp_period')
  final String? totpPeriod;
  @JsonKey(name: 'totp_digits')
  final String? totpDigits;
  @JsonKey(name: 'totp_algorithm')
  final String? totpAlgorithm;
  @JsonKey(name: 'totp_notes')
  final String? totpNotes;
  @JsonKey(name: 'totp_url_filter')
  final String? totpUrlFilter;

  @JsonKey(name: 'website_password_title')
  final String? websitePasswordTitle;
  @JsonKey(name: 'website_password_url')
  final String? websitePasswordUrl;
  @JsonKey(name: 'website_password_username')
  final String? websitePasswordUsername;
  @JsonKey(name: 'website_password_password')
  final String? websitePasswordPassword;
  @JsonKey(name: 'website_password_notes')
  final String? websitePasswordNotes;
  @JsonKey(name: 'website_password_auto_submit')
  final bool? websitePasswordAutoSubmit;
  @JsonKey(name: 'website_password_url_filter')
  final String? websitePasswordUrlFilter;
  @JsonKey(name: 'website_password_totp_code')
  final String? websitePasswordTotpCode;
  @JsonKey(name: 'website_password_totp_period')
  final String? websitePasswordTotpPeriod;
  @JsonKey(name: 'website_password_totp_digits')
  final String? websitePasswordTotpDigits;
  @JsonKey(name: 'website_password_totp_algorithm')
  final String? websitePasswordTotpAlgorithm;

  @JsonKey(name: 'note_title')
  final String? noteTitle;
  @JsonKey(name: 'note_notes')
  final String? noteNotes;

  @JsonKey(name: 'environment_variables_title')
  final String? environmentVariablesTitle;
  @JsonKey(name: 'environment_variables_variables')
  final List<KeyValue>? environmentVariablesVariables;
  @JsonKey(name: 'environment_variables_notes')
  final String? environmentVariablesNotes;

  @JsonKey(name: 'file_title')
  final String? fileTitle;
  @JsonKey(name: 'file')
  final String? file;
  @JsonKey(name: 'file_id')
  final String? fileId;
  @JsonKey(name: 'file_shard_id')
  final String? fileShardId;
  @JsonKey(name: 'file_repository_id')
  final String? fileRepositoryId;
  @JsonKey(name: 'file_destinations')
  final String? fileDestination;
  @JsonKey(name: 'file_secret_key')
  final String? fileSecretKey;
  @JsonKey(name: 'file_size')
  final String? fileSize;
  @JsonKey(name: 'file_chunks')
  final String? fileChunks;

  @JsonKey(name: 'credit_card_title')
  final String? creditCardTitle;
  @JsonKey(name: 'credit_card_number')
  final String? creditCardNumber;
  @JsonKey(name: 'credit_card_cvc')
  final String? creditCardCVC;
  @JsonKey(name: 'credit_card_pin')
  final String? creditCardPIN;
  @JsonKey(name: 'credit_card_name')
  final String? creditCardName;
  @JsonKey(name: 'credit_card_valid_through')
  final String? creditCardValidThrough;
  @JsonKey(name: 'credit_card_notes')
  final String? creditCardNotes;

  @JsonKey(name: 'ssh_own_key_title')
  final String? sshOwnKeyTitle;
  @JsonKey(name: 'ssh_own_key_public')
  final String? sshOwnKeyPublic;
  @JsonKey(name: 'ssh_own_key_private')
  final String? sshOwnKeyPrivate;
  @JsonKey(name: 'ssh_own_key_notes')
  final String? sshOwnKeyNotes;

  @JsonKey(name: 'elster_certificate_title')
  final String? elsterCertificateTitle;
  @JsonKey(name: 'elster_certificate_file_content')
  final String? elsterCertificateFileContent;
  @JsonKey(name: 'elster_certificate_password')
  final String? elsterCertificatePassword;
  @JsonKey(name: 'elster_certificate_retrieval_code')
  final String? elsterCertificateRetrievalCode;
  @JsonKey(name: 'elster_certificate_notes')
  final String? elsterCertificateNotes;

  @JsonKey(name: 'mail_gpg_own_key_title')
  final String? mailGPGOwnKeyTitle;
  @JsonKey(name: 'mail_gpg_own_key_email')
  final String? mailGPGOwnKeyEmail;
  @JsonKey(name: 'mail_gpg_own_key_name')
  final String? mailGPGOwnKeyName;
  @JsonKey(name: 'mail_gpg_own_key_public')
  final String? mailGPGOwnKeyPublic;
  @JsonKey(name: 'mail_gpg_own_key_private')
  final String? mailGPGOwnKeyPrivate;
  @JsonKey(name: 'mail_gpg_own_key_generate_new')
  final String? mailGPGOwnKeyGenerateNew;
  @JsonKey(name: 'mail_gpg_own_key_generate_import_text')
  final String? mailGPGOwnKeyGenerateImportText;
  @JsonKey(name: 'mail_gpg_own_key_encrypt_message')
  final String? mailGPGOwnKeyEncryptMessaget;
  @JsonKey(name: 'mail_gpg_own_key_decrypt_message')
  final String? mailGPGOwnKeyDecryptMessaget;
  @JsonKey(name: 'bookmark_title')
  final String? bookmarkTitle;
  @JsonKey(name: 'bookmark_url')
  final String? bookmarkUrl;
  @JsonKey(name: 'bookmark_notes')
  final String? bookmarkNotes;
  @JsonKey(name: 'bookmark_url_filter')
  final String? bookmarkUrlFilter;

  factory SecretData.fromJson(Map<String, dynamic> json) =>
      _$SecretDataFromJson(json);

  Map<String, dynamic> toJson() => _$SecretDataToJson(this);
}

/// Represents a datastore
@JsonSerializable()
class Secret {
  Secret({
    this.createDate,
    this.writeDate,
    this.secretId,
    this.type,
    this.data,
    this.callbackUrl,
    this.callbackUser,
    this.callbackPass,
    this.secretKey,
  });

  final String? createDate;
  final String? writeDate;
  String? secretId;
  String? type;
  dynamic data;
  String? callbackUrl;
  String? callbackUser;
  String? callbackPass;
  @JsonKey(name: 'secret_key', fromJson: fromHex, toJson: toHex)
  Uint8List? secretKey;

  factory Secret.fromJson(Map<String, dynamic> json) => _$SecretFromJson(json);

  Map<String, dynamic> toJson() => _$SecretToJson(this);

  save() async {
    return await managerSecret.writeSecret(
      this.secretId,
      this.secretKey!,
      data,
      this.callbackUrl,
      this.callbackUser,
      this.callbackPass,
    );
  }
}
