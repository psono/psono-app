// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_result_decrypted.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Policies _$PoliciesFromJson(Map<String, dynamic> json) => Policies(
      complianceEnforce2fa: json['compliance_enforce_2fa'] as bool?,
      complianceDisableExport: json['compliance_disable_export'] as bool?,
      complianceServerSecrets: json['compliance_server_secrets'] as String?,
      complianceDisableDeleteAccount:
          json['compliance_disable_delete_account'] as bool?,
      complianceDisableOfflineMode:
          json['compliance_disable_offline_mode'] as bool?,
      complianceMaxOfflineCacheTimeValid:
          (json['compliance_max_offline_cache_time_valid'] as num?)?.toInt(),
      complianceDisableApiKeys: json['compliance_disable_api_keys'] as bool?,
      complianceDisableEmergencyCodes:
          json['compliance_disable_emergency_codes'] as bool?,
      complianceDisableRecoveryCodes:
          json['compliance_disable_recovery_codes'] as bool?,
      complianceDisableFileRepositories:
          json['compliance_disable_file_repositories'] as bool?,
      complianceDisableLinkShares:
          json['compliance_disable_link_shares'] as bool?,
      allowedSecondFactors: (json['allowed_second_factors'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      allowUserSearchByEmail: json['allow_user_search_by_email'] as bool?,
      allowUserSearchByUsernamePartial:
          json['allow_user_search_by_username_partial'] as bool?,
    );

Map<String, dynamic> _$PoliciesToJson(Policies instance) => <String, dynamic>{
      'compliance_enforce_2fa': instance.complianceEnforce2fa,
      'compliance_disable_export': instance.complianceDisableExport,
      'compliance_server_secrets': instance.complianceServerSecrets,
      'compliance_disable_delete_account':
          instance.complianceDisableDeleteAccount,
      'compliance_disable_offline_mode': instance.complianceDisableOfflineMode,
      'compliance_max_offline_cache_time_valid':
          instance.complianceMaxOfflineCacheTimeValid,
      'compliance_disable_api_keys': instance.complianceDisableApiKeys,
      'compliance_disable_emergency_codes':
          instance.complianceDisableEmergencyCodes,
      'compliance_disable_recovery_codes':
          instance.complianceDisableRecoveryCodes,
      'compliance_disable_file_repositories':
          instance.complianceDisableFileRepositories,
      'compliance_disable_link_shares': instance.complianceDisableLinkShares,
      'allowed_second_factors': instance.allowedSecondFactors,
      'allow_user_search_by_email': instance.allowUserSearchByEmail,
      'allow_user_search_by_username_partial':
          instance.allowUserSearchByUsernamePartial,
    };

User _$UserFromJson(Map<String, dynamic> json) => User(
      username: json['username'] as String?,
      publicKey: fromHex(json['public_key'] as String?),
      privateKey: fromHex(json['private_key'] as String?),
      privateKeyNonce: fromHex(json['private_key_nonce'] as String?),
      userSauce: json['user_sauce'] as String?,
      policies: json['policies'] == null
          ? null
          : Policies.fromJson(json['policies'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'username': instance.username,
      'public_key': toHex(instance.publicKey),
      'private_key': toHex(instance.privateKey),
      'private_key_nonce': toHex(instance.privateKeyNonce),
      'user_sauce': instance.userSauce,
      'policies': instance.policies,
    };

LoginResultDecrypted _$LoginResultDecryptedFromJson(
        Map<String, dynamic> json) =>
    LoginResultDecrypted(
      token: json['token'] as String?,
      requiredMultifactors: (json['required_multifactors'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      sessionPublicKey: fromHex(json['session_public_key'] as String?),
      sessionSecretKey: fromHex(json['session_secret_key'] as String?),
      sessionSecretKeyNonce:
          fromHex(json['session_secret_key_nonce'] as String?),
      password: json['password'] as String?,
      userValidator: fromHex(json['user_validator'] as String?),
      userValidatorNonce: fromHex(json['user_validator_nonce'] as String?),
      user: json['user'] == null
          ? null
          : User.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResultDecryptedToJson(
        LoginResultDecrypted instance) =>
    <String, dynamic>{
      'token': instance.token,
      'required_multifactors': instance.requiredMultifactors,
      'session_public_key': toHex(instance.sessionPublicKey),
      'session_secret_key': toHex(instance.sessionSecretKey),
      'session_secret_key_nonce': toHex(instance.sessionSecretKeyNonce),
      'password': instance.password,
      'user_validator': toHex(instance.userValidator),
      'user_validator_nonce': toHex(instance.userValidatorNonce),
      'user': instance.user,
    };
