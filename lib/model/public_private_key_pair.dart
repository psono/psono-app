import 'dart:typed_data';

/// Represents a secret key and a corresponding private key.
class PublicPrivateKeyPair {
  final Uint8List publicKey, privateKey;

  const PublicPrivateKeyPair(this.publicKey, this.privateKey);
}
