class OTP {
  OTP({
    this.type,
    this.label,
    this.issuer,
    this.secret,
    this.algorithm,
    this.period,
    this.digits,
  });

  String? type;
  String? label;
  String? issuer;
  String? secret;
  String? algorithm = 'SHA1';
  int? counter;
  int? period;
  int? digits;

  String getDescription() {
    String description = '';
    if (issuer != null) {
      description = description + issuer!;
    }
    if (description != '' && label != null && label != '') {
      description = description + ':';
    }
    if (label != null && label != '') {
      description = description + label!;
    }
    return description;
  }

  @override
  String toString() {
    return 'OTP{type: $type, label: $label, issuer: $issuer, secret: $secret, algorithm: $algorithm, period: $period, digits: $digits}';
  }
}
