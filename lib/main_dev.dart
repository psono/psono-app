import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:psono/services/offline_cache.dart' as offline_cache;
import 'package:psono/utils/app_constants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await offline_cache.init();

  Timer(Duration(milliseconds: 10000), () {
    offline_cache.incrementalUpdate(ratelimit: true);
  });

  var configuredApp = Builder(
    builder: (context) {
      final double ratio = MediaQuery.of(context).size.width /
          MediaQuery.of(context).size.height;
      const double designHeight = K.kDesignHeight;
      final double designWidth = ratio > 0.6 ? 470 : K.kDesignWidth;
      final designSize = Size(designWidth, designHeight);
      return ScreenUtilInit(
        designSize: designSize,
        builder: (context, _) {
          return AppConfig(
            appName: 'Psono Dev',
            flavorName: 'development',
            defaultServerUrl:
                'https://d18e-2a09-bac5-29c3-246e-00-3a1-26.ngrok-free.app/server',
            child: MyApp(),
          );
        },
      );
    },
  );
  runApp(configuredApp);
}
