import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:psono/components/text_dm_sans.dart';

class BtnText extends StatelessWidget {
  BtnText(
      {Key? key,
      required this.text,
      required this.onPressed,
      this.color = Colors.white,
      this.fontWeight = FontWeight.w700,
      double? fontSize})
      : this.fontSize = fontSize ?? 14.sp,
        super(key: key);
  final String text;
  final Function() onPressed;
  final Color color;
  final FontWeight fontWeight;
  final double fontSize;
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        textStyle: GoogleFonts.dmSans(
          fontSize: 14.sp,
          fontWeight: FontWeight.w700,
        ),
      ),
      onPressed: onPressed,
      child: TextDmSans(
        text: text,
        textAlign: TextAlign.center,
        color: color,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
    );
  }
}
