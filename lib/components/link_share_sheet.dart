import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:omni_datetime_picker/omni_datetime_picker.dart';
import 'package:psono/services/manager_share_link.dart' as managerShareLink;
import 'package:psono/model/datastore.dart' as datastore_model;
import 'package:psono/services/manager_datastore_password.dart'
    as manager_datastore_password;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/components/_index.dart' as component;

linkShare(BuildContext context, datastore_model.Item? item) async {
  final publicTitleController = TextEditingController(text: item?.name);
  final allowedUsageController = TextEditingController(text: "1");
  final phasephrase = TextEditingController();
  DateTime validTill = DateTime.now().add(const Duration(days: 1));
  final validTillController = TextEditingController(
      text: DateFormat('yyyy/MM/dd HH:mm').format(validTill));
  bool isPassphraseObscure = true;
  return await component.showCustomBottomSheet(
    context: context,
    isScrollControlled: false,
    scrollControlDisabledMaxHeightRatio: 0.80,
    child: Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: StatefulBuilder(builder: (context, setState) {
        return Material(
          color: Colors.transparent,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              decoration: BoxDecoration(
                color: const Color(0xff171717),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.r),
                  topRight: Radius.circular(30.r),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      component.TextDmSans(
                        text:
                            FlutterI18n.translate(context, "CREATE_SHARE_LINK"),
                        textAlign: TextAlign.center,
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                      const Spacer(),
                      IconButton(
                        icon: const Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 32.h),
                  component.CustomTextField(
                    controller: publicTitleController,
                    labelText: FlutterI18n.translate(
                      context,
                      "PUBLIC_TITLE",
                    ),
                    onChanged: (value) {},
                  ),
                  SizedBox(height: 6.h),
                  component.TextDmSans(
                    text: FlutterI18n.translate(
                      context,
                      "INFO_PUBLIC_TITLE_WILL_BE_VISIBLE",
                    ),
                    color: Colors.white.withOpacity(0.60),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w300,
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    controller: allowedUsageController,
                    labelText: FlutterI18n.translate(
                      context,
                      "ALLOWED_USAGE",
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {},
                  ),
                  SizedBox(height: 6.h),
                  component.TextDmSans(
                    text: FlutterI18n.translate(
                      context,
                      "INFO_HOW_OFTEN_CAN_LINK_SHARE_BE_USED",
                    ),
                    color: Colors.white.withOpacity(0.60),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w300,
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    isEnabled: false,
                    onTap: () async {
                      //Hide keyboard
                      FocusScope.of(context).unfocus();
                      await Future.delayed(const Duration(milliseconds: 100));
                      DateTime? dateTime = await showOmniDateTimePicker(
                          context: context,
                          is24HourMode: true,
                          initialDate: validTill);
                      setState(() {
                        validTill = dateTime ?? validTill;
                        validTillController.text =
                            DateFormat('yyyy/MM/dd HH:mm').format(validTill);
                      });
                    },
                    controller: validTillController,
                    labelText: FlutterI18n.translate(
                      context,
                      "VALID_TILL",
                    ),
                    onChanged: (value) {},
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    controller: phasephrase,
                    labelText: FlutterI18n.translate(
                      context,
                      "PASSPHRASE",
                    ),
                    obscureText: isPassphraseObscure,
                    onChanged: (value) {},
                    suffixIcon: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        component.CopyIcon(
                          onTap: () {
                            Clipboard.setData(
                              ClipboardData(
                                text: phasephrase.text,
                              ),
                            );
                            // _clipboardCopyToast();
                          },
                        ),
                        IconButton(
                          icon: Icon(
                            isPassphraseObscure
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: Colors.white.withOpacity(0.60),
                          ),
                          onPressed: () {
                            setState(() {
                              isPassphraseObscure = !isPassphraseObscure;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 6.h),
                  component.TextDmSans(
                    text: FlutterI18n.translate(
                      context,
                      "SHARE_LINK_PASSPHRASE_INFO",
                    ),
                    color: Colors.white.withOpacity(0.60),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w300,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: component.BtnText(
                      text: FlutterI18n.translate(
                        context,
                        "GENERATE_PASSPHRASE",
                      ).toUpperCase(),
                      onPressed: () async {
                        component.Loader.show(context);
                        String password =
                            await manager_datastore_password.generate();
                        component.Loader.hide();
                        component.generatePasswordSheet(
                          context: context,
                          password: password,
                          onSetPassword: (password) {
                            setState(() {
                              phasephrase.text = password;
                            });
                          },
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 16.h),
                  component.BtnPrimary(
                    text: FlutterI18n.translate(context, "CREATE"),
                    onPressed: () async {
                      String? url;
                      try {
                        component.Loader.show(context);
                        url = await managerShareLink.createShareLink(
                            item!,
                            publicTitleController.text,
                            allowedUsageController.text,
                            validTill,
                            phasephrase.text);
                      } catch (e) {
                      } finally {
                        component.Loader.hide();
                      }
                      Navigator.pop(context, url);
                    },
                  )
                ],
              ),
            ),
          ),
        );
      }),
    ),
  );
}
