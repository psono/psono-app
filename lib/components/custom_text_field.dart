import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/utils/app_assets.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField({
    Key? key,
    required this.controller,
    this.suffixIcon,
    this.labelText,
    this.keyboardType,
    Color? backgroundColor,
    this.obscureText = false,
    this.focusNode,
    this.autofocus = false,
    this.validator,
    this.inputFormatters,
    this.onChanged,
    this.maxLines = 1,
    int? minLines = -1,
    this.isEnabled = true,
    this.showHeader = true,
    this.onTap,
    this.textCapitalization = TextCapitalization.none,
  })  : _backgroundColor = backgroundColor ?? Colors.white.withOpacity(0.10),
        minLines = minLines == -1 ? 1 : minLines,
        super(key: key);

  final TextEditingController controller;
  final Widget? suffixIcon;
  final String? labelText;
  Color? _backgroundColor;
  final bool obscureText;
  final TextInputType? keyboardType;
  final FocusNode? focusNode;
  final bool autofocus;
  final int? maxLines;
  final int? minLines;
  final bool isEnabled;
  final bool showHeader;
  TextCapitalization textCapitalization;
  List<TextInputFormatter>? inputFormatters;
  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    _backgroundColor ??= Colors.black.withOpacity(0.10);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (showHeader) ...[
          TextDmSans(
            text: labelText?.toUpperCase() ?? "",
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            color: Colors.white.withOpacity(0.50),
          ),
          SizedBox(height: 8.h),
        ],
        TextFormField(
          readOnly: !isEnabled,
          canRequestFocus: isEnabled,
          minLines: minLines,
          maxLines: maxLines,
          controller: controller,
          keyboardType: keyboardType,
          focusNode: focusNode,
          obscureText: obscureText,
          autofocus: autofocus,
          onChanged: onChanged,
          validator: validator,
          inputFormatters: inputFormatters,
          textCapitalization: textCapitalization,
          onTap: () => onTap?.call(),
          style: GoogleFonts.dmSans(color: Colors.white, fontSize: 16.sp),
          decoration: InputDecoration(
            hintText: labelText,
            hintStyle: GoogleFonts.dmSans(
              color: Colors.white.withOpacity(0.50),
              fontSize: 16.sp,
              fontWeight: FontWeight.w400,
            ),
            filled: true,
            fillColor: _backgroundColor,
            contentPadding:
                EdgeInsets.symmetric(vertical: 13.h, horizontal: 16.w),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.r),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.r),
              borderSide: BorderSide(
                color: Colors.white.withOpacity(0.22),
                style: BorderStyle.solid,
                strokeAlign: BorderSide.strokeAlignCenter,
              ),
            ),
            suffixIcon: suffixIcon != null
                ? Container(
                    constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.30,
                    ),
                    padding: EdgeInsets.fromLTRB(
                      0.w,
                      1.0.h,
                      4.0.w,
                      1.0.h,
                    ),
                    child: suffixIcon,
                  )
                : null,
          ),
        ),
      ],
    );
  }
}

class ObscureIcon extends StatefulWidget {
  const ObscureIcon({Key? key, required this.onTap, required this.obscure})
      : super(key: key);
  final Function() onTap;

  final bool obscure;
  @override
  State<ObscureIcon> createState() => _ObscureIconState();
}

class _ObscureIconState extends State<ObscureIcon> {
  @override
  Widget build(BuildContext context) {
    return Ink(
      child: InkWell(
        onTap: widget.onTap,
        child: Padding(
          padding: EdgeInsets.all(12.w),
          child: Image.asset(
            widget.obscure ? AppAssets.iconEyeOpen : AppAssets.iconEyeClose,
            height: 20.h,
            width: 20.h,
          ),
        ),
      ),
    );
  }
}

class CopyIcon extends StatelessWidget {
  const CopyIcon({Key? key, required this.onTap}) : super(key: key);

  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Ink(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.all(12.w),
          child: Icon(
            Icons.copy_outlined,
            color: Colors.white.withOpacity(0.50),
            size: 20.sp,
          ),
        ),
      ),
    );
  }
}
