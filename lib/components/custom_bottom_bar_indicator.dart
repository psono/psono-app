import 'dart:ui';

import 'package:flutter/material.dart';

class BackgroundContainerTabIndicator extends Decoration {
  const BackgroundContainerTabIndicator({
    this.borderRadius,
    this.color = Colors.white,
    this.verticalMargin = 4.0, // Add verticalMargin
  });

  final BorderRadius? borderRadius;
  final Color color;
  final double verticalMargin; // Add verticalMargin

  @override
  EdgeInsetsGeometry get padding =>
      EdgeInsets.symmetric(vertical: verticalMargin); // Add padding

  @override
  Decoration? lerpFrom(Decoration? a, double t) {
    if (a is BackgroundContainerTabIndicator) {
      return BackgroundContainerTabIndicator(
        color: Color.lerp(a.color, color, t)!,
        verticalMargin: lerpDouble(a.verticalMargin, verticalMargin, t)!,
      );
    }
    return super.lerpFrom(a, t);
  }

  @override
  Decoration? lerpTo(Decoration? b, double t) {
    if (b is BackgroundContainerTabIndicator) {
      return BackgroundContainerTabIndicator(
        color: Color.lerp(color, b.color, t)!,
        verticalMargin: lerpDouble(verticalMargin, b.verticalMargin, t)!,
      );
    }
    return super.lerpTo(b, t);
  }

  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _BackgroundContainerPainter(this, borderRadius, onChanged);
  }
}

class _BackgroundContainerPainter extends BoxPainter {
  _BackgroundContainerPainter(
    this.decoration,
    this.borderRadius,
    VoidCallback? onChanged,
  ) : super(onChanged);

  final BackgroundContainerTabIndicator decoration;
  final BorderRadius? borderRadius;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration.size != null);
    final Rect rect = offset & configuration.size!;
    final TextDirection textDirection = configuration.textDirection!;
    final Paint paint = Paint()..color = decoration.color;
    final Rect indicator = decoration.padding
        .resolve(textDirection)
        .deflateRect(rect); // Use padding instead of insets
    if (borderRadius != null) {
      canvas.drawRRect(
          borderRadius!.resolve(textDirection).toRRect(indicator), paint);
    } else {
      canvas.drawRect(indicator, paint);
    }
  }
}
