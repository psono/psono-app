import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/utils/app_assets.dart';

class ScaffoldDark extends StatelessWidget {
  final PreferredSizeWidget? appBar;
  final Widget? body;
  final bool? resizeToAvoidBottomInset;
  final String backgroundPath;
  List<Widget>? persistentFooterButtons;
  ScaffoldDark({
    Key? key,
    this.appBar,
    this.body,
    this.resizeToAvoidBottomInset,
    this.backgroundPath = AppAssets.darkScaffoldBg,
    this.persistentFooterButtons,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            backgroundPath,
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: Scaffold(
        appBar: appBar,
        persistentFooterButtons: persistentFooterButtons,
        resizeToAvoidBottomInset: resizeToAvoidBottomInset,
        backgroundColor: Colors.transparent,
        body: SafeArea(
            top: appBar == null,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: body ?? Container(),
            )),
      ),
    );
  }
}
