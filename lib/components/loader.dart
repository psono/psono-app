import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart'
    as overlayLoader;
import 'package:psono/theme.dart';

class Loader {
  static show(context, [overlayColor]) {
    overlayLoader.Loader.show(
      context,
      overlayColor: overlayColor ?? Colors.white24,
      progressIndicator: CircularProgressIndicator(
        color: const Color(0xFF151f2b),
        backgroundColor: primarySwatch.shade500,
      ),
    );
  }

  static get isShown => overlayLoader.Loader.isShown;

  static hide() {
    if (overlayLoader.Loader.isShown) {
      overlayLoader.Loader.hide();
    }
  }
}
