import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/manager_widget.dart' as managerWidget;
import 'package:psono/theme.dart';

class Item extends StatelessWidget {
  final datastoreModel.Item? item;
  final VoidCallback? onPressed;
  final datastoreModel.ItemCallback? onLongPress;
  bool? isShare;
  final bool showSelectBox;
  final bool isSelected;
  final bool isGridView;

  Item({
    this.item,
    this.onPressed,
    this.onLongPress,
    this.isShare,
    this.showSelectBox = false,
    this.isSelected = false,
    this.isGridView = true,
  });

  @override
  Widget build(BuildContext context) {
    if (isGridView) {
      return _GridViewItem(
        item: item,
        isShare: isShare,
        showSelectBox: showSelectBox,
        isSelected: isSelected,
        onLongPress: onLongPress,
        onPressed: onPressed,
      );
    } else {
      return _ListViewItem(
          item: item,
          isShare: isShare,
          showSelectBox: showSelectBox,
          isSelected: isSelected,
          onLongPress: onLongPress,
          onPressed: onPressed);
    }
  }
}

class _ListViewItem extends StatelessWidget {
  const _ListViewItem({
    Key? key,
    required this.item,
    required this.isShare,
    required this.showSelectBox,
    required this.isSelected,
    required this.onLongPress,
    required this.onPressed,
  }) : super(key: key);

  final datastoreModel.Item? item;
  final bool? isShare;
  final bool showSelectBox;
  final bool isSelected;
  final datastoreModel.ItemCallback? onLongPress;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    Widget icon = Container(
      height: 48.w,
      width: 48.w,
      padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 8.w),
      decoration: BoxDecoration(
        color: const Color(0xff1A1A1A),
        borderRadius: BorderRadius.circular(5.r),
      ),
      child: Icon(
        managerWidget.itemIcon(item!),
        color: primarySwatch.shade500,
        size: 20.sp,
      ),
    );
    Widget shareIcon = Container();
    if (isShare!) {
      shareIcon = Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.bottomRight,
        children: [
          Stack(
            alignment: const Alignment(0, 0),
            children: [
              Icon(
                FontAwesome.circle,
                color: Colors.white,
                size: 20.sp,
              ),
              Icon(
                FontAwesome.users,
                color: primarySwatch.shade500,
                size: 12.sp,
              ),
            ],
          )
        ],
      );
    }

    Widget selectedIcon = Container();
    if (showSelectBox && item!.shareRights!.delete!) {
      List<Widget> children = [
        Icon(
          FontAwesome.square,
          color: Colors.white,
          size: 20.sp,
        ),
      ];

      if (isSelected) {
        children.add(
          Icon(
            FontAwesome.check,
            size: 20.sp,
            color: primarySwatch.shade500,
          ),
        );
      }

      selectedIcon = Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        child: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.bottomLeft,
          children: children,
        ),
      );
    }
    return Column(
      children: [
        GestureDetector(
          onLongPress: () {
            if (onLongPress != null) {
              onLongPress!(item);
            }
          },
          child: TextButton(
            onPressed: onPressed,
            child: Row(
              children: [
                selectedIcon,
                icon,
                SizedBox(width: 10.w),
                Expanded(
                  flex: 100,
                  child: TextDmSans(
                    text: item!.name!,
                    color: Colors.white,
                    textAlign: TextAlign.start,
                    maxLines: 2,
                    fontSize: 18,
                    overflow: TextOverflow.ellipsis,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const Spacer(flex: 10),
                shareIcon,
              ],
            ),
          ),
        ),
        Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(vertical: 12.h),
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(
                width: 1,
                strokeAlign: BorderSide.strokeAlignCenter,
                color: Colors.white.withOpacity(0.20000000298023224),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _GridViewItem extends StatelessWidget {
  const _GridViewItem({
    Key? key,
    required this.item,
    required this.isShare,
    required this.showSelectBox,
    required this.isSelected,
    required this.onLongPress,
    required this.onPressed,
  }) : super(key: key);

  final datastoreModel.Item? item;
  final bool? isShare;
  final bool showSelectBox;
  final bool isSelected;
  final datastoreModel.ItemCallback? onLongPress;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    Widget icon = Icon(
      managerWidget.itemIcon(item!),
      color: primarySwatch.shade500,
      size: 20.sp,
    );
    Widget shareIcon = Container();
    if (isShare!) {
      shareIcon = Positioned(
        bottom: -10.h,
        right: -10.h,
        child: Stack(
          alignment: const Alignment(0, 0),
          children: [
            Icon(
              FontAwesome.circle,
              color: Colors.white,
              size: 20.sp,
            ),
            Icon(
              FontAwesome.users,
              color: primarySwatch.shade500,
              size: 12.sp,
            ),
          ],
        ),
      );
    }
    List<Widget> selectedIcons = [];
    if (showSelectBox && item!.shareRights!.delete!) {
      selectedIcons = [
        Positioned(
          bottom: -10.h,
          left: -10.h,
          child: Stack(
            children: [
              Icon(
                FontAwesome.square,
                color: Colors.white,
                size: 20.sp,
              ),
            ],
          ),
        ),
      ];

      if (isSelected) {
        selectedIcons.add(
          Positioned(
            bottom: -10.h,
            left: -10.h,
            child: Icon(
              FontAwesome.check,
              color: primarySwatch.shade500,
              size: 20.sp,
            ),
          ),
        );
      }
    }

    return GestureDetector(
      onLongPress: () {
        if (onLongPress != null) {
          onLongPress!(item);
        }
      },
      child: TextButton(
        style: TextButton.styleFrom(
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),
        onPressed: () {
          onPressed!();
        },
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 8.h),
                  decoration: BoxDecoration(
                    color: const Color(0xff1A1A1A),
                    borderRadius: BorderRadius.circular(5.r),
                  ),
                  child: icon,
                ),
                SizedBox(width: 8.w),
                SizedBox(
                  width: 90.w,
                  child: TextDmSans(
                    text: item!.name != null ? item!.name! : '',
                    color: Colors.white,
                    fontSize: 18,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
            ...selectedIcons,
            shareIcon,
          ],
        ),
      ),
    );
  }
}
