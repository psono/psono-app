import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/data_score_components/datascore_scaffold/datascore_scaffold.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/screens/download_file/index.dart';
import 'package:psono/screens/edit_secret/edit_secret.dart';
import 'package:psono/screens/folder/folder_screen.dart';
import 'package:psono/services/autofill.dart' as autofill_service;
import 'package:psono/services/helper.dart' as helper;

import 'folder.dart' as componentFolder;
import 'item.dart' as componentItem;

class FolderSearchTree extends StatelessWidget {
  final datastoreModel.Folder? root;
  final List<datastoreModel.Folder?> filteredFolders = [];
  final List<List<String?>?> filteredFolderPaths = [];
  final List<datastoreModel.Item?> filteredItems = [];
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;
  final String? search;
  final Map<String, String> filters;
  final bool? autofill;

  FolderSearchTree({
    this.root,
    this.datastore,
    this.share,
    this.path,
    this.relativePath,
    this.search,
    this.autofill,
    this.filters = const {},
  });

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    filterFolderWithSearch(root, path);
    _filterItems();

    if (_calculateEntryCount() == 0) {
      return component.TextDmSans(
        text: FlutterI18n.translate(
          context,
          "NO_MATCHING_ENTRIES_FOUND",
        ),
        fontSize: 16.sp,
        fontWeight: FontWeight.w500,
        color: Colors.white.withOpacity(0.6),
      );
    }

    return CustomScrollView(
      slivers: [
        if (DatastoreScaffold.isGridView.value) ...[
          SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 180,
              mainAxisExtent: 200,
              mainAxisSpacing: 0.0,
              crossAxisSpacing: 0.0,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                datastoreModel.Folder folder = filteredFolders[index]!;
                return _Folder(
                    root: root,
                    folder: folder,
                    datastore: datastore,
                    share: share,
                    filteredFolderPaths: filteredFolderPaths,
                    relativePath: relativePath,
                    index: index,
                    autofill: autofill);
              },
              childCount: _getFolderCount(),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent:
                  orientation == Orientation.portrait ? 190.w : 190.w,
              mainAxisExtent:
                  orientation == Orientation.portrait ? 50.h : 120.h,
              mainAxisSpacing: 5.h,
              crossAxisSpacing: 10.w,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                datastoreModel.Item item = filteredItems[index]!;
                return _Item(
                    item: item,
                    autofill: autofill,
                    root: root,
                    datastore: datastore,
                    share: share,
                    path: path,
                    relativePath: relativePath);
              },
              childCount: _getItemCount(),
            ),
          )
        ] else ...[
          SliverList.builder(
            itemBuilder: (BuildContext context, int index) {
              datastoreModel.Folder folder = filteredFolders[index]!;
              return _Folder(
                root: root,
                folder: folder,
                datastore: datastore,
                share: share,
                filteredFolderPaths: filteredFolderPaths,
                relativePath: relativePath,
                autofill: autofill,
                index: index,
              );
            },
            itemCount: _getFolderCount(),
          ),
          SliverList.builder(
            itemBuilder: (context, index) {
              datastoreModel.Item item = filteredItems[index]!;
              return _Item(
                item: item,
                autofill: autofill,
                root: root,
                datastore: datastore,
                share: share,
                path: path,
                relativePath: relativePath,
              );
            },
            itemCount: _getItemCount(),
          ),
        ]
      ],
    );
  }

  void filterFolderWithSearch(
      datastoreModel.Folder? root, List<String?>? path) {
    Function filter = helper.getPasswordFilter(search);
    if (root == null) {
      return;
    }

    if (root.folders != null) {
      for (var i = 0; i < root.folders!.length; i++) {
        if (filter(root.folders![i]) && root.folders![i]!.deleted != true) {
          var alreadyExists = filteredFolders
              .any((folder) => folder!.id == root.folders![i]!.id);
          if (!alreadyExists) {
            filteredFolders.add(root.folders![i]);
            filteredFolderPaths.add(path);
          }
        }

        if (root.folders![i]!.deleted != true) {
          filterFolderWithSearch(
            root.folders![i],
            List<String?>.from(path!)..addAll([root.folders![i]!.id]),
          );
        }
      }
    }
    if (root.items != null) {
      for (var i = 0; i < root.items!.length; i++) {
        if (filter(root.items![i]) && root.items![i]!.deleted != true) {
          var alreadyExists =
              filteredItems.any((item) => item!.id == root.items![i]!.id);
          if (!alreadyExists) {
            filteredItems.add(root.items![i]);
          }
        }
      }
    }
    filteredFolders.sort((a, b) => a!.name!.compareTo(b!.name!));
    filteredItems.sort((a, b) => a!.name!.compareTo(b!.name!));
  }

  _filterItems() {
    List<String> selectedFilters = filters.values.toList();
    if (selectedFilters.isEmpty) {
      return;
    }
    filteredItems
        .removeWhere((element) => !selectedFilters.contains(element!.type));
    filteredFolders.clear();
  }

  int _getFolderCount() {
    return filteredFolders.length;
  }

  int _getItemCount() {
    return filteredItems.length;
  }

  int _calculateEntryCount() {
    return _getFolderCount() + _getItemCount();
  }
}

class _Item extends StatelessWidget {
  const _Item({
    Key? key,
    required this.item,
    required this.autofill,
    required this.root,
    required this.datastore,
    required this.share,
    required this.path,
    required this.relativePath,
  }) : super(key: key);

  final datastoreModel.Item item;
  final bool? autofill;
  final datastoreModel.Folder? root;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;

  @override
  Widget build(BuildContext context) {
    return componentItem.Item(
      item: item,
      isGridView: DatastoreScaffold.isGridView.value,
      onPressed: () async {
        if (autofill!) {
          await autofill_service.autofill(item.secretId, item.secretKey);
        } else {
          if (item.fileId != null) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DownloadFileScreen(
                  parent: root,
                  datastore: datastore,
                  share: share,
                  item: item,
                  path: path,
                  relativePath: relativePath,
                ),
              ),
            );
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EditSecretScreen(
                  parent: root,
                  datastore: datastore,
                  share: share,
                  item: item,
                  path: path,
                  relativePath: relativePath,
                ),
              ),
            );
          }
        }
      },
      isShare: item.shareId != null,
    );
  }
}

class _Folder extends StatelessWidget {
  const _Folder({
    Key? key,
    required this.root,
    required this.folder,
    required this.datastore,
    required this.share,
    required this.filteredFolderPaths,
    required this.relativePath,
    required this.autofill,
    required this.index,
  }) : super(key: key);
  final datastoreModel.Folder? root;
  final datastoreModel.Folder folder;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<List<String?>?> filteredFolderPaths;
  final List<String?>? relativePath;
  final bool? autofill;
  final int index;

  @override
  Widget build(BuildContext context) {
    return componentFolder.Folder(
      folder: folder,
      isGridView: DatastoreScaffold.isGridView.value,
      onPressed: () async {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FolderScreen(
              parent: root,
              autoNavigate: [],
              folder: folder,
              datastore: datastore,
              share: share,
              path: List.from(filteredFolderPaths[index]!)..addAll([folder.id]),
              relativePath: List.from(relativePath!)..addAll([folder.id]),
              autofill: autofill,
            ),
          ),
        );
      },
      isShare: folder.shareId != null,
    );
  }
}
