part of 'datascore_scaffold.dart';

class _SearchTextField extends StatelessWidget {
  const _SearchTextField({
    Key? key,
    required this.onSearchChange,
    this.backgroundColor = const Color(0xff1A1A1A),
  }) : super(key: key);

  final Function(String? p1)? onSearchChange;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 46,
      child: TextField(
        style: GoogleFonts.dmSans(
          color: Colors.white,
          fontWeight: FontWeight.w700,
          fontSize: 16,
        ),
        onChanged: (value) {
          onSearchChange?.call(value);
        },
        decoration: InputDecoration(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 17.w, vertical: 10.h),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100.r),
            borderSide: BorderSide.none,
          ),
          prefixIcon: Icon(
            Icons.search,
            color: Colors.white.withOpacity(0.40),
          ),
          hintText: FlutterI18n.translate(context, "SEARCH").toUpperCase(),
          hintStyle: GoogleFonts.dmSans(
            color: Colors.white.withOpacity(0.40),
            fontWeight: FontWeight.w700,
          ),
          fillColor: backgroundColor,
          filled: true,
        ),
      ),
    );
  }
}
