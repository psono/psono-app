part of 'datascore_scaffold.dart';

class _FilterButton extends StatefulWidget {
  const _FilterButton(
      {Key? key, required this.selectedFilters, required this.onApplyFilter})
      : super(key: key);
  final Map<String, String> selectedFilters;
  final Function(Map<String, String>) onApplyFilter;
  @override
  State<_FilterButton> createState() => __FilterButtonState();
}

class __FilterButtonState extends State<_FilterButton> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          filterSheet();
        },
        child: Stack(
          children: [
            Image.asset(
              AppAssets.filterIcon,
              width: 46,
              height: 46,
            ),
            if (widget.selectedFilters.isNotEmpty)
              Positioned(
                top: 5.h,
                right: 5.r,
                child: Container(
                  width: 8,
                  height: 8,
                  decoration: const ShapeDecoration(
                    color: Color(0xFF57C7A3),
                    shape: OvalBorder(),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  filterSheet() {
    Map<String, String> selectedFilters = widget.selectedFilters;
    component.showCustomBottomSheet(
      context: context,
      child: StatefulBuilder(
        builder: (context, setState) {
          return Container(
            padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
            decoration: BoxDecoration(
              color: const Color(0xff171717),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.r),
                topRight: Radius.circular(30.r),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 50.w,
                  height: 3.h,
                  decoration: ShapeDecoration(
                    color: Colors.white.withOpacity(0.30),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100),
                    ),
                  ),
                ),
                SizedBox(height: 20.h),
                component.TextDmSans(
                  text: FlutterI18n.translate(context, "APPLY_FILTER"),
                  textAlign: TextAlign.center,
                  color: Colors.white,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w700,
                ),
                SizedBox(height: 24.h),
                Column(
                  children: DatastoreScaffold.filterItems.keys.map(
                    (e) {
                      return _filterItem(
                        FlutterI18n.translate(context, e),
                        selectedFilters.keys.contains(e),
                        () {
                          setState(() {
                            if (selectedFilters.keys.contains(e)) {
                              selectedFilters.remove(e);
                            } else {
                              selectedFilters[e] =
                                  DatastoreScaffold.filterItems[e]!;
                            }
                          });
                        },
                      );
                    },
                  ).toList(),
                ),
                SizedBox(height: 24.h),
                component.BtnPrimary(
                  text: "APPLY",
                  onPressed: () {
                    widget.onApplyFilter(selectedFilters);
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _filterItem(String title, bool isSelected, Function() onTap) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(
                width: 1,
                strokeAlign: BorderSide.strokeAlignCenter,
                color: Colors.white.withOpacity(0.20000000298023224),
              ),
            ),
          ),
        ),
        TextButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(
              EdgeInsets.zero,
            ),
          ),
          onPressed: onTap,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: component.TextDmSans(
              text: title,
              color: Colors.white,
              fontSize: 16.sp,
              fontWeight: FontWeight.w500,
            ),
            trailing: Container(
              height: 24.h,
              width: 24.h,
              margin: EdgeInsets.all(4.h),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.10),
                borderRadius: BorderRadius.circular(8.r),
                border: Border.all(
                  color: isSelected
                      ? C.mountainMeadow
                      : Colors.white.withOpacity(0.20),
                  width: 1,
                ),
              ),
              child: !isSelected
                  ? Container()
                  : const Icon(
                      Icons.check,
                      color: C.mountainMeadow,
                      size: 20,
                    ),
            ),
          ),
        ),
      ],
    );
  }
}
