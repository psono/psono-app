import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastore_model;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';

part 'action_buttons.dart';
part 'filter_button.dart';
part 'new_entity_buttons.dart';
part 'search_text_field.dart';
part 'view_switch_button.dart';

class DatastoreScaffold extends StatefulWidget {
  final Widget child;
  final bool isSelectionMode;
  final bool isMultiSelected;
  final bool isFolderSelected;
  final bool isSearchMode;
  final bool isSubScreen;
  final Map<String, String> selectedFilters;
  final datastore_model.Folder? folder;
  final Function()? onNewFolderTap;
  final Function()? onNewEntryTap;
  final Function()? onDeleteTap;
  final Function()? onEditTap;
  final Function(String?)? onSearchChange;
  final Function()? thisFolderEditTap;
  final Function()? onLinkShareTap;
  final Function(Map<String, String>)? onApplyFilter;

  static ValueNotifier<bool> isGridView = ValueNotifier<bool>(true);

  static const filterItems = {
    "APPLICATION_PASSWORD": "application_password",
    "BOOKMARK": "bookmark",
    "CREDIT_CARD": "credit_card",
    "NOTE": "note",
    "TOTP_AUTHENTICATOR": "totp",
    "WEBSITE_PASSWORD": "website_password",
  };
  const DatastoreScaffold({
    Key? key,
    required this.child,
    this.isSelectionMode = false,
    this.isMultiSelected = false,
    this.isSearchMode = false,
    this.isSubScreen = false,
    this.isFolderSelected = false,
    this.selectedFilters = const {},
    this.folder,
    this.onNewFolderTap,
    this.onNewEntryTap,
    this.onSearchChange,
    this.onDeleteTap,
    this.onEditTap,
    this.thisFolderEditTap,
    this.onApplyFilter,
    this.onLinkShareTap,
  }) : super(key: key);

  @override
  State<DatastoreScaffold> createState() => _DatastoreScaffoldState();
}

class _DatastoreScaffoldState extends State<DatastoreScaffold> {
  @override
  Widget build(BuildContext context) {
    if (widget.isSubScreen) {
      return subScreen();
    }
    return parentScreen();
  }

  Widget subScreen() {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: InkWell(
          child: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: component.TextDmSans(
          text: widget.folder?.name ?? "",
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),
        actions: [
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Image.asset(
                AppAssets.iconEdit,
                width: 24.w,
                height: 24.h,
              ),
            ),
            onTap: () {
              widget.thisFolderEditTap?.call();
            },
          ),
        ],
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Column(
            children: [
              SizedBox(height: 20.h),
              Row(
                children: [
                  Expanded(
                    flex: 1000,
                    child: (widget.isSelectionMode)
                        ? Container()
                        : _SearchTextField(
                            onSearchChange: widget.onSearchChange,
                            backgroundColor: Colors.white.withOpacity(0.10),
                          ),
                  ),
                  const Spacer(),
                  _FilterButton(
                    selectedFilters: widget.selectedFilters,
                    onApplyFilter: widget.onApplyFilter ?? (_) {},
                  ),
                  const Spacer(),
                  _ViewSwitchButton(widget: widget),
                ],
              ),
              SizedBox(height: 24.h),
              Expanded(
                flex: 1000,
                child: widget.child,
              )
            ],
          ),
          if (!widget.isSelectionMode && !widget.isSearchMode)
            _NewEntityButtons(
              onNewFolderTap: widget.onNewFolderTap,
              onNewEntryTap: widget.onNewEntryTap,
            ),
          if (widget.isSelectionMode && !widget.isSearchMode)
            _ActionButtons(
              isFolderSelected: widget.isFolderSelected,
              onEditTap: widget.onEditTap,
              onDeleteTap: widget.onDeleteTap,
              isMultiSelected: widget.isMultiSelected,
              onShareTap: widget.onLinkShareTap,
            ),
        ],
      ),
    );
  }

  Widget parentScreen() {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.42,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  AppAssets.homeScreenBg,
                ),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SafeArea(
                bottom: false,
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 19.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flex(
                        direction: orientation == Orientation.landscape
                            ? Axis.horizontal
                            : Axis.vertical,
                        children: <Widget>[
                          Row(children: [
                            Image.asset(
                              AppAssets.logoRound,
                              width: 40.w,
                              height: 40.w,
                            ),
                          ]),
                          SizedBox(height: 13.h),
                          Padding(
                            padding: EdgeInsets.only(
                                left: orientation == Orientation.landscape
                                    ? 10.w
                                    : 0),
                            child: component.TextDmSans(
                              text: FlutterI18n.translate(
                                context,
                                "MANAGE_YOUR_PRIVACY_WITH_PSONO",
                              ),
                              fontSize: orientation == Orientation.landscape
                                  ? 24.sp
                                  : 28.sp,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20.h),
                      if (!widget.isSelectionMode && !widget.isSearchMode)
                        _NewEntityButtons(
                          onNewFolderTap: widget.onNewFolderTap,
                          onNewEntryTap: widget.onNewEntryTap,
                        ),
                      if (widget.isSelectionMode && !widget.isSearchMode)
                        _ActionButtons(
                          isFolderSelected: widget.isFolderSelected,
                          onEditTap: widget.onEditTap,
                          onDeleteTap: widget.onDeleteTap,
                          isMultiSelected: widget.isMultiSelected,
                          onShareTap: widget.onLinkShareTap,
                        ),
                    ],
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.65,
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 24.h),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.r),
                    topRight: Radius.circular(30.r),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 20.h),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 100,
                            child: widget.isSelectionMode
                                ? Container()
                                : _SearchTextField(
                                    onSearchChange: widget.onSearchChange),
                          ),
                          const Spacer(),
                          _FilterButton(
                            selectedFilters: widget.selectedFilters,
                            onApplyFilter: widget.onApplyFilter ?? (_) {},
                          ),
                          const Spacer(),
                          _ViewSwitchButton(widget: widget),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1000,
                      child: widget.child,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
