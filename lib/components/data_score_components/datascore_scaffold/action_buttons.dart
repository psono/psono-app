part of 'datascore_scaffold.dart';

class _ActionButtons extends StatelessWidget {
  const _ActionButtons(
      {Key? key,
      this.onEditTap,
      this.onDeleteTap,
      required this.isMultiSelected,
      required this.isFolderSelected,
      this.onShareTap})
      : super(key: key);
  final Function()? onEditTap;
  final Function()? onShareTap;
  final Function()? onDeleteTap;
  final bool isMultiSelected;
  final bool isFolderSelected;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (!isMultiSelected && !isFolderSelected) ...[
          Expanded(
            flex: 30,
            child: _Button(
              icon: Icons.share,
              onTap: () => onShareTap?.call(),
              text: "SHARE",
            ),
          ),
          const Spacer(),
        ],
        if (!isMultiSelected)
          Expanded(
            flex: 30,
            child: _Button(
              icon: Icons.edit,
              onTap: () => onEditTap?.call(),
              text: "EDIT",
            ),
          ),
        const Spacer(),
        Expanded(
          flex: 30,
          child: _Button(
            icon: Icons.delete,
            onTap: () => onDeleteTap?.call(),
            text: "DELETE",
          ),
        ),
      ],
    );
  }
}

class _Button extends StatelessWidget {
  const _Button({
    Key? key,
    required this.text,
    required this.onTap,
    this.icon = Icons.add,
  }) : super(key: key);
  final String text;
  final Function() onTap;
  final IconData icon;
  @override
  Widget build(BuildContext context) {
    return component.Btn(
      padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 20.w),
      customChild: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            icon,
            size: 20.sp,
            color: Colors.black,
          ),
          SizedBox(width: 5.w),
          component.TextDmSans(
            text: FlutterI18n.translate(
              context,
              text,
            ).toUpperCase(),
            fontSize: 12.sp,
            fontWeight: FontWeight.w700,
            color: Colors.black,
            height: 1.5,
          )
        ],
      ),
      onPressed: onTap,
    );
  }
}
