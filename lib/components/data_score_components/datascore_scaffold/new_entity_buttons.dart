part of 'datascore_scaffold.dart';

class _NewEntityButtons extends StatelessWidget {
  const _NewEntityButtons({
    Key? key,
    required this.onNewFolderTap,
    required this.onNewEntryTap,
  }) : super(key: key);

  final Function()? onNewFolderTap;
  final Function()? onNewEntryTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 30,
          child: _Button(
            onTap: () => onNewFolderTap?.call(),
            text: "NEW_FOLDER",
          ),
        ),
        const Spacer(),
        Expanded(
          flex: 30,
          child: _Button(
            onTap: () => onNewEntryTap?.call(),
            text: "NEW_ENTRY",
          ),
        ),
      ],
    );
  }
}
