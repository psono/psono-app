part of 'datascore_scaffold.dart';

class _ViewSwitchButton extends StatelessWidget {
  const _ViewSwitchButton({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final DatastoreScaffold widget;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          DatastoreScaffold.isGridView.value =
              !DatastoreScaffold.isGridView.value;
        },
        child: Image.asset(
          DatastoreScaffold.isGridView.value
              ? AppAssets.listIcon
              : AppAssets.gridIcon,
          width: 46,
          height: 46,
        ),
      ),
    );
  }
}
