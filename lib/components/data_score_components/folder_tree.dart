import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/data_score_components/datascore_scaffold/datascore_scaffold.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/screens/download_file/index.dart';
import 'package:psono/screens/edit_secret/edit_secret.dart';
import 'package:psono/screens/folder/folder_screen.dart';
import 'package:psono/services/autofill.dart' as autofill_service;
import 'package:psono/utils/app_assets.dart';

import 'folder.dart' as componentFolder;
import 'item.dart' as componentItem;

class FolderTree extends StatelessWidget {
  final datastoreModel.Folder? root;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;
  final datastoreModel.FolderCallback? onLongPressFolder;
  final datastoreModel.ItemCallback? onLongPressItem;
  final List<datastoreModel.Folder?> filteredFolders = [];
  final List<datastoreModel.Item?> filteredItems = [];
  final bool? autofill;

  FolderTree({
    this.root,
    this.datastore,
    this.share,
    this.path,
    this.relativePath,
    this.onLongPressFolder,
    this.onLongPressItem,
    this.autofill,
  });

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    _filterContent(root, path);

    if (_calculateEntryCount() == 0) {
      return const _EmptyView();
    }

    return CustomScrollView(
      slivers: [
        if (DatastoreScaffold.isGridView.value) ...[
          SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 180,
              mainAxisExtent: 200,
              mainAxisSpacing: 0.0,
              crossAxisSpacing: 0.0,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                datastoreModel.Folder folder = filteredFolders[index]!;
                return _Folder(
                  root: root,
                  folder: folder,
                  onLongPressFolder: onLongPressFolder,
                  share: share,
                  relativePath: relativePath,
                  datastore: datastore,
                  path: path,
                  autofill: autofill,
                );
              },
              childCount: _getFolderCount(),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent:
                  orientation == Orientation.portrait ? 190.w : 190.w,
              mainAxisExtent:
                  orientation == Orientation.portrait ? 50.h : 120.h,
              mainAxisSpacing: 5.h,
              crossAxisSpacing: 10.w,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                datastoreModel.Item item = filteredItems[index]!;
                return _Item(
                    item: item,
                    onLongPressItem: onLongPressItem,
                    autofill: autofill,
                    root: root,
                    datastore: datastore,
                    share: share,
                    path: path,
                    relativePath: relativePath);
              },
              childCount: _getItemCount(),
            ),
          ),
        ] else ...[
          SliverList.builder(
            itemBuilder: (BuildContext context, int index) {
              datastoreModel.Folder folder = filteredFolders[index]!;
              return _Folder(
                  root: root,
                  folder: folder,
                  onLongPressFolder: onLongPressFolder,
                  share: share,
                  relativePath: relativePath,
                  datastore: datastore,
                  path: path,
                  autofill: autofill);
            },
            itemCount: _getFolderCount(),
          ),
          SliverList.builder(
            itemBuilder: (BuildContext context, int index) {
              datastoreModel.Item item = filteredItems[index]!;
              return _Item(
                  item: item,
                  onLongPressItem: onLongPressItem,
                  autofill: autofill,
                  root: root,
                  datastore: datastore,
                  share: share,
                  path: path,
                  relativePath: relativePath);
            },
            itemCount: _getItemCount(),
          ),
        ],
      ],
    );
  }

  int _getFolderCount() {
    return filteredFolders.length;
  }

  int _getItemCount() {
    return filteredItems.length;
  }

  int _calculateEntryCount() {
    return _getFolderCount() + _getItemCount();
  }

  bool _filterDeleted(datastoreEntry) {
    return datastoreEntry.deleted != true;
  }

  void _filterContent(datastoreModel.Folder? root, List<String?>? path) {
    if (root == null) {
      return;
    }
    filteredFolders.clear();
    if (root.folders != null) {
      for (var i = 0; i < root.folders!.length; i++) {
        if (!_filterDeleted(root.folders![i])) {
          continue;
        }
        var alreadyExists =
            filteredFolders.any((folder) => folder!.id == root.folders![i]!.id);
        if (alreadyExists) {
          continue;
        }
        filteredFolders.add(root.folders![i]);
      }
    }
    filteredItems.clear();
    if (root.items != null) {
      for (var i = 0; i < root.items!.length; i++) {
        if (!_filterDeleted(root.items![i])) {
          continue;
        }
        var alreadyExists =
            filteredItems.any((item) => item!.id == root.items![i]!.id);
        if (alreadyExists) {
          continue;
        }
        filteredItems.add(root.items![i]);
      }
    }
    filteredFolders.sort((a, b) {
      if (a!.name == null) {
        return -1;
      }
      if (b!.name == null) {
        return 1;
      }
      return a.name!.compareTo(b.name!);
    });
    filteredItems.sort((a, b) {
      if (a!.name == null) {
        return -1;
      }
      if (b!.name == null) {
        return 1;
      }
      return a.name!.compareTo(b.name!);
    });
  }
}

class _Item extends StatelessWidget {
  const _Item({
    Key? key,
    required this.item,
    required this.onLongPressItem,
    required this.autofill,
    required this.root,
    required this.datastore,
    required this.share,
    required this.path,
    required this.relativePath,
  }) : super(key: key);

  final datastoreModel.Item item;
  final datastoreModel.ItemCallback? onLongPressItem;
  final bool? autofill;
  final datastoreModel.Folder? root;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;

  @override
  Widget build(BuildContext context) {
    return componentItem.Item(
      item: item,
      isGridView: DatastoreScaffold.isGridView.value,
      onLongPress: (datastoreModel.Item? item) {
        if (onLongPressItem != null) {
          onLongPressItem!(item);
        }
      },
      onPressed: () async {
        if (autofill ?? false) {
          await autofill_service.autofill(item.secretId, item.secretKey);
        } else {
          if (item.fileId != null) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DownloadFileScreen(
                  parent: root,
                  datastore: datastore,
                  share: share,
                  item: item,
                  path: path,
                  relativePath: relativePath,
                ),
              ),
            );
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EditSecretScreen(
                  parent: root,
                  datastore: datastore,
                  share: share,
                  item: item,
                  path: path,
                  relativePath: relativePath,
                ),
              ),
            );
          }
        }
      },
      isShare: item.shareId != null,
    );
  }
}

class _Folder extends StatelessWidget {
  const _Folder({
    Key? key,
    required this.root,
    required this.folder,
    required this.onLongPressFolder,
    required this.share,
    required this.relativePath,
    required this.datastore,
    required this.path,
    required this.autofill,
  }) : super(key: key);
  final datastoreModel.Folder? root;
  final datastoreModel.Folder folder;
  final datastoreModel.FolderCallback? onLongPressFolder;
  final datastoreModel.Folder? share;
  final List<String?>? relativePath;
  final datastoreModel.Datastore? datastore;
  final List<String?>? path;
  final bool? autofill;

  @override
  Widget build(BuildContext context) {
    return componentFolder.Folder(
      folder: folder,
      isGridView: DatastoreScaffold.isGridView.value,
      onLongPress: (datastoreModel.Folder? folder) {
        if (onLongPressFolder != null) {
          onLongPressFolder!(folder);
        }
      },
      onPressed: () async {
        datastoreModel.Folder? newShare = share;
        List<String?> newRelativePath;
        if (folder.shareId != null) {
          newShare = folder;
          newRelativePath = [];
        } else {
          newRelativePath = List.from(relativePath!)..addAll([folder.id]);
        }

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FolderScreen(
              parent: root,
              autoNavigate: [],
              folder: folder,
              datastore: datastore,
              share: newShare,
              path: List.from(path!)..addAll([folder.id]),
              relativePath: newRelativePath,
              autofill: autofill,
            ),
          ),
        );
      },
      isShare: folder.shareId != null,
    );
  }
}

class _EmptyView extends StatelessWidget {
  const _EmptyView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        Image.asset(
          AppAssets.psonoEmptyFolder,
          width: 74.1.w,
          height: 66.w,
          fit: BoxFit.fill,
        ),
        component.TextDmSans(
          text: FlutterI18n.translate(context, "START_ADDING_ITEMS"),
          fontSize: 24.sp,
          fontWeight: FontWeight.w500,
          color: Colors.white,
        ),
        SizedBox(height: 10.h),
        SizedBox(
          width: 250.w,
          child: component.TextDmSans(
            text: FlutterI18n.translate(context, "STORE_PASSWORDS"),
            fontSize: 16.sp,
            fontWeight: FontWeight.w400,
            color: Colors.white.withOpacity(0.60),
            textAlign: TextAlign.center,
          ),
        ),
        const Spacer(),
      ],
    );
  }
}
