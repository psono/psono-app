import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/theme.dart';
import 'package:psono/utils/app_assets.dart';

class Folder extends StatelessWidget {
  final datastoreModel.Folder? folder;
  final VoidCallback? onPressed;
  final datastoreModel.FolderCallback? onLongPress;
  final bool? isShare;
  final bool showSelectBox;
  final bool isSelected;
  final bool isGridView;

  Folder({
    this.folder,
    this.onPressed,
    this.onLongPress,
    this.isShare,
    this.showSelectBox = false,
    this.isSelected = false,
    this.isGridView = true,
  });

  @override
  Widget build(BuildContext context) {
    if (isGridView) {
      return _GridViewFolder(
        isShare: isShare,
        showSelectBox: showSelectBox,
        folder: folder,
        isSelected: isSelected,
        onLongPress: onLongPress,
        onPressed: onPressed,
      );
    }
    return _ListViewFolder(
      isShare: isShare,
      showSelectBox: showSelectBox,
      folder: folder,
      isSelected: isSelected,
      onLongPress: onLongPress,
      onPressed: onPressed,
    );
  }
}

class _ListViewFolder extends StatelessWidget {
  const _ListViewFolder({
    Key? key,
    required this.isShare,
    required this.showSelectBox,
    required this.folder,
    required this.isSelected,
    required this.onLongPress,
    required this.onPressed,
  }) : super(key: key);

  final bool? isShare;
  final bool showSelectBox;
  final datastoreModel.Folder? folder;
  final bool isSelected;
  final datastoreModel.FolderCallback? onLongPress;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    Widget icon = Image.asset(
      AppAssets.entityFolderIcon,
      height: 48.w,
      width: 48.w,
    );
    Widget shareIcon = Container();
    if (isShare!) {
      shareIcon = Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.bottomRight,
        children: [
          Stack(
            alignment: const Alignment(0, 0),
            children: [
              const Icon(
                FontAwesome.circle,
                color: Colors.white,
                size: 32,
              ),
              Icon(
                FontAwesome.users,
                color: primarySwatch.shade500,
                size: 18,
              ),
            ],
          )
        ],
      );
    }

    Widget selectedIcon = Container();
    if (showSelectBox && folder!.shareRights!.delete!) {
      List<Widget> children = [
        const Icon(
          FontAwesome.square,
          color: Colors.white,
          size: 20,
        ),
      ];

      if (isSelected) {
        children.add(
          Icon(
            FontAwesome.check,
            size: 20,
            color: primarySwatch.shade500,
          ),
        );
      }

      selectedIcon = Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        child: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.bottomLeft,
          children: children,
        ),
      );
    }

    return Column(
      children: [
        GestureDetector(
          onLongPress: () {
            if (onLongPress != null) {
              onLongPress!(folder);
            }
          },
          child: TextButton(
            onPressed: onPressed,
            child: Row(
              children: [
                selectedIcon,
                icon,
                SizedBox(width: 10.w),
                Expanded(
                  flex: 100,
                  child: TextDmSans(
                    text: folder!.name!,
                    color: Colors.white,
                    textAlign: TextAlign.start,
                    maxLines: 2,
                    fontSize: 18,
                    overflow: TextOverflow.ellipsis,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const Spacer(flex: 10),
                shareIcon,
              ],
            ),
          ),
        ),
        Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(vertical: 12.h),
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(
                width: 1,
                strokeAlign: BorderSide.strokeAlignCenter,
                color: Colors.white.withOpacity(0.20000000298023224),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _GridViewFolder extends StatelessWidget {
  const _GridViewFolder({
    Key? key,
    required this.isShare,
    required this.showSelectBox,
    required this.folder,
    required this.isSelected,
    required this.onLongPress,
    required this.onPressed,
  }) : super(key: key);

  final bool? isShare;
  final bool showSelectBox;
  final datastoreModel.Folder? folder;
  final bool isSelected;
  final datastoreModel.FolderCallback? onLongPress;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    Widget icon = Image.asset(
      AppAssets.entityFolderIcon,
      height: 120,
      width: 120,
    );

    if (isShare!) {
      icon = Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.bottomRight,
        children: [
          icon,
          Positioned(
            child: Stack(
              alignment: const Alignment(0, 0),
              children: [
                Icon(
                  FontAwesome.circle,
                  color: Colors.white,
                  size: 32,
                ),
                Icon(
                  FontAwesome.users,
                  color: primarySwatch.shade500,
                  size: 18,
                ),
              ],
            ),
          )
        ],
      );
    }

    if (showSelectBox && folder!.shareRights!.delete!) {
      List<Widget> children = [
        Icon(
          FontAwesome.square,
          color: Colors.white,
          size: 20.sp,
        ),
      ];

      if (isSelected) {
        children.add(
          Icon(
            FontAwesome.check,
            size: 20.sp,
            color: primarySwatch.shade500,
          ),
        );
      }

      icon = Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.bottomLeft,
        children: [
          icon,
          Stack(
            children: children,
          )
        ],
      );
    }

    return GestureDetector(
      onLongPress: () {
        if (onLongPress != null) {
          onLongPress!(folder);
        }
      },
      child: TextButton(
        onPressed: () {
          onPressed!();
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            icon,
            TextDmSans(
              text: folder!.name!,
              color: Colors.white,
              textAlign: TextAlign.center,
              maxLines: 2,
              fontSize: 18,
              overflow: TextOverflow.ellipsis,
              fontWeight: FontWeight.w500,
            )
          ],
        ),
      ),
    );
  }
}
