import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/data_score_components/datascore_scaffold/datascore_scaffold.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;

import 'folder.dart' as componentFolder;
import 'item.dart' as componentItem;

class FolderSelectTree extends StatelessWidget {
  final datastoreModel.Folder? root;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;
  final List<String?>? selectedFolders;
  final List<String?>? selectedItems;
  final datastoreModel.FolderCallback? onSelectFolder;
  final datastoreModel.ItemCallback? onSelectItem;
  final List<datastoreModel.Folder?> filteredFolders = [];
  final List<datastoreModel.Item?> filteredItems = [];

  FolderSelectTree(
      {this.root,
      this.datastore,
      this.share,
      this.path,
      this.relativePath,
      this.selectedFolders,
      this.selectedItems,
      this.onSelectFolder,
      this.onSelectItem});

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    _filterContent(root, path);

    return CustomScrollView(
      slivers: [
        if (DatastoreScaffold.isGridView.value) ...[
          SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 180,
              mainAxisExtent: 200,
              mainAxisSpacing: 0.0,
              crossAxisSpacing: 0.0,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                datastoreModel.Folder folder = filteredFolders[index]!;
                return _Folder(
                  folder: folder,
                  onSelectFolder: onSelectFolder,
                  selectedFolders: selectedFolders,
                );
              },
              childCount: _getFolderCount(),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent:
                  orientation == Orientation.portrait ? 190.w : 190.w,
              mainAxisExtent:
                  orientation == Orientation.portrait ? 50.h : 120.h,
              mainAxisSpacing: 5.h,
              crossAxisSpacing: 10.w,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                datastoreModel.Item item = filteredItems[index]!;
                return _Item(
                  item: item,
                  onSelectItem: onSelectItem,
                  selectedItems: selectedItems,
                );
              },
              childCount: _getItemCount(),
            ),
          )
        ] else ...[
          SliverList.builder(
            itemBuilder: (BuildContext context, int index) {
              datastoreModel.Folder folder = filteredFolders[index]!;
              return _Folder(
                folder: folder,
                onSelectFolder: onSelectFolder,
                selectedFolders: selectedFolders,
              );
            },
            itemCount: _getFolderCount(),
          ),
          SliverList.builder(
            itemBuilder: (context, index) {
              datastoreModel.Item item = filteredItems[index]!;
              return _Item(
                item: item,
                onSelectItem: onSelectItem,
                selectedItems: selectedItems,
              );
            },
            itemCount: _getItemCount(),
          )
        ],
      ],
    );
  }

  bool _filterDeleted(datastoreEntry) {
    return datastoreEntry.deleted != true;
  }

  void _filterContent(datastoreModel.Folder? root, List<String?>? path) {
    if (root == null) {
      return;
    }

    if (root.folders != null) {
      for (var i = 0; i < root.folders!.length; i++) {
        if (!_filterDeleted(root.folders![i])) {
          continue;
        }
        var alreadyExists =
            filteredFolders.any((folder) => folder!.id == root.folders![i]!.id);
        if (alreadyExists) {
          continue;
        }
        filteredFolders.add(root.folders![i]);
      }
    }
    if (root.items != null) {
      for (var i = 0; i < root.items!.length; i++) {
        if (!_filterDeleted(root.items![i])) {
          continue;
        }
        var alreadyExists =
            filteredItems.any((item) => item!.id == root.items![i]!.id);
        if (alreadyExists) {
          continue;
        }
        filteredItems.add(root.items![i]);
      }
    }
    filteredFolders.sort((a, b) {
      if (a!.name == null) {
        return -1;
      }
      if (b!.name == null) {
        return 1;
      }
      return a.name!.compareTo(b.name!);
    });
    filteredItems.sort((a, b) {
      if (a!.name == null) {
        return -1;
      }
      if (b!.name == null) {
        return 1;
      }
      return a.name!.compareTo(b.name!);
    });
  }

  int _getFolderCount() {
    return filteredFolders.length;
  }

  int _getItemCount() {
    return filteredItems.length;
  }
}

class _Item extends StatelessWidget {
  const _Item({
    Key? key,
    required this.item,
    required this.onSelectItem,
    required this.selectedItems,
  }) : super(key: key);

  final datastoreModel.Item item;
  final datastoreModel.ItemCallback? onSelectItem;
  final List<String?>? selectedItems;

  @override
  Widget build(BuildContext context) {
    return componentItem.Item(
      item: item,
      isGridView: DatastoreScaffold.isGridView.value,
      onPressed: () async {
        if (onSelectItem != null) {
          onSelectItem!(item);
        }
      },
      isShare: item.shareId != null,
      showSelectBox: true,
      isSelected: selectedItems!.contains(item.id),
    );
  }
}

class _Folder extends StatelessWidget {
  const _Folder({
    Key? key,
    required this.folder,
    required this.onSelectFolder,
    required this.selectedFolders,
  }) : super(key: key);

  final datastoreModel.Folder folder;
  final datastoreModel.FolderCallback? onSelectFolder;
  final List<String?>? selectedFolders;

  @override
  Widget build(BuildContext context) {
    return componentFolder.Folder(
      isGridView: DatastoreScaffold.isGridView.value,
      folder: folder,
      onPressed: () async {
        if (onSelectFolder != null) {
          onSelectFolder!(folder);
        }
      },
      isShare: folder.shareId != null,
      showSelectBox: true,
      isSelected: selectedFolders!.contains(folder.id),
    );
  }
}
