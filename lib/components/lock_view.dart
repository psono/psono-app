import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/utils/app_assets.dart';

typedef bool PassCodeVerify(String? passcode);

class LockView extends StatefulWidget {
  final VoidCallback onSuccess;
  final VoidCallback? fingerFunction;
  final VoidCallback? signOut;
  final bool? fingerVerify;
  final int passLength;
  final PassCodeVerify passCodeVerify;

  LockView({
    required this.onSuccess,
    required this.passLength,
    required this.passCodeVerify,
    this.fingerFunction,
    this.signOut,
    this.fingerVerify = false,
  }) : assert(passLength <= 8);

  @override
  _LockViewState createState() => _LockViewState();
}

class _LockViewState extends State<LockView> {
  final controller = TextEditingController();
  final focusNode = FocusNode();

  @override
  void dispose() {
    controller.dispose();
    focusNode.dispose();
    super.dispose();
  }

  _fingerPrint() {
    if (widget.fingerVerify!) {
      widget.onSuccess();
    }
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      widget.fingerFunction!();
    });
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 300), () {
      _fingerPrint();
    });
    Orientation orientation = MediaQuery.of(context).orientation;

    Widget titleAndPinInput = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: orientation == Orientation.portrait ? 40.sp : 0),
        TextDmSans(
          text: FlutterI18n.translate(context, "AUTHORIZATION"),
          color: Colors.white,
          fontSize: orientation == Orientation.portrait ? 40.sp : 20.sp,
          fontWeight: FontWeight.w500,
        ),
        SizedBox(height: orientation == Orientation.portrait ? 80.sp : 20.sp),
        TextDmSans(
          text: FlutterI18n.translate(context, "ENTER_YOUR_PIN"),
          fontSize: orientation == Orientation.portrait ? 14.sp : 12.sp,
          color: Colors.white.withOpacity(0.60),
          fontWeight: FontWeight.w400,
        ),
        SizedBox(height: 16.sp),
        component.PinInputCustom(
          controller: controller,
          focusNode: focusNode,
          onContinue: (code) {},
          validator: (code) {
            if (!widget.passCodeVerify(code)) {
              controller.text = '';
              return FlutterI18n.translate(context, "PIN_INCORRECT");
            }
            return null;
          },
        ),
      ],
    );

    Widget customKeyboard = component.CustomKeyBoard(
      maxLength: 6,
      flex: 10,
      specialKey: Image.asset(
        Platform.isIOS ? AppAssets.faceIdSymbol : AppAssets.fingerprint,
        width: orientation == Orientation.portrait ? 30.w : 18.w,
        height: orientation == Orientation.portrait ? 30.w : 18.w,
        color: Colors.white,
      ),
      specialKeyOnTap: () {
        widget.fingerFunction?.call();
      },
      onChanged: (str) {
        controller.text = str;
      },
      pinTheme: component.PinTheme(
        keysColor: Colors.white,
      ),
    );

    return component.ScaffoldDark(
      body: orientation == Orientation.portrait
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Spacer(),
                titleAndPinInput,
                const Spacer(
                  flex: 3,
                ),
                customKeyboard,
                const Spacer(),
              ],
            )
          : Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(16.w),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        titleAndPinInput,
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(16.w),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        customKeyboard,
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
