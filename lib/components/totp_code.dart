import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp/otp.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/utils/app_colors.dart';

import 'icons.dart';

class TotpCode extends StatefulWidget {
  final String? code;
  final String? algorithm;
  final int? digits;
  final int? period;

  TotpCode({
    this.code,
    this.algorithm,
    this.digits,
    this.period,
  });

  @override
  _TotpCodeState createState() => _TotpCodeState();
}

class _TotpCodeState extends State<TotpCode> {
  int millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
  late int countdown;
  late String remainingTime;
  int? length = 6; // default length 6
  Algorithm algorithm = Algorithm.SHA1; // default algorithm SHA1
  int? interval = 30; // default interval 30
  final ValueNotifier<double> percentNotifier = ValueNotifier<double>(1);
  late Timer timer;
  final ValueNotifier<String> generatedCode = ValueNotifier<String>('');
  late FToast _fToast;

  @override
  void initState() {
    if (widget.digits != null) {
      length = widget.digits;
    }

    if (widget.algorithm == 'SHA256') {
      algorithm = Algorithm.SHA256;
    } else if (widget.algorithm == 'SHA512') {
      algorithm = Algorithm.SHA512;
    }

    if (widget.period != null) {
      interval = widget.period;
      remainingTime =
          '${(interval! ~/ 60).toString().padLeft(2, '0')}:${(interval! % 60).toString().padLeft(2, '0')}';
    }

    countdown = interval! -
        ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() % interval!);
    percentNotifier.value = 1 -
        (interval! -
                ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() %
                    interval!)) /
            interval!;

    _fToast = FToast();
    _fToast.init(context);

    timer = Timer.periodic(const Duration(milliseconds: 500), (timer) {
      if (helper.isValidTOTPCode(widget.code)) {
        millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
        countdown =
            interval! - ((millisecondsSinceEpoch ~/ 1000).round() % interval!);
        remainingTime =
            '${(countdown ~/ 60).toString().padLeft(2, '0')}:${(countdown % 60).toString().padLeft(2, '0')}';
        percentNotifier.value = 1 - countdown / interval!;

        generatedCode.value = OTP.generateTOTPCodeString(
          widget.code!,
          millisecondsSinceEpoch,
          algorithm: algorithm,
          interval: interval!,
          length: length!,
          isGoogle: true,
        );
      } else {
        generatedCode.value = FlutterI18n.translate(context, 'INVALID_CODE');
      }
      setState(() {});
    });

    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: ValueListenableBuilder<double>(
            valueListenable: percentNotifier,
            builder: (context, val, child) {
              return CircularPercentIndicator(
                percent: val,
                animation: true,
                lineWidth: 8,
                circularStrokeCap: CircularStrokeCap.round,
                animateFromLastPercent: true,
                radius: 100.r,
                backgroundColor: Colors.white.withOpacity(0.10),
                progressColor: C.mountainMeadow,
                center: component.TextDmSans(
                  text: remainingTime,
                  color: Colors.white,
                  fontSize: 32.sp,
                  fontWeight: FontWeight.w500,
                ),
              );
            },
          ),
        ),
        SizedBox(height: 20.h),
        component.TextDmSans(
          text: FlutterI18n.translate(context, 'YOUR_ONE_TIME_PASSWORD'),
          color: Colors.white.withOpacity(0.5),
          fontSize: 16.sp,
          fontWeight: FontWeight.w500,
        ),
        SizedBox(height: 16.h),
        ValueListenableBuilder<String>(
          valueListenable: generatedCode,
          builder: (context, value, child) {
            final code = value;
            final List<Widget> codeWidgets = [];
            if (code == FlutterI18n.translate(context, 'INVALID_CODE')) {
              return component.AlertDanger(
                text: code,
              );
            }
            if (code.length < length!) {
              return Container();
            }
            for (var i = 0; i < length!; i++) {
              codeWidgets.add(
                Expanded(
                  flex: 10,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.10),
                      border: Border.all(
                        color: Colors.white.withOpacity(0.20),
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Center(
                      child: component.TextDmSans(
                        text: code[i],
                        color: Colors.white,
                        fontSize: 32.sp,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              );
              if (i < (length! - 1)) {
                codeWidgets.add(const Spacer());
              }
            }
            return Row(
              children: codeWidgets,
            );
          },
        ),
        SizedBox(height: 24.h),
        InkWell(
          borderRadius: BorderRadius.circular(100.r),
          onTap: () {
            Clipboard.setData(ClipboardData(text: generatedCode.value));
            _clipboardCopyToast();
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 13.h, horizontal: 50.w),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.10),
              borderRadius: BorderRadius.circular(100.r),
              border: Border.all(
                color: Colors.white.withOpacity(0.20),
              ),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.copy,
                  color: Colors.white,
                  size: 20.sp,
                ),
                SizedBox(width: 6.w),
                component.TextDmSans(
                  text: FlutterI18n.translate(context, 'COPY'),
                  color: Colors.white,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w700,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
      child: toast,
      toastDuration: Duration(seconds: 2),
      positionedToastBuilder: (context, child) {
        return Positioned(child: child, top: 110, left: 0, right: 0);
      },
    );
  }
}
