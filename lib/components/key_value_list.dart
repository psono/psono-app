import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:psono/components/_index.dart' as component;

import 'icons.dart';

class KeyValueList extends StatefulWidget {
  final List? contentKV;
  final Function(List)? onChangeContentKV;
  KeyValueList({
    this.contentKV,
    this.onChangeContentKV,
  });

  @override
  _KeyValueListState createState() => _KeyValueListState();
}

class _KeyValueListState extends State<KeyValueList> {
  late FToast _fToast;
  List<TextEditingController> controllers = [];
  List? contentKV;

  @override
  void initState() {
    super.initState();

    _fToast = FToast();
    _fToast.init(context);
    setState(() {
      contentKV = widget.contentKV;
    });
  }

  @override
  void dispose() {
    for (var i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    for (var i = 0; i < widget.contentKV!.length; i++) {
      final keyTextController = TextEditingController(
        text: widget.contentKV![i]['key'],
      );
      controllers.add(keyTextController);
      children.add(
        Padding(
          padding: EdgeInsets.only(bottom: 16.h),
          child: component.CustomTextField(
            controller: keyTextController,
            onChanged: (text) {
              widget.contentKV![i]['key'] = text;
            },
            labelText: FlutterI18n.translate(context, 'KEY'),
            suffixIcon: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                  onTap: () {
                    widget.contentKV!.removeAt(i);
                    widget.onChangeContentKV!(widget.contentKV!);
                  },
                  child: Padding(
                    padding: EdgeInsets.all(12.w),
                    child: Icon(
                      Icons.delete,
                      color: Colors.white.withOpacity(0.50),
                      size: 20.sp,
                    ),
                  ),
                ),
                component.CopyIcon(
                  onTap: () {
                    Clipboard.setData(
                      ClipboardData(
                        text: widget.contentKV![i]['key'],
                      ),
                    );
                    _clipboardCopyToast();
                  },
                )
              ],
            ),
          ),
        ),
      );

      final valueTextController = TextEditingController(
        text: widget.contentKV![i]['value'],
      );
      controllers.add(valueTextController);

      children.add(
        Padding(
          padding: EdgeInsets.only(bottom: 16.h),
          child: Stack(
            alignment: Alignment.centerRight,
            children: [
              component.CustomTextField(
                controller: valueTextController,
                onChanged: (text) {
                  widget.contentKV![i]['value'] = text;
                  widget.onChangeContentKV!(widget.contentKV!);
                },
                labelText: FlutterI18n.translate(
                  context,
                  'VALUE',
                ),
                suffixIcon: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    InkWell(
                      onTap: () {
                        widget.contentKV!.removeAt(i);
                        widget.onChangeContentKV!(widget.contentKV!);
                      },
                      child: Padding(
                        padding: EdgeInsets.all(12.w),
                        child: Icon(
                          Icons.delete,
                          color: Colors.white.withOpacity(0.50),
                          size: 20.sp,
                        ),
                      ),
                    ),
                    component.CopyIcon(
                      onTap: () {
                        Clipboard.setData(
                          ClipboardData(
                            text: widget.contentKV![i]['value'],
                          ),
                        );
                        _clipboardCopyToast();
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    children.add(
      Padding(
        padding: EdgeInsets.only(top: 16.h, bottom: 16.h),
        child: component.BtnPrimary(
          onPressed: () async {
            widget.contentKV!.add({
              'key': '',
              'value': '',
            });

            widget.onChangeContentKV!(contentKV!);
          },
          text: FlutterI18n.translate(context, "ADD_ENTRY"),
        ),
      ),
    );
    return Row(
      children: <Widget>[
        Expanded(
          flex: 10,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: children,
          ),
        )
      ],
    );
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: const TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
      child: toast,
      toastDuration: const Duration(seconds: 2),
      positionedToastBuilder: (context, child) {
        return Positioned(top: 110, left: 0, right: 0, child: child);
      },
    );
  }
}
