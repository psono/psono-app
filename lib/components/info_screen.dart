import 'package:flutter/material.dart';

import './alert_info.dart';

class InfoScreen extends StatelessWidget {
  final String? text;
  final List<Widget> buttons;

  InfoScreen({this.text, this.buttons = const []});

  @override
  Widget build(BuildContext context) {
    final buttonsWithSpacer = <Widget>[];
    for (var i = 0; i < buttons.length; i++) {
      buttonsWithSpacer.add(buttons[i]);
      if (i < buttons.length - 1) {
        buttonsWithSpacer.add(Spacer());
      }
    }
    return Center(
      child: Column(
        children: <Widget>[
          const Spacer(flex: 1),
          AlertInfo(text: text),
          const Spacer(flex: 1),
          ...buttonsWithSpacer,
          const Spacer(flex: 4),
        ],
      ),
    );
  }
}
