import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class Btn extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final Color? color;
  final Color textColor;
  final Widget? customChild;
  final EdgeInsetsGeometry? _padding;

  Btn(
      {this.text,
      this.onPressed,
      this.color = Colors.white,
      this.textColor = const Color(0xFF333333),
      this.customChild,
      EdgeInsetsGeometry? padding})
      : _padding = padding ?? EdgeInsets.all(18.w);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressed!();
      },
      style: ElevatedButton.styleFrom(
        backgroundColor: color,
        padding: _padding,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100.r),
        ),
      ),
      child: customChild ??
          Text(
            text!,
            style: GoogleFonts.dmSans(
              color: textColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.w700,
            ),
          ),
    );
  }
}
