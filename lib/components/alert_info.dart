import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/utils/app_assets.dart';

class AlertInfo extends StatelessWidget {
  final String? text;

  AlertInfo({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.w),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.10),
        border: Border.all(
          width: 1,
          color: Colors.white.withOpacity(0.20),
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(20.r),
        ),
      ),
      child: Row(
        children: [
          Image.asset(
            AppAssets.info,
            width: 32.w,
            height: 32.w,
            fit: BoxFit.fill,
          ),
          SizedBox(width: 12.w),
          Expanded(
            child: TextDmSans(
              text: text ?? "",
              fontSize: 14.sp,
              fontWeight: FontWeight.w400,
              color: Colors.white.withOpacity(0.60),
            ),
          ),
        ],
      ),
    );
  }
}
