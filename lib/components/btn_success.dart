import 'package:flutter/material.dart';
import './btn.dart';

class BtnSuccess extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final double? width;

  BtnSuccess({this.text, this.onPressed, this.width});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? double.infinity,
      child: Btn(
        text: text,
        onPressed: () {
          onPressed!();
        },
        color: Color(0xFF2dbb93),
        textColor: Colors.white,
      ),
    );
  }
}
