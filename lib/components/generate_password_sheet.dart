import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/settings.dart' as settings;
import 'package:psono/utils/app_colors.dart';

import '_index.dart' as component;

generatePasswordSheet(
    {required BuildContext context,
    required String password,
    required Function(String) onSetPassword}) {
  String upperCase = settings.getSetting("setting_password_letters_uppercase")!;
  String lowerCase = settings.getSetting("setting_password_letters_lowercase")!;
  String numbers = settings.getSetting("setting_password_numbers")!;
  String length = settings.getSetting("setting_password_length")!;
  String specialChars = settings.getSetting("setting_password_special_chars")!;
  TextEditingController lengthController = TextEditingController(text: length);
  TextEditingController upperCaseController =
      TextEditingController(text: upperCase);
  TextEditingController lowerCaseController =
      TextEditingController(text: lowerCase);
  TextEditingController numbersController =
      TextEditingController(text: numbers);
  TextEditingController specialCharsController =
      TextEditingController(text: specialChars);
  Widget divider = Container(
    decoration: ShapeDecoration(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          width: 1,
          strokeAlign: BorderSide.strokeAlignCenter,
          color: Colors.white.withOpacity(0.20),
        ),
      ),
    ),
  );
  component.showCustomBottomSheet(
    context: context,
    isScrollControlled: false,
    scrollControlDisabledMaxHeightRatio: 0.80,
    child: Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: Material(
        color: Colors.transparent,
        child: StatefulBuilder(builder: (context, stState) {
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              decoration: BoxDecoration(
                color: const Color(0xff171717),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.r),
                  topRight: Radius.circular(30.r),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      component.TextDmSans(
                        text: FlutterI18n.translate(
                            context, "PASSWORD_GENERATOR"),
                        textAlign: TextAlign.center,
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                      const Spacer(),
                      IconButton(
                        icon: const Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 32.h),
                  component.TextDmSans(
                    text: password,
                    textAlign: TextAlign.center,
                    color: C.mountainMeadow,
                    fontSize: 32.sp,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(height: 32.h),
                  Row(
                    children: [
                      Expanded(
                        flex: 20,
                        child: component.Btn(
                          onPressed: () async {
                            component.Loader.show(context);
                            String newPassword =
                                await managerDatastorePassword.generate(
                                    length:
                                        int.parse(lengthController.text.trim()),
                                    allowedCharacters:
                                        upperCaseController.text.trim() +
                                            lowerCaseController.text.trim() +
                                            numbersController.text.trim() +
                                            specialCharsController.text.trim());
                            component.Loader.hide();
                            stState(() {
                              password = newPassword;
                            });
                          },
                          customChild: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.swap_horiz_sharp,
                                color: Colors.black,
                                size: 20.sp,
                              ),
                              SizedBox(width: 5.w),
                              component.TextDmSans(
                                text:
                                    FlutterI18n.translate(context, "GENERATE"),
                                color: Colors.black,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w700,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Spacer(),
                      Expanded(
                        flex: 20,
                        child: component.Btn(
                          onPressed: () {
                            Clipboard.setData(ClipboardData(text: password));
                          },
                          customChild: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.copy,
                                color: Colors.black,
                                size: 20.sp,
                              ),
                              SizedBox(width: 5.w),
                              component.TextDmSans(
                                text: FlutterI18n.translate(context, "COPY"),
                                color: Colors.black,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w700,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 8.h),
                  component.Btn(
                    onPressed: () {
                      onSetPassword(password);
                      Navigator.pop(context);
                    },
                    customChild: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.check_circle_outline,
                          color: Colors.black,
                          size: 20.sp,
                        ),
                        SizedBox(width: 5.w),
                        component.TextDmSans(
                          text: FlutterI18n.translate(context, "USE_PASSWORD")
                              .toUpperCase(),
                          color: Colors.black,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w700,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 32.h),
                  divider,
                  SizedBox(height: 24.h),
                  component.CustomTextField(
                    labelText:
                        FlutterI18n.translate(context, "PASSWORD_LENGTH"),
                    controller: lengthController,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    labelText:
                        FlutterI18n.translate(context, "LETTERS_UPPERCASE"),
                    controller: upperCaseController,
                    textCapitalization: TextCapitalization.characters,
                    inputFormatters: [
                      // FilteringTextInputFormatter.
                      // UPPER_CASE
                      FilteringTextInputFormatter.allow(RegExp(r'[A-Z]'))
                    ],
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    labelText:
                        FlutterI18n.translate(context, "LETTERS_LOWERCASE"),
                    controller: lowerCaseController,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-z]'))
                    ],
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    labelText: FlutterI18n.translate(context, "NUMBERS"),
                    controller: numbersController,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                    ],
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 16.h),
                  component.CustomTextField(
                    labelText: FlutterI18n.translate(context, "SPECIAL_CHARS"),
                    controller: specialCharsController,
                    keyboardType: TextInputType.text,
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    ),
  );
}
