import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pinput/pinput.dart';
import 'package:psono/utils/app_colors.dart';

class PinInputCustom extends StatelessWidget {
  const PinInputCustom(
      {Key? key,
      required this.controller,
      required this.focusNode,
      required this.onContinue,
      this.validator})
      : super(key: key);

  final TextEditingController controller;
  final FocusNode focusNode;
  final Function(String) onContinue;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Pinput(
      length: 6,
      hapticFeedbackType: HapticFeedbackType.lightImpact,
      obscureText: true,
      useNativeKeyboard: false,
      obscuringCharacter: "*",
      cursor: Container(),
      controller: controller,
      autofocus: true,
      focusNode: focusNode,
      validator: validator,
      submittedPinTheme: PinTheme(
        width: 48.w,
        height: orientation == Orientation.portrait ? 48.h : 70.h,
        textStyle: TextStyle(
          fontSize: orientation == Orientation.portrait ? 36.sp : 16.sp,
          color: Colors.white,
        ),
        decoration: const BoxDecoration(
          color: Colors.transparent,
          border: Border(
            bottom: BorderSide(
              color: C.springGreen,
              width: 1,
            ),
          ),
        ),
      ),
      focusedPinTheme: PinTheme(
        width: 48.w,
        height: orientation == Orientation.portrait ? 48.h : 70.h,
        textStyle: TextStyle(
          fontSize: 36.sp,
          color: Colors.white,
        ),
        decoration: const BoxDecoration(
          color: Colors.transparent,
          border: Border(
            bottom: BorderSide(
              color: Colors.white,
              width: 1,
            ),
          ),
        ),
      ),
      defaultPinTheme: PinTheme(
        width: 48.w,
        height: orientation == Orientation.portrait ? 48.h : 70.h,
        textStyle: TextStyle(
          fontSize: 36.sp,
          color: Colors.white,
        ),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border(
            bottom: BorderSide(
              color: Colors.white.withOpacity(0.4),
              width: 1,
            ),
          ),
        ),
      ),
      onChanged: (code) async {
        bool canContinue = code.length == 6;
        if (canContinue) {
          onContinue(code);
        }
      },
    );
  }
}
