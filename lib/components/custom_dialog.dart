import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

showCustomDialog({required BuildContext context, required Widget content}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: const Color(0xff171717),
        shadowColor: const Color(0x3F000000),
        elevation: 10.0,
        insetPadding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 24.h),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24.r),
        ),
        child: content,
      );
    },
  );
}
