import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

typedef void CallbackFunction(String? search);

class SliverAppSearchBar extends StatefulWidget {
  static String tag = 'datastore-screen';

  final String? title;
  final CallbackFunction? onSearch;
  final String? defaultSearch;

  SliverAppSearchBar({
    this.title,
    this.onSearch,
    this.defaultSearch,
  });

  @override
  _SliverAppSearchBarState createState() => _SliverAppSearchBarState();
}

class _SliverAppSearchBarState extends State<SliverAppSearchBar> {
  Widget? appBarTitle;
  Icon? actionIcon;
  TextEditingController? search;

  @override
  void initState() {
    super.initState();
    search = TextEditingController();
    search!.addListener(() {
      if (search!.text.isEmpty) {
        widget.onSearch!(null);
      } else {
        widget.onSearch!(search!.text);
      }
    });
  }

  @override
  void dispose() {
    search?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      if (widget.defaultSearch != null && widget.defaultSearch!.isNotEmpty) {
        search!.text = widget.defaultSearch!;
        actionIcon = const Icon(Icons.close);
        appBarTitle = TextField(
          autofocus: true,
          controller: search,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 16.0,
          ),
          decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(top: 12),
              prefixIcon: const Icon(Icons.search, color: Colors.black),
              hintText: FlutterI18n.translate(context, "SEARCH"),
              hintStyle: const TextStyle(color: Colors.black)),
        );
      }
    });

    actionIcon ??= Icon(Icons.search);
    appBarTitle ??= Text(this.widget.title!);
    return SliverAppBar(
//      automaticallyImplyLeading: false,
//      floating: true,
//      titleSpacing: 0,
//      elevation: 4.0,
      backgroundColor: Colors.white,
      pinned: true,
      title: appBarTitle,
      actions: <Widget>[
        IconButton(
          icon: actionIcon!,
          onPressed: () {
            setState(() {
              if (actionIcon!.icon == Icons.search) {
                actionIcon = const Icon(Icons.close);
                appBarTitle = TextField(
                  autofocus: true,
                  controller: search,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                  ),
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(top: 12),
                      prefixIcon: const Icon(Icons.search, color: Colors.black),
                      hintText: FlutterI18n.translate(context, "SEARCH"),
                      hintStyle: const TextStyle(color: Colors.black)),
                );
              } else {
                actionIcon = const Icon(Icons.search);
                appBarTitle = Text(
                  widget.title!,
                );
                search!.clear();
              }
            });
          },
        ),
      ],
    );
  }
}
