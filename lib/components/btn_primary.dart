import 'package:flutter/material.dart';

import './btn.dart';

class BtnPrimary extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final double? width;
  final Color textColor;
  BtnPrimary(
      {this.text,
      required this.onPressed,
      this.width,
      this.textColor = Colors.black});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? double.infinity,
      child: Btn(
        text: text,
        onPressed: () {
          onPressed!();
        },
        color: Colors.white,
        textColor: textColor,
      ),
    );
  }
}
