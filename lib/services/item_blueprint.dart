import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/blueprint.dart';
import 'package:psono/utils/app_assets.dart';

final Blueprint _blueprintWebsitePassword = Blueprint(
  id: "website_password", // Unique ID
  name: "WEBSITE_PASSWORD", // Displayed in Dropdown Menu
  icon: component.FontAwesome.key,
  imagePath: AppAssets.iconPassword,
  settingField:
      "setting_show_website_password", // is the name of the setting to show / hide this type in the setting datastore
  settingFieldDefault: true,
  titleField:
      "website_password_title", // is the main column, that is used as filename
  descriptionField: "website_password_username",
  urlfilterField:
      "website_password_url_filter", // is the filter column for url matching
  autosubmitField:
      "website_password_auto_submit", // is the filter column for auto submit
  search: [
    'website_password_title',
    'website_password_url_filter',
  ], // are searched when the user search his entries
  fields: [
    BlueprintField(
      name: "website_password_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "website_password_url",
      field: "input",
      type: "text",
      validationType: "url",
      title: "URL",
      placeholder: "URL",
    ),
    BlueprintField(
      name: "website_password_username",
      field: "input",
      type: "text",
      title: "USERNAME",
      placeholder: "USERNAME",
    ),

    BlueprintField(
      name: "website_password_password",
      field: "input",
      type: "password",
      title: "PASSWORD",
      placeholder: "PASSWORD",
    ),
    BlueprintField(
      name: "website_password_totp_period",
      field: "input",
      type: "text",
      title: "PERIOD_EG_30",
      placeholder: "PERIOD_EG_30",
      hidden: true,
      required: false,
    ),
    BlueprintField(
      name: "website_password_totp_digits",
      field: "input",
      type: "text",
      title: "DIGITS_EG_6",
      placeholder: "DIGITS_EG_6",
      hidden: true,
      required: false,
    ),
    // website_password_totp_code
    BlueprintField(
      name: "website_password_totp_code",
      field: "input",
      type: "website_totp_code",
      title: "TOTP_CODE",
      placeholder: "TOTP_CODE",
      hidden: false,
      required: false,
    ),
    // website_password_totp_algorithm

    BlueprintField(
      name: "website_password_totp_algorithm",
      field: "input",
      type: "text",
      title: "ALGORITHM_EG_SHA1",
      placeholder: "ALGORITHM_EG_SHA1",
      hidden: true,
      required: false,
    ),

    BlueprintField(
      name: "website_password_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
    BlueprintField(
      name: "website_password_auto_submit",
      field: "input",
      type: "checkbox",
      title: "AUTOMATIC_SUBMIT",
      position: "advanced",
    ),
    BlueprintField(
      name: "website_password_url_filter",
      field: "textarea",
      title: "DOMAIN_FILTER",
      placeholder: "URL_FILTER_EG",
      position: "advanced",
    ),
  ],
);

final Blueprint _blueprintApplicationPassword = Blueprint(
  id: "application_password", // Unique ID
  name: "APPLICATION_PASSWORD", // Displayed in Dropdown Menu
  icon: component.FontAwesome.key,
  imagePath: AppAssets.iconPassword,
  settingField:
      "setting_show_application_password", // is the name of the setting to show / hide this type in the setting datastore
  settingFieldDefault: true,
  titleField:
      "application_password_title", // is the main column, that is used as filename

  descriptionField: "application_password_username",
  search: [
    'application_password_title',
  ], // are searched when the user search his entries
  fields: [
    BlueprintField(
      name: "application_password_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "application_password_username",
      field: "input",
      type: "text",
      title: "USERNAME",
      placeholder: "USERNAME",
    ),
    BlueprintField(
      name: "application_password_password",
      field: "input",
      type: "password",
      title: "PASSWORD",
      placeholder: "PASSWORD",
    ),
    BlueprintField(
      name: "application_password_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintTOTP = Blueprint(
  id: "totp", // Unique ID
  name: "TOTP_AUTHENTICATOR", // Displayed in Dropdown Menu
  imagePath: AppAssets.iconTotp,
  icon: component.FontAwesome.qrcode,
  settingField:
      "setting_show_totp", // is the name of the setting to show / hide this type in the setting datastore

  settingFieldDefault: true,
  titleField: "totp_title", // is the main column
  search: [
    'totp_title',
  ], // are searched when the user search his entries
  fields: [
    BlueprintField(
      name: "totp_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "totp_code",
      field: "input",
      type: "totp_code",
      title: "SECRET",
      placeholder: "SECRET",
    ),
    BlueprintField(
      name: "totp_period",
      field: "input",
      type: "text",
      title: "PERIOD_EG_30",
      placeholder: "PERIOD_EG_30",
      hidden: true,
      required: false,
    ),
    BlueprintField(
      name: "totp_algorithm",
      field: "input",
      type: "text",
      title: "ALGORITHM_EG_SHA1",
      placeholder: "ALGORITHM_EG_SHA1",
      hidden: true,
      required: false,
    ),
    BlueprintField(
      name: "totp_digits",
      field: "input",
      type: "text",
      title: "DIGITS_EG_6",
      placeholder: "DIGITS_EG_6",
      hidden: true,
      required: false,
    ),
    BlueprintField(
      name: "totp_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintNote = Blueprint(
  id: "note",
  name: "NOTE",
  icon: component.FontAwesome.sticky_note,
  imagePath: AppAssets.iconNote,
  settingField: "setting_show_note",
  settingFieldDefault: true,
  titleField: "note_title",
  search: ['note_title'],
  fields: [
    BlueprintField(
      name: "note_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "note_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);
final Blueprint _blueprintPasskey = Blueprint(
  id: "passkey",
  name: "PASSKEY",
  icon: component.FontAwesome.sticky_note,
  imagePath: AppAssets.iconNote,
  settingField: "setting_show_passkey",
  settingFieldDefault: true,
  titleField: "passkey_title",
  search: ['passkey_title'],
  fields: [
    BlueprintField(
      name: "passkey_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
  ],
);

final Blueprint _blueprintEnvironmentVariables = Blueprint(
  id: "environment_variables",
  name: "ENVIRONMENT_VARIABLES",
  icon: component.FontAwesome.superscript,
  imagePath: AppAssets.iconPassword,
  settingField: "setting_show_environment_variables",
  settingFieldDefault: false,
  titleField: "environment_variables_title",
  search: ['environment_variables_title'],
  fields: [
    BlueprintField(
      name: "environment_variables_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "environment_variables_variables",
      field: "key_value_list",
      title: "VARIABLES",
      placeholder: "VARIABLES",
      onAdd: "onAdd",
      onDelete: "onDelete",
    ),
    BlueprintField(
      name: "environment_variables_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintBookmark = Blueprint(
  id: "bookmark", // Unique ID
  name: "BOOKMARK", // Displayed in Dropdown Menu
  icon: component.FontAwesome.bookmark_o,
  imagePath: AppAssets.iconBookmark,
  settingField: "setting_show_bookmark",
  settingFieldDefault: true,
  titleField: "bookmark_title", // is the main column, that is used as filename
  urlfilterField:
      "bookmark_url_filter", // is the filter column for url matching
  search: [
    'bookmark_title',
    'bookmark_url_filter'
  ], // are searched when the user search his entries
  fields: [
    // All fields for this object with unique names
    BlueprintField(
      name: "bookmark_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "bookmark_url",
      field: "input",
      type: "text",
      validationType: "url",
      title: "URL",
      placeholder: "URL",
    ),
    BlueprintField(
      name: "bookmark_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
    BlueprintField(
      name: "bookmark_url_filter",
      field: "textarea",
      title: "DOMAIN_FILTER",
      placeholder: "URL_FILTER_EG",
      position: "advanced",
    ),
  ],
);

final Blueprint _blueprintSSHOwnKey = Blueprint(
  id: "ssh_own_key",
  name: "SSH_KEY",
  imagePath: AppAssets.iconLock2,
  icon: component.FontAwesome.lock,
  settingField: "setting_show_ssh_own_key",
  settingFieldDefault: false,
  titleField: "ssh_own_key_title",
  search: ['ssh_own_key_title'],
  fields: [
    BlueprintField(
      name: "ssh_own_key_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "ssh_own_key_public",
      field: "textarea",
      title: "PUBLIC_KEY",
      placeholder: "PUBLIC_KEY",
    ),
    BlueprintField(
      name: "ssh_own_key_private",
      field: "textarea",
      type: "password",
      title: "PRIVATE_KEY",
      placeholder: "PRIVATE_KEY",
    ),
    BlueprintField(
      name: "ssh_own_key_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintElsterCertificate = Blueprint(
  id: "elster_certificate",
  name: "ELSTER_CERTIFICATE",
  icon: component.FontAwesome.lock,
  imagePath: AppAssets.iconNote,
  settingField: "setting_show_elster_certificate",
  settingFieldDefault: false,
  titleField: "elster_certificate_title",
  search: ['elster_certificate_title'],
  fields: [
    BlueprintField(
      name: "elster_certificate_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "elster_certificate_password",
      field: "input",
      type: "password",
      title: "CERTIFICATE_PASSWORD",
      placeholder: "CERTIFICATE_PASSWORD",
      required: true,
      errorMessageRequired: 'CERTIFICATE_PASSWORD_IS_REQUIRED',
    ),
    BlueprintField(
      name: "elster_certificate_retrieval_code",
      field: "input",
      type: "password",
      title: "RETRIEVAL_CODE",
      placeholder: "RETRIEVAL_CODE",
    ),
    BlueprintField(
      name: "elster_certificate_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintCreditCard = Blueprint(
  id: "credit_card",
  name: "CREDIT_CARD",
  icon: component.FontAwesome.credit_card,
  imagePath: AppAssets.iconCreditCard,
  settingField: "setting_show_credit_card",
  settingFieldDefault: true,
  titleField: "credit_card_title",
  descriptionField: "credit_card_number",
  search: ['credit_card_title'],
  fields: [
    BlueprintField(
      name: "credit_card_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "credit_card_number",
      field: "input",
      type: "text",
      title: "CREDIT_CARD_NUMBER",
      placeholder: "1234 1234 1234 1234",
      required: true,
      errorMessageRequired: 'CREDIT_CARD_NUMBER_IS_REQUIRED',
      inputFormatter: MaskTextInputFormatter(
        mask: '#### #### #### #### ####',
        filter: {"#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy,
      ),
    ),
    BlueprintField(
      name: "credit_card_name",
      field: "input",
      type: "text",
      title: "NAME",
      required: true,
    ),
    BlueprintField(
      name: "credit_card_valid_through",
      field: "input",
      type: "text",
      title: "VALID_THROUGH",
      placeholder: "MM / YY",
      required: true,
      inputFormatter: MaskTextInputFormatter(
        mask: '## / ##',
        filter: {"#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy,
      ),
    ),
    BlueprintField(
      name: "credit_card_cvc",
      field: "input",
      type: "text",
      title: "CVC",
      placeholder: "123",
      required: true,
      inputFormatter: MaskTextInputFormatter(
        mask: '####',
        filter: {"#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy,
      ),
    ),
    BlueprintField(
      name: "credit_card_pin",
      field: "input",
      type: "password",
      title: "PIN",
      placeholder: "123",
      required: false,
      inputFormatter: MaskTextInputFormatter(
        mask: '############',
        filter: {"#": RegExp(r'[0-9]')},
        type: MaskAutoCompletionType.lazy,
      ),
    ),
    BlueprintField(
      name: "credit_card_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintMailGPGOwnKey = Blueprint(
  id: "mail_gpg_own_key",
  name: "GPG_KEY",
  imagePath: AppAssets.iconLock2,
  icon: component.FontAwesome.lock,
  settingField: "setting_show_mail_gpg_own_key",
  settingFieldDefault: false,
  titleField: "mail_gpg_own_key_title",
  search: ['mail_gpg_own_key_title', 'mail_gpg_own_key_email'],
  fields: [
    BlueprintField(
      name: "mail_gpg_own_key_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "mail_gpg_own_key_email",
      field: "input",
      type: "text",
      title: "EMAIL",
      placeholder: "EMAIL",
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_name",
      field: "input",
      type: "text",
      title: "NAME",
      placeholder: "NAME",
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_public",
      field: "textarea",
      title: "PUBLIC_KEY",
      placeholder: "PUBLIC_KEY",
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_private",
      field: "textarea",
      type: "password",
      title: "PRIVATE_KEY",
      placeholder: "PRIVATE_KEY",
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_decrypt_message",
      field: "button",
      type: "button",
      title: "DECRYPT_MESSAGE",
      hidden: true,
      classname: 'btn-default',
      onClick: "onClickDecryptMessageButton",
    ),
  ],
);

Map<String, Blueprint> _blueprints = {
  'application_password': _blueprintApplicationPassword,
  'website_password': _blueprintWebsitePassword,
  'totp': _blueprintTOTP,
  'passkey': _blueprintPasskey,
  'note': _blueprintNote,
  'environment_variables': _blueprintEnvironmentVariables,
  'elster_certificate': _blueprintElsterCertificate,
  'ssh_own_key': _blueprintSSHOwnKey,
  'credit_card': _blueprintCreditCard,
  'mail_gpg_own_key': _blueprintMailGPGOwnKey,
  'bookmark': _blueprintBookmark,
};

/// Returns the blueprint with the given [key] otherwise null
Blueprint? getBlueprint(String? key) {
  if (_blueprints.containsKey(key)) {
    return _blueprints[key!];
  }
  return null;
}

/// returns an overview of all available blueprints with name and id
List<Blueprint> getBlueprints() {
  List<Blueprint> result = [];

  _blueprints.forEach((String type, Blueprint blueprint) {
    result.add(blueprint);
  });

  return result;
}
