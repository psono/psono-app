import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:psono/model/check_host_result.dart';
import 'package:psono/model/check_known_host_result.dart';
import 'package:psono/model/known_host.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart';
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_constants.dart';

List<KnownHost> getDefaultHosts() {
  return [
    new KnownHost(
      url: 'https://www.psono.pw/server',
      verifyKey: converter.fromHex(
        'a16301bd25e3a445a83b279e7091ea91d085901933f310fdb1b137db9676de59',
      ),
    )
  ];
}

/// Reads the stored known hosts (extend by the default trusted servers).
Future<List<KnownHost>> getKnownHosts() async {
  String? knownHostsJson = await storage.read(key: SK.knownHosts);

  if (knownHostsJson == null) {
    List<KnownHost> defaultHosts = getDefaultHosts();

    knownHostsJson = jsonEncode(defaultHosts);
    storage.write(
        key: SK.knownHosts, value: knownHostsJson, iOptions: secureIOSOptions);
  }

  Iterable l = jsonDecode(knownHostsJson);
  List<KnownHost> knownHostList = l.map((i) => KnownHost.fromJson(i)).toList();

  return knownHostList;
}

/// Updates teh stored known hosts
Future updateKnownHosts(List<KnownHost> knownHosts) async {
  String knownHostsString = jsonEncode(knownHosts);
  await storage.write(
      key: SK.knownHosts, value: knownHostsString, iOptions: secureIOSOptions);
}

/// Approves a host by updating or adding it in the known hosts
Future approveHost(String serverUrl, Uint8List? verifyKey) async {
  var knownHosts = await getKnownHosts();

  serverUrl = serverUrl.toLowerCase();

  for (var i = 0; i < knownHosts.length; i++) {
    if (knownHosts[i].url != serverUrl) {
      continue;
    }
    if (converter.toHex(knownHosts[i].verifyKey) !=
        converter.toHex(verifyKey)) {
      knownHosts[i].verifyKey = verifyKey;
    }

    await updateKnownHosts(knownHosts);
    return;
  }

  knownHosts.add(KnownHost(url: serverUrl, verifyKey: verifyKey));

  await updateKnownHosts(knownHosts);
}

/// Compares a given url and fingerprint against the existing known hosts.
Future<CheckKnownHostResult> checkKnownHost(
    String serverUrl, Uint8List? verifyKey) async {
  var knownHosts = await getKnownHosts();

  serverUrl = serverUrl.toLowerCase();

  for (var i = 0; i < knownHosts.length; i++) {
    if (knownHosts[i].url != serverUrl) {
      continue;
    }
    if (converter.toHex(knownHosts[i].verifyKey) !=
        converter.toHex(verifyKey)) {
      return CheckKnownHostResult(
          status: 'signature_changed', verifyKeyOld: knownHosts[i].verifyKey);
    }

    return CheckKnownHostResult(status: 'matched');
  }

  return CheckKnownHostResult(status: 'not_found');
}

int semverCompare(String a, String b) {
  // Remove everything after whitespace
  a = a.replaceAll(RegExp(r'\s.*'), '');
  b = b.replaceAll(RegExp(r'\s.*'), '');

  // Remove everything after + sign
  a = a.replaceAll(RegExp(r'\+.*'), '');
  b = b.replaceAll(RegExp(r'\+.*'), '');

  // Handles cases like "1.2.3" > "1.2.3-asdf"
  if (a.startsWith('$b-')) return -1;
  if (b.startsWith('$a-')) return 1;

  // Normalize the versions by padding shorter version strings with ".0"
  a = _normalizeVersion(a, b);
  b = _normalizeVersion(b, a);

  // Compare the versions with numeric sensitivity
  return _localeCompare(a, b);
}

/// Helper function to normalize version strings by padding with ".0"
String _normalizeVersion(String version1, String version2) {
  final parts = version1.split(RegExp(r'[.-]'));
  final partsReference = version2.split(RegExp(r'[.-]'));

  while (parts.length < partsReference.length) {
    parts.add('0'); // Pad missing parts with '0'
  }

  return parts.join('.');
}

/// Helper function for locale comparison with numeric handling
int _localeCompare(String a, String b) {
  final aParts = a.split(RegExp(r'[.-]'));
  final bParts = b.split(RegExp(r'[.-]'));

  for (var i = 0; i < aParts.length && i < bParts.length; i++) {
    final aPart = aParts[i];
    final bPart = bParts[i];

    final aIsNumeric = int.tryParse(aPart);
    final bIsNumeric = int.tryParse(bPart);

    if (aIsNumeric != null && bIsNumeric != null) {
      if (aIsNumeric != bIsNumeric) return aIsNumeric.compareTo(bIsNumeric);
    } else {
      final result = aPart.compareTo(bPart);
      if (result != 0) return result;
    }
  }

  return aParts.length.compareTo(bParts.length);
}

/// Takes the server url and the server info object and compares the public key
/// against the known ones in the storage.
Future<CheckHostResult> checkHost(
    String serverUrl, api_client.Info info) async {
  serverUrl = serverUrl.toLowerCase();

  var valid =
      await validateSignature(info.info!, info.signature!, info.verifyKey!);

  if (!valid) {
    return CheckHostResult(
      serverUrl: serverUrl,
      status: 'invalid_signature',
      verifyKey: null,
      info: info,
    );
  }

  const minVersion = {
    "CE": '4.0.0',
    "EE": '4.0.0',
  };

  if (info.type != null &&
      minVersion.containsKey(info.type) &&
      info.version != null &&
      semverCompare(minVersion[info.type]!, info.version!) > 0) {
    return CheckHostResult(
      serverUrl: serverUrl,
      status: 'unsupported_server_version',
      verifyKey: info.verifyKey,
      info: info,
    );
  }

  var checkResult = await checkKnownHost(serverUrl, info.verifyKey);

  if (checkResult.status == 'matched') {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'matched',
        verifyKey: info.verifyKey,
        info: info);
  } else if (checkResult.status == 'signature_changed') {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'signature_changed',
        verifyKey: checkResult.verifyKeyOld,
        info: info);
  } else {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'new_server',
        verifyKey: info.verifyKey,
        info: info);
  }
}

/// Loads a remote config. It takes a URL of a remote web client and loads its config.
/// Loads a remote config. It takes a URL of a remote web client and loads its config.
Future<String> loadRemoteConfig(String serverUrl) async {
  api_client.Info packageInfo = await api_client.info();

  final Dio dio = Dio(
    BaseOptions(
      connectTimeout: Duration(seconds: 5),
      receiveTimeout: Duration(seconds: 5),
    ),
  );

  try {
    Response response = await dio.get("${packageInfo.webClient!}/config.json",
      options: Options(
        responseType: ResponseType.plain,
      ),
    );

    return response.data as String;
  } on DioException catch (e) {
    // Handle error, you can log or rethrow it
    if (e.type == DioExceptionType.connectionTimeout ||
        e.type == DioExceptionType.receiveTimeout) {
      // Handle timeout
      print('Request timed out');
    } else {
      print('Request failed: $e');
    }
    rethrow;
  }
}
