import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;

readAvatar() async {
  return await api_client.readAvatar(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
  );
}

createAvatar(String base64) async {
  try {
    return await api_client.createAvatar(
      reduxStore.state.token,
      reduxStore.state.sessionSecretKey,
      base64,
    );
  } catch (e) {
    return null;
  }
}

deleteAvatar(String id) async {
  try {
    return await api_client.deleteAvatar(
      reduxStore.state.token,
      reduxStore.state.sessionSecretKey,
      id,
    );
  } catch (e) {
    return null;
  }
}
