import 'package:json_annotation/json_annotation.dart';

part 'create_secret.g.dart';

@JsonSerializable()
class CreateSecret {
  CreateSecret({
    this.secretId,
  });

  @JsonKey(name: 'secret_id')
  final String? secretId;

  factory CreateSecret.fromJson(Map<String, dynamic> json) =>
      _$CreateSecretFromJson(json);

  Map<String, dynamic> toJson() => _$CreateSecretToJson(this);
}
