// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activate_token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as String?,
      email: json['email'] as String?,
      authentication: json['authentication'] as String?,
      serverSecretExists: json['server_secret_exists'] as bool?,
      secretKey: fromHex(json['secret_key'] as String?),
      secretKeyNonce: fromHex(json['secret_key_nonce'] as String?),
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'authentication': instance.authentication,
      'server_secret_exists': instance.serverSecretExists,
      'secret_key': toHex(instance.secretKey),
      'secret_key_nonce': toHex(instance.secretKeyNonce),
    };

ActivateToken _$ActivateTokenFromJson(Map<String, dynamic> json) =>
    ActivateToken(
      user: json['user'] == null
          ? null
          : User.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ActivateTokenToJson(ActivateToken instance) =>
    <String, dynamic>{
      'user': instance.user,
    };
