import 'package:json_annotation/json_annotation.dart';

part 'read_metadata_secret.g.dart';

@JsonSerializable()
class ReadMetadataSecret {
  ReadMetadataSecret({
    this.writeDate,
    this.id,
  });

  @JsonKey(name: 'write_date')
  final String? writeDate;
  final String? id;

  factory ReadMetadataSecret.fromJson(Map<String, dynamic> json) =>
      _$ReadMetadataSecretFromJson(json);

  Map<String, dynamic> toJson() => _$ReadMetadataSecretToJson(this);
}
