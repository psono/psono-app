// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_share_rights_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadShareRightsList _$ReadShareRightsListFromJson(Map<String, dynamic> json) =>
    ReadShareRightsList(
      shareRights: (json['share_rights'] as List<dynamic>?)
          ?.map((e) =>
              ReadShareRightsListEntry.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ReadShareRightsListToJson(
        ReadShareRightsList instance) =>
    <String, dynamic>{
      'share_rights': instance.shareRights,
    };

ReadShareRightsListEntry _$ReadShareRightsListEntryFromJson(
        Map<String, dynamic> json) =>
    ReadShareRightsListEntry(
      shareId: json['share_id'] as String?,
      read: json['read'] as bool?,
      write: json['write'] as bool?,
      grant: json['grant'] as bool?,
    );

Map<String, dynamic> _$ReadShareRightsListEntryToJson(
        ReadShareRightsListEntry instance) =>
    <String, dynamic>{
      'share_id': instance.shareId,
      'read': instance.read,
      'write': instance.write,
      'grant': instance.grant,
    };
