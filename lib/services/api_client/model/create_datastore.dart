import 'package:json_annotation/json_annotation.dart';

part 'create_datastore.g.dart';

@JsonSerializable()
class CreateDatastore {
  CreateDatastore({
    this.datastoreId,
  });

  @JsonKey(name: 'datastore_id')
  final String? datastoreId;

  factory CreateDatastore.fromJson(Map<String, dynamic> json) =>
      _$CreateDatastoreFromJson(json);

  Map<String, dynamic> toJson() => _$CreateDatastoreToJson(this);
}
