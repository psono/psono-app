import 'package:json_annotation/json_annotation.dart';

part 'read_share_rights_list.g.dart';

@JsonSerializable()
class ReadShareRightsList {
  ReadShareRightsList({
    this.shareRights,
  });

  @JsonKey(name: 'share_rights')
  List<ReadShareRightsListEntry>? shareRights;

  factory ReadShareRightsList.fromJson(Map<String, dynamic> json) =>
      _$ReadShareRightsListFromJson(json);

  Map<String, dynamic> toJson() => _$ReadShareRightsListToJson(this);
}

@JsonSerializable()
class ReadShareRightsListEntry {
  ReadShareRightsListEntry({
    this.shareId,
    this.read,
    this.write,
    this.grant,
  });

  @JsonKey(name: 'share_id')
  final String? shareId;
  final bool? read;
  final bool? write;
  final bool? grant;

  factory ReadShareRightsListEntry.fromJson(Map<String, dynamic> json) =>
      _$ReadShareRightsListEntryFromJson(json);

  Map<String, dynamic> toJson() => _$ReadShareRightsListEntryToJson(this);
}
