import 'package:json_annotation/json_annotation.dart';

part 'ivalt_verify.g.dart';

@JsonSerializable()
class IvaltVerify {
  IvaltVerify();

  factory IvaltVerify.fromJson(Map<String, dynamic> json) =>
      _$IvaltVerifyFromJson(json);

  Map<String, dynamic> toJson() => _$IvaltVerifyToJson(this);
}
