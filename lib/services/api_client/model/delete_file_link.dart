import 'package:json_annotation/json_annotation.dart';

part 'delete_file_link.g.dart';

@JsonSerializable()
class DeleteFileLink {
  DeleteFileLink();

  factory DeleteFileLink.fromJson(Map<String, dynamic> json) =>
      _$DeleteFileLinkFromJson(json);

  Map<String, dynamic> toJson() => _$DeleteFileLinkToJson(this);
}
