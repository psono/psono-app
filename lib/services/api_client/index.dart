import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/device.dart' as device;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/offline_cache.dart' as offline_cache;
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_constants.dart';

import 'exceptions.dart';
import 'model/_index.dart';

export 'exceptions.dart';
export 'model/_index.dart';

Future<Response> decryptData(
  Uint8List? sessionSecretKey,
  Response response,
) async {
  if (sessionSecretKey == null) {
    return response;
  }

  try {
    final Map bodyJson = response.data;
    final String decryptedBody = await cryptoLibrary.decryptData(
      converter.fromHex(bodyJson['text'])!,
      converter.fromHex(bodyJson['nonce'])!,
      sessionSecretKey,
    );
    return Response(
      data: jsonDecode(decryptedBody),
      statusCode: response.statusCode,
      headers: response.headers,
      requestOptions: response.requestOptions,
    );
  } catch (e) {
    return response;
  }
}

/// Small wrapper
Future<Response<dynamic>> call(
  String connectionType,
  String endpoint,
  Map? data,
  Map<String, String>? headers,
  int expectedStatus, [
  Uint8List? sessionSecretKey,
  bool useCache = true,
]) async {
  final Dio dio = Dio(
    BaseOptions(
      connectTimeout: Duration(seconds: useCache ? 5 : 20),
      receiveTimeout: Duration(seconds: useCache ? 5 : 20),
    ),
  );

  final options = Options(
      method: connectionType,
      headers: headers ?? {},
      responseType: ResponseType.json,
      followRedirects: false,
      validateStatus: (status) {
        return true;
      });

  if (data != null && sessionSecretKey != null && sessionSecretKey.isNotEmpty) {
    final EncryptedData encryptedData = await cryptoLibrary.encryptData(
      jsonEncode(data),
      sessionSecretKey,
    );
    data = {
      'text': converter.toHex(encryptedData.text),
      'nonce': converter.toHex(encryptedData.nonce),
    };
  }

  if (sessionSecretKey != null &&
      sessionSecretKey.isNotEmpty &&
      headers != null &&
      headers.containsKey('Authorization')) {
    final String requestTime = helper.dateTimeToIso(DateTime.now());
    final String? requestDeviceFingerprint =
        await device.getDeviceFingerprint();
    final Map validator = {
      'request_time': requestTime,
      'request_device_fingerprint': requestDeviceFingerprint,
    };
    final EncryptedData encryptedValidator = await cryptoLibrary.encryptData(
      jsonEncode(validator),
      sessionSecretKey,
    );
    headers['Authorization-Validator'] = jsonEncode({
      'text': converter.toHex(encryptedValidator.text),
      'nonce': converter.toHex(encryptedValidator.nonce),
    });
  }

  if (headers != null) {
    options.headers!.addAll(headers);
  }

  Response<dynamic> response;
  try {
    response = await dio.request(
      reduxStore.state.serverUrl + endpoint,
      data: data != null ? jsonEncode(data) : null,
      options: options,
    );
  } on DioException catch (e) {
    // e.g. DNS resolution issue or server offline
    Response<dynamic>? cachedResp = await offline_cache.get(
      connectionType,
      endpoint,
    );
    if (cachedResp != null) {
      return cachedResp;
    } else {
      rethrow;
    }
  }

  Response<dynamic> resp = await decryptData(
    sessionSecretKey,
    response,
  );

  if (response.statusCode == expectedStatus ||
      response.headers.value('content-type') == 'application/json') {
    // we got a successful live request from the server, so lets update the
    // lastServerConnectionTimeSinceEpoch
    int nowInMillisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
    if (reduxStore.state.lastServerConnectionTimeSinceEpoch <
        nowInMillisecondsSinceEpoch - 60000) {
      reduxStore.dispatch(
        SetLastServerConnectionTimeSinceEpochReducerAction(
          nowInMillisecondsSinceEpoch,
        ),
      );
      await storage.write(
        key: SK.lastServerConnectionTimeSinceEpoch,
        value: nowInMillisecondsSinceEpoch.toString(),
        iOptions: secureIOSOptions,
      );
    }
  }

  if (useCache && response.statusCode != 401) {
    if (response.statusCode != expectedStatus ||
        response.headers.value('content-type') != 'application/json') {
      Response<dynamic>? cachedResp = await offline_cache.get(
        connectionType,
        endpoint,
      );
      if (cachedResp != null) {
        return cachedResp;
      }
      handleError(resp);
    }

    // we only cache the response if we have the expected Status and content type
    offline_cache.set(
      connectionType,
      endpoint,
      resp,
    );
  } else {
    if (response.statusCode != expectedStatus) {
      handleError(resp);
    }
  }

  return resp;
}

void throwHttpException(int statusCode, String message) {
  if (statusCode == 301 && message == '') {
    throw BadRequestException('ERROR_RECEIVED_301');
  }
  if (statusCode == 400) {
    throw BadRequestException(message);
  }
  if (statusCode == 401) {
    throw UnauthorizedException(message);
  }
  if (statusCode == 403) {
    throw ForbiddenException(message);
  }
  if (statusCode == 500) {
    throw InternalServerErrorException(message);
  }
  if (statusCode == 503) {
    throw ServiceUnavailableException(message);
  }

  throw UnknownException(message, statusCode);
}

void handleError(Response response) {
  if (response.statusCode! >= 500) {
    throwHttpException(response.statusCode!, response.toString());
  }

  if (response.data?.containsKey('non_field_errors')) {
    throwHttpException(
        response.statusCode!, response.data['non_field_errors'][0]);
  } else {
    throwHttpException(response.statusCode!, response.toString());
  }
}

/// GET request to get the server info
/// Returns a future with server's public information
Future<Info> info() async {
  const endpoint = '/info/';
  const connectionType = 'GET';
  const Map? data = null;
  const Map<String, String>? headers = null;

  final response = await call(connectionType, endpoint, data, headers, 200, null, false,
    // useCache
  );

  return Info.fromJson(
    response.data,
  );
}

/// POST request to the backend with email and authkey for login
/// Returns a future with the login status
Future<Login> login(
  Uint8List? loginInfo,
  Uint8List? loginInfoNonce,
  Uint8List publicKey,
  int sessionDuration,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/login/';
  final Map data = {
    'login_info': converter.toHex(loginInfo),
    'login_info_nonce': converter.toHex(loginInfoNonce),
    'public_key': converter.toHex(publicKey),
    'session_duration': sessionDuration,
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    200,
    null,
    false, // useCache
  );

  return Login.fromJson(
    response.data,
  );
}

/// POST request to initiate the saml login
Future<SamlInitiateLogin> samlInitiateLogin(
  int? samlProviderId,
  String returnToUrl,
) async {
  const connectionType = 'POST';
  final endpoint = '/saml/$samlProviderId/initiate-login/';
  final Map data = {
    'return_to_url': returnToUrl,
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return SamlInitiateLogin.fromJson(
    response.data,
  );
}

/// POST request to initiate the saml login
Future<SamlLogin> samlLogin(
  Uint8List? loginInfo,
  Uint8List? loginInfoNonce,
  Uint8List publicKey,
  int sessionDuration,
) async {
  const connectionType = 'POST';
  const endpoint = '/saml/login/';
  final Map data = {
    'login_info': converter.toHex(loginInfo),
    'login_info_nonce': converter.toHex(loginInfoNonce),
    'public_key': converter.toHex(publicKey),
    'session_duration': sessionDuration,
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return SamlLogin.fromJson(
    response.data,
  );
}

/// POST request to initiate the OIDC login
Future<OidcInitiateLogin> oidcInitiateLogin(
  int? oidcProviderId,
  String returnToUrl,
) async {
  const connectionType = 'POST';
  final endpoint = '/oidc/$oidcProviderId/initiate-login/';
  final Map data = {
    'return_to_url': returnToUrl,
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return OidcInitiateLogin.fromJson(
    response.data,
  );
}

/// POST request to initiate the OIDC login
Future<OidcLogin> oidcLogin(
  Uint8List? loginInfo,
  Uint8List? loginInfoNonce,
  Uint8List publicKey,
  int sessionDuration,
) async {
  const connectionType = 'POST';
  const endpoint = '/oidc/login/';
  final Map data = {
    'login_info': converter.toHex(loginInfo),
    'login_info_nonce': converter.toHex(loginInfoNonce),
    'public_key': converter.toHex(publicKey),
    'session_duration': sessionDuration,
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return OidcLogin.fromJson(
    response.data,
  );
}

/// POST request to destroy the token and logout the user
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the login status
Future<Logout> logout(
  String token,
  Uint8List? sessionSecretKey,
  String? sessionId,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/logout/';
  final Map data = {
    'session_id': sessionId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return Logout.fromJson(
    response.data,
  );
}

/// AJAX POST request to the backend with the recovery_authkey to initiate the
/// reset of the password
Future<EnableRecoverycode> enableRecoverycode(
  String username,
  String recoveryAuthkey,
) async {
  const connectionType = 'POST';
  const endpoint = '/password/';
  final Map data = {
    'username': username,
    'recovery_authkey': recoveryAuthkey,
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return EnableRecoverycode.fromJson(
    response.data,
  );
}

/// AJAX POST request to the backend with the recovery_authkey to initiate the
/// reset of the password
Future<void> setPassword(
  String username,
  String recoveryAuthkey,
  Uint8List? updateData,
  Uint8List? updateDataNonce,
) async {
  const connectionType = 'PUT';
  const endpoint = '/password/';
  final Map data = {
    'username': username,
    'recovery_authkey': recoveryAuthkey,
    'update_data': converter.toHex(updateData),
    'update_data_nonce': converter.toHex(updateDataNonce),
  };
  const Map<String, String>? headers = null;

  await call(
    connectionType, endpoint, data, headers, 200,
    null,
    false, // useCache
  );

  return;
}

/// POST request to register a user
/// Returns a future with the registration status
Future<Logout> register(
  String email,
  String username,
  String authkey,
  Uint8List publicKey,
  Uint8List? privateKey,
  Uint8List? privateKeyNonce,
  Uint8List? secretKey,
  Uint8List? secretKeyNonce,
  String userSauce,
  String baseUrl,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/register/';
  final Map data = {
    'email': email,
    'username': username,
    'authkey': authkey,
    'public_key': converter.toHex(publicKey),
    'private_key': converter.toHex(privateKey),
    'private_key_nonce': converter.toHex(privateKeyNonce),
    'secret_key': converter.toHex(secretKey),
    'secret_key_nonce': converter.toHex(secretKeyNonce),
    'user_sauce': userSauce,
    'base_url': baseUrl
  };
  const Map<String, String>? headers = null;

  final response = await call(
    connectionType, endpoint, data, headers, 201,
    null,
    false, // useCache
  );

  return Logout.fromJson(
    response.data,
  );
}

/// POST request to activate the token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the rest of the login info
Future<ActivateToken> activateToken(
  String token,
  Uint8List? verification,
  Uint8List? verificationNonce,
  Uint8List? sessionSecretKey,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/activate-token/';
  final Map data = {
    'verification': converter.toHex(verification),
    'verification_nonce': converter.toHex(verificationNonce),
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    200,
    sessionSecretKey,
    false, // useCache
  );

  return ActivateToken.fromJson(
    response.data,
  );
}

/// GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
Future<ReadDatastore> readDatastore(
  String token,
  Uint8List? sessionSecretKey,
  String datastoreId,
) async {
  const connectionType = 'GET';
  final endpoint = '/datastore/$datastoreId/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    200,
    sessionSecretKey,
    true, // useCache
  );

  return ReadDatastore.fromJson(
    response.data,
  );
}

/// GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
Future<ReadSecret> readSecret(
  String token,
  Uint8List? sessionSecretKey,
  String secretId,
) async {
  const connectionType = 'GET';
  final endpoint = '/secret/$secretId/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    200,
    sessionSecretKey,
    true, // useCache
  );

  return ReadSecret.fromJson(
    response.data,
  );
}

/// PUT request to create a secret with the token as authentication together with the encrypted data and nonce
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the secret id
Future<CreateSecret> createSecret(
  String token,
  Uint8List? sessionSecretKey,
  Uint8List? encryptedData,
  Uint8List? encryptedDataNonce,
  String linkId,
  String? parentDatastoreId,
  String? parentShareId,
  String callbackUrl,
  String callbackUser,
  String callbackPass,
) async {
  const connectionType = 'PUT';
  const endpoint = '/secret/';
  final Map data = {
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
    'link_id': linkId,
    'callback_url': callbackUrl,
    'callback_user': callbackUser,
    'callback_pass': callbackPass,
  };

  if (parentDatastoreId != null) {
    data['parent_datastore_id'] = parentDatastoreId;
  }

  if (parentShareId != null) {
    data['parent_share_id'] = parentShareId;
  }

  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 201,
    sessionSecretKey,
    false, // useCache
  );

  return CreateSecret.fromJson(
    response.data,
  );
}

/// PUT request with the token as authentication and the new secret content
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the secret id
Future<WriteSecret> writeSecret(
  String token,
  Uint8List? sessionSecretKey,
  String? secretId,
  Uint8List? encryptedData,
  Uint8List? encryptedDataNonce,
  String? callbackUrl,
  String? callbackUser,
  String? callbackPass,
) async {
  const connectionType = 'POST';
  const endpoint = '/secret/';
  final Map data = {
    'secret_id': secretId,
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
    'callback_url': callbackUrl,
    'callback_user': callbackUser,
    'callback_pass': callbackPass,
  };

  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );
  return WriteSecret.fromJson(
    response.data,
  );
}

/// GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
Future<ReadDatastoreList> readDatastoreList(
  String token,
  Uint8List? sessionSecretKey,
) async {
  const connectionType = 'GET';
  const endpoint = '/datastore/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    true, // useCache
  );
  return ReadDatastoreList.fromJson(
    response.data,
  );
}

/// GET request with the [token] as authentication to get all the users share rights.
/// The request is encrypted with the [sessionSecretKey]
Future<ReadShareRightsList> readShareRightsOverview(
  String token,
  Uint8List? sessionSecretKey,
) async {
  const connectionType = 'GET';
  const endpoint = '/share/right/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    true, // useCache
  );

  return ReadShareRightsList.fromJson(
    response.data,
  );
}

/// DELETE request with the token as authentication to delete a user account
Future<DeleteAccount> deleteAccount(
  String token,
  Uint8List? sessionSecretKey,
  String authkey,
) async {
  const connectionType = 'DELETE';
  const endpoint = '/user/delete/';
  final Map data = {
    'authkey': authkey,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType,
    endpoint,
    data,
    headers,
    200,
    sessionSecretKey,
    false, // useCache
  );
  return DeleteAccount();
}

/// GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the encrypted datastore
Future<CreateDatastore> createDatastore(
  String token,
  Uint8List? sessionSecretKey,
  String type,
  String description,
  Uint8List encryptedData,
  Uint8List encryptedDataNonce,
  bool isDefault,
  Uint8List? secretKey,
  Uint8List? secretKeyNonce,
) async {
  const connectionType = 'PUT';
  const endpoint = '/datastore/';
  final Map data = {
    'type': type,
    'description': description,
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
    'is_default': isDefault,
    'secret_key': converter.toHex(secretKey),
    'secret_key_nonce': converter.toHex(secretKeyNonce),
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 201,
    sessionSecretKey,
    false, // useCache
  );

  return CreateDatastore.fromJson(
    response.data,
  );
}

/// POST request with the token as authentication and the datastore's new content
Future<void> writeDatastore(
  String token,
  Uint8List? sessionSecretKey,
  String? datastoreId,
  Uint8List? encryptedData,
  Uint8List? encryptedDataNonce,
  Uint8List? encryptedDataSecretKey,
  Uint8List? encryptedDataSecretKeyNonce,
  String? description,
  bool? isDefault,
) async {
  const connectionType = 'POST';
  const endpoint = '/datastore/';
  final Map data = {
    'datastore_id': datastoreId,
  };

  if (encryptedData != null) {
    data['data'] = converter.toHex(encryptedData);
  }

  if (encryptedDataNonce != null) {
    data['data_nonce'] = converter.toHex(encryptedDataNonce);
  }

  if (encryptedDataSecretKey != null) {
    data['secret_key'] = converter.toHex(encryptedDataSecretKey);
  }

  if (encryptedDataSecretKeyNonce != null) {
    data['secret_key_nonce'] = converter.toHex(encryptedDataSecretKeyNonce);
  }

  if (isDefault != null) {
    data['is_default'] = isDefault;
  }

  if (description != null) {
    data['description'] = description;
  }

  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );
  return;
}

/// POST request to the backend with the YubiKey OTP Token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future the verification status
Future<YubikeyOtpVerify> yubikeyOtpVerify(
  String token,
  Uint8List? sessionSecretKey,
  String yubikeyOtp,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/yubikey-otp-verify/';
  final Map data = {
    'yubikey_otp': yubikeyOtp,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return YubikeyOtpVerify();
}

/// POST request to the backend with the OATH-TOTP Token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future the verification status
Future<GaVerify> gaVerify(
  String token,
  Uint8List? sessionSecretKey,
  String gaToken,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/ga-verify/';
  final Map data = {
    'ga_token': gaToken,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return GaVerify();
}

Future<IvaltVerify> ivaltVerify(
  String token,
  Uint8List? sessionSecretKey,
  String requestType,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/ivalt-verify/';
  final Map data = {
    "request_type": requestType,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return IvaltVerify();
}

/// POST request to the backend with the Duo Token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the verification status
Future<DuoVerify> duoVerify(
  String token,
  Uint8List? sessionSecretKey,
  String duoToken,
) async {
  const connectionType = 'POST';
  const endpoint = '/authentication/duo-verify/';
  Map? data;
  if (duoToken != '') {
    data = {
      'duo_token': duoToken,
    };
  }
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return DuoVerify();
}

/// GET request with the token as authentication to get the content for a single share
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the encrypted share content
Future<ReadShare> readShare(
  String token,
  Uint8List? sessionSecretKey,
  String shareId,
) async {
  const connectionType = 'GET';
  final endpoint = '/share/$shareId/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    true, // useCache
  );

  return ReadShare.fromJson(
    response.data,
  );
}

/// PUT request with the token as authentication and the share's new content
Future<void> writeShare(
  String token,
  Uint8List? sessionSecretKey,
  String? shareId,
  Uint8List? encryptedData,
  Uint8List? encryptedDataNonce,
) async {
  const connectionType = 'PUT';
  const endpoint = '/share/';
  final Map data = {
    'share_id': shareId,
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
  };

  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return;
}

/// DELETE request with the token as authentication to delete a share link
Future<DeleteShareLink> deleteShareLink(
  String token,
  Uint8List? sessionSecretKey,
  String? linkId,
) async {
  const connectionType = 'DELETE';
  const endpoint = '/share/link/';
  final Map data = {
    'link_id': linkId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return DeleteShareLink();
}

/// DELETE request with the token as authentication to delete a secret link
Future<DeleteSecretLink> deleteSecretLink(
  String token,
  Uint8List? sessionSecretKey,
  String? linkId,
) async {
  const connectionType = 'DELETE';
  const endpoint = '/secret/link/';
  final Map data = {
    'link_id': linkId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return DeleteSecretLink();
}

/// DELETE request with the token as authentication to delete a secret link
Future<DeleteFileLink> deleteFileLink(
  String token,
  Uint8List? sessionSecretKey,
  String? linkId,
) async {
  const connectionType = 'DELETE';
  const endpoint = '/file/link/';
  final Map data = {
    'link_id': linkId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return DeleteFileLink();
}

/// AJAX PUT request to the backend with new user informations like for example a new password (means new
/// authkey) or new public key
Future<void> updateUser(
  String token,
  Uint8List? sessionSecretKey,
  String? email,
  String? authkey,
  String authkeyOld,
  Uint8List? privateKey,
  Uint8List? privateKeyNonce,
  Uint8List? secretKey,
  Uint8List? secretKeyNonce,
) async {
  const connectionType = 'PUT';
  const endpoint = '/user/update/';
  final Map data = {};

  if (email != null) {
    data['email'] = email;
  }

  if (authkey != null) {
    data['authkey'] = authkey;
  }

  data['authkey_old'] = authkeyOld;

  if (privateKey != null) {
    data['private_key'] = converter.toHex(privateKey);
  }

  if (privateKeyNonce != null) {
    data['private_key_nonce'] = converter.toHex(privateKeyNonce);
  }

  if (secretKey != null) {
    data['secret_key'] = converter.toHex(secretKey);
  }

  if (secretKeyNonce != null) {
    data['secret_key_nonce'] = converter.toHex(secretKeyNonce);
  }

  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return;
}

/// GET request with the token as authentication [sessionSecretKey] to get the metadata of
/// a specific datastore [datastoreId]
Future<ReadMetadataDatastore> readMetadataDatastore(
  String token,
  Uint8List? sessionSecretKey,
  String datastoreId,
) async {
  const connectionType = 'GET';
  final endpoint = '/metadata-datastore/$datastoreId/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return ReadMetadataDatastore.fromJson(
    response.data,
  );
}

/// GET request with the token as authentication [sessionSecretKey] to get the metadata of
/// a specific datastore [shareId]
Future<ReadMetadataShare> readMetadataShare(
  String token,
  Uint8List? sessionSecretKey,
  String shareId,
) async {
  const connectionType = 'GET';
  final endpoint = '/metadata-share/$shareId/';
  const Map? data = null;
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );

  return ReadMetadataShare.fromJson(
    response.data,
  );
}

Future<String> readAvatar(String token, Uint8List sessionSecretKey) async {
  const connectionType = 'GET';
  const endpoint = '/avatar/';
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };
  final response = await call(
    connectionType, endpoint, null, headers, 200,
    sessionSecretKey,
    false, // useCache
  );
  if (response.data['avatars'].isEmpty) {
    return '';
  }
  return response.data['avatars'][0]['id'];
}

createAvatar(
    String token, Uint8List sessionSecretKey, String base64Image) async {
  const connectionType = 'POST';
  const endpoint = '/avatar/';
  final Map<String, String> data = {
    'data_base64': base64Image,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };
  final response = await call(
    connectionType, endpoint, data, headers, 201,
    sessionSecretKey,
    false, // useCache
  );
  return "Avatar created ${response.data}";
}

deleteAvatar(String token, Uint8List sessionSecretKey, String id) async {
  const connectionType = 'DELETE';
  const endpoint = '/avatar/';
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };
  final Map<String, String> data = {
    'avatar_id': id,
  };
  final response = await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );
  return "Avatar deleted ${response.data}";
}

/// POST request to create the user's server secret with the token as authentication
Future<void> createServerSecret(
  String token,
  Uint8List? sessionSecretKey,
  String secretKey,
  String privateKey,
) async {
  const connectionType = 'POST';
  const endpoint = '/server-secret/';
  final Map data = {
    'secret_key': secretKey,
    'private_key': privateKey,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 201,
    sessionSecretKey,
    false, // useCache
  );
}

/// DELETE request to delete the user's server secret with the token as authentication
Future<void> deleteServerSecret(
  String token,
  Uint8List? sessionSecretKey,
  String authkey,
  String privateKey,
  String privateKeyNonce,
  String secretKey,
  String secretKeyNonce,
  String userSauce,
) async {
  const connectionType = 'DELETE';
  const endpoint = '/server-secret/';
  final Map data = {
    'authkey': authkey,
    'private_key': privateKey,
    'private_key_nonce': privateKeyNonce,
    'secret_key': secretKey,
    'secret_key_nonce': secretKeyNonce,
    'user_sauce': userSauce,
  };
  final Map<String, String> headers = {
    "Authorization": "Token $token",
  };

  await call(
    connectionType, endpoint, data, headers, 200,
    sessionSecretKey,
    false, // useCache
  );
}

Future<String> createShareLink(
  String token,
  Uint8List? sessionSecretKey,
  String? secretId,
  String? fileId,
  Uint8List? node,
  Uint8List? nodeNonce,
  String publicTitle,
  int? allowedReads,
  String passphrase,
  String validTill,
) async {
  const endpoint = "/link-share/";
  const method = "PUT";

  final data = {
    "node": converter.toHex(node),
    "node_nonce": converter.toHex(nodeNonce),
    "public_title": publicTitle,
    "allowed_reads": allowedReads,
    "valid_till": validTill,
  };
  if (fileId != null) {
    data["file_id"] = fileId;
  } else {
    data["secret_id"] = secretId;
  }
  if (passphrase.isNotEmpty) {
    data["passphrase"] = passphrase;
  }
  final headers = {
    "Authorization": "Token $token",
  };
  final response =
      await call(method, endpoint, data, headers, 201, sessionSecretKey, false);
  return response.data["link_share_id"];
}
