import 'dart:convert';
import 'dart:developer';
import 'package:psono/services/converter.dart' as converter;

import 'package:psono/model/datastore.dart' as datastore_model;
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;

/// Delete a share link
///
/// Returns a promise with the status of the delete operation
Future<api_client.DeleteShareLink> deleteShareLink(String? linkId) async {
  return await api_client.deleteShareLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    linkId,
  );
}

/// triggered once a share is deleted.
///
/// Returns noting
Future<void> onShareDeleted(String? linkId) async {
  await deleteShareLink(linkId);
}

Future createShareLink(datastore_model.Item item, String publicTitle,
    String allowedReads, DateTime validTill, String passphrase) async {
  final linkShareSecret = await cryptoLibrary.generateSecretKey();
  final Map<String, dynamic> content = {
    "secret_id": item.secretId,
    "secret_key": converter.toHex(item.secretKey),
    "type": item.type,
  };
  if (item.fileChunks != null) {
    content["file_chunks"] = item.fileChunks;
  }
  if (item.fileId != null) {
    content["file_id"] = item.fileId;
  }
  if (item.fileSecretKey != null) {
    content["file_secret_key"] = item.fileSecretKey;
  }
  if (item.fileShardId != null) {
    content["file_shard_id"] = item.fileShardId;
  }
  if (item.fileTitle != null) {
    content["file_title"] = item.fileTitle;
  }
  final itemEncrypted =
      await cryptoLibrary.encryptData(jsonEncode(content), linkShareSecret);
  final validTillStr = validTill.toIso8601String();
  final allowedReadsValidated = int.tryParse(allowedReads);
  String? fileId;
  String? secretId;
  if (item.fileId != null) {
    fileId = item.fileId;
  } else {
    secretId = item.secretId;
  }
  final response = await api_client.createShareLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    secretId,
    fileId,
    itemEncrypted.text,
    itemEncrypted.nonce,
    publicTitle,
    allowedReadsValidated,
    passphrase,
    validTillStr,
  );
  final id = response;
  var serverUrl = reduxStore.state.serverUrl;
  //remove /server from the end
  if (serverUrl.endsWith("/server")) {
    serverUrl = serverUrl.substring(0, serverUrl.length - 7);
  }
  final encodedServerUrl =
      converter.toBase58(converter.encodeUTF8(reduxStore.state.serverUrl));
  final verifyKey = converter.toHex(reduxStore.state.verifyKey);
  final url = serverUrl +
      "/link-share-access.html#!/link-share-access/" +
      id +
      "/" +
      converter.toHex(linkShareSecret) +
      "/" +
      encodedServerUrl +
      "/" +
      verifyKey;
  return url;
}
