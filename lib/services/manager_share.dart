import 'dart:convert';
import 'dart:typed_data';

import 'package:psono/model/datastore.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/model/share.dart';
import 'package:psono/model/share_right.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;

/// Returns all the share rights of the current user
Future<api_client.ReadShareRightsList> readShareRightsOverview() async {
  api_client.ReadShareRightsList tempShareRightsOverview =
      await api_client.readShareRightsOverview(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
  );

  return tempShareRightsOverview;
}

/// Returns a share object with decrypted data
Future<Share> readShare(shareId, secretKey) async {
  api_client.ReadShare share = await api_client.readShare(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    shareId,
  );

  var data =
      await cryptoLibrary.decryptData(share.data!, share.dataNonce!, secretKey);
  Map jsonDecodedData = jsonDecode(data);

  Folder? folder;
  Item? item;
  if (jsonDecodedData.containsKey('type')) {
    item = Item.fromJson(jsonDecodedData as Map<String, dynamic>);
  } else {
    folder = Folder.fromJson(jsonDecodedData as Map<String, dynamic>);
  }

  Share decodedShare = new Share(
    shareId: shareId,
    shareSecretKey: secretKey,
    item: item,
    folder: folder,
    rights: ShareRight(
      read: share.rights!.read,
      write: share.rights!.write,
      grant: share.rights!.grant,
    ),
  );

  return decodedShare;
}

/// updates a share
Future<void> writeShare(
    String? shareId, String jsonContent, Uint8List secretKey) async {
  EncryptedData encryptedData =
      await cryptoLibrary.encryptData(jsonContent, secretKey);

  return api_client.writeShare(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    shareId,
    encryptedData.text,
    encryptedData.nonce,
  );
}
