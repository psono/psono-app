import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;

/// Delete a file link
///
/// Returns a promise with the status of the delete operation
Future<api_client.DeleteFileLink> deleteFileLink(String? linkId) async {
  return await api_client.deleteFileLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    linkId,
  );
}

/// triggered once a file is deleted.
///
/// Returns noting
Future<void> onFileDeleted(String? linkId) async {
  await deleteFileLink(linkId);
}
