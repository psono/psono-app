import 'package:flutter/foundation.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/converter.dart';
import 'package:psono/services/crypto_library.dart' as crypto_library;

/// Reads the history of a secret from the server. Returns a list of history items
Future createServerSecret() async {
  var token = reduxStore.state.token;
  var sessionSecretKey = reduxStore.state.sessionSecretKey;
  var secretKey = reduxStore.state.secretKey;
  var privateKey = reduxStore.state.privateKey;

  await api_client.createServerSecret(
    token,
    sessionSecretKey,
    toHex(secretKey)!,
    toHex(privateKey)!,
  );
}

/// Deletes the stored server secrets of a user. Takes a password to encrypt
/// the secret key with. Returns a list of history items
Future deleteServerSecret(password) async {
  var token = reduxStore.state.token;
  var sessionSecretKey = reduxStore.state.sessionSecretKey;

  var username = reduxStore.state.username;
  Uint8List? secretKey = reduxStore.state.secretKey;
  Uint8List? privateKey = reduxStore.state.privateKey;
  var userSauce = reduxStore.state.userSauce;

  EncryptedData privateKeyEnc = await crypto_library.encryptSecret(
    toHex(privateKey)!,
    password,
    userSauce,
  );
  EncryptedData secretKeyEnc = await crypto_library.encryptSecret(
    toHex(secretKey)!,
    password,
    userSauce,
  );
  String authkey = await crypto_library.generateAuthkey(
    username,
    password,
  );

  await api_client.deleteServerSecret(
    token,
    sessionSecretKey,
    authkey,
    toHex(privateKeyEnc.text)!,
    toHex(privateKeyEnc.nonce)!,
    toHex(secretKeyEnc.text)!,
    toHex(secretKeyEnc.nonce)!,
    userSauce,
  );
}
