import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;

/// Delete a secret link
///
/// Returns a promise with the status of the delete operation
Future<api_client.DeleteSecretLink> deleteSecretLink(String? linkId) async {
  return await api_client.deleteSecretLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    linkId,
  );
}

/// triggered once a secret is deleted.
///
/// Returns noting
Future<void> onSecretDeleted(String? linkId) async {
  await deleteSecretLink(linkId);
}
