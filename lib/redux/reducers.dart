import 'dart:typed_data';

import 'package:psono/model/app_state.dart';
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';
import 'package:redux/redux.dart';

// serverUrlReducer
String? setServerUrlReducer(String? state, action) {
  return action.serverUrl;
}

final Reducer<String?> serverUrlReducer = combineReducers<String?>([
  TypedReducer<String?, InitiateLoginAction>(setServerUrlReducer),
  TypedReducer<String?, InitiateStateAction>(setServerUrlReducer),
]);

// verifyKeyOldReducer
Uint8List? setVerifyKeyOldReducer(Uint8List? state, action) {
  return action.verifyKeyOld;
}

final Reducer<Uint8List?> verifyKeyOldReducer = combineReducers<Uint8List?>([
  TypedReducer<Uint8List?, SetVerifyKeyAction>(setVerifyKeyOldReducer),
]);

// verifyKeyReducer
Uint8List? setVerifyKeyReducer(Uint8List? state, action) {
  return action.verifyKey;
}

final Reducer<Uint8List?> verifyKeyReducer = combineReducers<Uint8List?>([
  TypedReducer<Uint8List?, SetVerifyKeyAction>(setVerifyKeyReducer),
]);

// usernameReducer
String? setUsernameReducer(String? state, action) {
  return action.username;
}

final Reducer<String?> usernameReducer = combineReducers<String?>([
  TypedReducer<String?, InitiateLoginAction>(setUsernameReducer),
  TypedReducer<String?, InitiateStateAction>(setUsernameReducer),
  TypedReducer<String?, SetUserUsernameAction>(setUsernameReducer),
]);

//// passwordReducer
//String setPasswordReducer(String state, action) {
//  return action.password;
//}
//
//final Reducer<String> passwordReducer = combineReducers<String>([
//  TypedReducer<String, InitiateLoginAction>(setPasswordReducer),
//]);

// authenticationReducer
String? setAuthenticationReducer(String? state, action) {
  return action.authentication;
}

final Reducer<String?> authenticationReducer = combineReducers<String?>([
  TypedReducer<String?, SetSessionInfoAction>(setAuthenticationReducer),
  TypedReducer<String?, InitiateStateAction>(setAuthenticationReducer),
]);

// tokenReducer
String? setTokenReducer(String? state, action) {
  return action.token;
}

final Reducer<String?> tokenReducer = combineReducers<String?>([
  TypedReducer<String?, SetSessionInfoAction>(setTokenReducer),
  TypedReducer<String?, InitiateStateAction>(setTokenReducer),
]);

// sessionSecretKeyReducer
Uint8List? setSessionSecretKeyReducer(Uint8List? state, action) {
  return action.sessionSecretKey;
}

final Reducer<Uint8List?> sessionSecretKeyReducer =
    combineReducers<Uint8List?>([
  TypedReducer<Uint8List?, SetSessionInfoAction>(setSessionSecretKeyReducer),
  TypedReducer<Uint8List?, InitiateStateAction>(setSessionSecretKeyReducer),
]);

// lastServerConnectionTimeSinceEpochReducer
int? setLastServerConnectionTimeSinceEpochReducer(int? state, action) {
  return action.lastServerConnectionTimeSinceEpoch;
}

final Reducer<int?> lastServerConnectionTimeSinceEpochReducer =
    combineReducers<int?>([
  TypedReducer<int?, SetLastServerConnectionTimeSinceEpochReducerAction>(
      setLastServerConnectionTimeSinceEpochReducer),
  TypedReducer<int?, InitiateStateAction>(
      setLastServerConnectionTimeSinceEpochReducer),
]);

// lastCacheTimeSinceEpochReducer
int? setLastCacheTimeSinceEpochReducer(int? state, action) {
  return action.lastCacheTimeSinceEpoch;
}

final Reducer<int?> lastCacheTimeSinceEpochReducer = combineReducers<int?>([
  TypedReducer<int?, SetLastCacheTimeSinceEpochReducerAction>(
      setLastCacheTimeSinceEpochReducer),
  TypedReducer<int?, InitiateStateAction>(setLastCacheTimeSinceEpochReducer),
]);

// secretKeyReducer
Uint8List? setSecretKeyReducer(Uint8List? state, action) {
  return action.secretKey;
}

final Reducer<Uint8List?> secretKeyReducer = combineReducers<Uint8List?>([
  TypedReducer<Uint8List?, SetSessionInfoAction>(setSecretKeyReducer),
  TypedReducer<Uint8List?, InitiateStateAction>(setSecretKeyReducer),
]);

// publicKeyReducer
Uint8List? setPublicKeyReducer(Uint8List? state, action) {
  return action.publicKey;
}

final Reducer<Uint8List?> publicKeyReducer = combineReducers<Uint8List?>([
  TypedReducer<Uint8List?, SetSessionInfoAction>(setPublicKeyReducer),
  TypedReducer<Uint8List?, InitiateStateAction>(setPublicKeyReducer),
]);

// privateKeyReducer
Uint8List? setPrivateKeyReducer(Uint8List? state, action) {
  return action.privateKey;
}

final Reducer<Uint8List?> privateKeyReducer = combineReducers<Uint8List?>([
  TypedReducer<Uint8List?, SetSessionInfoAction>(setPrivateKeyReducer),
  TypedReducer<Uint8List?, InitiateStateAction>(setPrivateKeyReducer),
]);

// lockscreenPinReducer
String? setLockscreenPassphraseReducer(String? state, action) {
  return action.lockscreenPin;
}

final Reducer<String?> lockscreenPinReducer = combineReducers<String?>([
  TypedReducer<String?, UpdateLockscreenSettingAction>(
      setLockscreenPassphraseReducer),
  TypedReducer<String?, InitiateStateAction>(setLockscreenPassphraseReducer),
]);

// serverSecretExistsReducer
bool? setServerSecretExistsReducer(bool? state, action) {
  return action.serverSecretExists;
}

final Reducer<bool?> serverSecretExistsReducer = combineReducers<bool?>([
  TypedReducer<bool?, InitiateStateAction>(setServerSecretExistsReducer),
  TypedReducer<bool?, SetServerSecretExists>(setServerSecretExistsReducer),
  TypedReducer<bool?, SetSessionInfoAction>(setServerSecretExistsReducer),
]);

// complianceServerSecretsReducer
String? setComplianceServerSecretsReducer(String? state, action) {
  return action.complianceServerSecrets;
}

final Reducer<String?> complianceServerSecretsReducer =
    combineReducers<String?>([
  TypedReducer<String?, InitiateStateAction>(setComplianceServerSecretsReducer),
  TypedReducer<String?, SetVerifyKeyAction>(setComplianceServerSecretsReducer),
  TypedReducer<String?, SetServerPolicyAction>(
      setComplianceServerSecretsReducer),
]);

// complianceDisableDeleteAccountReducer
bool? setComplianceDisableDeleteAccountReducer(bool? state, action) {
  return action.complianceDisableDeleteAccount;
}

final Reducer<bool?> complianceDisableDeleteAccountReducer =
    combineReducers<bool?>([
  TypedReducer<bool?, InitiateStateAction>(
      setComplianceDisableDeleteAccountReducer),
  TypedReducer<bool?, SetVerifyKeyAction>(
      setComplianceDisableDeleteAccountReducer),
  TypedReducer<bool?, SetServerPolicyAction>(
      setComplianceDisableDeleteAccountReducer),
]);

// complianceDisableOfflineModeReducer
bool? setComplianceDisableOfflineModeReducer(bool? state, action) {
  return action.complianceDisableOfflineMode;
}

final Reducer<bool?> complianceDisableOfflineModeReducer =
    combineReducers<bool?>([
  TypedReducer<bool?, InitiateStateAction>(
      setComplianceDisableOfflineModeReducer),
  TypedReducer<bool?, SetVerifyKeyAction>(
      setComplianceDisableOfflineModeReducer),
  TypedReducer<bool?, SetServerPolicyAction>(
      setComplianceDisableOfflineModeReducer),
]);

// complianceMaxOfflineCacheTimeValidReducer
int? setComplianceMaxOfflineCacheTimeValidReducer(int? state, action) {
  return action.complianceMaxOfflineCacheTimeValid;
}

final Reducer<int?> complianceMaxOfflineCacheTimeValidReducer =
    combineReducers<int?>([
  TypedReducer<int?, InitiateStateAction>(
      setComplianceMaxOfflineCacheTimeValidReducer),
  TypedReducer<int?, SetVerifyKeyAction>(
      setComplianceMaxOfflineCacheTimeValidReducer),
  TypedReducer<int?, SetServerPolicyAction>(
      setComplianceMaxOfflineCacheTimeValidReducer),
]);

// userIdReducer
String? setUserIdReducer(String? state, action) {
  return action.userId;
}

final Reducer<String?> userIdReducer = combineReducers<String?>([
  TypedReducer<String?, SetSessionInfoAction>(setUserIdReducer),
  TypedReducer<String?, InitiateStateAction>(setUserIdReducer),
]);

// userEmailReducer
String? setUserEmailReducer(String? state, action) {
  return action.userEmail;
}

final Reducer<String?> userEmailReducer = combineReducers<String?>([
  TypedReducer<String?, SetSessionInfoAction>(setUserEmailReducer),
  TypedReducer<String?, InitiateStateAction>(setUserEmailReducer),
  TypedReducer<String?, SetUserEmailAction>(setUserEmailReducer),
]);

// userSauceReducer
String? setUserSauceReducer(String? state, action) {
  return action.userSauce;
}

final Reducer<String?> userSauceReducer = combineReducers<String?>([
  TypedReducer<String?, SetSessionInfoAction>(setUserSauceReducer),
  TypedReducer<String?, InitiateStateAction>(setUserSauceReducer),
]);

// configReducer
Config? setConfigJsonReducer(Config? state, action) {
  return action.config;
}

final Reducer<Config?> configReducer = combineReducers<Config?>([
  TypedReducer<Config?, InitiateStateAction>(setConfigJsonReducer),
  TypedReducer<Config?, ConfigUpdatedAction>(setConfigJsonReducer),
]);

// passwordLengthReducer
String? setPasswordLengthReducer(String? state, action) {
  return action.passwordLength;
}

final Reducer<String?> passwordLengthReducer = combineReducers<String?>([
  TypedReducer<String?, PasswordGeneratorSettingAction>(
      setPasswordLengthReducer),
]);

// lettersUppercaseReducer
String? setLettersUppercaseReducer(String? state, action) {
  return action.lettersUppercase;
}

final Reducer<String?> lettersUppercaseReducer = combineReducers<String?>([
  TypedReducer<String?, PasswordGeneratorSettingAction>(
      setLettersUppercaseReducer),
]);

// lettersLowercaseReducer
String? setLettersLowercaseReducer(String? state, action) {
  return action.lettersLowercase;
}

final Reducer<String?> lettersLowercaseReducer = combineReducers<String?>([
  TypedReducer<String?, PasswordGeneratorSettingAction>(
      setLettersLowercaseReducer),
]);

// numbersReducer
String? setNumbersReducer(String? state, action) {
  return action.numbers;
}

final Reducer<String?> numbersReducer = combineReducers<String?>([
  TypedReducer<String?, PasswordGeneratorSettingAction>(setNumbersReducer),
]);

// specialCharsReducer
String? setSpecialCharsReducer(String? state, action) {
  return action.specialChars;
}

final Reducer<String?> specialCharsReducer = combineReducers<String?>([
  TypedReducer<String?, PasswordGeneratorSettingAction>(setSpecialCharsReducer),
]);

AppState appReducer(AppState state, action) {
  return AppState(
    serverUrl: serverUrlReducer(state.serverUrl, action),
    verifyKeyOld: verifyKeyOldReducer(state.verifyKeyOld, action),
    verifyKey: verifyKeyReducer(state.verifyKey, action),
    username: usernameReducer(state.username, action),
    //password: passwordReducer(state.password, action),
    token: tokenReducer(state.token, action),
    authentication: authenticationReducer(state.authentication, action),
    sessionSecretKey: sessionSecretKeyReducer(state.sessionSecretKey, action),
    lastServerConnectionTimeSinceEpoch:
        lastServerConnectionTimeSinceEpochReducer(
            state.lastServerConnectionTimeSinceEpoch, action),
    lastCacheTimeSinceEpoch:
        lastCacheTimeSinceEpochReducer(state.lastCacheTimeSinceEpoch, action),
    secretKey: secretKeyReducer(state.secretKey, action),
    publicKey: publicKeyReducer(state.publicKey, action),
    privateKey: privateKeyReducer(state.privateKey, action),
    lockscreenPin: lockscreenPinReducer(state.lockscreenPin, action),
    serverSecretExists:
        serverSecretExistsReducer(state.serverSecretExists, action),
    complianceServerSecrets: complianceServerSecretsReducer(
      state.complianceServerSecrets,
      action,
    ),
    complianceDisableDeleteAccount: complianceDisableDeleteAccountReducer(
      state.complianceDisableDeleteAccount,
      action,
    ),
    complianceDisableOfflineMode: complianceDisableOfflineModeReducer(
      state.complianceDisableOfflineMode,
      action,
    ),
    complianceMaxOfflineCacheTimeValid:
        complianceMaxOfflineCacheTimeValidReducer(
      state.complianceMaxOfflineCacheTimeValid,
      action,
    ),
    userId: userIdReducer(state.userId, action),
    userEmail: userEmailReducer(state.userEmail, action),
    userSauce: userSauceReducer(state.userSauce, action),
    config: configReducer(state.config, action),

    passwordLength: passwordLengthReducer(state.passwordLength, action),
    lettersUppercase: lettersUppercaseReducer(state.lettersUppercase, action),
    lettersLowercase: lettersLowercaseReducer(state.lettersLowercase, action),
    numbers: numbersReducer(state.numbers, action),
    specialChars: specialCharsReducer(state.specialChars, action),
  );
}
