import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/loaders/decoders/json_decode_strategy.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:psono/app_config.dart';
import 'package:psono/model/app_state.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/theme.dart';
import 'package:redux/redux.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var config = AppConfig.of(context)!;
    final Store store = reduxStore;

    return StoreProvider<AppState>(
      store: store as Store<AppState>,
      child: MaterialApp(
        title: config.appName,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            useMaterial3: false,
            canvasColor: primarySwatch.shade500,
            brightness: Brightness.light,
            fontFamily: 'Open Sans',
            colorScheme:
                ColorScheme.fromSwatch(primarySwatch: primarySwatch).copyWith(
              secondary: primarySwatch.shade500,
            ),
            scaffoldBackgroundColor: const Color(0xFF151f2b)),
        initialRoute: AppRoutes.initial,
        onGenerateRoute: AppRoutes.onGenerateRoute,
        supportedLocales: const [
          // ATTENTION: leave en this as first language as this is the default
          Locale('en'), // English
          Locale('ar'), // Arabic
          Locale('bn'), // Bengali
          Locale('ca'), // Catalan
          Locale('cs'), // Czech
          Locale('de'), // German
          Locale('es'), // Spanish
          Locale('fi'), // Finnish
          Locale('fr'), // French
          Locale('hi'), // Hindi
          Locale('hu'), // Hungarian
          Locale('hr'), // Croatian
          Locale('it'), // Italian
          Locale('ja'), // Japanese
          Locale('ko'), // Korean
          Locale('nl'), // Dutch
          // Locale('nb'), // Norwegian
          // Locale('nn'), // Norwegian
          Locale('ru'), // Russian
          Locale('pl'), // Polish
          Locale('pt'), // Polish
          Locale('sv'), // Swedish
          Locale('uk'), // Ukrainian
          Locale('zh'), // Chinese
        ],
        localizationsDelegates: [
          FlutterI18nDelegate(
            translationLoader: FileTranslationLoader(
              fallbackFile: 'en',
              basePath: "assets/locales",
              decodeStrategies: [JsonDecodeStrategy()],
            ),
            missingTranslationHandler: (key, locale) {
              print(
                  "--- Missing Key: $key, languageCode: ${locale!.languageCode}");
            },
          ),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        builder: (context, child) {
          final mediaQueryData = MediaQuery.of(context);

          final scale = mediaQueryData.textScaler.clamp(
            minScaleFactor: 1.0,
            maxScaleFactor: 1.3,
          );

          return MediaQuery(
            data: mediaQueryData.copyWith(
              textScaler: scale,
            ),
            child: child!,
          );
        },
      ),
    );
  }
}
