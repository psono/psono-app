import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_constants.dart';

class AutofillOnboardingIOS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        children: <Widget>[
          SizedBox(height: 30.h),
          Image.asset(
            AppAssets.autofillLock,
            height: 181.12.w,
            width: 121.74.h,
          ),
          SizedBox(height: 16.h),
          TextDmSans(
            text: FlutterI18n.translate(context, "ENABLE_AUTO_FILL_PASSWORDS"),
            textAlign: TextAlign.center,
            color: Colors.white,
            fontSize: 40.sp,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 16.h),
          TextDmSans(
            text: FlutterI18n.translate(context, "USE_YOUR_PASSWORDS_WHERE"),
            textAlign: TextAlign.center,
            color: Colors.white.withOpacity(0.60),
            fontSize: 16.sp,
            fontWeight: FontWeight.w400,
          ),
          SizedBox(height: 32.h),
          Container(
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.10),
              borderRadius: BorderRadius.circular(20.r),
            ),
            padding: EdgeInsets.all(40.h),
            child: Column(
              children: [
                _Step(
                  iconPath: AppAssets.iconSettings,
                  text: FlutterI18n.translate(context, 'OPEN_SETTINGS_APP'),
                ),
                SizedBox(height: 34.h),
                _Step(
                  iconPath: AppAssets.iconPasswordsAccounts,
                  text: FlutterI18n.translate(
                    context,
                    "TAP_INTO_PASSWORDS_AND_ACCOUNTS",
                  ),
                ),
                SizedBox(height: 34.h),
                _Step(
                  iconPath: AppAssets.iconAutofill,
                  text: FlutterI18n.translate(
                    context,
                    "TAP_INTO_AUTOFILL_PASSWORDSS",
                  ),
                ),
                SizedBox(height: 34.h),
                _Step(
                  iconPath: AppAssets.iconOn,
                  text: FlutterI18n.translate(
                    context,
                    "TURN_ON_AUTOFILL_PASSWORDS",
                  ),
                ),
                SizedBox(height: 34.h),
                _Step(
                  iconPath: AppAssets.iconPsonoApp,
                  text: FlutterI18n.translate(
                    context,
                    "SELECT_PSONO",
                  ),
                ),
                SizedBox(height: 34.h),
                _Step(
                  iconPath: AppAssets.iconPasswordsAccounts,
                  text: FlutterI18n.translate(
                    context,
                    "DESELECT_KEYCHAIN",
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 16.h),
          component.BtnPrimary(
            onPressed: () async {
              await storage.write(
                key: SK.passedAutofillOnboarding,
                value: 'true',
                iOptions: secureIOSOptions,
              );
              if (context.mounted) {
                Navigator.pushReplacementNamed(context, AppRoutes.initial);
              }
            },
            text: FlutterI18n.translate(context, "OK"),
          ),
          SizedBox(height: 24.h),
          Center(
            child: component.BtnText(
              onPressed: () async {
                await storage.write(
                  key: SK.passedAutofillOnboarding,
                  value: 'true',
                  iOptions: secureIOSOptions,
                );
                Navigator.pushReplacementNamed(context, AppRoutes.initial);
              },
              text: FlutterI18n.translate(context, "REMIND_ME_LATER")
                  .toUpperCase(),
              color: Colors.white.withOpacity(0.60),
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(height: 12.h),
        ],
      ),
    );
  }
}

class _Step extends StatelessWidget {
  const _Step({
    Key? key,
    required this.iconPath,
    required this.text,
  }) : super(key: key);

  final String iconPath;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image(
          image: AssetImage(iconPath),
          height: 40.h,
          width: 40.h,
          fit: BoxFit.fitWidth,
        ),
        SizedBox(width: 20.w),
        Expanded(
          flex: 10,
          child: TextDmSans(
            text: text,
            color: Colors.white,
            fontSize: 16.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
