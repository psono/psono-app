import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/autofill.dart' as autofill_service;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_constants.dart';

class AutofillOnboardingAndroid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: orientation == Orientation.portrait ? 60.h : 0.h),
            Image.asset(
              AppAssets.autofillLock,
              height: 181.12,
              width: 121.74,
            ),
            TextDmSans(
              text:
                  FlutterI18n.translate(context, "ENABLE_AUTO_FILL_PASSWORDS"),
              textAlign: TextAlign.center,
              color: Colors.white,
              fontSize: 40,
              fontWeight: FontWeight.w500,
            ),
            SizedBox(height: 16.h),
            component.AlertInfo(
              text: FlutterI18n.translate(
                context,
                "AUTOFILL_SERVICE_ACTIVATE_NOW_INFO",
              ),
            ),
            SizedBox(height: 16.h),
            Padding(
              padding: const EdgeInsets.only(right: 5.0),
              child: component.BtnPrimary(
                onPressed: () async {
                  await storage.write(
                    key: SK.passedAutofillOnboarding,
                    value: 'true',
                    iOptions: secureIOSOptions,
                  );
                  await autofill_service.enable();
                  if (context.mounted) {
                    Navigator.pushReplacementNamed(context, AppRoutes.initial);
                  }
                },
                text: FlutterI18n.translate(context, "ACTIVATE"),
              ),
            ),
            const SizedBox(height: 16.0),
            Row(
              children: [
                const Spacer(flex: 2),
                component.BtnText(
                  onPressed: () async {
                    await storage.write(
                      key: SK.passedAutofillOnboarding,
                      value: 'true',
                      iOptions: secureIOSOptions,
                    );
                    Navigator.pushReplacementNamed(context, AppRoutes.initial);
                  },
                  text: FlutterI18n.translate(context, "REMIND_ME_LATER")
                      .toUpperCase(),
                  color: Colors.white.withOpacity(0.60),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                ),
                const Spacer(flex: 1),
                component.BtnText(
                  onPressed: () async {
                    await storage.write(
                      key: SK.passedAutofillOnboarding,
                      value: 'true',
                      iOptions: secureIOSOptions,
                    );
                    helper.openUrl('https://www.psono.pw/privacy-policy.html');
                  },
                  text: FlutterI18n.translate(context, "PRIVACY_POLICY")
                      .toUpperCase(),
                  color: Colors.white.withOpacity(0.60),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                ),
                const Spacer(flex: 2),
              ],
            ),
            SizedBox(height: 12.h),
          ],
        ),
      ),
    );
  }
}
