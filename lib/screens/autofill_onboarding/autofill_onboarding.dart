import 'dart:io';
import 'package:flutter/material.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/utils/app_assets.dart';
import './autofill_onboarding_android.dart';
import './autofill_onboarding_ios.dart';

class AutofillOnboardingScreen extends StatefulWidget {
  static String tag = 'autofill-onboarding-screen';
  @override
  _AutofillOnboardingScreenState createState() =>
      _AutofillOnboardingScreenState();
}

class _AutofillOnboardingScreenState extends State<AutofillOnboardingScreen> {
  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.faceIDBg,
      body: Platform.isAndroid
          ? AutofillOnboardingAndroid()
          : AutofillOnboardingIOS(),
    );
  }
}
