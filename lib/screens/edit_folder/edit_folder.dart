import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/components/_index.dart' as component;
import 'package:psono/utils/app_assets.dart';

class EditFolderScreen extends StatefulWidget {
  static String tag = 'edit-folder-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final datastoreModel.Folder? folder;
  final List<String?>? path;
  final List<String?>? relativePath;

  EditFolderScreen({
    this.parent,
    this.datastore,
    this.share,
    this.folder,
    this.path,
    this.relativePath,
  });

  @override
  _EditFolderScreenState createState() => _EditFolderScreenState();
}

class _EditFolderScreenState extends State<EditFolderScreen> {
  var folderName;

  String? message;

  @override
  void initState() {
    folderName = TextEditingController(
      text: widget.folder!.name,
    );
    super.initState();
  }

  @override
  void dispose() {
    folderName?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    return Material(
      color: Colors.transparent,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
        margin:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        decoration: BoxDecoration(
          color: const Color(0xff171717),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.r),
            topRight: Radius.circular(30.r),
          ),
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 50.w,
                height: 3.h,
                decoration: ShapeDecoration(
                  color: Colors.white.withOpacity(0.30),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100),
                  ),
                ),
              ),
              SizedBox(height: 25.h),
              Image.asset(
                AppAssets.folder2,
                width: 48.w,
                height: 48.h,
              ),
              SizedBox(height: 25.h),
              component.TextDmSans(
                text: FlutterI18n.translate(
                  context,
                  'ENTER_FOLDER_NAME',
                ),
                fontSize: 18.sp,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: folderName,
                showHeader: false,
                autofocus: true,
                labelText: FlutterI18n.translate(
                  context,
                  'FOLDER_NAME',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'FOLDER_NAME_IS_REQUIRED',
                    );
                  }
                  return null;
                },
              ),
              SizedBox(height: 16.h),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: component.BtnPrimary(
                  onPressed: () async {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }

                    List<String?> folderPath =
                        List<String?>.from(widget.relativePath!);
                    folderPath.add(widget.folder!.id);

                    if (widget.folder!.shareId != null) {
                      shareModel.Share share = await managerShare.readShare(
                        widget.folder!.shareId,
                        widget.folder!.shareSecretKey,
                      );

                      share.folder!.name = folderName.text;
                      widget.folder!.name = folderName.text;
                      await share.save();
                    } else if (widget.share == null) {
                      datastoreModel.Datastore datastore =
                          await managerDatastorePassword.getPasswordDatastore(
                        widget.datastore!.datastoreId,
                      );

                      List<String> pathCopy = List<String>.from(folderPath);
                      List search = managerDatastorePassword.findInDatastore(
                        pathCopy,
                        datastore.data,
                      );
                      datastoreModel.Folder remoteFolder = search[0][search[1]];

                      remoteFolder.name = folderName.text;
                      widget.folder!.name = folderName.text;

                      await datastore.save();
                    } else {
                      shareModel.Share share = await managerShare.readShare(
                        widget.share!.shareId,
                        widget.share!.shareSecretKey,
                      );

                      List<String> pathCopy = List<String>.from(
                        folderPath,
                      );
                      List search = managerDatastorePassword.findInDatastore(
                        pathCopy,
                        share.folder,
                      );
                      datastoreModel.Folder remoteFolder = search[0][search[1]];

                      remoteFolder.name = folderName.text;
                      widget.folder!.name = folderName.text;
                      await share.save();
                    }

                    setState(() {
                      message = FlutterI18n.translate(context, "SAVE_SUCCESS");
                    });
                    Navigator.pop(context);
                  },
                  text: FlutterI18n.translate(context, "SAVE"),
                ),
              ),
              messageWidget,
            ],
          ),
        ),
      ),
    );
  }
}
