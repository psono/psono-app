import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/model/share_right.dart';
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/utils/app_assets.dart';

class AddFolderScreen extends StatefulWidget {
  static String tag = 'add-folder-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;

  AddFolderScreen(
      {this.parent, this.datastore, this.share, this.path, this.relativePath});

  @override
  _AddFolderScreenState createState() => _AddFolderScreenState();
}

class _AddFolderScreenState extends State<AddFolderScreen> {
  final folderName = TextEditingController(
    text: '',
  );

  String? message;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    folderName.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    return Material(
      color: Colors.transparent,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
        margin:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        decoration: BoxDecoration(
          color: const Color(0xff171717),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.r),
            topRight: Radius.circular(30.r),
          ),
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 50.w,
                height: 3.h,
                decoration: ShapeDecoration(
                  color: Colors.white.withOpacity(0.30),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100),
                  ),
                ),
              ),
              SizedBox(height: 25.h),
              Image.asset(
                AppAssets.folder2,
                width: 48.w,
                height: 48.h,
              ),
              SizedBox(height: 25.h),
              component.TextDmSans(
                text: FlutterI18n.translate(
                  context,
                  'ENTER_FOLDER_NAME',
                ),
                fontSize: 18.sp,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                showHeader: false,
                autofocus: true,
                labelText: FlutterI18n.translate(
                  context,
                  'FOLDER_NAME',
                ),
                controller: folderName,
                validator: (value) {
                  if (value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'FOLDER_NAME_IS_REQUIRED',
                    );
                  }
                  return null;
                },
              ),
              SizedBox(height: 16.h),
              component.BtnPrimary(
                onPressed: () async {
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }

                  if (widget.parent!.folders == null) {
                    widget.parent!.folders = [];
                  }

                  datastoreModel.Folder newFolder = datastoreModel.Folder(
                    id: await cryptoLibrary.generateUUID(),
                    name: folderName.text,
                  );

                  if (widget.share == null) {
                    newFolder.shareRights = ShareRight(
                      read: true,
                      write: true,
                      grant: true,
                      delete: true,
                    );
                  } else {
                    newFolder.shareRights = ShareRight(
                      read: widget.share!.shareRights!.read,
                      write: widget.share!.shareRights!.write,
                      grant: widget.share!.shareRights!.grant! &&
                          widget.share!.shareRights!.write!,
                      delete: widget.share!.shareRights!.write,
                    );
                  }

                  widget.parent!.folders!.add(newFolder);

                  if (widget.share == null) {
                    datastoreModel.Datastore datastore =
                        await managerDatastorePassword.getPasswordDatastore(
                      widget.datastore!.datastoreId,
                    );

                    datastoreModel.Folder? parent;
                    if (widget.path!.isEmpty) {
                      parent = datastore.data;
                    } else {
                      List<String> pathCopy = List<String>.from(widget.path!);
                      List search = managerDatastorePassword.findInDatastore(
                        pathCopy,
                        datastore.data,
                      );
                      parent = search[0][search[1]];
                    }

                    if (parent!.folders == null) {
                      parent.folders = [];
                    }
                    parent.folders!.add(newFolder);
                    await datastore.save();
                  } else {
                    shareModel.Share share = await managerShare.readShare(
                      widget.share!.shareId,
                      widget.share!.shareSecretKey,
                    );
                    datastoreModel.Folder? parent;
                    if (widget.relativePath!.isEmpty) {
                      parent = share.folder;
                    } else {
                      List<String> pathCopy = List<String>.from(
                        widget.relativePath!,
                      );
                      List search = managerDatastorePassword.findInDatastore(
                        pathCopy,
                        share.folder,
                      );
                      parent = search[0][search[1]];
                    }
                    if (parent!.folders == null) {
                      parent.folders = [];
                    }
                    parent.folders!.add(newFolder);
                    await share.save();
                  }

                  setState(() {
                    message = FlutterI18n.translate(context, "SAVE_SUCCESS");
                  });
                  Navigator.pop(context, newFolder);
                },
                text: FlutterI18n.translate(context, "CREATE_NEW_FOLDER"),
              ),
              messageWidget,
            ],
          ),
        ),
      ),
    );
  }
}
