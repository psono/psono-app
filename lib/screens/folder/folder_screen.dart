import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/screens/add_folder/add_folder.dart';
import 'package:psono/screens/add_item/add_item_screen.dart';
import 'package:psono/screens/datastore/datastore_screen.dart';
import 'package:psono/screens/download_file/index.dart';
import 'package:psono/screens/edit_folder/edit_folder.dart';
import 'package:psono/screens/edit_secret/edit_secret.dart';
import 'package:psono/services/manager_widget.dart' as managerWidget;
import 'package:psono/theme.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:share_plus/share_plus.dart' as share_plus;

class FolderScreen extends StatefulWidget {
  static String tag = 'folder-screen';
  final List<String>? autoNavigate;
  final datastoreModel.Folder? parent;
  final datastoreModel.Folder? folder;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;
  final bool? autofill;

  const FolderScreen({
    required this.autoNavigate,
    required this.folder,
    required this.datastore,
    required this.share,
    required this.path,
    required this.relativePath,
    required this.autofill,
    required this.parent,
  });

  @override
  _FolderScreenState createState() => _FolderScreenState();
}

class _FolderScreenState extends State<FolderScreen> {
  String? _search;
  Map<String, String> _selectedFilters = {};
  List<String?> selectedItems = [];
  List<String?> selectedFolders = [];
  bool alreadyLoadedOnce = false;
  late final _listener;
  @override
  void initState() {
    super.initState();
    _listener = listner;
    component.DatastoreScaffold.isGridView.addListener(_listener);
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    component.DatastoreScaffold.isGridView.removeListener(_listener);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget contentView = _folderTree();
    bool isSelectionMode =
        selectedItems.isNotEmpty || selectedFolders.isNotEmpty;
    bool isSearchMode =
        _search?.isNotEmpty ?? false || _selectedFilters.isNotEmpty;
    if (isSearchMode) {
      contentView = _searchFolderTree();
    } else if (isSelectionMode) {
      contentView = _selectedFolderTree();
    }

    return component.DatastoreScaffold(
      isSubScreen: true,
      folder: widget.folder,
      onNewFolderTap: _onNewFolderCallBack,
      onNewEntryTap: _onNewEntryCallBack,
      onLinkShareTap: _linkShareCallback,
      onDeleteTap: _deleteCallback,
      onEditTap: _editCallback,
      onSearchChange: (String? search) {
        setState(() {
          _search = search;
        });
      },
      thisFolderEditTap: _thisFolderEditCallBack,
      isSelectionMode: isSelectionMode,
      isSearchMode: _search?.isNotEmpty ?? false,
      selectedFilters: _selectedFilters,
      isMultiSelected: (selectedItems.length + selectedFolders.length) > 1,
      onApplyFilter: (selectedFilters) {
        setState(() {
          _selectedFilters = selectedFilters;
        });
      },
      child: RefreshIndicator(
        onRefresh: handleRefresh,
        child: contentView,
      ),
    );
  }

  _linkShareCallback() async {
    if (selectedItems.length == 1) {
      datastoreModel.Item? item = widget.folder!.items!
          .where((item) => selectedItems.contains(item!.id))
          .first;
      final url = await component.linkShare(context, item);
      if (url != null) {
        share_plus.Share.share(url);
      }
    }
  }

  Future<void> loadDatastore({List<String?>? newPath}) async {
    Navigator.pushReplacement(
      context,
      NoAnimationMaterialPageRoute(
        builder: (context) => DatastoreScreen(
          autoNavigate: newPath ?? widget.path,
        ),
      ),
    );
  }

  Future<void> initStateAsync() async {
    if (widget.autoNavigate != null && widget.autoNavigate!.isNotEmpty) {
      List<String> newAutoNavigate = List.from(widget.autoNavigate!);
      String folderId = newAutoNavigate.removeAt(0);

      for (var i = 0; i < widget.folder!.folders!.length; i++) {
        datastoreModel.Folder folder = widget.folder!.folders![i]!;
        if (folder.id != folderId) {
          continue;
        }
        datastoreModel.Folder? newShare;
        List<String?> newRelativePath;
        if (folder.shareId != null) {
          newShare = folder;
          newRelativePath = [];
        } else {
          newRelativePath = List.from(widget.relativePath!)
            ..addAll([folder.id]);
        }

        Navigator.push(
          context,
          NoAnimationMaterialPageRoute(
            builder: (context) => FolderScreen(
              autoNavigate: newAutoNavigate,
              parent: widget.folder,
              folder: folder,
              datastore: widget.datastore,
              share: newShare,
              path: List.from(widget.path!)..addAll([folder.id]),
              relativePath: newRelativePath,
              autofill: widget.autofill,
            ),
          ),
        );
        return;
      }
    }
  }

  listner() {
    setState(
      () {},
    );
  }

  Widget _folderTree() {
    return component.FolderTree(
      root: widget.folder,
      datastore: widget.datastore,
      share: widget.share,
      path: widget.path,
      relativePath: widget.relativePath,
      autofill: widget.autofill,
      onLongPressFolder: (datastoreModel.Folder? folder) async {
        if (folder == null) {
          return;
        }
        if (!folder.shareRights!.delete!) {
          return;
        }
        setState(() {
          selectedItems = [];
          selectedFolders = [folder.id];
        });
      },
      onLongPressItem: (datastoreModel.Item? item) async {
        if (item == null) {
          return;
        }
        if (!item.shareRights!.delete!) {
          return;
        }
        setState(() {
          selectedItems = [item.id];
          selectedFolders = [];
        });
      },
    );
  }

  Widget _searchFolderTree() {
    return component.FolderSearchTree(
      root: widget.folder,
      datastore: widget.datastore,
      share: widget.share,
      path: widget.path,
      relativePath: widget.relativePath,
      search: _search,
      autofill: widget.autofill,
      filters: _selectedFilters,
    );
  }

  Widget _selectedFolderTree() {
    return component.FolderSelectTree(
      root: widget.folder,
      datastore: widget.datastore,
      share: widget.share,
      path: widget.path,
      relativePath: widget.relativePath,
      selectedFolders: selectedFolders,
      selectedItems: selectedItems,
      onSelectFolder: (datastoreModel.Folder? folder) {
        if (folder == null) {
          return;
        }
        if (selectedFolders.contains(folder.id)) {
          setState(() {
            selectedFolders =
                selectedFolders.where((e) => e != folder.id).toList();
          });
        } else {
          setState(() {
            selectedFolders = [...selectedFolders, folder.id];
          });
        }
      },
      onSelectItem: (datastoreModel.Item? item) {
        if (item == null) {
          return;
        }
        if (selectedItems.contains(item.id)) {
          setState(() {
            selectedItems = selectedItems.where((e) => e != item.id).toList();
          });
        } else {
          setState(() {
            selectedItems = [...selectedItems, item.id];
          });
        }
      },
    );
  }

  Future handleRefresh({List<String?>? newPath}) async {
    await loadDatastore(newPath: newPath);
  }

  void searchCallback(String? search) {
    setState(() {
      _search = search;
    });
  }

  _onNewFolderCallBack() {
    component
        .showCustomBottomSheet(
      context: context,
      child: AddFolderScreen(
        parent: widget.folder,
        datastore: widget.datastore,
        share: widget.share,
        path: widget.path,
        relativePath: widget.relativePath,
      ),
    )
        .then((value) {
      final folder = value as datastoreModel.Folder;
      List<String?> path = widget.path!..add(folder.id);
      handleRefresh(newPath: path);
    });
  }

  _onNewEntryCallBack() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AddItemScreen(
          parent: widget.folder,
          datastore: widget.datastore,
          share: widget.share,
          path: widget.path,
          relativePath: widget.relativePath,
        ),
      ),
    ).then((value) {
      handleRefresh();
    });
  }

  void _deleteCallback() async {
    component.showCustomDialog(
      context: context,
      content: Padding(
        padding: EdgeInsets.all(20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  component.TextDmSans(
                    text: FlutterI18n.translate(context, 'DELETE_ENTRY'),
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.h),
            Divider(
              color: Colors.white.withOpacity(0.30),
              height: 0.50,
            ),
            SizedBox(height: 20.h),
            component.TextDmSans(
              text: FlutterI18n.translate(context, 'DELETE_ENTRY_WARNING'),
              color: Colors.white,
              fontSize: 15.sp,
              fontWeight: FontWeight.w300,
              letterSpacing: 0.48,
            ),
            SizedBox(height: 20.h),
            Row(
              children: [
                const Spacer(),
                component.BtnText(
                  text: FlutterI18n.translate(context, "CANCEL").toUpperCase(),
                  color: const Color(0x99B6B6B6),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                const Spacer(flex: 2),
                Expanded(
                  flex: 10,
                  child: component.Btn(
                    onPressed: () async {
                      await managerWidget.deleteItem(
                        selectedItems,
                        selectedFolders,
                        widget.relativePath,
                        widget.share,
                        widget.datastore,
                        'password',
                      );

                      setState(() {
                        selectedItems = [];
                        selectedFolders = [];
                      });
                      if (context.mounted) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DatastoreScreen(
                              autoNavigate: widget.path,
                            ),
                          ),
                        );
                      }
                    },
                    color: const Color(0xFFDC1C56),
                    customChild: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.delete_outline,
                          color: Colors.white,
                          size: 20.sp,
                        ),
                        SizedBox(width: 6.w),
                        component.TextDmSans(
                          text: FlutterI18n.translate(context, "DELETE")
                              .toUpperCase(),
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _editCallback() async {
    if (selectedItems.length == 1) {
      // Trigger edit of an item
      datastoreModel.Item? item = widget.folder!.items!
          .where((item) => selectedItems.contains(item!.id))
          .first;

      if (item?.fileId != null) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DownloadFileScreen(
              parent: widget.folder,
              datastore: widget.datastore,
              share: widget.share,
              item: item!,
              path: widget.path,
              relativePath: widget.relativePath,
            ),
          ),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditSecretScreen(
              parent: widget.folder,
              datastore: widget.datastore,
              share: widget.share,
              item: item,
              path: widget.path,
              relativePath: widget.relativePath,
            ),
          ),
        );
      }
    } else if (selectedFolders.length == 1) {
      // Trigger edit of a folder
      datastoreModel.Folder? folder = widget.folder!.folders!
          .where((folder) => selectedFolders.contains(folder!.id))
          .first;
      component.showCustomBottomSheet(
        context: context,
        child: EditFolderScreen(
          parent: widget.folder,
          datastore: widget.datastore,
          share: widget.share,
          folder: folder,
          path: widget.path,
          relativePath: widget.relativePath,
        ),
      );
    }
  }

  void _thisFolderEditCallBack() {
    component.showCustomBottomSheet(
      context: context,
      child: EditFolderScreen(
        parent: widget.parent,
        datastore: widget.datastore,
        share: widget.share,
        folder: widget.folder,
        path: widget.path,
        relativePath: widget.relativePath,
      ),
    );
  }
}
