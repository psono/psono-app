import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:local_auth/local_auth.dart';
import 'package:psono/components/_index.dart' as components;
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/storage.dart';
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_constants.dart';

class PinScreen extends StatefulWidget {
  PinScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _PinScreenState createState() => _PinScreenState();
}

class _PinScreenState extends State<PinScreen> {
  bool? isFingerprint;
  int loginAttempts = 0;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      String? loginAttemptsStr = await storage.read(key: SK.loginAttempts);
      int loginAttemptsInt = 0;
      if (loginAttemptsStr != null) {
        try {
          loginAttemptsInt = int.parse(loginAttemptsStr);
        } catch (e) {
          // pass
        }
      }
      setState(() {
        loginAttempts = loginAttemptsInt;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    String passphrase = reduxStore.state.lockscreenPin;

    if (passphrase.isEmpty) {
      Navigator.pushReplacementNamed(context, AppRoutes.datastore);
    }

    isFingerprint ??= false;
    return PopScope(
      canPop: false,
      child: components.LockView(
        passLength: passphrase.length,
        fingerFunction: biometrics,
        signOut: () async {
          await user_service.logout();
          await storage.delete(key: SK.lastUnlockTime);
          await storage.delete(key: SK.loginAttempts);
          await storage.delete(key: SK.lockscreenPin);

          await Future.delayed(const Duration(milliseconds: 500));
          if (context.mounted) {
            Navigator.pushReplacementNamed(context, AppRoutes.signin);
          }
        },
        fingerVerify: isFingerprint,
        passCodeVerify: (passcode) {
          if (passcode != passphrase) {
            loginAttempts += 1;
            if (loginAttempts >= 5) {
              // we allow the user to try this passphrase 5 times before we lock him out
              loginAttempts = 0;
            }
            if (loginAttempts == 0) {
              Future.delayed(const Duration(seconds: 0), () async {
                await storage.write(
                  key: SK.loginAttempts,
                  value: "0",
                  iOptions: secureIOSOptions,
                );
                setState(() {
                  loginAttempts = 0;
                });
                user_service.logout();
                if (context.mounted) {
                  Navigator.pushReplacementNamed(context, AppRoutes.signin);
                }
              });
            } else {
              storage.write(
                key: SK.loginAttempts,
                value: loginAttempts.toString(),
                iOptions: secureIOSOptions,
              );
              setState(() {
                loginAttempts = loginAttempts;
              });
            }
            return false;
          } else {
            Future.delayed(const Duration(seconds: 0), () async {
              await storage.write(
                key: SK.loginAttempts,
                value: "0",
                iOptions: secureIOSOptions,
              );
              setState(() {
                loginAttempts = 0;
              });
              if (context.mounted) {
                helper.makeFirstNamed(context, AppRoutes.datastore);
              }
            });
            return true;
          }
        },
        onSuccess: () {
          helper.makeFirstNamed(context, AppRoutes.datastore);
        },
      ),
    );
  }

  Future<void> biometrics() async {
    final LocalAuthentication auth = LocalAuthentication();
    bool authenticated = false;

    try {
      authenticated = await auth.authenticate(
        localizedReason: FlutterI18n.translate(
          context,
          "USE_BIOMETRIC_AUTHENTICATION_TO_UNLOCK_YOUR_PSONO",
        ),
        options: const AuthenticationOptions(
          biometricOnly: true,
          useErrorDialogs: true,
          stickyAuth: false,
        ),
      );
    } on PlatformException catch (e) {
      print(e);
    }

    await storage.write(
      key: SK.lastUnlockTime,
      value: DateTime.now().millisecondsSinceEpoch.toString(),
      iOptions: secureIOSOptions,
    );

    if (!mounted) return;
    if (authenticated) {
      setState(() {
        isFingerprint = true;
      });
    }
  }
}
