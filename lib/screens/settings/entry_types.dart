import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/blueprint.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/routes/routes.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';

class SettingEntryTypes extends StatefulWidget {
  static String tag = 'settings-password-generator-screen';

  @override
  _SettingEntryTypesState createState() => _SettingEntryTypesState();
}

class _SettingEntryTypesState extends State<SettingEntryTypes> {
  bool enabled = false;
  String? message;
  datastoreModel.Datastore? datastore;
  List blueprints = [];

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(
      context,
      title,
      content,
    );
  }

  Future<void> loadDatastore() async {
    component.Loader.show(context);
    datastoreModel.Datastore? newDatastore;
    try {
      newDatastore = await managerDatastoreSetting.getSettingsDatastore();
    } on api_client.ServiceUnavailableException {
      if (context.mounted) {
        _showErrorDialog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on api_client.UnauthorizedException {
      user_service.logout();

      if (context.mounted) {
        Navigator.pushReplacementNamed(context, AppRoutes.signin);
      }
      return;
    } finally {
      component.Loader.hide();
    }
    setState(() {
      datastore = newDatastore;
    });
  }

  Future<void> initStateAsync() async {
    await loadDatastore();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Blueprint> blueprints = itemBlueprint.getBlueprints();
    blueprints.sort((a, b) => FlutterI18n.translate(context, a.name)
        .compareTo(FlutterI18n.translate(context, b.name)));

    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'ENTRY_TYPES',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnPrimary(
            onPressed: () async {
              if (datastore != null) {
                await datastore!.save();
              }

              if (!mounted) {
                return;
              }
              final SnackBar snackBar = SnackBar(
                content: Text(FlutterI18n.translate(context, "SAVE_SUCCESS")),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE"),
          ),
        )
      ],
      body: datastore == null
          ? Container()
          : Padding(
              padding: EdgeInsets.symmetric(vertical: 20.h),
              child: ListView.builder(
                itemCount: blueprints.length,
                itemBuilder: (BuildContext context, int index) {
                  var bp = blueprints[index];

                  var checked = bp.settingFieldDefault; // fallback if not found
                  for (var i = 0; i < datastore!.dataKV.length; i++) {
                    if (datastore!.dataKV[i]['key'] != bp.settingField ||
                        datastore!.dataKV[i]['value'] == null) {
                      continue;
                    }
                    checked = datastore!.dataKV[i]['value'];
                    break;
                  }
                  return Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          color: Colors.white.withOpacity(0.2),
                          width: 1,
                        ),
                      ),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 14.h),
                    child: Row(
                      children: [
                        component.TextDmSans(
                          text: FlutterI18n.translate(
                            context,
                            bp.name,
                          ),
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                        const Spacer(),
                        CupertinoSwitch(
                          value: checked,
                          onChanged: (value) {
                            onChanged(value, bp);
                          },
                          trackColor: const Color(0xff787880).withOpacity(0.4),
                          activeColor: C.mountainMeadow,
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
    );
  }

  onChanged(value, Blueprint bp) async {
    var found = false;
    for (var i = 0; i < datastore!.dataKV.length; i++) {
      if (datastore!.dataKV[i]['key'] != bp.settingField) {
        continue;
      }
      datastore!.dataKV[i]['value'] = value;
      found = true;
      break;
    }
    if (!found) {
      datastore!.dataKV.add(
        {
          'key': bp.settingField,
          'value': value,
        },
      );
    }

    setState(() {
      datastore = datastore;
    });
  }
}
