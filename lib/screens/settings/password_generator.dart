import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';

class SettingPasswordGenerator extends StatefulWidget {
  static String tag = 'settings-password-generator-screen';

  @override
  _SettingPasswordGeneratorState createState() =>
      _SettingPasswordGeneratorState();
}

class _SettingPasswordGeneratorState extends State<SettingPasswordGenerator> {
  final passwordLength = TextEditingController(
    text: '',
  );
  final lettersUppercase = TextEditingController(
    text: '',
  );
  final lettersLowercase = TextEditingController(
    text: '',
  );
  final numbers = TextEditingController(
    text: '',
  );
  final specialChars = TextEditingController(
    text: '',
  );

  bool enabled = false;
  datastoreModel.Datastore? datastore;

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(
      context,
      title,
      content,
    );
  }

  Future<void> loadDatastore() async {
    component.Loader.show(context);
    try {
      datastore = await managerDatastoreSetting.getSettingsDatastore();
    } on api_client.ServiceUnavailableException {
      if (context.mounted) {
        _showErrorDialog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on api_client.UnauthorizedException {
      user_service.logout();

      if (context.mounted) {
        Navigator.pushReplacementNamed(context, AppRoutes.signin);
      }
      return;
    } finally {
      component.Loader.hide();
    }

    for (var i = 0; i < datastore!.dataKV.length; i++) {
      if (datastore!.dataKV[i]['key'] == 'setting_password_length') {
        passwordLength.text = datastore!.dataKV[i]['value'].toString();
      }
      if (datastore!.dataKV[i]['key'] == 'setting_password_letters_uppercase') {
        lettersUppercase.text = datastore!.dataKV[i]['value'];
      }
      if (datastore!.dataKV[i]['key'] == 'setting_password_letters_lowercase') {
        lettersLowercase.text = datastore!.dataKV[i]['value'];
      }
      if (datastore!.dataKV[i]['key'] == 'setting_password_numbers') {
        numbers.text = datastore!.dataKV[i]['value'];
      }
      if (datastore!.dataKV[i]['key'] == 'setting_password_special_chars') {
        specialChars.text = datastore!.dataKV[i]['value'];
      }
    }
  }

  Future<void> initStateAsync() async {
    await loadDatastore();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    passwordLength.dispose();
    lettersUppercase.dispose();
    lettersLowercase.dispose();
    numbers.dispose();
    specialChars.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'PASSWORD_GENERATOR',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnPrimary(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              if (datastore != null) {
                for (var i = 0; i < datastore!.dataKV.length; i++) {
                  if (datastore!.dataKV[i]['key'] ==
                      'setting_password_length') {
                    datastore!.dataKV[i]['value'] = passwordLength.text;
                  }
                  if (datastore!.dataKV[i]['key'] ==
                      'setting_password_letters_uppercase') {
                    datastore!.dataKV[i]['value'] = lettersUppercase.text;
                  }
                  if (datastore!.dataKV[i]['key'] ==
                      'setting_password_letters_lowercase') {
                    datastore!.dataKV[i]['value'] = lettersLowercase.text;
                  }
                  if (datastore!.dataKV[i]['key'] ==
                      'setting_password_numbers') {
                    datastore!.dataKV[i]['value'] = numbers.text;
                  }
                  if (datastore!.dataKV[i]['key'] ==
                      'setting_password_special_chars') {
                    datastore!.dataKV[i]['value'] = specialChars.text;
                  }
                }

                reduxStore.dispatch(
                  PasswordGeneratorSettingAction(
                    passwordLength.text,
                    lettersUppercase.text,
                    lettersLowercase.text,
                    numbers.text,
                    specialChars.text,
                  ),
                );

                await datastore!.save();
              }

              final SnackBar snackBar = SnackBar(
                content: Text(FlutterI18n.translate(context, "SAVE_SUCCESS")),
              );

              // Find the ScaffoldMessenger in the widget tree
              // and use it to show a SnackBar.
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE"),
          ),
        ),
      ],
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              component.CustomTextField(
                controller: passwordLength,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                ],
                keyboardType: TextInputType.number,
                labelText: FlutterI18n.translate(
                  context,
                  'PASSWORD_LENGTH',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: lettersUppercase,
                validator: (value) {
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'LETTERS_UPPERCASE',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: lettersLowercase,
                validator: (value) {
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'LETTERS_LOWERCASE',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: numbers,
                validator: (value) {
                  return null;
                },
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                ],
                keyboardType: TextInputType.number,
                labelText: FlutterI18n.translate(
                  context,
                  'NUMBERS',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: specialChars,
                validator: (value) {
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'SPECIAL_CHARS',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
