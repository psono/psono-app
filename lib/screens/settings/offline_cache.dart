import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/offline_cache.dart' as offline_cache;
import 'package:sqlite3/sqlite3.dart';

class OfflineCacheDataSource extends DataTableSource {
  OfflineCacheDataSource(
    this.context,
    this.updateCacheSingle,
  );

  final BuildContext context;
  final void Function(String endpoint)? updateCacheSingle;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index > rowCount) return null;

    ResultSet? result = offline_cache.database?.select("""SELECT * FROM (
        SELECT
            ROW_NUMBER () OVER ( 
                ORDER BY endpoint
            ) row_num,
            write_date_milliseconds,
            endpoint
        FROM
            offline_cache
    ) t
    WHERE 
        row_num = ?""", [index + 1]);
    if (result == null) {
      return null;
    }
    Orientation orientation = MediaQuery.of(context).orientation;
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.68,
            child: component.TextDmSans(
              text: result.first['endpoint'],
              fontSize: orientation == Orientation.portrait ? 16.sp : 12.sp,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
        ),
        DataCell(
          IconButton(
            icon: Icon(
              Icons.refresh,
              color: Colors.white,
              size: orientation == Orientation.portrait ? 25.sp : 12.sp,
            ),
            onPressed: () {
              updateCacheSingle!(result.first['endpoint']);
            },
          ),
        ),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount {
    ResultSet? result =
        offline_cache.database?.select("""SELECT COUNT(*) as counter
      FROM offline_cache""");
    if (result == null) {
      return 0;
    }
    return result.first['counter'];
  }

  @override
  int get selectedRowCount => 0;
}

class SettingOfflineCacheScreen extends StatefulWidget {
  static String tag = 'settings-datastore-screen';

  @override
  _SettingOfflineCacheScreenState createState() =>
      _SettingOfflineCacheScreenState();
}

class _SettingOfflineCacheScreenState extends State<SettingOfflineCacheScreen> {
  bool initializing = offline_cache.database == null;
  bool? success;

  Future<void> updateCache() async {
    bool newSuccess = await offline_cache.incrementalUpdate(ratelimit: false);

    setState(() {
      success = newSuccess;
    });

    Timer(Duration(milliseconds: 5000), () {
      setState(() {
        success = null;
      });
    });
  }

  Future<void> updateCacheSingle(String endpoint) async {
    bool newSuccess = true;
    log("he");
    try {
      if (endpoint == '/datastore/') {
        await api_client.readDatastoreList(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
        );
      } else if (endpoint == '/share/right/') {
        await api_client.readShareRightsOverview(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
        );
      } else if (endpoint.startsWith('/share/')) {
        await api_client.readShare(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          endpoint.substring('/share/'.length, endpoint.length - 1),
        );
      } else if (endpoint.startsWith('/datastore/')) {
        await api_client.readDatastore(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          endpoint.substring('/datastore/'.length, endpoint.length - 1),
        );
      } else if (endpoint.startsWith('/secret/')) {
        await api_client.readSecret(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          endpoint.substring('/secret/'.length, endpoint.length - 1),
        );
      }
    } catch (e) {
      newSuccess = false;
    }

    setState(() {
      success = newSuccess;
    });

    Timer(Duration(milliseconds: 5000), () {
      setState(() {
        success = null;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      if (offline_cache.database == null) {
        setState(() {
          initializing = true;
        });

        await offline_cache.init();
        setState(() {
          initializing = offline_cache.database == null;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    if (success == true) {
      children.add(
        component.AlertInfo(
          text: FlutterI18n.translate(
            context,
            'SUCCESS_CACHE_UPDATED',
          ),
        ),
      );
    } else if (success == false) {
      children.add(
        component.AlertWarning(
          text: FlutterI18n.translate(
            context,
            'ERROR_CACHE_NOT_UPDATED',
          ),
        ),
      );
    }

    if (initializing == true) {
      children.add(
        component.AlertWarning(
          text: FlutterI18n.translate(
            context,
            'OFFLINE_CACHE_INITIALIZING_PLEASE_WAIT',
          ),
        ),
      );
    } else {
      children.add(
        Theme(
          data: Theme.of(context).copyWith(
            cardTheme: const CardTheme(
              color: Colors.transparent,
            ),
            dividerColor: Colors.white.withOpacity(0.20),
            textTheme: Theme.of(context).textTheme.copyWith(
                  bodySmall: const TextStyle(
                    color: Colors.white,
                  ),
                ),
          ),
          child: PaginatedDataTable(
            columnSpacing: 20.w,
            dataRowMinHeight: 55.h,
            dataRowMaxHeight: 70.h,
            showEmptyRows: false,
            headingRowHeight: 0.h,
            arrowHeadColor: Colors.white,
            columns: [
              DataColumn(
                label: Text(
                  FlutterI18n.translate(
                    context,
                    'ENDPOINT',
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  FlutterI18n.translate(
                    context,
                    'REFRESH',
                  ),
                ),
              ),
            ],
            source: OfflineCacheDataSource(
              context,
              updateCacheSingle,
            ),
            rowsPerPage: 10,
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'OFFLINE_CACHE',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh, color: Colors.white),
            onPressed: () async {
              component.Loader.show(context);
              await updateCache();
              component.Loader.hide();
            },
          )
        ],
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: ListView(
          shrinkWrap: true,
          children: children,
        ),
      ),
    );
  }
}
