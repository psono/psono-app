import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/manager_datastore.dart' as managerDatastore;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';

class SettingDatastoreScreen extends StatefulWidget {
  static String tag = 'settings-datastore-screen';

  @override
  _SettingDatastoreScreenState createState() => _SettingDatastoreScreenState();
}

class _SettingDatastoreScreenState extends State<SettingDatastoreScreen> {
  List<api_client.ReadDatastoreListEntry> _datastores = [];

  Future<void> loadDatastoreList() async {
    api_client.ReadDatastoreList? readDatastoreList =
        await managerDatastore.getDatastoreOverview();

    setState(() {
      _datastores = readDatastoreList!.datastores!
          .where((e) => e.type == 'password')
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();
    loadDatastoreList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'DATASTORES',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: ListView.builder(
        itemCount: _datastores.length,
        itemBuilder: (context, index) {
          final dataStore = _datastores[index];
          return Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: Colors.white.withOpacity(0.2),
                  width: 1,
                ),
              ),
            ),
            padding: EdgeInsets.symmetric(vertical: 14.h),
            child: Row(
              children: [
                Expanded(
                  flex: 100,
                  child: Padding(
                    padding: EdgeInsets.only(left: 20.w),
                    child: Text(
                      dataStore.description!,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize:
                            orientation == Orientation.portrait ? 16.sp : 12.sp,
                      ),
                    ),
                  ),
                ),
                const Spacer(),
                CupertinoSwitch(
                  value: dataStore.isDefault!,
                  trackColor: const Color(0xff787880).withOpacity(0.4),
                  activeColor: C.mountainMeadow,
                  onChanged: (!dataStore.isDefault!)
                      ? (value) async {
                          await managerDatastore.saveDatastoreMeta(
                              dataStore.id, dataStore.description, true);
                          loadDatastoreList();
                        }
                      : null,
                ),
              ],
            ),
          );
        },
      ),

      // DataTable(
      //   onSelectAll: (b) {},
      //   sortAscending: true,
      //   columns: <DataColumn>[
      //     DataColumn(
      //       label: Text(FlutterI18n.translate(
      //         context,
      //         'DESCRIPTION',
      //       )),
      //     ),
      //     DataColumn(
      //       label: Text(FlutterI18n.translate(
      //         context,
      //         'DEFAULT',
      //       )),
      //     ),
      //   ],
      //   rows: _datastores.map(
      //     (datastore) {
      //       var onChanged;
      //       if (!datastore.isDefault!) {
      //         onChanged = (value) async {
      //           await managerDatastore.saveDatastoreMeta(
      //               datastore.id, datastore.description, true);
      //           loadDatastoreList();
      //         };
      //       }

      //       return DataRow(
      //         cells: [
      //           DataCell(
      //             Text(datastore.description!),
      //             showEditIcon: false,
      //             placeholder: false,
      //           ),
      //           DataCell(Switch(
      //             value: datastore.isDefault!,
      //             onChanged: onChanged,
      //           )),
      //         ],
      //       );
      //     },
      //   ).toList(),
      // ),
    );
  }
}
