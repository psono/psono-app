import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/utils/app_assets.dart';

class SettingAutofillIOSScreen extends StatefulWidget {
  static String tag = 'settings-autofill-screen';

  @override
  _SettingAutofillIOSScreenState createState() =>
      _SettingAutofillIOSScreenState();
}

class _SettingAutofillIOSScreenState extends State<SettingAutofillIOSScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'AUTOFILL_SERVICE',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: [
              component.AlertInfo(
                text: FlutterI18n.translate(
                  context,
                  "FOLLOW_STEPS_TO_ENABLE_AUTOFILL_SERVICE_IOS",
                ),
              ),
              SizedBox(height: 30.h),
              _Step(
                iconPath: AppAssets.iconSettings,
                text: FlutterI18n.translate(context, 'OPEN_SETTINGS_APP'),
              ),
              SizedBox(height: 34.h),
              _Step(
                iconPath: AppAssets.iconPasswordsAccounts,
                text: FlutterI18n.translate(
                  context,
                  "TAP_INTO_PASSWORDS_AND_ACCOUNTS",
                ),
              ),
              SizedBox(height: 34.h),
              _Step(
                iconPath: AppAssets.iconAutofill,
                text: FlutterI18n.translate(
                  context,
                  "TAP_INTO_AUTOFILL_PASSWORDSS",
                ),
              ),
              SizedBox(height: 34.h),
              _Step(
                iconPath: AppAssets.iconOn,
                text: FlutterI18n.translate(
                  context,
                  "TURN_ON_AUTOFILL_PASSWORDS",
                ),
              ),
              SizedBox(height: 34.h),
              _Step(
                iconPath: AppAssets.iconPsonoApp,
                text: FlutterI18n.translate(
                  context,
                  "SELECT_PSONO",
                ),
              ),
              SizedBox(height: 34.h),
              _Step(
                iconPath: AppAssets.iconPasswordsAccounts,
                text: FlutterI18n.translate(
                  context,
                  "DESELECT_KEYCHAIN",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _Step extends StatelessWidget {
  const _Step({
    Key? key,
    required this.iconPath,
    required this.text,
  }) : super(key: key);

  final String iconPath;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image(
          image: AssetImage(iconPath),
          height: 40.h,
          width: 40.h,
          fit: BoxFit.fitWidth,
        ),
        SizedBox(width: 20.w),
        Expanded(
          flex: 10,
          child: TextDmSans(
            text: text,
            color: Colors.white,
            fontSize: 16.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
