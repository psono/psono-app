import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/autofill.dart' as autofill_service;
import 'package:psono/utils/app_assets.dart';

class SettingAutofillAndroidScreen extends StatefulWidget {
  static String tag = 'settings-autofill-screen';

  @override
  _SettingAutofillAndroidScreenState createState() =>
      _SettingAutofillAndroidScreenState();
}

class _SettingAutofillAndroidScreenState
    extends State<SettingAutofillAndroidScreen> {
  bool? enabled = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      bool? autofillEnabled = await autofill_service.isEnabled();
      if (enabled != autofillEnabled) {
        setState(() {
          enabled = autofillEnabled;
        });
      }
    });

    Widget button;
    if (enabled!) {
      button = component.BtnWarning(
        onPressed: () async {
          autofill_service.disable();
          setState(() {
            enabled = false;
          });
        },
        text: FlutterI18n.translate(context, "DEACTIVATE"),
      );
    } else {
      button = component.BtnPrimary(
        onPressed: () async {
          autofill_service.enable();
          Navigator.pop(context);
        },
        text: FlutterI18n.translate(context, "ACTIVATE"),
      );
    }

    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'AUTOFILL_SERVICE',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: button,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
