import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/screens/pin_configuration/pin_configuration_screen.dart';
import 'package:psono/services/autofill.dart' as autofill_service;
import 'package:psono/utils/app_assets.dart';

import './autofill_android.dart';
import './autofill_ios.dart';
import './datastore.dart';
import './entry_types.dart';
import './offline_cache.dart';
import './password_generator.dart';

class SettingsScreen extends StatefulWidget {
  static String tag = 'settings-screen';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool? _autofillSupported = false;

  Future<void> initStateAsync() async {
    bool? autofillSupported = await autofill_service.isSupported();

    setState(() {
      _autofillSupported = autofillSupported;
    });
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      initStateAsync();
    });
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    List<Widget> settings = [
      _Item(
        text: FlutterI18n.translate(context, 'PASSWORD_GENERATOR'),
        leadingImage: AppAssets.iconLock3,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SettingPasswordGenerator(),
            ),
          );
        },
      ),
      _Item(
        text: FlutterI18n.translate(context, 'ENTRY_TYPES'),
        leadingImage: AppAssets.iconPassword,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SettingEntryTypes(),
            ),
          );
        },
      ),
      _Item(
        text: FlutterI18n.translate(context, 'CHANGE_PIN'),
        leadingImage: AppAssets.iconLock4,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PinConfigurationScreen(),
            ),
          );
        },
      ),
      _Item(
        text: FlutterI18n.translate(context, 'DATASTORES'),
        leadingImage: AppAssets.iconDatabases,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SettingDatastoreScreen(),
            ),
          );
        },
      ),
    ];

    if (!reduxStore.state.complianceDisableOfflineMode) {
      settings.add(
        _Item(
          text: FlutterI18n.translate(context, 'OFFLINE_CACHE'),
          leadingImage: AppAssets.iconOfflineCache,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingOfflineCacheScreen(),
              ),
            );
          },
        ),
      );
    }

    if (_autofillSupported! && Platform.isAndroid) {
      settings.add(
        _Item(
          text: FlutterI18n.translate(context, 'AUTOFILL_SERVICE'),
          leadingImage: AppAssets.iconSettings2,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingAutofillAndroidScreen(),
              ),
            );
          },
        ),
      );
    }

    if (_autofillSupported! && Platform.isIOS) {
      settings.add(
        _Item(
          text: FlutterI18n.translate(context, 'AUTOFILL_SERVICE'),
          leadingImage: AppAssets.iconSettings2,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingAutofillIOSScreen(),
              ),
            );
          },
        ),
      );
    }

    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: component.TextDmSans(
                text: FlutterI18n.translate(context, 'SETTINGS'),
                color: Colors.white,
                fontSize: orientation == Orientation.portrait ? 40.sp : 20.sp,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 32.h),
            Container(
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    width: 1,
                    strokeAlign: BorderSide.strokeAlignCenter,
                    color: Colors.white.withOpacity(0.2),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 100,
              child: ListView(
                children: settings,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  const _Item({
    super.key,
    required this.text,
    required this.leadingImage,
    this.onTap,
  });
  final String text;
  final String leadingImage;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Padding(
      padding: EdgeInsets.only(bottom: 12.h),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12.h),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.white.withOpacity(0.2),
                width: 1,
              ),
            ),
          ),
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.all(8.w),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.10),
                  borderRadius: BorderRadius.circular(8.w),
                ),
                child: Image.asset(
                  leadingImage,
                  width: orientation == Orientation.portrait ? 24.w : 18.w,
                  height: orientation == Orientation.portrait ? 24.w : 18.w,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(width: 12.w),
              component.TextDmSans(
                text: text,
                color: Colors.white,
                fontSize: orientation == Orientation.portrait ? 16.sp : 12.sp,
                fontWeight: FontWeight.w500,
              ),
              const Spacer(),
              IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                  size: orientation == Orientation.portrait ? 20.sp : 16.sp,
                ),
                onPressed: onTap,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
