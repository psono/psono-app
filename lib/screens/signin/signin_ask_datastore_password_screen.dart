part of 'signin_screen.dart';

class _SigninAskDatastorePasswordScreen extends StatefulWidget {
  const _SigninAskDatastorePasswordScreen(
      {Key? key,
      required this.passwordController,
      required this.decrypt,
      required this.cancelBtn})
      : super(key: key);
  final TextEditingController passwordController;
  final Function decrypt;
  final Widget cancelBtn;
  @override
  State<_SigninAskDatastorePasswordScreen> createState() =>
      _SigninAskDatastorePasswordScreenState();
}

class _SigninAskDatastorePasswordScreenState
    extends State<_SigninAskDatastorePasswordScreen> {
  bool _obscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Column(
        children: <Widget>[
          const Spacer(flex: 2),
          const _Logo(),
          const Spacer(flex: 2),
          TextDmSans(
            text: FlutterI18n.translate(
              context,
              'ENTER_PASSWORD_TO_DECRYPT_YOUR_DATASTORE',
            ),
            textAlign: TextAlign.center,
            color: Colors.white,
            fontSize: 24.sp,
            fontWeight: FontWeight.w500,
          ),
          const Spacer(flex: 2),
          component.CustomTextField(
            controller: widget.passwordController,
            autofocus: true,
            obscureText: _obscurePassword,
            suffixIcon: component.ObscureIcon(
              onTap: () {
                setState(() {
                  _obscurePassword = !_obscurePassword;
                });
              },
              obscure: _obscurePassword,
            ),
            labelText: FlutterI18n.translate(context, "PASSWORD"),
          ),
          const Spacer(flex: 2),
          component.BtnPrimary(
            onPressed: () {
              widget.decrypt();
            },
            text: FlutterI18n.translate(context, "DECRYPT").toUpperCase(),
          ),
          const Spacer(flex: 2),
          widget.cancelBtn,
          const Spacer(flex: 10),
        ],
      ),
    );
  }
}
