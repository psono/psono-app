part of 'signin_screen.dart';

class _SigninUnsupportedServerVersion extends StatefulWidget {
  const _SigninUnsupportedServerVersion({Key? key, required this.onCancelTap})
      : super(key: key);
  final Function() onCancelTap;
  @override
  State<_SigninUnsupportedServerVersion> createState() =>
      __SigninUnsupportedServerVersionState();
}

class __SigninUnsupportedServerVersionState
    extends State<_SigninUnsupportedServerVersion> {
  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: Column(
          children: [
            const Spacer(flex: 2),
            const _Logo(),
            const Spacer(flex: 2),
            TextDmSans(
              text: FlutterI18n.translate(context, "SERVER_UNSUPPORTED"),
              textAlign: TextAlign.center,
              color: Colors.white,
              fontSize: 32.sp,
              fontWeight: FontWeight.w500,
            ),
            const Spacer(flex: 1),
            component.AlertDanger(
              text: FlutterI18n.translate(
                context,
                "THE_VERSION_OF_THE_SERVER_IS_TOO_OLD_AND_NOT_SUPPORTED_PLEASE_UPGRADE",
              ),
            ),
            const Spacer(flex: 1),
            component.BtnPrimary(
              text: FlutterI18n.translate(context, "BACK").toUpperCase(),
              onPressed: () {
                widget.onCancelTap();
              },
            ),
            const Spacer(flex: 4),
          ],
        ),
      ),
    );
  }
}
