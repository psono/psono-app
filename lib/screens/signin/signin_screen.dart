// ignore_for_file: prefer_interpolation_to_compose_strings

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/model/config.dart';
import 'package:psono/model/login_result_decrypted.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_host.dart' as managerHost;
import 'package:psono/services/storage.dart';
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_constants.dart';

part 'signin_ask_datastore_password_screen.dart';
part 'signin_ask_send_plain.dart';
part 'signin_default.dart';
part 'signin_ivalt.dart';
part 'signin_new_server.dart';
part 'signin_pick_second_factor.dart';
part 'signin_second_factor_screen.dart';
part 'signin_server_signature_changed.dart';
part 'signin_unsupported_server_version.dart';

enum _ScreenType {
  defaultScreen,
  askSendPlain,
  approveNewServer,
  serverSignatureChanged,
  unsupportedServerVersion,
  googleAuthenticator2fa,
  duo2fa,
  yubikeyOtp2fa,
  ivalt2fa,
  pickSecondFactor,
  askDatastoreDecryptionPassword,
}

class SigninScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final _usernameController = TextEditingController(
    text: reduxStore.state.username,
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordController2 = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );
  final yubikeyOTPTokenController = TextEditingController(
    text: '',
  );
  final googleAuthenticatorCodeController = TextEditingController(
    text: '',
  );
  final duoCodeController = TextEditingController(
    text: '',
  );

  bool? _complianceEnforce2fa;
  LoginResultDecrypted? _loginResultDecrypted;
  _ScreenType _screen = _ScreenType.defaultScreen;
  List<String> _requiredMultifactors = [];
  bool? multifactorEnabled = true;
  List<String>? _authenticationMethods = [];
  String _loginType = '';
  SamlProvider? _samlProvider;
  OidcProvider? _oidcProvider;
  late api_client.Info info;
  GlobalKey<_SigninIvaltState> _iValtScreenKey = GlobalKey<_SigninIvaltState>();

  String _domainSuffix = "@" +
      (reduxStore.state.config != null &&
              reduxStore.state.config.configJson != null &&
              reduxStore.state.config.configJson.backendServers != null &&
              reduxStore.state.config.configJson.backendServers.length > 0 &&
              reduxStore.state.config.configJson.backendServers[0].domain !=
                  null
          ? reduxStore.state.config.configJson.backendServers[0].domain
          : helper.getDomain(reduxStore.state.serverUrl)!);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    _usernameController.dispose();
    _passwordController.dispose();
    _passwordController2.dispose();
    _serverUrlController.dispose();
    yubikeyOTPTokenController.dispose();
    googleAuthenticatorCodeController.dispose();
    duoCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final cancelButton = component.BtnText(
      text: FlutterI18n.translate(context, "CANCEL"),
      onPressed: _cancelButtonTap,
      color: Colors.white.withOpacity(0.60),
      fontSize: 14.sp,
      fontWeight: FontWeight.w700,
    );

    if (_screen == _ScreenType.askSendPlain) {
      return _SignInAskSendPlain(initiateLogin: (value) {
        initiateLogin(value, info);
      });
    } else if (_screen == _ScreenType.approveNewServer) {
      return _SigninNewServer(
        onApproveTap: _newServerApproveTap,
        cancelButton: cancelButton,
      );
    } else if (_screen == _ScreenType.serverSignatureChanged) {
      return _SigninServerSignatureChanged(
        onConfirmTap: _ignoreAndContinueTap,
        onCancelTap: _cancelButtonTap,
      );
    } else if (_screen == _ScreenType.unsupportedServerVersion) {
      return _SigninUnsupportedServerVersion(
        onCancelTap: _cancelButtonTap,
      );
    } else if (_screen == _ScreenType.pickSecondFactor) {
      return _SignInPickSecondFactor(
        requiredMultifactors: _requiredMultifactors,
        cancelButton: cancelButton,
        onGoogleAuthenticatorTap: showGa2faForm,
        onYubikeyOtpTap: showYubikeyOtp2faForm,
        onDuoTap: showDuo2faForm,
        onIvaltTap: showIvalt2faForm,
      );
    } else if (_screen == _ScreenType.yubikeyOtp2fa ||
        _screen == _ScreenType.googleAuthenticator2fa ||
        _screen == _ScreenType.duo2fa) {
      return _SigninSecondFactorScreen(
        screenType: _screen,
        yubikeyOTPTokenController: yubikeyOTPTokenController,
        googleAuthenticatorTokenController: googleAuthenticatorCodeController,
        duoTokenController: duoCodeController,
        sendYubiKeyToken: _sendYubiKeyToken,
        sendGoogleAuthenticatorCode: sendGoogleAuthenticatorCode,
        sendDuoCode: sendDuoCode,
        cancelBtn: cancelButton,
      );
    } else if (_screen == _ScreenType.askDatastoreDecryptionPassword) {
      return _SigninAskDatastorePasswordScreen(
        passwordController: _passwordController2,
        decrypt: _decrypt,
        cancelBtn: cancelButton,
      );
    } else if (_screen == _ScreenType.ivalt2fa) {
      return _SigninIvalt(
        key: _iValtScreenKey,
        cancelButton: cancelButton,
        initialTime: 120,
        onCancelCallback: _cancelButtonTap,
        onTenSecondCallback: () async {
          bool result = await _performVerification();
          if (result) {
            _cancelButtonTap();
            nextLoginStep([], true);
          }
        },
      );
    } else {
      return _DefaultSignin(
        serverUrlController: _serverUrlController,
        usernameController: _usernameController,
        passwordController: _passwordController,
        domainSuffix: _domainSuffix,
        initiateOidcLogin: initiateOidcLogin,
        initiateSamlLogin: initiateSamlLogin,
        oidcProvider: _oidcProvider,
        samlProvider: _samlProvider,
        screen: _screen,
        multifactorEnabled: multifactorEnabled,
        onLoginTap: _onLoginTap,
        loginType: _loginType,
        authenticationMethods: _authenticationMethods,
        onInfoChange: (info) {
          this.info = info;
        },
        onLoginTypeChange: (loginType) =>
            setState(() => _loginType = loginType),
        onSamlProviderChange: (samlProvider) =>
            setState(() => _samlProvider = samlProvider),
        onOidcProviderChange: (oidcProvider) =>
            setState(() => _oidcProvider = oidcProvider),
        onScreenChange: (screen) => setState(() => _screen = screen),
        onAuthenticationMethodsChange: (authenticationMethods) =>
            setState(() => _authenticationMethods = authenticationMethods),
        onDomainSuffixChange: (domainSuffix) =>
            setState(() => _domainSuffix = domainSuffix),
        onMultifactorEnabledChange: (multifactorEnabled) =>
            setState(() => this.multifactorEnabled = multifactorEnabled),
      );
    }
  }

  _cancelButtonTap() {
    setState(() {
      _screen = _ScreenType.defaultScreen;
    });
  }

  _ignoreAndContinueTap() async {
    await managerHost.approveHost(
      reduxStore.state.serverUrl,
      reduxStore.state.verifyKey,
    );
    if (_authenticationMethods!.contains('LDAP')) {
      setState(() {
        _screen = _ScreenType.askSendPlain;
      });
    } else {
      initiateLogin(false, info);
    }
  }

  _decrypt() async {
    component.Loader.show(context);

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.decodeKeys(
        _loginResultDecrypted!,
        _passwordController2.text,
      );
    } on CryptoException {
      helper.showErrorDialog(context, 'ERROR', 'PASSWORD_INCORRECT');
      component.Loader.hide();
      return;
    }

    if (requiredMultifactors!.isEmpty && _complianceEnforce2fa!) {
      helper.showErrorDialog(context, 'SECOND_FACTOR_REQUIRED',
          'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
      component.Loader.hide();
      return;
    }

    nextLoginStep(requiredMultifactors, false);
  }

  _newServerApproveTap() async {
    await managerHost.approveHost(
        reduxStore.state.serverUrl, reduxStore.state.verifyKey);
    if (_authenticationMethods!.contains('LDAP')) {
      setState(() {
        _screen = _ScreenType.askSendPlain;
      });
    } else {
      if (_loginType == 'SAML') {
        initiateSamlLogin(_samlProvider!, info);
      } else if (_loginType == 'OIDC') {
        initiateOidcLogin(_oidcProvider!, info);
      } else {
        initiateLogin(false, info);
      }
    }
  }

  void _onLoginTap() async {
    try {
      await storage.write(
        key: SK.serverUrl,
        value: _serverUrlController.text,
        iOptions: secureIOSOptions,
      );
      await storage.write(
        key: SK.username,
        value: _usernameController.text + _domainSuffix,
        iOptions: secureIOSOptions,
      );

      reduxStore.dispatch(
        InitiateLoginAction(
          _serverUrlController.text,
          _usernameController.text + _domainSuffix,
        ),
      );
      info = await api_client.info();
    } on api_client.ServiceUnavailableException {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on HandshakeException {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
          FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
        );
      }
      return;
    } catch (e) {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    }

    var checkHostResult =
        await managerHost.checkHost(_serverUrlController.text, info);

    await storage.write(
      key: SK.complianceServerSecrets,
      value: info.complianceServerSecrets.toString(),
      iOptions: secureIOSOptions,
    );
    await storage.write(
      key: SK.complianceDisableDeleteAccount,
      value: info.complianceDisableDeleteAccount.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: SK.complianceDisableOfflineMode,
      value: info.complianceDisableOfflineMode.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: SK.complianceMaxOfflineCacheTimeValid,
      value: info.complianceMaxOfflineCacheTimeValid.toString(),
      iOptions: secureIOSOptions,
    );

    reduxStore.dispatch(
      SetVerifyKeyAction(
        checkHostResult.info!.verifyKey,
        info.verifyKey,
        info.complianceServerSecrets,
        info.complianceDisableDeleteAccount,
        info.complianceDisableOfflineMode,
        info.complianceMaxOfflineCacheTimeValid,
      ),
    );

    setState(() {
      multifactorEnabled = checkHostResult.info!.multifactorEnabled;
    });

    if (checkHostResult.status == 'matched') {
      if (info.authenticationMethods!.contains('LDAP')) {
        setState(() {
          _screen = _ScreenType.askSendPlain;
        });
      } else {
        initiateLogin(false, info);
      }
      return;
    } else if (checkHostResult.status == 'new_server') {
      setState(() {
        _authenticationMethods = info.authenticationMethods;
        _screen = _ScreenType.approveNewServer;
      });
      return;
    } else if (checkHostResult.status == 'signature_changed') {
      setState(() {
        _authenticationMethods = info.authenticationMethods;
        _screen = _ScreenType.serverSignatureChanged;
      });
      return;
    } else if (checkHostResult.status == 'unsupported_server_version') {
      setState(() {
        _authenticationMethods = info.authenticationMethods;
        _screen = _ScreenType.unsupportedServerVersion;
      });
      return;
    } else {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "INVALID_SIGNATURE"),
        FlutterI18n.translate(context, "INVALID_SIGNATURE_DETAILS"),
      );
    }
  }

  Future<void> _startNFC() async {
    bool isAvailable = await NfcManager.instance.isAvailable();
    if (!isAvailable) {
      return;
    }

    NfcManager.instance.startSession(
      onDiscovered: (NfcTag tag) async {
        Ndef? ndef = Ndef.from(tag);
        if (ndef == null) {
          // Tag is not compatible with NDEF
          return;
        }
        // a yubikey NFC always contains the token at the end which is always 44 chars long
        String token = utf8.decode(ndef.cachedMessage!.records[0].payload);
        if (token.length >= 44) {
          token = token.substring(token.length - 44);
        }
        yubikeyOTPTokenController.text = token;
        _sendYubiKeyToken();
      },
    );
  }

  _sendYubiKeyToken() async {
    component.Loader.show(context);

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.yubikeyOtpVerify(
        yubikeyOTPTokenController.text,
      );
    } on api_client.BadRequestException catch (e) {
      component.Loader.hide();
      setState(() {
        _screen = _ScreenType.yubikeyOtp2fa;
      });
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      _startNFC();
      return;
    }

    requiredMultifactors!.remove('yubikey_otp_2fa');

    nextLoginStep(requiredMultifactors, true);
  }

  void nextLoginStep(
      List<String> requiredMultifactors, bool solvedTwoFa) async {
    setState(() {
      _requiredMultifactors = requiredMultifactors;
    });

    if (requiredMultifactors.isEmpty ||
        (solvedTwoFa && multifactorEnabled == false)) {
      component.Loader.show(context);
      await user_service.activateToken();
      if (context.mounted) {
        component.Loader.hide();
        Navigator.pushReplacementNamed(context, AppRoutes.datastore);
      }
      return;
    }

    if (multifactorEnabled == false && requiredMultifactors.length > 1) {
      component.Loader.hide();
      setState(() {
        _screen = _ScreenType.pickSecondFactor;
      });
    } else {
      String nextMultifactor = requiredMultifactors[0];

      if (nextMultifactor == 'google_authenticator_2fa') {
        showGa2faForm();
      } else if (nextMultifactor == 'duo_2fa') {
        showDuo2faForm();
      } else if (nextMultifactor == 'yubikey_otp_2fa') {
        showYubikeyOtp2faForm();
      } else if (nextMultifactor == 'ivalt_2fa') {
        showIvalt2faForm();
      } else {
        component.Loader.hide();
        helper.showErrorDialog(context, 'ERROR',
            'UNKNOWN_MULTIFACTOR_METHOD_REQUESTED_PLEASE_UPDATE_CLIENT');
        setState(() {
          _screen = _ScreenType.defaultScreen;
        });
      }
    }
  }

  void showGa2faForm() {
    component.Loader.hide();
    setState(() {
      _screen = _ScreenType.googleAuthenticator2fa;
    });
  }

  void showYubikeyOtp2faForm() {
    component.Loader.hide();
    _startNFC();
    setState(() {
      _screen = _ScreenType.yubikeyOtp2fa;
    });
  }

  void showIvalt2faForm() async {
    component.Loader.show(context);
    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.ivaltVerifyNotification();
    } on api_client.BadRequestException catch (e) {
      component.Loader.hide();
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      return;
    }
    requiredMultifactors!.remove('ivalt_2fa');
    component.Loader.hide();
    setState(() {
      _screen = _ScreenType.ivalt2fa;
    });
  }

  Future<bool> _performVerification() async {
    try {
      var response = await user_service.ivaltVerifyVerification();
      component.Loader.hide();
      return true;
    } on api_client.BadRequestException catch (e) {
      if (e.toString() == 'INVALID_GEOFENCE' || e.toString() == 'INVALID_TIMEZONE') {
        _iValtScreenKey.currentState?.cancelTimerAndShowError(e.toString());
      }
      component.Loader.hide();
      return false;
    } catch (e) {
      component.Loader.hide();
      return false;
    }
  }

  void showDuo2faForm() {
    component.Loader.hide();
    _sendDuoCodeBackground();
    setState(() {
      _screen = _ScreenType.duo2fa;
    });
  }

  _sendDuoCodeBackground() async {
    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.duoVerify(
        duoCodeController.text,
      );
    } on api_client.BadRequestException {
      return;
    }

    requiredMultifactors!.remove('duo_2fa');

    nextLoginStep(requiredMultifactors, true);
  }

  void initiateLogin(bool sendPlain, api_client.Info info) async {
    setState(() {
      _screen = _ScreenType.defaultScreen;
      _loginType = '';
    });

    component.Loader.show(context);
    LoginResultDecrypted loginResultDecrypted;
    try {
      loginResultDecrypted = await user_service.login(
        _usernameController.text + _domainSuffix,
        (reduxStore.state.config != null &&
                reduxStore.state.config.configJson != null &&
                reduxStore.state.config.configJson.backendServers != null &&
                reduxStore.state.config.configJson.backendServers.length > 0 &&
                reduxStore.state.config.configJson.backendServers[0].domain !=
                    null
            ? reduxStore.state.config.configJson.backendServers[0].domain
            : helper.getDomain(_serverUrlController.text)),
        _passwordController.text,
        info,
        sendPlain,
      );
    } on api_client.BadRequestException catch (e) {
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      component.Loader.hide();
      return;
    }

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.decodeKeys(
        loginResultDecrypted,
        _passwordController.text,
      );
    } on CryptoException {
      setState(() {
        _complianceEnforce2fa = info.complianceEnforce2fa;
        _loginResultDecrypted = loginResultDecrypted;
        _screen = _ScreenType.askDatastoreDecryptionPassword;
      });
      component.Loader.hide();
      return;
    }

    if (requiredMultifactors!.isEmpty && info.complianceEnforce2fa!) {
      helper.showErrorDialog(context, 'SECOND_FACTOR_REQUIRED',
          'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
      component.Loader.hide();
      return;
    }

    nextLoginStep(requiredMultifactors, false);
  }

  void initiateSamlLogin(SamlProvider provider, api_client.Info info) async {
    var screenSize = MediaQuery.of(context).size;

    String samlTokenId =
        await user_service.samlInitiateLogin(context, provider, screenSize);

    LoginResultDecrypted loginResultDecrypted;
    component.Loader.show(context);
    try {
      loginResultDecrypted = await user_service.samlLogin(samlTokenId, info);
    } on api_client.BadRequestException catch (e) {
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      component.Loader.hide();
      return;
    }

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.decodeKeys(
        loginResultDecrypted,
        _passwordController.text,
      );
    } on CryptoException {
      setState(() {
        _complianceEnforce2fa = info.complianceEnforce2fa;
        _loginResultDecrypted = loginResultDecrypted;
        _screen = _ScreenType.askDatastoreDecryptionPassword;
      });
      component.Loader.hide();
      return;
    }

    if (requiredMultifactors!.isEmpty && info.complianceEnforce2fa!) {
      helper.showErrorDialog(context, 'SECOND_FACTOR_REQUIRED',
          'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
      component.Loader.hide();
      return;
    }

    nextLoginStep(requiredMultifactors, false);
  }

  void initiateOidcLogin(OidcProvider provider, api_client.Info info) async {
    var screenSize = MediaQuery.of(context).size;

    String oidcTokenId =
        await user_service.oidcInitiateLogin(context, provider, screenSize);

    LoginResultDecrypted loginResultDecrypted;
    component.Loader.show(context);
    try {
      loginResultDecrypted = await user_service.oidcLogin(oidcTokenId, info);
    } on api_client.BadRequestException catch (e) {
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      component.Loader.hide();
      return;
    }

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.decodeKeys(
        loginResultDecrypted,
        _passwordController.text,
      );
    } on CryptoException {
      setState(() {
        _complianceEnforce2fa = info.complianceEnforce2fa;
        _loginResultDecrypted = loginResultDecrypted;
        _screen = _ScreenType.askDatastoreDecryptionPassword;
      });
      component.Loader.hide();
      return;
    }
    if (requiredMultifactors!.isEmpty && info.complianceEnforce2fa!) {
      helper.showErrorDialog(context, 'SECOND_FACTOR_REQUIRED',
          'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
      component.Loader.hide();
      return;
    }

    nextLoginStep(requiredMultifactors, false);
  }

  void sendGoogleAuthenticatorCode() async {
    component.Loader.show(context);

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.gaVerify(
        googleAuthenticatorCodeController.text,
      );
    } on api_client.BadRequestException catch (e) {
      component.Loader.hide();
      setState(() {
        _screen = _ScreenType.googleAuthenticator2fa;
      });
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      return;
    }

    requiredMultifactors!.remove('google_authenticator_2fa');

    nextLoginStep(requiredMultifactors, true);
  }

  void sendDuoCode() async {
    component.Loader.show(context);

    List<String>? requiredMultifactors;
    try {
      requiredMultifactors = await user_service.duoVerify(
        duoCodeController.text,
      );
    } on api_client.BadRequestException catch (e) {
      component.Loader.hide();
      setState(() {
        _screen = _ScreenType.duo2fa;
      });
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      return;
    }

    requiredMultifactors!.remove('duo_2fa');

    nextLoginStep(requiredMultifactors, true);
  }
}

class _Logo extends StatelessWidget {
  const _Logo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'hero',
      child: Image.asset(
        AppAssets.logoRound,
        height: 80.w,
        width: 80.w,
      ),
    );
  }
}
