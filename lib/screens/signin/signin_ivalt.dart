part of 'signin_screen.dart';

class _SigninIvalt extends StatefulWidget {
  const _SigninIvalt({
    super.key,
    required this.cancelButton,
    required this.initialTime,
    required this.onCancelCallback,
    required this.onTenSecondCallback,
  });

  final Widget cancelButton;
  final int? initialTime;
  final VoidCallback? onCancelCallback;
  final VoidCallback? onTenSecondCallback;

  @override
  State<_SigninIvalt> createState() => _SigninIvaltState();
}

class _SigninIvaltState extends State<_SigninIvalt> {
  Timer? timer;
  var actualTimeLeft = 0;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  void dispose() {
    super.dispose();
    timer?.cancel();
  }

  void startTimer() {
    actualTimeLeft = widget.initialTime!;
    timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      if (actualTimeLeft > 0) {
        setState(() {
          actualTimeLeft--;
        });
        if (actualTimeLeft % 10 == 0) {
          widget.onTenSecondCallback?.call();
        }
      } else {
        if (actualTimeLeft == 0) {
          helper.showErrorDialog(
            context,
            'FAILED',
            'IVALT_AUTH_TIMEOUT',
            onCloseCallback: () {
              widget.onCancelCallback?.call();
            },
          );
        }
        t.cancel();
      }
    });
  }

  void cancelTimerAndShowError(String error) {
    timer?.cancel();
    helper.showErrorDialog(context, 'FAILED', error, onCloseCallback: () {
      widget.onCancelCallback?.call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Spacer(flex: 2),
          const _Logo(),
          const Spacer(flex: 2),
          TextDmSans(
            text: FlutterI18n.translate(
              context,
              "WAITING",
            ),
            textAlign: TextAlign.center,
            color: Colors.white,
            fontSize: 24.sp,
            fontWeight: FontWeight.w500,
          ),
          const Spacer(flex: 1),
          component.AlertInfo(
            text: FlutterI18n.translate(
              context,
              "WAITING_IVALT_AUTHENTICATION",
            ),
          ),
          const Spacer(flex: 1),
          TextDmSans(
            text: '$actualTimeLeft',
            color: Colors.white,
            fontSize: 14.sp,
            fontWeight: FontWeight.w300,
            textAlign: TextAlign.center,
          ),
          const Spacer(flex: 2),
          widget.cancelButton,
          const Spacer(flex: 10),
        ],
      ),
    );
  }
}
