part of 'signin_screen.dart';

class _SigninSecondFactorScreen extends StatefulWidget {
  const _SigninSecondFactorScreen(
      {Key? key,
      required this.screenType,
      required this.yubikeyOTPTokenController,
      required this.googleAuthenticatorTokenController,
      required this.duoTokenController,
      required this.sendYubiKeyToken,
      required this.sendGoogleAuthenticatorCode,
      required this.sendDuoCode,
      required this.cancelBtn})
      : super(key: key);
  final _ScreenType screenType;
  final TextEditingController yubikeyOTPTokenController;
  final TextEditingController googleAuthenticatorTokenController;
  final TextEditingController duoTokenController;
  final Function sendYubiKeyToken;
  final Function sendGoogleAuthenticatorCode;
  final Function sendDuoCode;
  final Widget cancelBtn;
  @override
  State<_SigninSecondFactorScreen> createState() =>
      _SigninSecondFactorScreenState();
}

class _SigninSecondFactorScreenState extends State<_SigninSecondFactorScreen> {
  bool get isGoogle2fa =>
      widget.screenType == _ScreenType.googleAuthenticator2fa;
  bool get isYubi2fa => widget.screenType == _ScreenType.yubikeyOtp2fa;
  bool get isDuo2fa => widget.screenType == _ScreenType.duo2fa;
  TextEditingController get textEditingController {
    if (isGoogle2fa) {
      return widget.googleAuthenticatorTokenController;
    }
    if (isYubi2fa) {
      return widget.yubikeyOTPTokenController;
    }
    return widget.duoTokenController;
  }

  String get headingText {
    if (isGoogle2fa) {
      return "ENTER_YOUR_GOOGLE_AUTHENTICATOR_CODE";
    }
    if (isYubi2fa) {
      return "ENTER_YOUR_YUBIKEY_OTP_TOKEN";
    }

    return "APPROVE_THE_PUSH_NOTIFICATION";
  }

  Function get sendFunction {
    if (isGoogle2fa) {
      return widget.sendGoogleAuthenticatorCode;
    }
    if (isYubi2fa) {
      return widget.sendYubiKeyToken;
    }
    return widget.sendDuoCode;
  }

  String get textFieldText {
    if (isYubi2fa) {
      return "YUBIKEY_TOKEN";
    }
    if (isGoogle2fa) {
      return "GOOGLE_AUTHENTICATOR_CODE";
    }
    return "DUO_CODE";
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Column(
        children: <Widget>[
          const Spacer(flex: 2),
          const _Logo(),
          const Spacer(flex: 2),
          TextDmSans(
            text: FlutterI18n.translate(
              context,
              headingText,
            ),
            textAlign: TextAlign.center,
            color: Colors.white,
            fontSize: 24.sp,
            fontWeight: FontWeight.w500,
          ),
          const Spacer(flex: 2),
          component.CustomTextField(
            controller: textEditingController,
            autofocus: true,
            maxLines: 4,
            labelText: FlutterI18n.translate(context, textFieldText),
          ),
          const Spacer(flex: 2),
          component.BtnPrimary(
            onPressed: () {
              sendFunction();
            },
            text: FlutterI18n.translate(context, "SEND").toUpperCase(),
          ),
          const Spacer(flex: 2),
          widget.cancelBtn,
          const Spacer(flex: 10),
        ],
      ),
    );
  }
}

class _CopyIcon extends StatelessWidget {
  const _CopyIcon({
    Key? key,
    required this.textEditingController,
  }) : super(key: key);

  final TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Icon(
        Icons.copy_outlined,
        color: Colors.white.withOpacity(0.50),
      ),
      onTap: () {
        Clipboard.setData(
          ClipboardData(text: textEditingController.text),
        );
      },
    );
  }
}
