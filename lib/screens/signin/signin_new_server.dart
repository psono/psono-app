part of 'signin_screen.dart';

class _SigninNewServer extends StatefulWidget {
  const _SigninNewServer(
      {Key? key, required this.onApproveTap, required this.cancelButton})
      : super(key: key);
  final Function onApproveTap;
  final Widget cancelButton;
  @override
  State<_SigninNewServer> createState() => __SigninNewServerState();
}

class __SigninNewServerState extends State<_SigninNewServer> {
  late final TextEditingController _fingerPrintController;
  @override
  void initState() {
    _fingerPrintController = TextEditingController(
        text: converter.toHex(reduxStore.state.verifyKey)!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: Column(
          children: [
            const Spacer(flex: 2),
            const _Logo(),
            const Spacer(flex: 2),
            TextDmSans(
              text: FlutterI18n.translate(context, "NEW_SERVER"),
              color: Colors.white,
              fontSize: 32.sp,
              fontWeight: FontWeight.w500,
            ),
            const Spacer(flex: 4),
            component.CustomTextField(
              controller: _fingerPrintController,
              isEnabled: false,
              maxLines: 5,
              labelText: FlutterI18n.translate(
                  context, "FINGERPRINT_OF_THE_NEW_SERVER"),
              keyboardType: TextInputType.url,
              suffixIcon: _CopyIcon(
                textEditingController: _fingerPrintController,
              ),
            ),
            const Spacer(flex: 2),
            component.BtnPrimary(
              text: FlutterI18n.translate(context, "APPROVE").toUpperCase(),
              onPressed: () {
                widget.onApproveTap();
              },
            ),
            const Spacer(flex: 1),
            widget.cancelButton,
            const Spacer(flex: 10),
            component.AlertInfo(
              text: FlutterI18n.translate(
                  context, "IT_APPEARS_THAT_YOU_WANT_TO_CONNECT"),
            ),
            const Spacer(flex: 5),
          ],
        ),
      ),
    );
  }
}
