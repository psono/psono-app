part of 'signin_screen.dart';

class _SignInAskSendPlain extends StatefulWidget {
  const _SignInAskSendPlain({Key? key, required this.initiateLogin})
      : super(key: key);

  final Function(bool) initiateLogin;
  @override
  State<_SignInAskSendPlain> createState() => __SignInAskSendPlainState();
}

class __SignInAskSendPlainState extends State<_SignInAskSendPlain> {
  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: Column(
          children: <Widget>[
            const Spacer(flex: 2),
            const _Logo(),
            const Spacer(flex: 2),
            TextDmSans(
              text: FlutterI18n.translate(
                context,
                "SERVER_ASKS_FOR_YOUR_PLAINTEXT_PASSWORD",
              ),
              textAlign: TextAlign.center,
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 24.sp,
            ),
            const Spacer(flex: 4),
            component.AlertWarning(
              text: FlutterI18n.translate(
                context,
                "ACCEPTING_THIS_WILL_SEND_YOUR_PLAIN_PASSWORD",
              ),
            ),
            const Spacer(flex: 2),
            component.BtnPrimary(
              onPressed: () async {
                // initiateLogin(true, info);
                widget.initiateLogin(true);
              },
              textColor: const Color(0xFFE77E32),
              text: FlutterI18n.translate(context, "APPROVE_UNSAFE")
                  .toUpperCase(),
            ),
            const Spacer(),
            component.BtnPrimary(
              onPressed: () async {
                // initiateLogin(true, info);
                widget.initiateLogin(false);
              },
              text:
                  FlutterI18n.translate(context, "DECLINE_SAFE").toUpperCase(),
            ),
            const Spacer(flex: 15),
          ],
        ),
      ),
    );
  }
}
