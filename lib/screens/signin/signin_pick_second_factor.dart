part of 'signin_screen.dart';

class _SignInPickSecondFactor extends StatefulWidget {
  const _SignInPickSecondFactor({
    Key? key,
    required this.requiredMultifactors,
    required this.cancelButton,
    required this.onGoogleAuthenticatorTap,
    required this.onYubikeyOtpTap,
    required this.onDuoTap,
    required this.onIvaltTap,
  }) : super(key: key);
  final List<String> requiredMultifactors;
  final Widget cancelButton;
  final Function() onGoogleAuthenticatorTap;
  final Function() onYubikeyOtpTap;
  final Function() onDuoTap;
  final Function() onIvaltTap;

  @override
  State<_SignInPickSecondFactor> createState() =>
      __SignInPickSecondFactorState();
}

class __SignInPickSecondFactorState extends State<_SignInPickSecondFactor> {
  bool get isGoogle2fa =>
      widget.requiredMultifactors.contains('google_authenticator_2fa');
  bool get isYubi2fa => widget.requiredMultifactors.contains('yubikey_otp_2fa');
  bool get isDuo2fa => widget.requiredMultifactors.contains('duo_2fa');
  bool get isIvalt2fa => widget.requiredMultifactors.contains('ivalt_2fa');
  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Spacer(flex: 2),
          const _Logo(),
          const Spacer(flex: 2),
          TextDmSans(
            text: FlutterI18n.translate(
              context,
              "PICK_SECOND_FACTOR",
            ),
            textAlign: TextAlign.center,
            color: Colors.white,
            fontSize: 24.sp,
            fontWeight: FontWeight.w500,
          ),
          const Spacer(flex: 2),
          if (isGoogle2fa)
            component.BtnPrimary(
              text: FlutterI18n.translate(
                context,
                "GOOGLE_AUTHENTICATOR",
              ).toUpperCase(),
              onPressed: () {
                widget.onGoogleAuthenticatorTap();
              },
            ),
          if (isYubi2fa) ...[
            const Spacer(),
            component.BtnPrimary(
              text: FlutterI18n.translate(
                context,
                "YUBIKEY",
              ).toUpperCase(),
              onPressed: () {
                widget.onYubikeyOtpTap();
              },
            ),
          ],
          if (isDuo2fa) ...[
            const Spacer(),
            component.BtnPrimary(
              text: FlutterI18n.translate(
                context,
                "DUO",
              ).toUpperCase(),
              onPressed: () {
                widget.onDuoTap();
              },
            ),
          ],
          if (isIvalt2fa) ...[
            const Spacer(),
            component.BtnPrimary(
              text: FlutterI18n.translate(
                context,
                "IVALT",
              ).toUpperCase(),
              onPressed: () {
                widget.onIvaltTap();
              },
            ),
          ],
          const Spacer(flex: 2),
          widget.cancelButton,
          const Spacer(flex: 10),
        ],
      ),
    );
  }
}
