part of 'signin_screen.dart';

class _SigninServerSignatureChanged extends StatefulWidget {
  const _SigninServerSignatureChanged(
      {Key? key, required this.onConfirmTap, required this.onCancelTap})
      : super(key: key);
  final Function() onConfirmTap;
  final Function() onCancelTap;
  @override
  State<_SigninServerSignatureChanged> createState() =>
      __SigninServerSignatureChangedState();
}

class __SigninServerSignatureChangedState
    extends State<_SigninServerSignatureChanged> {
  TextEditingController _newFingerPrintController = TextEditingController();
  TextEditingController _oldFingerPrintController = TextEditingController();
  @override
  void initState() {
    _newFingerPrintController.text =
        converter.toHex(reduxStore.state.verifyKey)!;
    _oldFingerPrintController.text =
        converter.toHex(reduxStore.state.verifyKeyOld)!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: Column(
          children: [
            const Spacer(flex: 2),
            const _Logo(),
            const Spacer(flex: 2),
            TextDmSans(
              text: FlutterI18n.translate(context, "SERVER_SIGNATURE_CHANGED"),
              textAlign: TextAlign.center,
              color: Colors.white,
              fontSize: 32.sp,
              fontWeight: FontWeight.w500,
            ),
            const Spacer(flex: 4),
            component.CustomTextField(
              controller: _newFingerPrintController,
              isEnabled: false,
              maxLines: 5,
              labelText:
                  FlutterI18n.translate(context, "THE_NEW_FINGERPRINT_IS"),
              keyboardType: TextInputType.url,
              suffixIcon: _CopyIcon(
                textEditingController: _newFingerPrintController,
              ),
            ),
            const Spacer(flex: 2),
            component.CustomTextField(
              controller: _oldFingerPrintController,
              isEnabled: false,
              maxLines: 5,
              labelText:
                  FlutterI18n.translate(context, "THE_OLD_FINGERPRINT_WAS"),
              keyboardType: TextInputType.url,
              suffixIcon: _CopyIcon(
                textEditingController: _oldFingerPrintController,
              ),
            ),
            const Spacer(flex: 2),
            component.AlertDanger(
              text: FlutterI18n.translate(
                context,
                "THE_SIGNATURE_OF_THE_SERVER_CHANGED",
              ),
            ),
            const Spacer(flex: 3),
            component.BtnPrimary(
              text: FlutterI18n.translate(context, "IGNORE_AND_CONTINUE")
                  .toUpperCase(),
              onPressed: () {
                widget.onConfirmTap();
              },
            ),
            const Spacer(flex: 3),
            component.BtnPrimary(
              text: FlutterI18n.translate(context, "CANCEL").toUpperCase(),
              onPressed: () {
                widget.onCancelTap();
              },
            ),
            const Spacer(flex: 4),
          ],
        ),
      ),
    );
  }
}
