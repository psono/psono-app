part of 'signin_screen.dart';

class _DefaultSignin extends StatefulWidget {
  _DefaultSignin({
    Key? key,
    required this.serverUrlController,
    required this.usernameController,
    required this.passwordController,
    required this.domainSuffix,
    required this.screen,
    required this.loginType,
    required this.samlProvider,
    required this.oidcProvider,
    required this.authenticationMethods,
    required this.multifactorEnabled,
    required this.onLoginTap,
    required this.initiateOidcLogin,
    required this.initiateSamlLogin,
    required this.onInfoChange,
    required this.onScreenChange,
    required this.onLoginTypeChange,
    required this.onSamlProviderChange,
    required this.onOidcProviderChange,
    required this.onMultifactorEnabledChange,
    required this.onAuthenticationMethodsChange,
    required this.onDomainSuffixChange,
  }) : super(key: key);

  final TextEditingController serverUrlController;
  final TextEditingController usernameController;
  final TextEditingController passwordController;
  String domainSuffix;
  _ScreenType screen;
  String loginType;
  SamlProvider? samlProvider;
  OidcProvider? oidcProvider;
  List<String>? authenticationMethods;
  bool? multifactorEnabled;
  final Function() onLoginTap;

  Function(OidcProvider provider, api_client.Info info) initiateOidcLogin;
  Function(SamlProvider provider, api_client.Info info) initiateSamlLogin;
  Function(api_client.Info) onInfoChange;
  Function(_ScreenType) onScreenChange;
  Function(String) onLoginTypeChange;
  Function(SamlProvider) onSamlProviderChange;
  Function(OidcProvider) onOidcProviderChange;
  Function(bool?) onMultifactorEnabledChange;
  Function(List<String>?) onAuthenticationMethodsChange;
  Function(String) onDomainSuffixChange;

  @override
  State<_DefaultSignin> createState() => __DefaultSigninState();
}

class __DefaultSigninState extends State<_DefaultSignin> {
  bool _configExists = false;
  bool _obscurePassword = true;
  late api_client.Info info;
  bool allowRegistration = true;
  bool allowCustomServer = true;
  bool allowLostPassword = true;
  bool authkeyEnabled = false;
  bool ldapEnabled = false;
  bool samlEnabled = false;
  bool oidcEnabled = false;
  List<SamlProvider>? samlProvider = [];
  List<OidcProvider>? oidcProvider = [];
  @override
  void initState() {
    widget.usernameController.addListener(_serverAndUrlOnChange);
    widget.serverUrlController.addListener(_serverAndUrlOnChange);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _initialize();
    final Widget username = component.CustomTextField(
      labelText: FlutterI18n.translate(context, "USERNAME"),
      controller: widget.usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      suffixIcon: widget.domainSuffix.isEmpty
          ? null
          : Center(
              child: TextDmSans(
                text: widget.domainSuffix,
                overflow: TextOverflow.ellipsis,
                color: Colors.white.withOpacity(0.50),
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                maxLines: 1,
              ),
            ),
    );

    final Widget password = component.CustomTextField(
      controller: widget.passwordController,
      labelText: FlutterI18n.translate(context, "PASSWORD"),
      obscureText: _obscurePassword,
      suffixIcon: component.ObscureIcon(
        onTap: () {
          setState(() {
            _obscurePassword = !_obscurePassword;
          });
        },
        obscure: _obscurePassword,
      ),
    );

    final Widget server = component.CustomTextField(
      keyboardType: TextInputType.url,
      controller: widget.serverUrlController,
      labelText: FlutterI18n.translate(context, "SERVER_URL"),
    );

    final Widget lostPassword = component.BtnText(
      onPressed: () {
        Navigator.pushNamed(context, AppRoutes.lostPassword);
      },
      text: '${FlutterI18n.translate(context, "LOST_PASSWORD")}?',
      color: Colors.white.withOpacity(0.60),
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
    );

    final Widget signup = component.BtnText(
      onPressed: () {
        Navigator.pushNamed(context, AppRoutes.register);
      },
      text: FlutterI18n.translate(context, "SIGN_UP"),
      color: Colors.white,
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
    );

    final Widget privacyPolicy = component.BtnText(
      onPressed: () {
        helper.openUrl('https://www.psono.pw/privacy-policy.html');
      },
      text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
      color: Colors.white.withOpacity(0.60),
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
    );

    final Widget scanConfig = component.BtnText(
      onPressed: () {
        component.showCustomBottomSheet(
          context: context,
          barrierColor: Colors.black.withOpacity(0.7),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 50.w,
                  height: 3.h,
                  decoration: ShapeDecoration(
                    color: Colors.white.withOpacity(0.30),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100),
                    ),
                  ),
                ),
                SizedBox(height: 20.h),
                component.TextDmSans(
                  text: FlutterI18n.translate(
                    context,
                    "REMOTE_CONFIG",
                  ),
                  textAlign: TextAlign.center,
                  color: Colors.white,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w700,
                ),
                SizedBox(height: 24.h),
                Container(
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        width: 1,
                        strokeAlign: BorderSide.strokeAlignCenter,
                        color: Colors.white.withOpacity(0.2),
                      ),
                    ),
                  ),
                ),
                _Item(
                  text: FlutterI18n.translate(context, "SCAN_CONFIG"),
                  leadingImage: AppAssets.iconCamera,
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, AppRoutes.scanConfig)
                        .then((value) {
                      setState(() {});
                    });
                  },
                ),
                _Item(
                  text: FlutterI18n.translate(context, "LOAD_CONFIG"),
                  leadingImage: AppAssets.iconChangeEmail,
                  onTap: () async {
                    // await managerHost.approveHost(
                    //     widget.serverUrlController.text, config.config!.verifyKey!);

                    reduxStore.dispatch(
                      InitiateLoginAction(
                        widget.serverUrlController.text,
                        "",
                      ),
                    );

                    await storage.write(
                      key: SK.serverUrl,
                      value: widget.serverUrlController.text,
                      iOptions: secureIOSOptions,
                    );
                    Map? configMap;
                    String? result;
                    Config config;
                    try {
                      result = await managerHost
                          .loadRemoteConfig(widget.serverUrlController.text);
                      try {
                        configMap = jsonDecode(result);
                        config = Config(
                            version: 1,
                            configJson: ConfigJson.fromJson(
                                configMap as Map<String, dynamic>));
                      } on FormatException {
                        return;
                      }
                    } on api_client.ServiceUnavailableException {
                      component.Loader.hide();
                      if (!mounted) {
                        return;
                      }
                      helper.showErrorDialog(
                        context,
                        FlutterI18n.translate(context, "SERVER_OFFLINE"),
                        FlutterI18n.translate(
                            context, "SERVER_OFFLINE_DETAILS"),
                      );
                      return;
                    } on HandshakeException {
                      component.Loader.hide();
                      if (!mounted) {
                        return;
                      }
                      helper.showErrorDialog(
                        context,
                        FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
                        FlutterI18n.translate(
                            context, "HANDSHAKE_ERROR_DETAILS"),
                      );
                      return;
                    } catch (e) {
                      component.Loader.hide();
                      if (!mounted) {
                        return;
                      }
                      helper.showErrorDialog(
                        context,
                        FlutterI18n.translate(context, "SERVER_OFFLINE"),
                        FlutterI18n.translate(
                            context, "SERVER_OFFLINE_DETAILS"),
                      );
                      return;
                    }

                    await storage.write(
                      key: SK.configJson,
                      value: jsonEncode(config),
                      iOptions: secureIOSOptions,
                    );

                    reduxStore.dispatch(
                      ConfigUpdatedAction(
                        config,
                      ),
                    );
                    if (config.configJson != null &&
                        config.configJson!.backendServers != null &&
                        config.configJson!.backendServers!.length > 0 &&
                        config.configJson!.backendServers![0].url != null) {
                      reduxStore.dispatch(
                        InitiateLoginAction(
                          config.configJson!.backendServers![0].url,
                          "",
                        ),
                      );

                      await storage.write(
                        key: SK.serverUrl,
                        value: config.configJson!.backendServers![0].url,
                        iOptions: secureIOSOptions,
                      );
                    }
                    _serverAndUrlOnChange();
                    if (!mounted) {
                      return;
                    }
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        );
      },
      text: FlutterI18n.translate(context, "REMOTE_CONFIG"),
      color: Colors.white.withOpacity(0.60),
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
    );

    final Widget forgetConfig = component.BtnText(
      onPressed: () {
        setState(() {
          storage.delete(key: SK.configJson);
          reduxStore.dispatch(
            ConfigUpdatedAction(
              Config(),
            ),
          );

          _configExists = false;
        });
        _serverAndUrlOnChange();
      },
      text: FlutterI18n.translate(context, "FORGET_CONFIG"),
      color: Colors.white.withOpacity(0.60),
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
    );

    bool showSamlDivider = samlEnabled &&
        samlProvider!.isNotEmpty &&
        (authkeyEnabled || ldapEnabled);
    bool showOidcDivider = oidcEnabled &&
        (oidcProvider?.isNotEmpty ?? false) &&
        (samlEnabled || authkeyEnabled || ldapEnabled);

    return component.ScaffoldDark(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SizedBox(
          height: _calculateHeight(context),
          child: Column(
            children: [
              SizedBox(height: 20.h),
              Align(
                alignment: Alignment.centerLeft,
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(
                    AppAssets.iconNavBack,
                    height: 24.h,
                    width: 24.h,
                  ),
                ),
              ),
              SizedBox(height: 24.h),
              TextDmSans(
                text: FlutterI18n.translate(context, "WELCOME_TO_PSONO"),
                textAlign: TextAlign.center,
                color: Colors.white,
                fontSize: 32.sp,
                fontWeight: FontWeight.w500,
                height: 0,
              ),
              SizedBox(height: 24.h),
              if (oidcEnabled) ..._addOidcButtons(oidcProvider),
              if (showOidcDivider) ...[
                Divider(
                  color: Colors.white.withOpacity(0.22),
                ),
              ],
              if (samlEnabled) ..._addSamlButtons(samlProvider),
              if (showSamlDivider) ...[
                Divider(
                  color: Colors.white.withOpacity(0.22),
                ),
              ],
              if (authkeyEnabled || ldapEnabled) ...[
                username,
                SizedBox(height: 16.h),
                password,
                SizedBox(height: 24.h),
                component.BtnPrimary(
                  text: FlutterI18n.translate(context, "SIGN_IN").toUpperCase(),
                  onPressed: widget.onLoginTap,
                ),
              ],
              if (allowLostPassword) ...[
                SizedBox(height: 30.h),
                lostPassword,
              ],
              if (allowRegistration) ...[
                SizedBox(height: 40.h),
                TextDmSans(
                  text: FlutterI18n.translate(context, "DONT_HAVE_AN_ACCOUNT"),
                  color: Colors.white.withOpacity(0.60),
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(height: 8.h),
                signup,
              ],
              if (allowCustomServer) ...[
                KeyboardVisibilityBuilder(
                  builder: (context, isKeyboardVisible) {
                    if (isKeyboardVisible) {
                      return const SizedBox();
                    }
                    if ((oidcProvider?.isEmpty ?? false) &&
                        (samlProvider?.isEmpty ?? false)) {
                      return const Spacer();
                    }
                    return SizedBox(height: 80.h);
                  },
                ),
                server,
                SizedBox(height: 8.h),
              ],
              Row(
                children: [
                  const Spacer(),
                  privacyPolicy,
                  if (allowCustomServer && !_configExists) ...[
                    const Spacer(),
                    scanConfig,
                  ],
                  if (_configExists) ...[
                    const Spacer(),
                    forgetConfig,
                  ],
                  const Spacer(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  double? _calculateHeight(BuildContext context) {
    double? defaultHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom);
    if (samlEnabled && (samlProvider?.isNotEmpty ?? false)) {
      defaultHeight = null;
    }
    if (oidcEnabled && (oidcProvider?.isNotEmpty ?? false)) {
      defaultHeight = null;
    }
    return defaultHeight;
  }

  void _initialize() {
    if (reduxStore.state.config != null &&
        reduxStore.state.config.configJson != null) {
      setState(() {
        _configExists = true;
      });
      // widget.onSetState();
      ConfigJson configJson = reduxStore.state.config.configJson;
      allowRegistration = configJson.allowRegistration ?? true;
      allowCustomServer = configJson.allowCustomServer ?? true;
      allowLostPassword = configJson.allowLostPassword ?? true;
      if (configJson.authenticationMethods != null) {
        authkeyEnabled = configJson.authenticationMethods!.contains('AUTHKEY');
        ldapEnabled = configJson.authenticationMethods!.contains('LDAP');
        samlEnabled = configJson.authenticationMethods!.contains('SAML');
        oidcEnabled = configJson.authenticationMethods!.contains('OIDC');
      } else {
        authkeyEnabled = true;
        ldapEnabled = false;
        samlEnabled = false;
        oidcEnabled = false;
      }
      if (configJson.samlProvider != null) {
        samlProvider = configJson.samlProvider;
      } else {
        samlProvider = [];
      }
      if (configJson.oidcProvider != null) {
        oidcProvider = configJson.oidcProvider;
      } else {
        oidcProvider = [];
      }
    } else {
      allowRegistration = true;
      allowCustomServer = true;
      allowLostPassword = true;
      authkeyEnabled = true;
      ldapEnabled = false;
      samlEnabled = false;
      oidcEnabled = false;
      samlProvider = [];
      oidcProvider = [];
    }
  }

  List<Widget> _addSamlButtons(
    List<SamlProvider>? samlProvider,
  ) {
    if (samlProvider == null || samlProvider.isEmpty) return [];
    final List<Widget> children = [];
    for (var i = 0; i < samlProvider.length; i++) {
      if (samlProvider[i].title != null && samlProvider[i].title != '') {
        children.add(
          Text(
            samlProvider[i].title!,
            style: const TextStyle(color: Color(0xFFb1b6c1)),
          ),
        );
        children.add(const SizedBox(height: 8.0));
      }
      String? buttonTitle = FlutterI18n.translate(context, "LOGIN");
      if (samlProvider[i].buttonName != null &&
          samlProvider[i].buttonName != '') {
        buttonTitle = samlProvider[i].buttonName;
      }
      children.add(
        component.BtnPrimary(
          text: buttonTitle,
          onPressed: () async {
            await _samlButtonTap(samlProvider[i]);
          },
        ),
      );
      children.add(const SizedBox(height: 8.0));
    }
    return children;
  }

  _samlButtonTap(SamlProvider samlProvider) async {
    try {
      await storage.write(
        key: SK.serverUrl,
        value: widget.serverUrlController.text,
        iOptions: secureIOSOptions,
      );
      await storage.write(
        key: SK.username,
        value: widget.usernameController.text + widget.domainSuffix,
        iOptions: secureIOSOptions,
      );

      reduxStore.dispatch(
        InitiateLoginAction(
          widget.serverUrlController.text,
          widget.usernameController.text + widget.domainSuffix,
        ),
      );
      info = await api_client.info();
      widget.onInfoChange(info);
    } on api_client.ServiceUnavailableException {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on HandshakeException {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
          FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
        );
      }
      return;
    } catch (e) {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    }

    var checkHostResult =
        await managerHost.checkHost(widget.serverUrlController.text, info);

    await storage.write(
      key: SK.complianceServerSecrets,
      value: info.complianceServerSecrets.toString(),
      iOptions: secureIOSOptions,
    );
    await storage.write(
      key: SK.complianceDisableDeleteAccount,
      value: info.complianceDisableDeleteAccount.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: SK.complianceDisableOfflineMode,
      value: info.complianceDisableOfflineMode.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: SK.complianceMaxOfflineCacheTimeValid,
      value: info.complianceMaxOfflineCacheTimeValid.toString(),
      iOptions: secureIOSOptions,
    );

    reduxStore.dispatch(
      SetVerifyKeyAction(
        checkHostResult.info!.verifyKey,
        info.verifyKey,
        info.complianceServerSecrets,
        info.complianceDisableDeleteAccount,
        info.complianceDisableOfflineMode,
        info.complianceMaxOfflineCacheTimeValid,
      ),
    );
    widget.onLoginTypeChange('SAML');
    widget.onSamlProviderChange(samlProvider);
    widget.onMultifactorEnabledChange(checkHostResult.info!.multifactorEnabled);

    if (checkHostResult.status == 'matched') {
      widget.initiateSamlLogin(samlProvider, info);
      return;
    } else if (checkHostResult.status == 'new_server') {
      widget.onScreenChange(_ScreenType.approveNewServer);
      widget.onAuthenticationMethodsChange(info.authenticationMethods);
      return;
    } else if (checkHostResult.status == 'signature_changed') {
      widget.onScreenChange(_ScreenType.serverSignatureChanged);
      widget.onAuthenticationMethodsChange(info.authenticationMethods);
      return;
    } else if (checkHostResult.status == 'unsupported_server_version') {
      widget.onScreenChange(_ScreenType.unsupportedServerVersion);
      widget.onAuthenticationMethodsChange(info.authenticationMethods);
      return;
    } else {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "INVALID_SIGNATURE"),
        FlutterI18n.translate(context, "INVALID_SIGNATURE_DETAILS"),
      );
    }
  }

  List<Widget> _addOidcButtons(
    List<OidcProvider>? oidcProvider,
  ) {
    if (oidcProvider == null || oidcProvider.isEmpty) {
      return [];
    }
    List<Widget> children = [];
    for (var i = 0; i < oidcProvider.length; i++) {
      if (oidcProvider[i].title != null && oidcProvider[i].title != '') {
        children.add(
          Text(
            oidcProvider[i].title!,
            style: const TextStyle(color: Color(0xFFb1b6c1)),
          ),
        );
        children.add(const SizedBox(height: 8.0));
      }
      String? buttonTitle = FlutterI18n.translate(context, "LOGIN");
      if (oidcProvider[i].buttonName != null &&
          oidcProvider[i].buttonName != '') {
        buttonTitle = oidcProvider[i].buttonName;
      }
      children.add(
        component.BtnPrimary(
          text: buttonTitle,
          onPressed: () async {
            await _oidcButtonTap(oidcProvider[i]);
          },
        ),
      );
      children.add(const SizedBox(height: 8.0));
    }
    return children;
  }

  _oidcButtonTap(OidcProvider oidcProvider) async {
    try {
      await storage.write(
        key: SK.serverUrl,
        value: widget.serverUrlController.text,
        iOptions: secureIOSOptions,
      );
      await storage.write(
        key: SK.username,
        value: widget.usernameController.text + widget.domainSuffix,
        iOptions: secureIOSOptions,
      );

      reduxStore.dispatch(
        InitiateLoginAction(
          widget.serverUrlController.text,
          widget.usernameController.text + widget.domainSuffix,
        ),
      );
      info = await api_client.info();
      widget.onInfoChange(info);
    } on api_client.ServiceUnavailableException {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on HandshakeException {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
          FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
        );
      }
      return;
    } catch (e) {
      if (context.mounted) {
        helper.showErrorDialog(
          context,
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    }

    var checkHostResult =
        await managerHost.checkHost(widget.serverUrlController.text, info);

    await storage.write(
      key: SK.complianceServerSecrets,
      value: info.complianceServerSecrets.toString(),
      iOptions: secureIOSOptions,
    );
    await storage.write(
      key: SK.complianceDisableDeleteAccount,
      value: info.complianceDisableDeleteAccount.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: SK.complianceDisableOfflineMode,
      value: info.complianceDisableOfflineMode.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: SK.complianceMaxOfflineCacheTimeValid,
      value: info.complianceMaxOfflineCacheTimeValid.toString(),
      iOptions: secureIOSOptions,
    );

    reduxStore.dispatch(
      SetVerifyKeyAction(
        checkHostResult.info!.verifyKey,
        info.verifyKey,
        info.complianceServerSecrets,
        info.complianceDisableDeleteAccount,
        info.complianceDisableOfflineMode,
        info.complianceMaxOfflineCacheTimeValid,
      ),
    );
    widget.onLoginTypeChange('OIDC');
    widget.onOidcProviderChange(oidcProvider);
    widget.onMultifactorEnabledChange(checkHostResult.info!.multifactorEnabled);

    if (checkHostResult.status == 'matched') {
      widget.initiateOidcLogin(oidcProvider, info);
      return;
    } else if (checkHostResult.status == 'new_server') {
      widget.onScreenChange(_ScreenType.approveNewServer);
      widget.onAuthenticationMethodsChange(info.authenticationMethods);
      return;
    } else if (checkHostResult.status == 'signature_changed') {
      widget.onScreenChange(_ScreenType.serverSignatureChanged);
      widget.onAuthenticationMethodsChange(info.authenticationMethods);
      return;
    } else if (checkHostResult.status == 'unsupported_server_version') {
      widget.onScreenChange(_ScreenType.unsupportedServerVersion);
      widget.onAuthenticationMethodsChange(info.authenticationMethods);
      return;
    } else {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "INVALID_SIGNATURE"),
        FlutterI18n.translate(context, "INVALID_SIGNATURE_DETAILS"),
      );
    }
  }

  void _serverAndUrlOnChange() {
    if (widget.usernameController.text.contains('@')) {
      widget.domainSuffix = '';
    } else {
      widget.domainSuffix = '@' +
          (reduxStore.state.config != null &&
                  reduxStore.state.config.configJson != null &&
                  reduxStore.state.config.configJson.backendServers != null &&
                  reduxStore.state.config.configJson.backendServers.length >
                      0 &&
                  reduxStore.state.config.configJson.backendServers[0].domain !=
                      null
              ? reduxStore.state.config.configJson.backendServers[0].domain
              : helper.getDomain(widget.serverUrlController.text)!);
    }

    widget.onDomainSuffixChange(widget.domainSuffix);
  }
}

class _Item extends StatelessWidget {
  const _Item({
    super.key,
    required this.text,
    required this.leadingImage,
    this.onTap,
    this.isDelete = false,
  });
  final String text;
  final String leadingImage;
  final Function()? onTap;
  final bool isDelete;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 0.h),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12.h),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.white.withOpacity(0.2),
                width: 1,
              ),
            ),
          ),
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.all(8.w),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.10),
                  borderRadius: BorderRadius.circular(8.w),
                ),
                child: Image.asset(
                  leadingImage,
                  width: 32.w,
                  height: 32.h,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(width: 12.w),
              component.TextDmSans(
                text: text,
                color: isDelete ? const Color(0xffA50031) : Colors.white,
                fontSize: 16.sp,
                fontWeight: FontWeight.w500,
              ),
              const Spacer(),
              IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_right,
                  color: isDelete ? const Color(0xffA50031) : Colors.white,
                  size: 20.sp,
                ),
                onPressed: onTap,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
