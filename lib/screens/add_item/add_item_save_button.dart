part of 'add_item_screen.dart';

class _SaveItemButton extends StatelessWidget {
  const _SaveItemButton({
    Key? key,
    required GlobalKey<FormState> formKey,
    required this.widget,
    required Secret secret,
    required this.type,
    required Blueprint bp,
    required this.mounted,
  })  : _formKey = formKey,
        _secret = secret,
        _bp = bp,
        super(key: key);

  final GlobalKey<FormState> _formKey;
  final AddItemScreen widget;
  final Secret _secret;
  final String? type;
  final Blueprint _bp;
  final bool mounted;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: component.BtnPrimary(
        onPressed: () async {
          if (!_formKey.currentState!.validate()) {
            return;
          }
          component.Loader.show(context);
          if (widget.parent == null || widget.parent!.items == null) {
            widget.parent!.items = [];
          }

          _secret.type = type;

          String linkId = await cryptoLibary.generateUUID();

          String? parentDatastoreId;
          String? parentShareId;
          if (widget.share == null) {
            parentDatastoreId = widget.datastore!.datastoreId;
          } else {
            parentShareId = widget.share!.shareId;
          }
          String callbackUrl = '';
          String callbackUser = '';
          String callbackPass = '';

          for (final field
              in _bp.fields!.where((field) => field.inputFormatter != null)) {
            if (_secret.data.containsKey(field.name)) {
              field.inputFormatter!.formatEditUpdate(
                TextEditingValue.empty,
                TextEditingValue(
                  text: _secret.data[field.name],
                ),
              );
              _secret.data[field.name] =
                  field.inputFormatter!.getUnmaskedText();
            }
          }

          Secret createdSecret = await managerSecret.createSecret(
            _secret.data,
            linkId,
            parentDatastoreId,
            parentShareId,
            callbackUrl,
            callbackUser,
            callbackPass,
          );
          _secret.secretId = createdSecret.secretId;
          _secret.secretKey = createdSecret.secretKey;

          String? description;
          if (_bp.descriptionField != null) {
            description = _secret.data[_bp.descriptionField];
            if (_bp.id == "credit_card" && description != null) {
              description = description.replaceAllMapped(
                RegExp(r'.(?=.{4})'),
                (match) => 'x',
              );
            }
          }

          datastoreModel.Item newItem = datastoreModel.Item(
            id: linkId,
            name: _secret.data[_bp.titleField],
            description: description,
            type: type,
            secretId: createdSecret.secretId,
            secretKey: createdSecret.secretKey,
          );

          if (_bp.urlfilterField != null) {
            newItem.urlfilter = _secret.data[_bp.urlfilterField];
          }

          if (widget.share == null) {
            newItem.shareRights = ShareRight(
              read: true,
              write: true,
              grant: true,
              delete: true,
            );
          } else {
            newItem.shareRights = ShareRight(
              read: widget.share!.shareRights!.read,
              write: widget.share!.shareRights!.write,
              grant: widget.share!.shareRights!.grant! &&
                  widget.share!.shareRights!.write!,
              delete: widget.share!.shareRights!.write,
            );
          }

          widget.parent!.items!.add(newItem);

          if (widget.share == null) {
            datastoreModel.Datastore datastore =
                await managerDatastorePassword.getPasswordDatastore(
              widget.datastore!.datastoreId,
            );

            datastoreModel.Folder? parent;
            if (widget.path!.isEmpty) {
              parent = datastore.data;
            } else {
              List<String> pathCopy = List<String>.from(widget.path!);
              List search = managerDatastorePassword.findInDatastore(
                pathCopy,
                datastore.data,
              );
              parent = search[0][search[1]];
            }

            if (parent!.items == null) {
              parent.items = [];
            }
            parent.items!.add(newItem);
            await datastore.save();
          } else {
            shareModel.Share share = await managerShare.readShare(
              widget.share!.shareId,
              widget.share!.shareSecretKey,
            );
            datastoreModel.Folder? parent;
            if (widget.relativePath!.isEmpty) {
              parent = share.folder;
            } else {
              List<String> pathCopy = List<String>.from(
                widget.relativePath!,
              );
              List search = managerDatastorePassword.findInDatastore(
                pathCopy,
                share.folder,
              );
              parent = search[0][search[1]];
            }
            if (parent!.items == null) {
              parent.items = [];
            }
            parent.items!.add(newItem);
            await share.save();
          }
          component.Loader.hide();
          if (mounted) {
            Navigator.pop(context);
            Navigator.pop(context);
          }
          return;
        },
        text: FlutterI18n.translate(context, "SAVE"),
      ),
    );
  }
}
