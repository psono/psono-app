import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/blueprint.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/otp.dart';
import 'package:psono/model/parsed_url.dart';
import 'package:psono/model/secret.dart';
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/model/share_right.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/screens/scan_qr/index.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/crypto_library.dart' as cryptoLibary;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/manager_secret.dart' as managerSecret;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';

part 'add_item_default_screen.dart';
part 'add_item_save_button.dart';
part 'add_item_textfield.dart';
part 'add_item_title_textfield.dart';

class AddItemScreen extends StatefulWidget {
  static String tag = 'add-item-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final List<String?>? path;
  final List<String?>? relativePath;
  final String? type;
  OTP? otp;

  AddItemScreen(
      {this.parent,
      this.datastore,
      this.share,
      this.path,
      this.relativePath,
      this.type,
      this.otp});

  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final String? type;
  final Secret _secret = Secret(
    data: {},
  );
  bool _obscurePassword = true;
  OTP? otp;
  datastoreModel.Datastore? settingsDatastore;
  List<TextEditingController> controllers = [];
  List contentKV = [];
  @override
  void initState() {
    super.initState();
    type = widget.type;
    otp = widget.otp;
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    for (var i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> menuList = [];

    menuList = _initMenuList(context);

    if (type != null) {
      var _bp = itemBlueprint.getBlueprint(type)!;

      int getFieldLength() {
        int advancedCount =
            _bp.fields!.where((field) => field.position == 'advanced').length;
        return _bp.fields!.length - advancedCount + 1;
      }

      return component.ScaffoldDark(
        backgroundPath: AppAssets.folderBg,
        appBar: AppBar(
          title: component.TextDmSans(
            text:
                '${FlutterI18n.translate(context, 'NEW')} ${FlutterI18n.translate(context, _bp.name)}',
            fontSize: 18.sp,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.transparent,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.navigate_before,
              color: Colors.white,
              size: 40.sp,
            ),
          ),
        ),
        body: Form(
          key: _formKey,
          child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: getFieldLength(),
            itemBuilder: (c, index) {
              if (index == getFieldLength() - 1) {
                // last element, render the save button

                return _SaveItemButton(
                    formKey: _formKey,
                    widget: widget,
                    secret: _secret,
                    type: type,
                    bp: _bp,
                    mounted: mounted);
              } else {
                // render field
                final field = _bp.fields![index];

                String? content = '';

                if (field.field == 'key_value_list') {
                  _secret.data[field.name] = contentKV;
                } else {
                  if (_secret.data.containsKey(field.name)) {
                    content = _secret.data[field.name].toString();
                  }
                }
                if (_bp.id == 'totp' && otp != null && content == '') {
                  if (field.name == 'totp_title') {
                    content = otp!.getDescription();
                    _secret.data[field.name] = content;
                  }
                  if (field.name == 'totp_code') {
                    content = otp!.secret;
                    _secret.data[field.name] = content;
                  }
                  if (field.name == 'totp_period') {
                    content = otp!.period.toString();
                    _secret.data[field.name] = otp!.period;
                  }
                  if (field.name == 'totp_algorithm') {
                    content = otp!.algorithm;
                    _secret.data[field.name] = content;
                  }
                  if (field.name == 'totp_digits') {
                    content = otp!.digits.toString();
                    _secret.data[field.name] = otp!.digits;
                  }
                }

                TextInputType? keyboardType;
                bool obscureText = false;
                int? maxLines = 1;
                int? minLines = 1;
                Widget? suffixIcon;
                List<MaskTextInputFormatter> inputFormatters = [];
                final textController = TextEditingController(
                  text: content,
                );

                if (field.inputFormatter != null) {
                  inputFormatters.add(field.inputFormatter!);
                }

                bool isWebsiteTotpFields =
                    field.name == "website_password_totp_period" ||
                        field.name == "website_password_totp_digits" ||
                        field.name == "website_password_totp_code" ||
                        field.name == "website_password_totp_algorithm";
                if (field.field == 'input' && field.type == 'text') {
                } else if (field.field == 'input' && field.type == 'password') {
                  obscureText = true && _obscurePassword;

                  suffixIcon = component.ObscureIcon(
                    onTap: () {
                      setState(() {
                        _obscurePassword = !_obscurePassword;
                      });
                    },
                    obscure: _obscurePassword,
                  );
                } else if (field.field == 'input' &&
                    field.type == 'totp_code') {
                  obscureText = true && _obscurePassword;
                  suffixIcon = Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      InkWell(
                        onTap: () async {
                          final String? result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const ScanQR(
                                backCount: 2,
                              ),
                            ),
                          );
                          if (result != null) {
                            var otp = helper.parseTOTPUri(result);
                            _secret.data[field.name] = otp.secret;
                            textController.text = otp.secret!;
                          }
                        },
                        child: Image.asset(
                          AppAssets.iconScan,
                          height: 20.h,
                          width: 20.h,
                        ),
                      ),
                      SizedBox(width: 12.w),
                      component.ObscureIcon(
                        onTap: () {
                          setState(() {
                            _obscurePassword = !_obscurePassword;
                          });
                        },
                        obscure: _obscurePassword,
                      ),
                    ],
                  );
                } else if (field.field == 'input' && field.type == 'checkbox') {
                } else if (field.field == 'textarea') {
                  keyboardType = TextInputType.multiline;
                  maxLines = 10;
                  minLines = null;
                } else if (field.field == 'button' && field.type == 'button') {
                  keyboardType = TextInputType.multiline;
                } else if (field.field == 'key_value_list') {
                } else if (isWebsiteTotpFields) {
                } else {
                  throw ("unknown field type combi");
                }

                controllers.add(textController);
                List<Widget> children = [];

                if (field.field == 'key_value_list') {
                  children.add(
                    component.KeyValueList(
                      contentKV: contentKV,
                      onChangeContentKV: (p0) {
                        setState(() {
                          _secret.data[field.name] = p0;
                        });
                      },
                    ),
                  );
                } else {
                  if (isWebsiteTotpFields) {
                    children.add(Container());
                  } else if (field.title == "TITLE") {
                    children.add(
                      _TitleTextField(
                        bp: _bp,
                        textController: textController,
                        inputFormatters: inputFormatters,
                        field: field,
                        secret: _secret,
                      ),
                    );
                  } else {
                    children.add(
                      _ItemTextfield(
                        obscureText: obscureText,
                        field: field,
                        textController: textController,
                        inputFormatters: inputFormatters,
                        keyboardType: keyboardType,
                        maxLines: maxLines,
                        minLines: minLines,
                        secret: _secret,
                        bp: _bp,
                        suffixIcon: suffixIcon,
                      ),
                    );
                  }
                }

                return Column(
                  children: children,
                );
              }
            },
          ),
        ),
      );
    } else {
      return _DefaultScreen(menuList: menuList);
    }
  }

  List<Widget> _initMenuList(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    List<Widget> menuList = [];
    if (settingsDatastore != null) {
      List<Blueprint> blueprints = itemBlueprint.getBlueprints();

      blueprints.sort((a, b) => FlutterI18n.translate(context, a.name)
          .compareTo(FlutterI18n.translate(context, b.name)));

      menuList = [];
      for (var blueprint in blueprints) {
        var visible = blueprint.settingFieldDefault;
        for (var i = 0; i < settingsDatastore!.dataKV.length; i++) {
          if (settingsDatastore!.dataKV[i]['key'] != blueprint.settingField ||
              settingsDatastore!.dataKV[i]['value'] == null) {
            continue;
          }
          visible = settingsDatastore!.dataKV[i]['value'];
        }

        if (!visible) {
          continue;
        }
        menuList.add(
          Container(
            margin: EdgeInsets.only(bottom: 8.h),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.10),
              borderRadius: BorderRadius.circular(20.r),
              border: Border.all(
                color: Colors.white.withOpacity(0.20),
              ),
            ),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 4.h, horizontal: 12.w),
              title: component.TextDmSans(
                text: FlutterI18n.translate(context, blueprint.name),
                color: Colors.white,
                fontSize: 16.sp,
                fontWeight: FontWeight.w500,
              ),
              leading: Image.asset(
                blueprint.imagePath,
                height: orientation == Orientation.portrait ? 40.h : 80.h,
                width: orientation == Orientation.portrait ? 38.w : 22.w,
              ),
              onTap: () async {
                if (blueprint.id == 'totp') {
                  final String? result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const ScanQR(
                        backCount: 2,
                      ),
                    ),
                  );
                  if (result != null) {
                    otp = helper.parseTOTPUri(result);
                  }
                }
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddItemScreen(
                      parent: widget.parent,
                      datastore: widget.datastore,
                      share: widget.share,
                      path: widget.path,
                      relativePath: widget.relativePath,
                      type: blueprint.id,
                      otp: otp,
                    ),
                  ),
                );
              },
            ),
          ),
        );
      }
    }
    return menuList;
  }

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(
      context,
      title,
      content,
    );
  }

  Future<void> loadDatastore() async {
    component.Loader.show(context);
    datastoreModel.Datastore? newDatastore;
    try {
      newDatastore = await managerDatastoreSetting.getSettingsDatastore();
    } on api_client.ServiceUnavailableException {
      if (context.mounted) {
        _showErrorDialog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on api_client.UnauthorizedException {
      user_service.logout();

      if (context.mounted) {
        Navigator.pushReplacementNamed(context, AppRoutes.signin);
      }
      return;
    } finally {
      component.Loader.hide();
    }
    setState(() {
      settingsDatastore = newDatastore;
    });
  }

  Future<void> initStateAsync() async {
    await loadDatastore();
  }
}
