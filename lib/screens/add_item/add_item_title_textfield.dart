part of 'add_item_screen.dart';

class _TitleTextField extends StatelessWidget {
  const _TitleTextField({
    Key? key,
    required Blueprint bp,
    required this.textController,
    required this.inputFormatters,
    required this.field,
    required Secret secret,
  })  : _bp = bp,
        _secret = secret,
        super(key: key);

  final Blueprint _bp;
  final TextEditingController textController;
  final List<MaskTextInputFormatter> inputFormatters;
  final BlueprintField field;
  final Secret _secret;

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.2),
        borderRadius: BorderRadius.circular(20.r),
      ),
      padding: EdgeInsets.all(10.h),
      margin: EdgeInsets.only(bottom: 24.h),
      child: Row(
        children: [
          Image.asset(
            _bp.imagePath,
            height: orientation == Orientation.portrait ? 44.h : 88.h,
            width: orientation == Orientation.portrait ? 44.w : 32.w,
          ),
          SizedBox(width: 12.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: textController,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.sp,
                  ),
                  inputFormatters: inputFormatters,
                  validator: (value) {
                    if (field.required && (value == null || value.isEmpty)) {
                      return FlutterI18n.translate(
                          context, field.errorMessageRequired!);
                    }
                    return null;
                  },
                  onChanged: (text) {
                    _secret.data[field.name] = text;
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    floatingLabelStyle: GoogleFonts.dmSans(
                      color: Colors.white.withOpacity(0.50),
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w500,
                    ),
                    label: component.TextDmSans(
                      text:
                          FlutterI18n.translate(context, 'NAME').toUpperCase(),
                      textAlign: TextAlign.center,
                      color: Colors.white.withOpacity(0.50),
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
