part of 'add_item_screen.dart';

class _ItemTextfield extends StatefulWidget {
  const _ItemTextfield({
    Key? key,
    required this.obscureText,
    required this.field,
    required this.textController,
    required this.inputFormatters,
    required this.keyboardType,
    required this.maxLines,
    required this.minLines,
    required this.suffixIcon,
    required Secret secret,
    required Blueprint bp,
  })  : _secret = secret,
        _bp = bp,
        super(key: key);

  final bool obscureText;
  final BlueprintField field;
  final TextEditingController textController;
  final List<MaskTextInputFormatter> inputFormatters;
  final TextInputType? keyboardType;
  final int? maxLines;
  final int? minLines;
  final Secret _secret;
  final Blueprint _bp;
  final Widget? suffixIcon;

  @override
  State<_ItemTextfield> createState() => _ItemTextfieldState();
}

class _ItemTextfieldState extends State<_ItemTextfield> {
  int length = 0;
  String? validPasswordResult;
  String? validPasswordMessage;
  @override
  void initState() {
    validPasswordResult = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 16.h),
          child: component.CustomTextField(
            obscureText: widget.obscureText,
            labelText: FlutterI18n.translate(context, widget.field.title!),
            controller: widget.textController,
            inputFormatters: widget.inputFormatters,
            validator: (value) {
              if (widget.field.required && (value == null || value.isEmpty)) {
                return FlutterI18n.translate(
                    context, widget.field.errorMessageRequired!);
              }
              if (widget.field.field == 'input' &&
                  widget.field.type == 'totp_code' &&
                  !helper.isValidTOTPCode(value)) {
                return FlutterI18n.translate(context, 'INVALID_SECRET');
              }
              return null;
            },
            keyboardType: widget.keyboardType,
            maxLines: widget.maxLines,
            minLines: widget.minLines,
            suffixIcon: widget.suffixIcon,
            onChanged: (text) {
              widget._secret.data[widget.field.name] = text;
              if (widget.field.field == 'input' &&
                  ['website_password_url', 'bookmark_url']
                      .contains(widget.field.name) &&
                  widget._bp.urlfilterField != null) {
                if (text == '') {
                  widget._secret.data[widget._bp.urlfilterField] = '';
                } else {
                  ParsedUrl parsedUrl = helper.parseUrl(text);
                  widget._secret.data[widget._bp.urlfilterField] =
                      parsedUrl.authority;
                }
              }
              length = text.length;
              if (widget.field.field == 'input' &&
                  widget.field.type == 'password') {
                validPasswordResult = helper.isValidPassword(text, text, 12, 0);
                if (validPasswordResult != null) {
                  validPasswordMessage = validPasswordResult;
                } else {
                  validPasswordMessage =
                      length < 16 ? "PASSWORD_GOOD" : "PASSWORD_FANTASTIC";
                }
                setState(() {});
              }
            },
          ),
        ),
        if (widget.field.field == 'input' &&
            widget.field.type == 'password' &&
            FlutterI18n.translate(context, widget.field.title!) ==
                FlutterI18n.translate(context, "PASSWORD")) ...[
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 3.h,
                  decoration: BoxDecoration(
                    color: validPasswordResult != null
                        ? const Color(0xFFFF0F57)
                        : length < 16
                            ? const Color(0xffFFBC10)
                            : C.mountainMeadow,
                    borderRadius: BorderRadius.all(
                      Radius.circular(100.h),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 10.h),
          component.TextDmSans(
            text: FlutterI18n.translate(
              context,
              validPasswordMessage ?? "",
            ),
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            color: Colors.white.withOpacity(0.5),
          ),
          SizedBox(height: 16.h),
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () async {
                component.Loader.show(context);
                String password = await managerDatastorePassword.generate();
                component.Loader.hide();
                component.generatePasswordSheet(
                  context: context,
                  password: password,
                  onSetPassword: (password) {
                    setState(() {
                      widget.textController.text = password;
                      widget._secret.data[widget.field.name] =
                          widget.textController.text;
                    });
                  },
                );
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  component.TextDmSans(
                    text: FlutterI18n.translate(
                      context,
                      'GENERATE_PASSWORD',
                    ),
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                  SizedBox(width: 2.w),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                    size: 22.sp,
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 16.h),
        ]
      ],
    );
  }

  // Widget _passwordLengthContainer(
  //     int postion, final passwordLength, String? passwordValidityRes) {
  //   return Expanded(
  //     flex: 50,
  //     child: Opacity(
  //       opacity: 0.1,
  //       child: Container(
  //         height: 3.h,
  //         decoration: BoxDecoration(
  //           color: const Color(0xFFFF0F57),
  //           borderRadius: BorderRadius.all(
  //             Radius.circular(100.h),
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }
}
