part of 'add_item_screen.dart';

class _DefaultScreen extends StatelessWidget {
  const _DefaultScreen({
    Key? key,
    required this.menuList,
  }) : super(key: key);

  final List<Widget> menuList;

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'NEW_ENTRY',
          ),
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: ListView(
          children: menuList,
        ),
      ),
    );
  }
}
