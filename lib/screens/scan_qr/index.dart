import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:psono/components/_index.dart' as component;

class ScanQR extends StatefulWidget {
  static String tag = 'scan-config-screen';
  const ScanQR({
    Key? key,
    required this.backCount,
  }) : super(key: key);

  final int backCount;

  @override
  _ScanQRState createState() => _ScanQRState();
}

class _ScanQRState extends State<ScanQR> {
  String? qrScan = '';
  bool redirect = false;
  bool redirectInitiated = false;
  MobileScannerController controller = MobileScannerController(formats: [
    BarcodeFormat.qrCode,
  ]);

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.stop();
    } else if (Platform.isIOS) {
      controller.start();
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (redirect && !redirectInitiated) {
        Navigator.of(context).pop(qrScan);
        redirectInitiated = true;
      }
    });

    return component.ScaffoldDark(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          Expanded(flex: 7, child: _buildQrView(context)),
          Expanded(
            flex: 1,
            child: Center(
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: component.BtnPrimary(
                      onPressed: () {
                        controller.switchCamera();
                      },
                      text: FlutterI18n.translate(context, "FLIP_CAMERA"),
                    ),
                  ),
                  const Spacer(),
                  Expanded(
                    flex: 10,
                    child: component.BtnPrimary(
                      onPressed: () {
                        int count = 0;
                        Navigator.of(context)
                            .popUntil((_) => count++ >= widget.backCount);
                      },
                      text: FlutterI18n.translate(context, "ABORT"),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;

    return MobileScanner(
      controller: controller,
      onDetect: (BarcodeCapture capture) {
        final List<Barcode> barcodes = capture.barcodes;
        if (barcodes.isNotEmpty) {
          setState(() {
            qrScan = barcodes.first.rawValue;
            redirect = true;
          });
        }
      },
      fit: BoxFit.contain,
      scanWindow: Rect.fromCenter(
        center: Offset(
          MediaQuery.of(context).size.width / 2,
          MediaQuery.of(context).size.height / 2,
        ),
        width: scanArea,
        height: scanArea,
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
