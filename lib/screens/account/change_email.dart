import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';

class AccountChangeEmailScreen extends StatefulWidget {
  static String tag = 'account-change-email-screen';

  @override
  _AccountChangeEmailScreenState createState() =>
      _AccountChangeEmailScreenState();
}

class _AccountChangeEmailScreenState extends State<AccountChangeEmailScreen> {
  final email = TextEditingController(
    text: reduxStore.state.userEmail,
  );
  final password = TextEditingController(
    text: '',
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    email.dispose();
    password.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(context, title, content);
  }

  bool _obscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'CHANGE_E_MAIL',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnPrimary(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              component.Loader.show(context);
              try {
                await user_service.saveNewEmail(
                  email.text,
                  password.text,
                );
              } catch (e) {
                _showErrorDialog('ERROR', e.toString());
                return;
              } finally {
                component.Loader.hide();
              }

              password.text = '';

              if (!mounted) {
                return;
              }
              final SnackBar snackBar = SnackBar(
                content: Text(
                  FlutterI18n.translate(context, "SAVE_SUCCESS"),
                ),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE").toUpperCase(),
          ),
        ),
      ],
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              component.AlertInfo(
                text:
                    FlutterI18n.translate(context, "CHANGE_E_MAIL_DESCRIPTION"),
              ),
              SizedBox(height: 24.h),
              component.CustomTextField(
                controller: email,
                keyboardType: TextInputType.emailAddress,
                autofocus: true,
                validator: (value) {
                  if (value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'INVALID_EMAIL_IN_EMAIL',
                    );
                  }
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'NEW_E_MAIL',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: password,
                obscureText: _obscurePassword,
                suffixIcon: component.ObscureIcon(
                  onTap: () {
                    setState(() {
                      _obscurePassword = !_obscurePassword;
                    });
                  },
                  obscure: _obscurePassword,
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'OLD_PASSWORD_REQUIRED',
                    );
                  }
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'CURRENT_PASSWORD',
                ),
              ),
              const SizedBox(height: 16.0),
            ],
          ),
        ),
      ),
    );
  }
}
