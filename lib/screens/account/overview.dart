import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/utils/app_assets.dart';

class AccountOverviewScreen extends StatefulWidget {
  static String tag = 'account-overview-screen';

  @override
  _AccountOverviewScreenState createState() => _AccountOverviewScreenState();
}

class _AccountOverviewScreenState extends State<AccountOverviewScreen> {
  final userId = TextEditingController(
    text: reduxStore.state.userId,
  );
  final username = TextEditingController(
    text: reduxStore.state.username,
  );
  final email = TextEditingController(
    text: reduxStore.state.userEmail,
  );
  final publicKey = TextEditingController(
    text: converter.toHex(reduxStore.state.publicKey),
  );
  final serverApiVersion = TextEditingController(
    text: '',
  );
  final serverVersion = TextEditingController(
    text: '',
  );
  final serverSignature = TextEditingController(
    text: '',
  );
  final serverAuditLogging = TextEditingController(
    text: '',
  );
  final serverPublicKey = TextEditingController(
    text: '',
  );
  final serverLicenseType = TextEditingController(
    text: '',
  );
  final serverMaxUsers = TextEditingController(
    text: '',
  );
  final serverLicenseValidFrom = TextEditingController(
    text: '',
  );
  final serverLicenseValidTill = TextEditingController(
    text: '',
  );

  Future<void> loadServerInfo() async {
    api_client.Info packageInfo = await api_client.info();
    serverApiVersion.text = packageInfo.api.toString();
    serverVersion.text = packageInfo.version!;
    serverSignature.text = converter.toHex(packageInfo.signature)!;
    serverAuditLogging.text = packageInfo.logAudit.toString();
    serverPublicKey.text = converter.toHex(packageInfo.publicKey)!;

    if (packageInfo.licenseType == null) {
      serverLicenseType.text = 'Community Edition (CE)';
    } else if (packageInfo.licenseType == 'paid') {
      serverLicenseType.text = 'Enterprise Edition (EE)';
    } else {
      serverLicenseType.text = 'Enterprise Edition (EE) limited';
    }

    if (packageInfo.licenseMaxUsers == null) {
      serverMaxUsers.text = 'unlimited';
    } else {
      serverMaxUsers.text = packageInfo.licenseMaxUsers.toString();
    }

    if (packageInfo.licenseValidFrom == null) {
      serverLicenseValidFrom.text = 'N/A';
    } else {
      serverLicenseValidFrom.text = packageInfo.licenseValidFrom.toString();
    }

    if (packageInfo.licenseValidTill == null) {
      serverLicenseValidTill.text = 'N/A';
    } else {
      serverLicenseValidTill.text = packageInfo.licenseValidTill.toString();
    }
  }

  @override
  void initState() {
    super.initState();
    loadServerInfo();
  }

  @override
  void dispose() {
    userId.dispose();
    username.dispose();
    email.dispose();
    publicKey.dispose();
    serverApiVersion.dispose();
    serverVersion.dispose();
    serverSignature.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'OVERVIEW',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              component.TextDmSans(
                text: FlutterI18n.translate(context, "CLIENT_INFO"),
                color: Colors.white.withOpacity(0.60),
                fontSize: 18.sp,
                fontWeight: FontWeight.w500,
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: userId,
                labelText: FlutterI18n.translate(
                  context,
                  'USER_ID',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: username,
                labelText: FlutterI18n.translate(
                  context,
                  'USERNAME',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: email,
                labelText: FlutterI18n.translate(
                  context,
                  'EMAIL',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: publicKey,
                labelText: FlutterI18n.translate(
                  context,
                  'PUBLIC_KEY',
                ),
              ),
              SizedBox(height: 32.h),
              component.TextDmSans(
                text: FlutterI18n.translate(context, "SERVER_INFO"),
                color: Colors.white.withOpacity(0.60),
                fontSize: 18.sp,
                fontWeight: FontWeight.w500,
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverApiVersion,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_API_VERSION',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverVersion,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_VERSION',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverSignature,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_SIGNATURE',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverAuditLogging,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_AUDIT_LOGGING',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverPublicKey,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_PUBLIC_KEY',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverLicenseType,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_LICENSE_TYPE',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverMaxUsers,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_MAX_USERS',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverLicenseValidFrom,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_LICENSE_VALID_FROM',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                isEnabled: false,
                controller: serverLicenseValidTill,
                labelText: FlutterI18n.translate(
                  context,
                  'SERVER_LICENSE_VALID_TILL',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
