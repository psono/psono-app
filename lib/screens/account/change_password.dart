import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';

class AccountChangePasswordScreen extends StatefulWidget {
  static String tag = 'account-change-password-screen';

  @override
  _AccountChangePasswordScreenState createState() =>
      _AccountChangePasswordScreenState();
}

class _AccountChangePasswordScreenState
    extends State<AccountChangePasswordScreen> {
  final newPassword = TextEditingController(
    text: '',
  );
  final newPasswordRepeat = TextEditingController(
    text: '',
  );
  final oldPassword = TextEditingController(
    text: '',
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    newPassword.dispose();
    newPasswordRepeat.dispose();
    oldPassword.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();
  bool _obscureNewPassword = true;
  bool _obscureRepeatPassword = true;
  bool _obscureOldPassword = true;

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(context, title, content);
  }

  @override
  Widget build(BuildContext context) {
    late api_client.Info info;

    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'CHANGE_E_MAIL',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnPrimary(
            onPressed: () async {
              component.Loader.show(context);
              try {
                info = await api_client.info();
              } on api_client.ServiceUnavailableException {
                component.Loader.hide();
                _showErrorDialog(
                  FlutterI18n.translate(context, "SERVER_OFFLINE"),
                  FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                );
                return;
              } on HandshakeException {
                component.Loader.hide();
                _showErrorDialog(
                  FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
                  FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
                );
                return;
              } catch (e) {
                component.Loader.hide();
                _showErrorDialog(
                  FlutterI18n.translate(context, "SERVER_OFFLINE"),
                  FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                );
                return;
              }

              if (!_formKey.currentState!.validate()) {
                return;
              }
              try {
                await user_service.saveNewPassword(
                  newPassword.text,
                  newPasswordRepeat.text,
                  oldPassword.text,
                );
              } catch (e) {
                component.Loader.hide();
                _showErrorDialog('ERROR', e.toString());
                return;
              }

              newPassword.text = '';
              newPasswordRepeat.text = '';
              oldPassword.text = '';
              component.Loader.hide();

              if (!mounted) {
                return;
              }
              final SnackBar snackBar = SnackBar(
                content: Text(
                  FlutterI18n.translate(context, "SAVE_SUCCESS"),
                ),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE").toUpperCase(),
          ),
        ),
      ],
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              component.AlertInfo(
                text: FlutterI18n.translate(
                  context,
                  "CHANGE_PASSWORD_DESCRIPTION",
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                obscureText: _obscureNewPassword,
                controller: newPassword,
                autofocus: true,
                suffixIcon: component.ObscureIcon(
                  onTap: () {
                    setState(() {
                      _obscureNewPassword = !_obscureNewPassword;
                    });
                  },
                  obscure: _obscureNewPassword,
                ),
                validator: (value) {
                  String? testFailure = helper.isValidPassword(
                    value ?? '',
                    newPasswordRepeat.text,
                    info.complianceMinMasterPasswordLength,
                    info.complianceMinMasterPasswordComplexity,
                  );
                  if (testFailure == null) {
                    return null;
                  }
                  return FlutterI18n.translate(
                    context,
                    testFailure,
                  );
                },
                labelText: FlutterI18n.translate(
                  context,
                  'NEW_PASSWORD',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                obscureText: _obscureRepeatPassword,
                controller: newPasswordRepeat,
                suffixIcon: component.ObscureIcon(
                  onTap: () {
                    setState(() {
                      _obscureRepeatPassword = !_obscureRepeatPassword;
                    });
                  },
                  obscure: _obscureRepeatPassword,
                ),
                validator: (value) {
                  if (value!.isNotEmpty && value != newPassword.text) {
                    return FlutterI18n.translate(
                      context,
                      'PASSWORDS_DONT_MATCH',
                    );
                  }
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'NEW_PASSWORD_REPEAT',
                ),
              ),
              SizedBox(height: 16.h),
              component.CustomTextField(
                controller: oldPassword,
                obscureText: _obscureOldPassword,
                suffixIcon: component.ObscureIcon(
                  onTap: () {
                    setState(() {
                      _obscureOldPassword = !_obscureOldPassword;
                    });
                  },
                  obscure: _obscureOldPassword,
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'OLD_PASSWORD_REQUIRED',
                    );
                  }
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'OLD_PASSWORD',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
