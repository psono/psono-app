import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/avatar.dart' as avatar_service;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';

import './change_email.dart';
import './change_password.dart';
import './delete_account.dart';
import './overview.dart';

class AccountScreen extends StatefulWidget {
  static String tag = 'settings-screen';

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  late Future _imageAvatarFuture;
  @override
  void initState() {
    _imageAvatarFuture = avatar_service.readAvatar();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _imageAvatarFuture = avatar_service.readAvatar();
    });
    super.initState();
  }

  String avatarID = '';

  @override
  Widget build(BuildContext context) {
    List<Widget> menuList = [];

    bool complianceDisableDeleteAccount =
        reduxStore.state.complianceDisableDeleteAccount;

    menuList.add(
      _Item(
        text: FlutterI18n.translate(context, "OVERVIEW"),
        leadingImage: AppAssets.iconOverview,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AccountOverviewScreen(),
            ),
          );
        },
      ),
    );

    menuList.add(
      _Item(
        text: FlutterI18n.translate(context, "CHANGE_E_MAIL"),
        leadingImage: AppAssets.iconChangeEmail,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AccountChangeEmailScreen(),
            ),
          );
        },
      ),
    );
    menuList.add(
      _Item(
        text: FlutterI18n.translate(context, "CHANGE_PASSWORD"),
        leadingImage: AppAssets.iconLock4,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AccountChangePasswordScreen(),
            ),
          );
        },
      ),
    );

    menuList.addAll(
      [
        _Item(
          text: FlutterI18n.translate(context, "LOGOUT"),
          leadingImage: AppAssets.iconUser2,
          onTap: () async {
            await user_service.logout();
            helper.makeFirstNamed(context, AppRoutes.appOnboarding);
          },
        ),
      ],
    );

    if (!complianceDisableDeleteAccount) {
      menuList.add(
        _Item(
          text: FlutterI18n.translate(context, "DELETE_ACCOUNT"),
          leadingImage: AppAssets.iconDeleteAccount,
          isDelete: true,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AccountDeleteAccountScreen(),
              ),
            );
          },
        ),
      );
    }

    menuList.addAll(
      [
        const SizedBox(height: 60),
      ],
    );

    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                alignment: Alignment.bottomRight,
                clipBehavior: Clip.none,
                children: [
                  FutureBuilder(
                    future: _imageAvatarFuture,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        final avatar = snapshot.data as String?;
                        if (avatar == null ||
                            avatar.isEmpty ||
                            reduxStore.state.userId == null) {
                          Future(() {
                            if (context.mounted) {
                              setState(() {
                                avatarID = "";
                              });
                            }
                          });
                          return const CircleAvatar(
                            radius: 68,
                            backgroundImage: AssetImage(
                              AppAssets.defaultAvatarImage,
                            ),
                          );
                        }
                        final serverUrl = reduxStore.state.serverUrl;
                        final userID = reduxStore.state.userId;
                        final url = serverUrl +
                            "/avatar-image/" +
                            userID +
                            "/" +
                            avatar +
                            "/";
                        Future(() {
                          if (context.mounted) {
                            setState(() {
                              avatarID = "";
                            });
                          }
                        });
                        return CircleAvatar(
                          radius: 68,
                          backgroundImage: NetworkImage(url),
                        );
                      }
                      Future(() {
                        if (context.mounted) {
                          setState(() {
                            avatarID = "";
                          });
                        }
                      });
                      return const CircleAvatar(
                        radius: 68,
                        backgroundImage: AssetImage(
                          AppAssets.defaultAvatarImage,
                        ),
                      );
                    },
                  ),
                  InkWell(
                    onTap: () async {
                      if (avatarID.isEmpty) {
                        _chooseImage();
                      } else {
                        component.showCustomBottomSheet(
                          context: context,
                          barrierColor: Colors.black.withOpacity(0.7),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15.h, horizontal: 20.w),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  width: 50.w,
                                  height: 3.h,
                                  decoration: ShapeDecoration(
                                    color: Colors.white.withOpacity(0.30),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20.h),
                                component.TextDmSans(
                                  text: FlutterI18n.translate(
                                    context,
                                    "CHANGE_AVATAR",
                                  ),
                                  textAlign: TextAlign.center,
                                  color: Colors.white,
                                  fontSize: 18.sp,
                                  fontWeight: FontWeight.w700,
                                ),
                                SizedBox(height: 24.h),
                                Container(
                                  decoration: ShapeDecoration(
                                    shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                        width: 1,
                                        strokeAlign:
                                            BorderSide.strokeAlignCenter,
                                        color: Colors.white.withOpacity(0.2),
                                      ),
                                    ),
                                  ),
                                ),
                                _Item(
                                  text: FlutterI18n.translate(
                                      context, "CHANGE_AVATAR"),
                                  leadingImage: AppAssets.iconCamera,
                                  onTap: () {
                                    Navigator.pop(context);
                                    _chooseImage();
                                  },
                                ),
                                _Item(
                                  text: FlutterI18n.translate(
                                      context, "DELETE_AVATAR"),
                                  leadingImage: AppAssets.iconDeleteAccount,
                                  isDelete: true,
                                  onTap: () {
                                    Navigator.pop(context);
                                    _deleteImage();
                                  },
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.all(4.h),
                      child: Icon(
                        avatarID.isEmpty ? Icons.camera_alt : Icons.edit,
                        color: Colors.black,
                        size: 16.sp,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 16.h),
              Center(
                child: component.TextDmSans(
                  text: reduxStore.state.username.split('@')[0],
                  color: Colors.white,
                  fontSize: 48,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(height: 40.h),
              Container(
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      width: 1,
                      strokeAlign: BorderSide.strokeAlignCenter,
                      color: Colors.white.withOpacity(0.2),
                    ),
                  ),
                ),
              ),
              ...menuList,
            ],
          ),
        ),
      ),
    );
  }

  _deleteImage() async {
    try {
      component.Loader.show(context);
      final avatar = await avatar_service.deleteAvatar(avatarID);
      log('avatar: $avatar');
      setState(() {
        avatarID = '';
        _imageAvatarFuture = avatar_service.readAvatar();
      });
    } catch (e) {
      component.Loader.hide();
      helper.showErrorDialog(
        context,
        "ERROR",
        e.toString(),
      );
    } finally {
      component.Loader.hide();
    }
  }

  _chooseImage() async {
    Uint8List? image;
    try {
      image = await helper.getImageFromGallery(context);
      component.Loader.show(context);
      if (image == null) {
        return;
      }

      String base64 = helper.uint8ListTob64(image);

      final avatar = await avatar_service.createAvatar(base64);
      if (avatar != null) {
        setState(() {
          _imageAvatarFuture = avatar_service.readAvatar();
        });
      }
    } catch (e) {
      helper.showErrorDialog(
        context,
        "ERROR",
        e.toString(),
      );
    } finally {
      component.Loader.hide();
    }
  }
}

class _Item extends StatelessWidget {
  const _Item({
    super.key,
    required this.text,
    required this.leadingImage,
    this.onTap,
    this.isDelete = false,
  });
  final String text;
  final String leadingImage;
  final Function()? onTap;
  final bool isDelete;

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Padding(
      padding: EdgeInsets.only(bottom: 12.h),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12.h),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.white.withOpacity(0.2),
                width: 1,
              ),
            ),
          ),
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.all(8.w),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.10),
                  borderRadius: BorderRadius.circular(8.w),
                ),
                child: Image.asset(
                  leadingImage,
                  width: orientation == Orientation.portrait ? 24.w : 18.w,
                  height: orientation == Orientation.portrait ? 24.w : 18.w,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(width: 12.w),
              component.TextDmSans(
                text: text,
                color: isDelete ? const Color(0xffA50031) : Colors.white,
                fontSize: orientation == Orientation.portrait ? 16.sp : 12.sp,
                fontWeight: FontWeight.w500,
              ),
              const Spacer(),
              IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_right,
                  color: isDelete ? const Color(0xffA50031) : Colors.white,
                  size: orientation == Orientation.portrait ? 20.sp : 16.sp,
                ),
                onPressed: onTap,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
