import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/routes/routes.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';

class AccountDeleteAccountScreen extends StatefulWidget {
  static String tag = 'account-delete-account-screen';

  @override
  _AccountDeleteAccountScreenState createState() =>
      _AccountDeleteAccountScreenState();
}

class _AccountDeleteAccountScreenState
    extends State<AccountDeleteAccountScreen> {
  final password = TextEditingController(
    text: '',
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    password.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    void _showErrorDialog(String title, String? content) {
      helper.showErrorDialog(context, title, content);
    }

    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      appBar: AppBar(
        title: component.TextDmSans(
          text: FlutterI18n.translate(
            context,
            'DELETE_ACCOUNT',
          ),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w700,
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.navigate_before,
            color: Colors.white,
            size: 40.sp,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnPrimary(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              component.Loader.show(context);
              try {
                await user_service.deleteAccount(password.text);
              } catch (e) {
                _showErrorDialog('ERROR', e.toString());
                return;
              } finally {
                component.Loader.hide();
              }
              if (mounted) {
                Navigator.pushReplacementNamed(context, AppRoutes.signin);
              }
            },
            text: FlutterI18n.translate(context, "DELETE").toUpperCase(),
          ),
        ),
      ],
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.h),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              component.AlertDanger(
                text: FlutterI18n.translate(
                    context, "YOU_ARE_ABOUT_TO_DELETE_YOUR_ACCOUNT"),
              ),
              SizedBox(height: 24.w),
              component.CustomTextField(
                obscureText: true,
                controller: password,
                validator: (value) {
                  if (value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'OLD_PASSWORD_REQUIRED',
                    );
                  }
                  return null;
                },
                labelText: FlutterI18n.translate(
                  context,
                  'YOUR_PASSWORD_AS_CONFIRMATION',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
