import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:local_auth/local_auth.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/utils/app_assets.dart';

//ARGs
class EnableBioMetricsArgs {
  final List<BiometricType> bioMetrics;
  EnableBioMetricsArgs({required this.bioMetrics});
}

class EnableBioMetrics extends StatefulWidget {
  const EnableBioMetrics({Key? key}) : super(key: key);

  @override
  State<EnableBioMetrics> createState() => _EnableBioMetricsState();
}

class _EnableBioMetricsState extends State<EnableBioMetrics> {
  final LocalAuthentication auth = LocalAuthentication();
  List<BiometricType>? bioMetrics = [];
  bool isFaceIDAvaliable = false;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final route = ModalRoute.of(context);
      if (route != null) {
        final args = route.settings.arguments as EnableBioMetricsArgs?;
        if (args != null) {
          bioMetrics = args.bioMetrics;
          isFaceIDAvaliable =
              bioMetrics != null && bioMetrics!.first == BiometricType.face;
          setState(() {});
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.faceIDBg,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: TextDmSans(
          text: FlutterI18n.translate(
              context, isFaceIDAvaliable ? "FACE_ID" : "TOUCH_ID"),
          color: Colors.white,
          fontSize: 18.sp,
          fontWeight: FontWeight.w800,
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          children: [
            const Spacer(flex: 4),
            Center(
              child: Image.asset(
                isFaceIDAvaliable ? AppAssets.faceLock : AppAssets.fingerLock,
                height: 236.h,
                width: 222.w,
              ),
            ),
            const Spacer(flex: 4),
            TextDmSans(
              text: FlutterI18n.translate(context,
                  isFaceIDAvaliable ? "ENABLE_FACE_ID" : "ENABLE_TOUCH_ID"),
              textAlign: TextAlign.center,
              color: Colors.white,
              fontSize: 40.sp,
              fontWeight: FontWeight.w500,
            ),
            const Spacer(),
            TextDmSans(
              text: FlutterI18n.translate(
                context,
                isFaceIDAvaliable
                    ? "ENABLE_FACE_ID_DESC"
                    : "ENABLE_TOUCH_ID_DESC",
              ),
              textAlign: TextAlign.center,
              color: Colors.white,
              fontSize: 14.sp,
              fontWeight: FontWeight.w400,
            ),
            const Spacer(flex: 2),
            component.BtnPrimary(
              text: FlutterI18n.translate(
                context,
                isFaceIDAvaliable ? "ENABLE_FACE_ID_DESC" : "ENABLE_TOUCH_ID",
              ),
              onPressed: () async {
                final isAuthorized = await auth.authenticate(
                  localizedReason: FlutterI18n.translate(
                    context,
                    "USE_BIOMETRIC_AUTHENTICATION_TO_UNLOCK_YOUR_PSONO",
                  ),
                  options: const AuthenticationOptions(
                    biometricOnly: true,
                    useErrorDialogs: true,
                    stickyAuth: false,
                  ),
                );
                if (isAuthorized) {
                  Navigator.of(context).pushNamed(AppRoutes.datastore);
                }
              },
            ),
            const Spacer(),
            component.BtnText(
              onPressed: () {
                Navigator.of(context).pushNamed(AppRoutes.datastore);
              },
              text: FlutterI18n.translate(context, "SKIP").toUpperCase(),
              color: Colors.white.withOpacity(0.60),
              fontSize: 14.sp,
              fontWeight: FontWeight.w700,
            ),
            SizedBox(height: 20.h),
          ],
        ),
      ),
    );
  }
}
