import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:local_auth/local_auth.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/screens/enable_biometrics/enable_biometrics.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_constants.dart';

class PinConfigurationScreen extends StatefulWidget {
  @override
  _PinConfigurationScreenState createState() => _PinConfigurationScreenState();
}

class _PinConfigurationScreenState extends State<PinConfigurationScreen> {
  final controller = TextEditingController();
  final focusNode = FocusNode();

  @override
  void dispose() {
    focusNode.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;

    Widget titleAndPinInput = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: orientation == Orientation.portrait ? 40.sp : 0),
        TextDmSans(
          text: FlutterI18n.translate(context, "AUTHORIZATION"),
          color: Colors.white,
          fontSize: orientation == Orientation.portrait ? 40.sp : 20.sp,
          fontWeight: FontWeight.w500,
        ),
        SizedBox(height: orientation == Orientation.portrait ? 80.sp : 20.sp),
        TextDmSans(
          text: FlutterI18n.translate(context, "CONFIGURE_PIN"),
          fontSize: orientation == Orientation.portrait ? 14.sp : 12.sp,
          color: Colors.white.withOpacity(0.60),
          fontWeight: FontWeight.w400,
        ),
        SizedBox(height: 16.sp),
        component.PinInputCustom(
          controller: controller,
          focusNode: focusNode,
          onContinue: (code) async {
            final LocalAuthentication auth = LocalAuthentication();
            await storage.write(
              key: SK.lockscreenPin,
              value: controller.text,
              iOptions: secureIOSOptions,
            );
            final isSupported = await auth.isDeviceSupported();
            final bioMetrics = await auth.getAvailableBiometrics();
            if (isSupported &&
                bioMetrics.isNotEmpty &&
                (bioMetrics.contains(BiometricType.face) ||
                    bioMetrics.contains(BiometricType.fingerprint))) {
              helper.makeFirstNamed(context, AppRoutes.enableBioMetrics,
                  arguments: EnableBioMetricsArgs(bioMetrics: bioMetrics));
            } else {
              Navigator.pushNamed(context, AppRoutes.datastore);
            }
          },
        ),
      ],
    );

    Widget customKeyboard = component.CustomKeyBoard(
      maxLength: 6,
      flex: 10,
      specialKey: Container(),
      onChanged: (str) {
        controller.text = str;
      },
      pinTheme: component.PinTheme(
        keysColor: Colors.white,
      ),
    );

    return component.ScaffoldDark(
      body: orientation == Orientation.portrait
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Spacer(),
                titleAndPinInput,
                const Spacer(
                  flex: 3,
                ),
                customKeyboard,
                const Spacer(),
              ],
            )
          : Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(16.w),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        titleAndPinInput,
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(16.w),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        customKeyboard,
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
