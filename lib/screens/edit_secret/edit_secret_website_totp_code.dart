part of 'edit_secret.dart';

class _WebsiteTotpCode extends StatefulWidget {
  const _WebsiteTotpCode({
    super.key,
    this.code,
    this.algorithm,
    this.digits,
    this.period,
    this.onTotpDelete,
  });
  final String? code;
  final String? algorithm;
  final int? digits;
  final int? period;
  final Function()? onTotpDelete;
  @override
  State<_WebsiteTotpCode> createState() => __WebsiteTotpCodeState();
}

class __WebsiteTotpCodeState extends State<_WebsiteTotpCode> {
  TextEditingController _textController = TextEditingController();
  int millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
  late int countdown;
  late String remainingTime;
  int? length = 6; // default length 6
  Algorithm algorithm = Algorithm.SHA1; // default algorithm SHA1
  int? interval = 30; // default interval 30
  final ValueNotifier<double> percentNotifier = ValueNotifier<double>(1);
  late Timer timer;
  final ValueNotifier<String> generatedCode = ValueNotifier<String>('');
  late FToast _fToast;
  bool _obscure = true;

  @override
  void initState() {
    if (widget.digits != null) {
      length = widget.digits;
    }

    if (widget.algorithm == 'SHA256') {
      algorithm = Algorithm.SHA256;
    } else if (widget.algorithm == 'SHA512') {
      algorithm = Algorithm.SHA512;
    }

    if (widget.period != null) {
      interval = widget.period;
      remainingTime =
          '${(interval! ~/ 60).toString().padLeft(2, '0')}:${(interval! % 60).toString().padLeft(2, '0')}';
    }

    countdown = interval! -
        ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() % interval!);
    percentNotifier.value = 1 -
        (interval! -
                ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() %
                    interval!)) /
            interval!;

    _fToast = FToast();
    _fToast.init(context);

    timer = Timer.periodic(const Duration(milliseconds: 200), (timer) {
      if (helper.isValidTOTPCode(widget.code)) {
        millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
        countdown =
            interval! - ((millisecondsSinceEpoch ~/ 1000).round() % interval!);
        remainingTime =
            '${(countdown ~/ 60).toString().padLeft(2, '0')}:${(countdown % 60).toString().padLeft(2, '0')}';
        percentNotifier.value = 1 - countdown / interval!;

        _textController.text = generatedCode.value = OTP.generateTOTPCodeString(
          widget.code!,
          millisecondsSinceEpoch,
          algorithm: algorithm,
          interval: interval!,
          length: length!,
          isGoogle: true,
        );
        if (!mounted) {
          return;
        }
      } else {
        if (!mounted) {
          return;
        }
        generatedCode.value = FlutterI18n.translate(context, 'INVALID_CODE');
        _textController.text = generatedCode.value;
      }
      setState(() {});
    });

    super.initState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 16.h),
      child: Column(
        children: [
          ValueListenableBuilder<String>(
            valueListenable: generatedCode,
            builder: (context, value, _) {
              return component.CustomTextField(
                labelText: FlutterI18n.translate(context, 'TOTP'),
                controller: _textController,
                obscureText: _obscure,
                isEnabled: false,
                suffixIcon: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    component.CopyIcon(
                      onTap: () {
                        Clipboard.setData(
                          ClipboardData(
                            text: _textController.text,
                          ),
                        );
                        _clipboardCopyToast();
                      },
                    ),
                    component.ObscureIcon(
                      onTap: () {
                        setState(() {
                          _obscure = !_obscure;
                        });
                      },
                      obscure: _obscure,
                    ),
                  ],
                ),
              );
            },
          ),
          ValueListenableBuilder<double>(
            valueListenable: percentNotifier,
            builder: (context, value, child) {
              return LinearProgressIndicator(
                color: AppColors.springGreen,
                backgroundColor: Colors.white.withOpacity(0.10),
                value: percentNotifier.value,
              );
            },
          ),
          SizedBox(height: 6.h),
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {
                widget.onTotpDelete?.call();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    FlutterI18n.translate(context, 'DELETE'),
                    style: TextStyle(
                      color: Colors.white.withOpacity(0.50),
                      fontSize: 14.sp,
                    ),
                  ),
                  SizedBox(width: 4.w),
                  Icon(
                    Icons.delete,
                    color: Colors.white.withOpacity(0.50),
                    size: 18.sp,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: const Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            component.FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: const TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
      child: toast,
      toastDuration: const Duration(seconds: 2),
      positionedToastBuilder: (context, child) {
        return Positioned(top: 110, left: 0, right: 0, child: child);
      },
    );
  }
}
