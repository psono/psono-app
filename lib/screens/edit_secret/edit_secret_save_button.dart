part of 'edit_secret.dart';

class _SaveButton extends StatelessWidget {
  const _SaveButton({
    Key? key,
    required GlobalKey<FormState> formKey,
    required Blueprint? bp,
    required Secret secret,
    required this.widget,
  })  : _formKey = formKey,
        _bp = bp,
        _secret = secret,
        super(key: key);

  final GlobalKey<FormState> _formKey;
  final Blueprint? _bp;
  final Secret _secret;
  final EditSecretScreen widget;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: component.BtnPrimary(
        onPressed: () async {
          if (!_formKey.currentState!.validate()) {
            return;
          }
          component.Loader.show(context);

          for (final field
              in _bp!.fields!.where((field) => field.inputFormatter != null)) {
            if (_secret.data.containsKey(field.name)) {
              field.inputFormatter!.formatEditUpdate(
                TextEditingValue.empty,
                TextEditingValue(
                  text: _secret.data[field.name],
                ),
              );
              _secret.data[field.name] =
                  field.inputFormatter!.getUnmaskedText();
            }
          }

          _secret.save();

          List<String?> itemPath = List<String?>.from(widget.relativePath!);
          itemPath.add(widget.item!.id);

          if (widget.item!.shareId != null) {
            shareModel.Share share = await managerShare.readShare(
              widget.item!.shareId,
              widget.item!.shareSecretKey,
            );

            share.item!.name = _secret.data[_bp!.titleField];
            if (_bp!.descriptionField != null) {
              share.item!.description = _secret.data[_bp!.descriptionField];
              if (_bp!.id == "credit_card" && share.item!.description != null) {
                share.item!.description =
                    share.item!.description!.replaceAllMapped(
                  RegExp(r'.(?=.{4})'),
                  (match) => 'x',
                );
              }
            }
            if (_bp!.urlfilterField != null) {
              share.item!.urlfilter = _secret.data[_bp!.urlfilterField];
            }

            widget.item!.name = _secret.data[_bp!.titleField];
            if (_bp!.descriptionField != null) {
              widget.item!.description = _secret.data[_bp!.descriptionField];
              if (_bp!.id == "credit_card" &&
                  widget.item!.description != null) {
                widget.item!.description =
                    widget.item!.description!.replaceAllMapped(
                  RegExp(r'.(?=.{4})'),
                  (match) => 'x',
                );
              }
            }
            if (_bp!.urlfilterField != null) {
              widget.item!.urlfilter = _secret.data[_bp!.urlfilterField];
            }

            await share.save();
          } else if (widget.share == null) {
            datastoreModel.Datastore datastore =
                await managerDatastorePassword.getPasswordDatastore(
              widget.datastore!.datastoreId,
            );

            List<String> pathCopy = List<String>.from(itemPath);
            List search = managerDatastorePassword.findInDatastore(
              pathCopy,
              datastore.data,
            );
            datastoreModel.Item remoteItem = search[0][search[1]];

            remoteItem.name = _secret.data[_bp!.titleField];
            if (_bp!.descriptionField != null) {
              remoteItem.description = _secret.data[_bp!.descriptionField];
              if (_bp!.id == "credit_card" && remoteItem.description != null) {
                remoteItem.description =
                    remoteItem.description!.replaceAllMapped(
                  RegExp(r'.(?=.{4})'),
                  (match) => 'x',
                );
              }
            }

            if (_bp!.urlfilterField != null) {
              remoteItem.urlfilter = _secret.data[_bp!.urlfilterField];
            }

            widget.item!.name = _secret.data[_bp!.titleField];
            if (_bp!.descriptionField != null) {
              widget.item!.description = _secret.data[_bp!.descriptionField];
              if (_bp!.id == "credit_card" &&
                  widget.item!.description != null) {
                widget.item!.description =
                    widget.item!.description!.replaceAllMapped(
                  RegExp(r'.(?=.{4})'),
                  (match) => 'x',
                );
              }
            }
            if (_bp!.urlfilterField != null) {
              widget.item!.urlfilter = _secret.data[_bp!.urlfilterField];
            }

            await datastore.save();
          } else {
            shareModel.Share share = await managerShare.readShare(
              widget.share!.shareId,
              widget.share!.shareSecretKey,
            );

            List<String> pathCopy = List<String>.from(itemPath);
            List search = managerDatastorePassword.findInDatastore(
              pathCopy,
              share.folder,
            );
            datastoreModel.Item remoteItem = search[0][search[1]];

            remoteItem.name = _secret.data[_bp!.titleField];
            if (_bp!.descriptionField != null) {
              remoteItem.description = _secret.data[_bp!.descriptionField];
              if (_bp!.id == "credit_card" && remoteItem.description != null) {
                remoteItem.description =
                    remoteItem.description!.replaceAllMapped(
                  RegExp(r'.(?=.{4})'),
                  (match) => 'x',
                );
              }
            }
            if (_bp!.urlfilterField != null) {
              remoteItem.urlfilter = _secret.data[_bp!.urlfilterField];
            }

            widget.item!.name = _secret.data[_bp!.titleField];
            if (_bp!.descriptionField != null) {
              widget.item!.description = _secret.data[_bp!.descriptionField];
              if (_bp!.id == "credit_card" &&
                  widget.item!.description != null) {
                widget.item!.description =
                    widget.item!.description!.replaceAllMapped(
                  RegExp(r'.(?=.{4})'),
                  (match) => 'x',
                );
              }
            }
            if (_bp!.urlfilterField != null) {
              widget.item!.urlfilter = _secret.data[_bp!.urlfilterField];
            }

            await share.save();
          }
          component.Loader.hide();
          return;
        },
        text: FlutterI18n.translate(context, "SAVE"),
      ),
    );
  }
}
