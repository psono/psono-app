part of 'edit_secret.dart';

class _ItemTextField extends StatefulWidget {
  const _ItemTextField({
    Key? key,
    required this.obscureText,
    required this.textController,
    required this.inputFormatters,
    required this.field,
    required this.keyboardType,
    required this.maxLines,
    required Secret secret,
    required Blueprint? bp,
    this.onTOTPScan,
    this.suffix,
  })  : _secret = secret,
        _bp = bp,
        super(key: key);

  final bool obscureText;
  final TextEditingController textController;
  final List<MaskTextInputFormatter> inputFormatters;
  final BlueprintField field;
  final TextInputType? keyboardType;
  final int? maxLines;
  final Secret _secret;
  final Blueprint? _bp;
  final Widget? suffix;
  final Function(modelOTP.OTP)? onTOTPScan;

  @override
  State<_ItemTextField> createState() => _ItemTextFieldState();
}

class _ItemTextFieldState extends State<_ItemTextField> {
  int length = 0;
  String? validPasswordResult;
  String? validPasswordMessage;
  @override
  void initState() {
    if (widget.field.field == 'input' && widget.field.type == 'password') {
      validPasswordResult = helper.isValidPassword(
          widget.textController.text, widget.textController.text, 12, 0);
      if (validPasswordResult != null) {
        validPasswordMessage = validPasswordResult;
      } else {
        validPasswordMessage =
            length < 16 ? "PASSWORD_GOOD" : "PASSWORD_FANTASTIC";
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 16.h),
          child: Column(
            children: [
              component.CustomTextField(
                obscureText: widget.obscureText,
                controller: widget.textController,
                inputFormatters: widget.inputFormatters,
                suffixIcon: widget.suffix,
                validator: (value) {
                  if (widget.field.required && value!.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      widget.field.errorMessageRequired!,
                    );
                  }
                  return null;
                },
                keyboardType: widget.keyboardType,
                maxLines: widget.maxLines,
                onChanged: (text) {
                  widget._secret.data[widget.field.name] = text;
                  if (widget.field.field == 'input' &&
                      ['website_password_url', 'bookmark_url']
                          .contains(widget.field.name) &&
                      widget._bp!.urlfilterField != null) {
                    if (text == '') {
                      widget._secret.data[widget._bp!.urlfilterField] = '';
                    } else {
                      ParsedUrl parsedUrl = helper.parseUrl(text);
                      widget._secret.data[widget._bp!.urlfilterField] =
                          parsedUrl.authority;
                    }
                    length = text.length;
                    if (widget.field.field == 'input' &&
                        widget.field.type == 'password') {
                      validPasswordResult =
                          helper.isValidPassword(text, text, 12, 0);
                      if (validPasswordResult != null) {
                        validPasswordMessage = validPasswordResult;
                      } else {
                        validPasswordMessage = length < 16
                            ? "PASSWORD_GOOD"
                            : "PASSWORD_FANTASTIC";
                      }
                      setState(() {});
                    }
                  }
                },
                labelText: FlutterI18n.translate(context, widget.field.title!),
              ),
            ],
          ),
        ),
        if (widget.field.field == 'input' &&
            widget.field.type == 'password' &&
            FlutterI18n.translate(context, widget.field.title!) ==
                FlutterI18n.translate(context, "PASSWORD"))
          ..._buildPasswordRelated(context),
      ],
    );
  }

  List<Widget> _buildPasswordRelated(BuildContext context) {
    return [
      Row(
        children: [
          Expanded(
            child: Container(
              height: 3.h,
              decoration: BoxDecoration(
                color: validPasswordResult != null
                    ? const Color(0xFFFF0F57)
                    : length < 16
                        ? const Color(0xffFFBC10)
                        : C.mountainMeadow,
                borderRadius: BorderRadius.all(
                  Radius.circular(100.h),
                ),
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 10.h),
      component.TextDmSans(
        text: FlutterI18n.translate(
          context,
          validPasswordMessage ?? "",
        ),
        fontSize: 12.sp,
        fontWeight: FontWeight.w500,
        color: Colors.white.withOpacity(0.5),
      ),
      SizedBox(height: 10.h),
      Row(
        children: [
          if (widget._bp?.id == "website_password" &&
              !(widget._secret.data
                  .containsKey("website_password_totp_period")) &&
              !(widget._secret.data
                  .containsKey("website_password_totp_digits")) &&
              !(widget._secret.data
                  .containsKey("website_password_totp_algorithm")))
            InkWell(
              onTap: () async {
                final String? result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ScanQR(
                      backCount: 1,
                    ),
                  ),
                );
                if (result != null) {
                  modelOTP.OTP otp = helper.parseTOTPUri(result);
                  log(otp.toString());
                  widget.onTOTPScan?.call(otp);
                }
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    AppAssets.iconScan,
                    height: 20.h,
                    width: 20.h,
                  ),
                  SizedBox(width: 4.w),
                  component.TextDmSans(
                    text: FlutterI18n.translate(
                      context,
                      'ADD_TOTP',
                    ),
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          const Spacer(),
          InkWell(
            onTap: () async {
              component.Loader.show(context);
              String password = await managerDatastorePassword.generate();
              component.Loader.hide();
              component.generatePasswordSheet(
                context: context,
                password: password,
                onSetPassword: (password) {
                  setState(() {
                    widget.textController.text = password;
                    widget._secret.data[widget.field.name] =
                        widget.textController.text;
                  });
                },
              );
            },
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  component.TextDmSans(
                    text: FlutterI18n.translate(
                      context,
                      'GENERATE_PASSWORD',
                    ),
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                  SizedBox(width: 2.w),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                    size: 22.sp,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 16.h),
    ];
  }
}
