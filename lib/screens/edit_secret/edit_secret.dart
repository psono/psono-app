import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:otp/otp.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/blueprint.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/otp.dart' as modelOTP;
import 'package:psono/model/parsed_url.dart';
import 'package:psono/model/secret.dart';
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/screens/scan_qr/index.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_secret.dart' as managerSecret;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';

part 'edit_secert_access_denied.dart';
part 'edit_secret_item_textfield.dart';
part 'edit_secret_save_button.dart';
part 'edit_secret_title_textfield.dart';
part 'edit_secret_website_totp_code.dart';

class EditSecretScreen extends StatefulWidget {
  static String tag = 'secret-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final datastoreModel.Item? item;
  final List<String?>? path;
  final List<String?>? relativePath;

  const EditSecretScreen({
    this.parent,
    this.datastore,
    this.share,
    this.item,
    this.path,
    this.relativePath,
  });

  @override
  _EditSecretScreenState createState() => _EditSecretScreenState();
}

class _EditSecretScreenState extends State<EditSecretScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final bool _advanced = false;
  late Secret _secret;
  bool secretLoaded = false;
  Blueprint? _bp;
  datastoreModel.Item? item;
  List<TextEditingController> controllers = [];
  bool _obscurePassword = true;
  late FToast _fToast;

  Future<void> loadSecret() async {
    if (widget.item == null ||
        widget.item!.secretId == null ||
        widget.item!.secretKey == null) {
      return;
    }
    component.Loader.show(context);
    Secret secret;
    try {
      secret = await managerSecret.readSecret(
          widget.item!.secretId!, widget.item!.secretKey!);
    } on api_client.ServiceUnavailableException {
      component.Loader.hide();
      if (!mounted) {
        return;
      }
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "SERVER_OFFLINE"),
        FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
      );
      return;
    } on HandshakeException {
      component.Loader.hide();
      if (!mounted) {
        return;
      }
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
        FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
      );
      return;
    } catch (e) {
      component.Loader.hide();
      if (!mounted) {
        return;
      }
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "SERVER_OFFLINE"),
        FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
      );
      return;
    }

    setState(() {
      _secret = secret;
      secretLoaded = true;
    });
    component.Loader.hide();
  }

  _EditSecretScreenState({this.item});

  @override
  void initState() {
    super.initState();

    _fToast = FToast();
    _fToast.init(context);

    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (widget.item!.secretId != null) {
        loadSecret();
        setState(() {
          _bp = itemBlueprint.getBlueprint(widget.item!.type);
        });
      }
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    for (var i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future handleRefresh() async {
      await loadSecret();
    }

    int getFieldLength() {
      if (_bp == null) {
        return 0;
      }
      if (_advanced) {
        return _bp!.fields!.length + 1;
      } else {
        int advancedCount =
            _bp!.fields!.where((field) => field.position == 'advanced').length;
        return _bp!.fields!.length - advancedCount + 1;
      }
    }

    if (widget.item!.secretId == null) {
      return const _AccessDenied();
    }

    if (!secretLoaded) {
      return component.ScaffoldDark(
        backgroundPath: AppAssets.folderBg,
      );
    } else {
      return component.ScaffoldDark(
        backgroundPath: AppAssets.folderBg,
        appBar: AppBar(
          title: component.TextDmSans(
            text: _secret.data[_bp!.titleField] ?? "",
            fontSize: 18.sp,
            color: Colors.white,
            fontWeight: FontWeight.w700,
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.navigate_before,
              color: Colors.white,
              size: 40.sp,
            ),
          ),
        ),
        body: RefreshIndicator(
          onRefresh: handleRefresh,
          child: Form(
            key: _formKey,
            child: ListView.builder(
              itemCount: getFieldLength(),
              itemBuilder: (context, index) {
                int? websiteTotpPeriod;
                int? websiteTotpDigits;
                String? websiteTotpAlgorithm;
                String? websiteTotpCode;

                if (index == getFieldLength() - 1) {
                  return _SaveButton(
                      formKey: _formKey,
                      bp: _bp,
                      secret: _secret,
                      widget: widget);
                } else {
                  final field = _bp!.fields![index];

                  String? content = '';
                  List? contentKV = [];

                  if (_secret.data.containsKey(field.name) &&
                      _secret.data[field.name] is String) {
                    content = _secret.data[field.name];
                  } else if (_secret.data.containsKey(field.name) &&
                      _secret.data[field.name] is List) {
                    contentKV = _secret.data[field.name];
                  }

                  TextInputType? keyboardType;
                  bool obscureText = false;
                  int? maxLines = 1;
                  // List<DropdownMenuItem<String>> buttons = [];
                  List<Widget> suffixIcon = [];
                  List<MaskTextInputFormatter> inputFormatters = [];
                  if (field.inputFormatter != null) {
                    inputFormatters.add(field.inputFormatter!);
                    if (content != null) {
                      content = field.inputFormatter!
                          .formatEditUpdate(TextEditingValue.empty,
                              TextEditingValue(text: content))
                          .text;
                    }
                  }

                  if (field.field == 'input' && field.type == 'text') {
                  } else if (field.field == 'input' &&
                      field.type == 'password') {
                    obscureText = true && _obscurePassword;
                    suffixIcon.add(
                      component.ObscureIcon(
                        onTap: () {
                          setState(() {
                            _obscurePassword = !_obscurePassword;
                          });
                        },
                        obscure: _obscurePassword,
                      ),
                    );
                  } else if (field.field == 'input' &&
                      field.type == 'totp_code') {
                    obscureText = true && _obscurePassword;
                    suffixIcon.add(
                      component.ObscureIcon(
                        onTap: () {
                          setState(() {
                            _obscurePassword = !_obscurePassword;
                          });
                        },
                        obscure: _obscurePassword,
                      ),
                    );
                  } else if (field.field == 'input' &&
                      field.type == 'checkbox') {
                  } else if (field.field == 'textarea' &&
                      field.type == 'password') {
                    keyboardType = TextInputType.multiline;
                    maxLines = _obscurePassword ? 1 : 10;

                    obscureText = true && _obscurePassword;
                    suffixIcon.add(
                      component.ObscureIcon(
                        onTap: () {
                          setState(() {
                            _obscurePassword = !_obscurePassword;
                          });
                        },
                        obscure: _obscurePassword,
                      ),
                    );
                  } else if (field.field == 'textarea') {
                    keyboardType = TextInputType.multiline;
                    maxLines = 10;
                  } else if (field.field == 'button' &&
                      field.type == 'button') {
                    keyboardType = TextInputType.multiline;
                  } else if (field.field == 'key_value_list') {
                  } else if (field.field == "input" &&
                      field.type == "website_totp_code") {
                    if (_secret.data
                        .containsKey("website_password_totp_period")) {
                      websiteTotpPeriod = int.parse(_secret
                          .data['website_password_totp_period']
                          .toString());
                    }
                    if (_secret.data
                        .containsKey("website_password_totp_digits")) {
                      websiteTotpDigits = int.parse(_secret
                          .data['website_password_totp_digits']
                          .toString());
                    }
                    if (_secret.data
                        .containsKey("website_password_totp_algorithm")) {
                      websiteTotpAlgorithm =
                          _secret.data['website_password_totp_algorithm'];
                    }
                    if (_secret.data
                        .containsKey("website_password_totp_code")) {
                      websiteTotpCode =
                          _secret.data['website_password_totp_code'];
                    }
                  } else {
                    throw ("unknown field type combi");
                  }

                  List<Widget> children = [];
                  if (field.hidden) {
                    // The field is hidden lets not create a child for it
                  } else if (field.field == 'input' &&
                      field.type == 'totp_code') {
                    int? period;
                    if (_secret.data.containsKey('totp_period')) {
                      if (_secret.data['totp_period'] is String) {
                        period = int.parse(_secret.data['totp_period']);
                      } else {
                        period = _secret.data['totp_period'];
                      }
                    }
                    String? algorithm;
                    if (_secret.data.containsKey('totp_algorithm')) {
                      algorithm = _secret.data['totp_algorithm'];
                    }
                    int? digits;
                    if (_secret.data.containsKey('totp_digits')) {
                      if (_secret.data['totp_digits'] is String) {
                        digits = int.parse(_secret.data['totp_digits']);
                      } else {
                        digits = _secret.data['totp_digits'];
                      }
                    }
                    children.add(
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: component.TotpCode(
                          code: content,
                          digits: digits,
                          algorithm: algorithm,
                          period: period,
                        ),
                      ),
                    );
                  } else if (field.field == 'key_value_list') {
                    children.add(
                      component.KeyValueList(
                        contentKV: contentKV,
                      ),
                    );
                  } else {
                    final textController = TextEditingController(
                      text: content,
                    );

                    controllers.add(textController);
                    suffixIcon.insert(
                      0,
                      component.CopyIcon(
                        onTap: () {
                          Clipboard.setData(
                            ClipboardData(
                              text: textController.text,
                            ),
                          );
                          _clipboardCopyToast();
                        },
                      ),
                    );
                    bool containsTotp = (_secret.data
                            .containsKey("website_password_totp_period")) &&
                        (_secret.data
                            .containsKey("website_password_totp_digits")) &&
                        (_secret.data
                            .containsKey("website_password_totp_algorithm"));
                    if (field.type == "website_totp_code" && containsTotp) {
                      children.add(
                        _WebsiteTotpCode(
                          period: websiteTotpPeriod,
                          digits: websiteTotpDigits,
                          algorithm: websiteTotpAlgorithm,
                          code: websiteTotpCode,
                          onTotpDelete: () {
                            setState(() {
                              _secret.data
                                  .remove("website_password_totp_period");
                              _secret.data
                                  .remove("website_password_totp_digits");
                              _secret.data
                                  .remove("website_password_totp_algorithm");
                              _secret.data.remove("website_password_totp_code");
                            });
                          },
                        ),
                      );
                    } else if (field.type == "website_totp_code" &&
                        !containsTotp) {
                      children.add(Container());
                    } else if (field.title == "TITLE") {
                      children.add(
                        _TitleTextField(
                          bp: _bp!,
                          textController: textController,
                          inputFormatters: inputFormatters,
                          field: field,
                          secret: _secret,
                        ),
                      );
                    } else {
                      children.add(
                        _ItemTextField(
                          obscureText: obscureText,
                          textController: textController,
                          inputFormatters: inputFormatters,
                          field: field,
                          keyboardType: keyboardType,
                          maxLines: maxLines,
                          secret: _secret,
                          bp: _bp,
                          suffix: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ...suffixIcon,
                            ],
                          ),
                          onTOTPScan: (modelOTP.OTP otp) {
                            setState(() {
                              _secret.data["website_password_totp_period"] =
                                  otp.period;
                              _secret.data["website_password_totp_digits"] =
                                  otp.digits;
                              _secret.data["website_password_totp_algorithm"] =
                                  otp.algorithm;
                              _secret.data["website_password_totp_code"] =
                                  otp.secret;
                            });
                          },
                        ),
                      );
                    }
                  }

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: children,
                  );
                }
              },
            ),
          ),
        ),
      );
    }
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: const Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            component.FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: const TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
      child: toast,
      toastDuration: const Duration(seconds: 2),
      positionedToastBuilder: (context, child) {
        return Positioned(top: 110, left: 0, right: 0, child: child);
      },
    );
  }
}
