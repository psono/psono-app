part of 'edit_secret.dart';

class _AccessDenied extends StatelessWidget {
  const _AccessDenied({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      backgroundPath: AppAssets.folderBg,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            component.AlertInfo(
              text: FlutterI18n.translate(
                context,
                "ACCESS_DENIED",
              ),
            ),
            const SizedBox(height: 8.0),
            component.BtnPrimary(
              onPressed: () async {
                Navigator.pop(context);
              },
              text: FlutterI18n.translate(context, "BACK"),
            ),
          ],
        ),
      ),
    );
  }
}
