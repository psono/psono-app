part of 'datastore_screen.dart';

class _DatastoreOfflineScreen extends StatelessWidget {
  const _DatastoreOfflineScreen(
      {Key? key,
      required this.screen,
      required this.handleRefresh,
      required this.handleLogout})
      : super(key: key);
  final _ScreenType screen;
  final Function() handleRefresh;
  final Function() handleLogout;
  @override
  Widget build(BuildContext context) {
    List<Widget> buttons = [
      component.BtnPrimary(
        text: FlutterI18n.translate(
          context,
          "RETRY",
        ),
        onPressed: handleRefresh,
      ),
      component.BtnPrimary(
        text: FlutterI18n.translate(
          context,
          "LOGOUT",
        ),
        onPressed: handleLogout,
      )
    ];

    return component.DatastoreScaffold(
      child: Container(
        child: (screen == _ScreenType.handshakeFailed)
            ? component.InfoScreen(
                text: FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
                buttons: buttons,
              )
            : component.InfoScreen(
                text: FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                buttons: buttons,
              ),
      ),
    );
  }
}
