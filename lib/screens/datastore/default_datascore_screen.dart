part of 'datastore_screen.dart';

class _DefaultDatascoreScreen extends StatefulWidget {
  const _DefaultDatascoreScreen({
    Key? key,
    required this.datastore,
    required this.root,
    required this.share,
    required this.path,
    required this.relativePath,
    required this.autofill,
    required this.handleRefresh,
    required this.selectedItems,
    required this.selectedFolders,
    required this.searchText,
    required this.selectedFilters,
    required this.changeSelectedItems,
    required this.changeSelectedFolders,
    required this.onSearchChange,
    required this.onApplyFilter,
  }) : super(key: key);
  final datastore_model.Datastore? datastore;
  final datastore_model.Folder? root;
  final datastore_model.Folder? share;
  final List<String> path;
  final List<String> relativePath;
  final bool autofill;
  final String? searchText;
  final Map<String, String> selectedFilters;
  final List<String?> selectedItems;
  final List<String?> selectedFolders;
  final Function(List<String?>) changeSelectedItems;
  final Function(List<String?>) changeSelectedFolders;
  final Function(String?) onSearchChange;
  final Future Function() handleRefresh;
  final Function(Map<String, String>) onApplyFilter;

  @override
  State<_DefaultDatascoreScreen> createState() =>
      _DefaultDatascoreScreenState();
}

class _DefaultDatascoreScreenState extends State<_DefaultDatascoreScreen> {
  late FToast _fToast;
  @override
  void initState() {
    _fToast = FToast();
    _fToast.init(context);
    component.DatastoreScaffold.isGridView.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget content = _folderTree();
    bool isSelectionMode =
        widget.selectedItems.isNotEmpty || widget.selectedFolders.isNotEmpty;
    bool isSearchMode = widget.searchText?.isNotEmpty ??
        false || widget.selectedFilters.isNotEmpty;
    if (isSearchMode) {
      content = _searchFolderTree();
    } else if (isSelectionMode) {
      content = _selectedFolderTree();
    }
    return component.DatastoreScaffold(
      onNewFolderTap: _onNewFolderCallBack,
      onNewEntryTap: _onNewEntryCallBack,
      onDeleteTap: _deleteCallback,
      onEditTap: _editCallback,
      onLinkShareTap: _linkShareCallback,
      onSearchChange: widget.onSearchChange,
      isSelectionMode: isSelectionMode,
      isSearchMode: isSearchMode,
      selectedFilters: widget.selectedFilters,
      isMultiSelected:
          (widget.selectedItems.length + widget.selectedFolders.length) > 1,
      isFolderSelected: widget.selectedFolders.isNotEmpty,
      onApplyFilter: widget.onApplyFilter,
      child: RefreshIndicator(
        onRefresh: widget.handleRefresh,
        child: content,
      ),
    );
  }

  _onNewFolderCallBack() {
    component
        .showCustomBottomSheet(
            context: context,
            child: AddFolderScreen(
              parent: widget.root,
              datastore: widget.datastore,
              share: widget.share,
              path: widget.path,
              relativePath: widget.relativePath,
            ))
        .then(
      (value) {
        final folder = value as datastore_model.Folder?;
        if (folder != null) {
          widget.changeSelectedFolders([folder.id]);
        }
        if (folder?.id == null) {
          return;
        }
        Navigator.pushReplacement(
          context,
          NoAnimationMaterialPageRoute(
            builder: (context) => DatastoreScreen(
              autoNavigate: List.from(widget.path)..addAll([folder!.id!]),
            ),
          ),
        );
      },
    );
  }

  _onNewEntryCallBack() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AddItemScreen(
          parent: widget.root,
          datastore: widget.datastore,
          share: widget.share,
          path: widget.path,
          relativePath: widget.relativePath,
        ),
      ),
    ).then((value) {
      widget.handleRefresh();
    });
  }

  _linkShareCallback() async {
    if (widget.selectedItems.length == 1) {
      datastore_model.Item? item = widget.root!.items!
          .where((item) => widget.selectedItems.contains(item!.id))
          .first;
      final url = await component.linkShare(context, item);
      if (url != null) {
        share_plus.Share.share(url);
      }
    }
  }

  _editCallback() async {
    if (widget.selectedItems.length == 1) {
      // Trigger edit of an item
      datastore_model.Item? item = widget.root!.items!
          .where((item) => widget.selectedItems.contains(item!.id))
          .first;

      if (item?.fileId != null) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DownloadFileScreen(
              parent: widget.root,
              datastore: widget.datastore,
              share: widget.share,
              item: item!,
              path: widget.path,
              relativePath: widget.relativePath,
            ),
          ),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditSecretScreen(
              parent: widget.root,
              datastore: widget.datastore,
              share: widget.share,
              item: item!,
              path: widget.path,
              relativePath: widget.relativePath,
            ),
          ),
        );
      }
    } else if (widget.selectedFolders.length == 1) {
      // Trigger edit of a folder
      datastore_model.Folder? folder = widget.root!.folders!
          .where((folder) => widget.selectedFolders.contains(folder!.id))
          .first;
      component.showCustomBottomSheet(
        context: context,
        child: EditFolderScreen(
          parent: widget.root,
          datastore: widget.datastore,
          share: widget.share,
          folder: folder,
          path: widget.path,
          relativePath: widget.relativePath,
        ),
      );
    }
  }

  void _deleteCallback() async {
    component.showCustomDialog(
      context: context,
      content: Padding(
        padding: EdgeInsets.all(20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  component.TextDmSans(
                    text: FlutterI18n.translate(context, 'DELETE_ENTRY'),
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.h),
            Divider(
              color: Colors.white.withOpacity(0.30),
              height: 0.50,
            ),
            SizedBox(height: 20.h),
            component.TextDmSans(
              text: FlutterI18n.translate(context, 'DELETE_ENTRY_WARNING'),
              color: Colors.white,
              fontSize: 15.sp,
              fontWeight: FontWeight.w300,
              letterSpacing: 0.48,
            ),
            SizedBox(height: 20.h),
            Row(
              children: [
                const Spacer(),
                component.BtnText(
                  text: FlutterI18n.translate(context, "CANCEL").toUpperCase(),
                  color: const Color(0x99B6B6B6),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                const Spacer(flex: 2),
                Expanded(
                  flex: 10,
                  child: component.Btn(
                    onPressed: () async {
                      await manager_widget.deleteItem(
                        widget.selectedItems,
                        widget.selectedFolders,
                        widget.relativePath,
                        widget.share,
                        widget.datastore,
                        'password',
                      );

                      widget.changeSelectedItems([]);
                      widget.changeSelectedFolders([]);

                      if (context.mounted) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const DatastoreScreen(
                              autoNavigate: [],
                            ),
                          ),
                        );
                      }
                    },
                    color: const Color(0xFFDC1C56),
                    customChild: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.delete_outline,
                          color: Colors.white,
                          size: 20.sp,
                        ),
                        SizedBox(width: 6.w),
                        component.TextDmSans(
                          text: FlutterI18n.translate(context, "DELETE")
                              .toUpperCase(),
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _folderTree() {
    return component.FolderTree(
      root: widget.root,
      datastore: widget.datastore,
      share: widget.share,
      path: widget.path,
      relativePath: widget.relativePath,
      autofill: widget.autofill,
      onLongPressFolder: (datastore_model.Folder? folder) async {
        if (folder == null) {
          return;
        }
        if (!folder.shareRights!.delete!) {
          return;
        }
        widget.changeSelectedItems(<String>[]);
        widget.changeSelectedFolders([folder.id]);
      },
      onLongPressItem: (datastore_model.Item? item) async {
        if (item == null) {
          return;
        }
        if (!item.shareRights!.delete!) {
          return;
        }
        widget.changeSelectedItems([item.id]);
        widget.changeSelectedFolders(<String?>[]);
      },
    );
  }

  _selectedFolderTree() {
    return component.FolderSelectTree(
      root: widget.root,
      datastore: widget.datastore,
      share: widget.share,
      path: widget.path,
      relativePath: widget.relativePath,
      selectedFolders: widget.selectedFolders,
      selectedItems: widget.selectedItems,
      onSelectFolder: (datastore_model.Folder? folder) {
        if (folder == null) {
          return;
        }
        if (widget.selectedFolders.contains(folder.id)) {
          widget.changeSelectedFolders(
              widget.selectedFolders.where((e) => e != folder.id).toList());
        } else {
          widget.changeSelectedFolders([...widget.selectedFolders, folder.id]);
        }
      },
      onSelectItem: (datastore_model.Item? item) {
        if (item == null) {
          return;
        }
        if (widget.selectedItems.contains(item.id)) {
          widget.changeSelectedItems(
              widget.selectedItems.where((e) => e != item.id).toList());
        } else {
          widget.changeSelectedItems([...widget.selectedItems, item.id]);
        }
      },
    );
  }

  _searchFolderTree() {
    return component.FolderSearchTree(
      root: widget.root,
      datastore: widget.datastore,
      share: widget.share,
      path: widget.path,
      relativePath: widget.relativePath,
      search: widget.searchText,
      autofill: widget.autofill,
      filters: widget.selectedFilters,
    );
  }
}
