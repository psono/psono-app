import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastore_model;
import 'package:psono/routes/routes.dart';
import 'package:psono/screens/add_folder/add_folder.dart';
import 'package:psono/screens/add_item/add_item_screen.dart';
import 'package:psono/screens/download_file/index.dart';
import 'package:psono/screens/edit_folder/edit_folder.dart';
import 'package:psono/screens/edit_secret/edit_secret.dart';
import 'package:psono/screens/folder/folder_screen.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore_password.dart'
    as manager_datastore_password;
import 'package:psono/services/manager_widget.dart' as manager_widget;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/theme.dart';
import 'package:share_plus/share_plus.dart' as share_plus;
import 'package:validators/validators.dart';

part 'datastore_offline_screen.dart';
part 'default_datascore_screen.dart';

enum _ScreenType {
  loading,
  defaultScreen,
  offline,
  handshakeFailed,
}

class DatastoreScreen extends StatefulWidget {
  static String tag = 'datastore-screen';
  final List<String?>? autoNavigate;

  const DatastoreScreen({this.autoNavigate});

  @override
  _DatastoreScreenState createState() => _DatastoreScreenState();
}

class _DatastoreScreenState extends State<DatastoreScreen> {
  String? defaultSearch;
  String? _search;
  Map<String, String> selectedFilters = {};
  _ScreenType _screen = _ScreenType.loading;
  datastore_model.Datastore? _datastore;
  datastore_model.Folder? _root;
  datastore_model.Folder? _share;
  Widget appBarTitle = Text("Datastore");
  Icon actionIcon = new Icon(Icons.search);
  final List<String> path = [];
  final List<String> relativePath = [];
  List<String?> selectedItems = [];
  List<String?> selectedFolders = [];

  static const sharedDataChannel =
      MethodChannel('com.psono.psono/psono_shared_data');
  bool autofill = false;

  Future<void> loadDatastore() async {
    setState(() {
      _screen = _ScreenType.loading;
    });
    datastore_model.Datastore datastore;
    try {
      datastore = await manager_datastore_password.getPasswordDatastore();
    } on api_client.ServiceUnavailableException {
      if (!mounted) return;
      setState(() {
        _screen = _ScreenType.offline;
      });
      return;
    } on api_client.UnauthorizedException {
      user_service.logout();
      if (context.mounted) {
        Navigator.pushReplacementNamed(context, AppRoutes.signin);
      }
      return;
    } on HandshakeException {
      setState(() {
        _screen = _ScreenType.handshakeFailed;
      });
      return;
    } catch (e) {
      setState(() {
        _screen = _ScreenType.offline;
      });
      return;
    }
    if (!mounted) return;
    setState(() {
      _screen = _ScreenType.defaultScreen;
      _datastore = datastore;
      _root = datastore.data;
      _share = null;
    });
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // _screen = _ScreenType.offline;
    if (_screen == _ScreenType.offline ||
        _screen == _ScreenType.handshakeFailed) {
      return _DatastoreOfflineScreen(
          screen: _screen,
          handleRefresh: _handleRefresh,
          handleLogout: _handleLogout);
    } else if (_screen == _ScreenType.loading) {
      return component.DatastoreScaffold(
        child: Center(
          child: CircularProgressIndicator(
            color: const Color(0xFF151f2b),
            backgroundColor: primarySwatch.shade500,
          ),
        ),
      );
    } else {
      return _DefaultDatascoreScreen(
        datastore: _datastore,
        root: _root,
        share: _share,
        autofill: autofill,
        path: path,
        relativePath: relativePath,
        searchText: _search,
        selectedFilters: selectedFilters,
        selectedFolders: selectedFolders,
        selectedItems: selectedItems,
        handleRefresh: _handleRefresh,
        onSearchChange: searchCallback,
        changeSelectedItems: (List<String?> selectedItems) {
          setState(() {
            this.selectedItems = selectedItems;
          });
        },
        changeSelectedFolders: (List<String?> selectedFolders) {
          setState(() {
            this.selectedFolders = selectedFolders;
          });
        },
        onApplyFilter: (selectedFilters) {
          setState(() {
            this.selectedFilters = selectedFilters;
          });
        },
      );
    }
  }

  Future<void> initStateAsync() async {
    if (!user_service.isLoggedIn()) {
      Navigator.pushReplacementNamed(context, AppRoutes.signin);
      return;
    }

    if (user_service.requireServerSecretModification()) {
      Navigator.pushReplacementNamed(context, AppRoutes.keyTransferScreen);
      return;
    }

    var sharedData = await sharedDataChannel.invokeMethod("getSharedText");
    if (sharedData != null) {
      List<String> sharedDataList = sharedData.split("::");
      if (sharedDataList.length == 3 && sharedDataList[0] == 'autofill') {
        setState(() {
          autofill = true;
        });
        if (sharedDataList[2].isNotEmpty) {
          String myDefaultSearch = sharedDataList[2];

          // check if we have a FQDN and more than 3 dots (ergo a subdomain)
          if (isFQDN(myDefaultSearch)) {
            List<String> fqdnSplit = myDefaultSearch.split(".");
            if (fqdnSplit.length > 1) {
              myDefaultSearch =
                  '${fqdnSplit[fqdnSplit.length - 2]}.${fqdnSplit[fqdnSplit.length - 1]}';
            }
          }

          setState(() {
            defaultSearch = myDefaultSearch;
          });
        }
      }
    }

    await loadDatastore();

    if (widget.autoNavigate != null && widget.autoNavigate!.isNotEmpty) {
      List<String> newAutoNavigate = List.from(widget.autoNavigate!);
      String folderId = newAutoNavigate.removeAt(0);

      for (var i = 0; i < _root!.folders!.length; i++) {
        if (_root!.folders![i]!.id != folderId) {
          continue;
        }
        datastore_model.Folder? newShare;
        List<String?> newRelativePath;
        if (_root!.folders![i]!.shareId != null) {
          newShare = _root!.folders![i];
          newRelativePath = [];
        } else {
          newRelativePath = List.from(relativePath)
            ..addAll([_root!.folders![i]!.id]);
        }

        if (context.mounted) {
          Navigator.push(
            context,
            NoAnimationMaterialPageRoute(
              builder: (context) => FolderScreen(
                parent: _root,
                autoNavigate: newAutoNavigate,
                folder: _root!.folders![i],
                datastore: _datastore,
                share: newShare,
                path: List.from(path)..addAll([_root!.folders![i]!.id]),
                relativePath: newRelativePath,
                autofill: autofill,
              ),
            ),
          );
        }

        return;
      }
    }
  }

  Future _handleRefresh() async {
    helper.makeFirstNamed(
      context,
      AppRoutes.datastore,
    );
  }

  Future _handleLogout() async {
    user_service.logout();
    Navigator.pushReplacementNamed(context, AppRoutes.signin);
  }

  void searchCallback(String? search) {
    setState(() {
      defaultSearch = null;
      _search = search;
    });
  }
}
