import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:psono/app_config.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/autofill.dart' as autofill_service;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/offline_cache.dart' as offline_cache;
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    _postFrameCallBack(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: component.Loading(hideLoadingIndicator: true),
    );
  }

  void _postFrameCallBack(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback(
      (_) async {
        var appConfig = AppConfig.of(context)!;
        // clean storage on fresh install
        await SharedPreferences.getInstance().then((prefs) async {
          if (prefs.getBool(K.appFirstRunKey) ?? true) {
            offline_cache.delete();
            await storage.deleteAll();
            prefs.setBool(K.appFirstRunKey, false);
          }
        });
        String? serverUrl = await storage.read(key: SK.serverUrl);
        String? username = await storage.read(key: SK.username);
        String? token = await storage.read(key: SK.token);
        String? authentication = await storage.read(key: SK.authentication);
        String? sessionSecretKey = await storage.read(key: SK.sessionSecretKey);
        String? lastServerConnectionTimeSinceEpoch =
            await storage.read(key: SK.lastServerConnectionTimeSinceEpoch);
        String? lastCacheTimeSinceEpoch =
            await storage.read(key: SK.lastCacheTimeSinceEpoch);
        String? complianceMaxOfflineCacheTimeValid =
            await storage.read(key: SK.complianceMaxOfflineCacheTimeValid);
        String? secretKey = await storage.read(key: SK.secretKey);
        String? privateKey = await storage.read(key: SK.privateKey);
        String? publicKey = await storage.read(key: SK.publicKey);
        String? lockscreenPin = await storage.read(key: SK.lockscreenPin);
        String? complianceServerSecrets =
            await storage.read(key: SK.complianceServerSecrets);
        bool serverSecretExists =
            (await storage.read(key: SK.serverSecretExists)) == 'true';
        bool complianceDisableDeleteAccount =
            (await storage.read(key: SK.complianceDisableDeleteAccount)) ==
                'true';
        bool complianceDisableOfflineMode =
            (await storage.read(key: SK.complianceDisableOfflineMode)) ==
                'true';
        String? userId = await storage.read(key: SK.userId);
        String? userEmail = await storage.read(key: SK.userEmail);
        String? userSauce = await storage.read(key: SK.userSauce);
        String? configJson = await storage.read(key: SK.configJson);
        bool passedAutofillOnboarding =
            (await storage.read(key: SK.passedAutofillOnboarding)) == 'true';

        Config? config;
        if (configJson != null) {
          try {
            Map configMap = jsonDecode(configJson);
            config = Config.fromJson(configMap as Map<String, dynamic>);
          } on FormatException {
            // pass
          }
        }

        if (serverUrl == null || serverUrl == '') {
          if (config != null &&
              config.configJson != null &&
              config.configJson!.backendServers != null &&
              config.configJson!.backendServers!.isNotEmpty &&
              config.configJson!.backendServers![0].url != null) {
            serverUrl = config.configJson!.backendServers![0].url;
          } else {
            serverUrl = appConfig.defaultServerUrl;
          }
        }

        Uint8List? sessionSecretKeyBin;
        if (sessionSecretKey != null) {
          sessionSecretKeyBin = converter.fromHex(sessionSecretKey);
        }

        int lastServerConnectionTimeSinceEpochInt = 0;
        if (lastServerConnectionTimeSinceEpoch != null) {
          try {
            lastServerConnectionTimeSinceEpochInt =
                int.parse(lastServerConnectionTimeSinceEpoch);
          } catch (e) {
            // pass
          }
        }

        int lastCacheTimeSinceEpochInt = 0;
        if (lastCacheTimeSinceEpoch != null) {
          try {
            lastCacheTimeSinceEpochInt = int.parse(lastCacheTimeSinceEpoch);
          } catch (e) {
            // pass
          }
        }

        int complianceMaxOfflineCacheTimeValidInt = 31536000;
        if (complianceMaxOfflineCacheTimeValid != null) {
          try {
            complianceMaxOfflineCacheTimeValidInt =
                int.parse(complianceMaxOfflineCacheTimeValid);
          } catch (e) {
            // pass
          }
        }

        Uint8List? secretKeyBin;
        if (secretKey != null) {
          secretKeyBin = converter.fromHex(secretKey);
        }

        Uint8List? publicKeyBin;
        if (publicKey != null) {
          publicKeyBin = converter.fromHex(publicKey);
        }

        Uint8List? privateKeyBin;
        if (privateKey != null) {
          privateKeyBin = converter.fromHex(privateKey);
        }

        reduxStore.dispatch(
          InitiateStateAction(
            serverUrl!,
            username,
            token,
            authentication,
            sessionSecretKeyBin,
            lastServerConnectionTimeSinceEpochInt,
            lastCacheTimeSinceEpochInt,
            secretKeyBin,
            publicKeyBin,
            privateKeyBin,
            lockscreenPin,
            serverSecretExists,
            complianceServerSecrets,
            complianceDisableDeleteAccount,
            complianceDisableOfflineMode,
            complianceMaxOfflineCacheTimeValidInt,
            userId,
            userEmail,
            userSauce,
            config,
          ),
        );

        bool autofillSupported = await autofill_service.isSupported();

        await Future.delayed(const Duration(milliseconds: 500));

        if (!context.mounted) {
          return;
        }

        if (autofillSupported && !passedAutofillOnboarding) {
          Navigator.pushReplacementNamed(
            context,
            AppRoutes.autofillOnboarding,
          );
        } else if (token == null || serverUrl == null || serverUrl == '') {
          helper.makeFirstNamed(context, AppRoutes.appOnboarding);
          return;
        } else if (lockscreenPin == null || lockscreenPin == '') {
          Navigator.pushReplacementNamed(
              context, AppRoutes.pinConfigurationScreen);
        } else {
          Navigator.pushReplacementNamed(context, AppRoutes.passphrase);
        }
      },
    );
  }
}
