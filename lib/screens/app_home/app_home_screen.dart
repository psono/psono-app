import 'package:floating_frosted_bottom_bar/app/frosted_bottom_bar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/custom_bottom_bar_indicator.dart';
import 'package:psono/screens/account/account_screen.dart';
import 'package:psono/screens/datastore/datastore_screen.dart';
import 'package:psono/screens/settings/settings_screen.dart';
import 'package:psono/utils/app_assets.dart';

class AppHome extends StatefulWidget {
  const AppHome({Key? key}) : super(key: key);

  @override
  State<AppHome> createState() => _AppHomeState();
}

class _AppHomeState extends State<AppHome> with SingleTickerProviderStateMixin {
  late int currentPage;
  late TabController tabController;
  List<Widget> screens = [
    const DatastoreScreen(),
    SettingsScreen(),
    AccountScreen(),
  ];
  @override
  void initState() {
    currentPage = 0;
    tabController = TabController(length: screens.length, vsync: this);
    tabController.animation!.addListener(
      () {
        final value = tabController.animation!.value.round();
        if (value != currentPage && mounted) {
          _changePage(value);
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FrostedBottomBar(
      opacity: 0.6,
      sigmaX: 5,
      sigmaY: 5,
      bottomBarColor: const Color(0xff2F2F2F),
      width: 300,
      curve: Curves.easeInOutSine,
      duration: const Duration(milliseconds: 10),
      body: (context, controller) {
        return TabBarView(
          controller: tabController,
          dragStartBehavior: DragStartBehavior.down,
          children: screens,
        );
      },
      borderRadius: const BorderRadius.all(Radius.circular(100)),
      child: TabBar(
        controller: tabController,
        dividerHeight: 0,
        indicatorWeight: 0,
        indicator: const BackgroundContainerTabIndicator(
          color: Colors.white,
          verticalMargin: 4,
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        indicatorSize: TabBarIndicatorSize.label,
        splashBorderRadius: const BorderRadius.all(Radius.circular(30)),
        tabAlignment: TabAlignment.center,
        tabs: [
          _item(AppAssets.bottomBarHome, "HOME", 0),
          _item(AppAssets.bottomBarSettings, "SETTINGS", 1),
          _item(AppAssets.bottomBarPerson, "ACCOUNT", 2),
        ],
      ),
    );
  }

  Widget _item(String iconPath, String text, int index) {
    bool isSelected = currentPage == index;
    if (isSelected) {
      iconPath = iconPath.replaceAll(".png", "_selected.png");
    }
    return AnimatedSize(
      duration: const Duration(milliseconds: 350),
      child: Container(
        height: 54,
        width: isSelected ? 132 : 35,
        padding: EdgeInsets.symmetric(horizontal: isSelected ? 0 : 0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              iconPath,
              height: 20,
              width: 20,
            ),
            if (isSelected)
              Padding(
                padding: EdgeInsets.only(left: 6),
                child: component.TextDmSans(
                  text: FlutterI18n.translate(context, text),
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: const Color(0xFF1B1B1B),
                  height: 1.5,
                ),
              ),
          ],
        ),
      ),
    );
  }

  void _changePage(int newPage) {
    setState(() {
      currentPage = newPage;
    });
  }
}
