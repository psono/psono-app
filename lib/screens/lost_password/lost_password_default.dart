part of 'lost_password_screen.dart';

class _DefaultScreen extends StatelessWidget {
  const _DefaultScreen({
    super.key,
    required this.username,
    required this.code1,
    required this.code2,
    required this.words,
    required this.requestPasswordResetButton,
    required this.server,
    required this.serverFocus,
  });

  final component.CustomTextField username;
  final component.CustomTextField code1;
  final component.CustomTextField code2;
  final component.CustomTextField words;
  final component.BtnPrimary requestPasswordResetButton;
  final component.CustomTextField server;
  final FocusNode serverFocus;

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.h),
              Align(
                alignment: Alignment.centerLeft,
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(
                    AppAssets.iconNavBack,
                    height: 24.h,
                    width: 24.h,
                  ),
                ),
              ),
              SizedBox(height: 24.h),
              component.TextDmSans(
                text: FlutterI18n.translate(context, "LOST_PASSWORD"),
                textAlign: TextAlign.center,
                color: Colors.white,
                fontSize: 32.sp,
                fontWeight: FontWeight.w500,
                height: 0,
              ),
              SizedBox(height: 24.h),
              username,
              SizedBox(height: 16.h),
              code1,
              SizedBox(height: 16.h),
              code2,
              SizedBox(height: 16.h),
              words,
              SizedBox(height: 40.h),
              requestPasswordResetButton,
              KeyboardVisibilityBuilder(
                builder: (context, isKeyboardVisible) {
                  if (isKeyboardVisible && serverFocus.hasFocus) {
                    return const SizedBox();
                  }
                  return const Spacer();
                },
              ),
              server,
              const SizedBox(height: 16.0),
              component.BtnText(
                onPressed: () {
                  helper.openUrl('https://www.psono.pw/privacy-policy.html');
                },
                text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                color: Colors.white.withOpacity(0.60),
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
