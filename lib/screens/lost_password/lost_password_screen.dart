import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/custom_text_field.dart';
import 'package:psono/model/recovery_enable.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/storage.dart';
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_constants.dart';

part 'lost_password_default.dart';
part 'lost_password_recovery_enabled.dart';
part 'lost_password_success.dart';

enum _ScreenType {
  defaultScreen,
  recoveryEnabledScreen,
  successScreen,
}

class LostPasswordScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _LostPasswordScreenState createState() => _LostPasswordScreenState();
}

class _LostPasswordScreenState extends State<LostPasswordScreen> {
  final _usernameController = TextEditingController(
    text: '',
  );
  final _code1Controller = TextEditingController(
    text: '',
  );
  final _code2Controller = TextEditingController(
    text: '',
  );
  final _wordsController = TextEditingController(
    text: '',
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordRepeatController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );

  _ScreenType _screen = _ScreenType.defaultScreen;
  String _recoveryCode = 'default';
  late RecoveryEnable _recoveryData;
  bool _obscurePassword = true;
  bool _obscureRepeatPassword = true;
  api_client.Info? info;
  final FocusNode serverFocus = FocusNode();

  String _domainSuffix = '@${helper.getDomain(reduxStore.state.serverUrl)!}';
  @override
  void initState() {
    _usernameController.addListener(_onTextFieldChange);
    _serverUrlController.addListener(_onTextFieldChange);
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    _usernameController.dispose();
    _code1Controller.dispose();
    _code2Controller.dispose();
    _wordsController.dispose();
    _passwordController.dispose();
    _passwordRepeatController.dispose();
    _serverUrlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final username = component.CustomTextField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      labelText: FlutterI18n.translate(context, "USERNAME"),
      suffixIcon: Center(
        child: component.TextDmSans(
          text: _domainSuffix,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          color: Colors.white.withOpacity(0.50),
          fontSize: 16.sp,
          fontWeight: FontWeight.w400,
        ),
      ),
    );

    final code1 = component.CustomTextField(
      controller: _code1Controller,
      autofocus: false,
      labelText: FlutterI18n.translate(context, "CODE_1"),
    );

    final code2 = component.CustomTextField(
      controller: _code2Controller,
      autofocus: false,
      labelText: FlutterI18n.translate(context, "CODE_2"),
    );

    final words = component.CustomTextField(
      controller: _wordsController,
      autofocus: false,
      labelText: FlutterI18n.translate(context, "WORD_LIST"),
    );

    final password = component.CustomTextField(
      controller: _passwordController,
      autofocus: true,
      obscureText: _obscurePassword,
      labelText: FlutterI18n.translate(context, "NEW_PASSWORD"),
      suffixIcon: ObscureIcon(
        obscure: _obscurePassword,
        onTap: () {
          setState(() {
            _obscurePassword = !_obscurePassword;
          });
        },
      ),
    );

    final passwordRepeat = component.CustomTextField(
      controller: _passwordRepeatController,
      autofocus: false,
      obscureText: _obscureRepeatPassword,
      labelText: FlutterI18n.translate(context, "NEW_PASSWORD_REPEAT"),
      suffixIcon: ObscureIcon(
        obscure: _obscureRepeatPassword,
        onTap: () {
          setState(() {
            _obscureRepeatPassword = !_obscureRepeatPassword;
          });
        },
      ),
    );

    final requestPasswordResetButton = component.BtnPrimary(
      onPressed: () async {
        component.Loader.show(context);
        try {
          await storage.write(
            key: SK.serverUrl,
            value: _serverUrlController.text,
            iOptions: secureIOSOptions,
          );
          await storage.write(
            key: SK.username,
            value: _usernameController.text + _domainSuffix,
            iOptions: secureIOSOptions,
          );

          reduxStore.dispatch(
            InitiateLoginAction(
              _serverUrlController.text,
              _usernameController.text + _domainSuffix,
            ),
          );
          info = await api_client.info();
        } on api_client.ServiceUnavailableException {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        if (_usernameController.text == '') {
          component.Loader.hide();
          return;
        }

        if (_wordsController.text == '' &&
            (_code1Controller.text == '' || _code2Controller.text == '')) {
          component.Loader.hide();
          return;
        }

        String username = _usernameController.text + _domainSuffix;

        String? testError = helper.isValidUsername(username);
        if (testError != null) {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }

        String recoveryCode;
        if (_wordsController.text != '') {
          recoveryCode = converter.hexToBase58(
              converter.wordsToHex(_wordsController.text.split(' ')));
        } else if (_code1Controller.text != '' && _code2Controller.text != '') {
          if (!await cryptoLibrary
                  .recoveryPasswordChunkPassChecksum(_code1Controller.text) ||
              !await cryptoLibrary
                  .recoveryPasswordChunkPassChecksum(_code2Controller.text)) {
            component.Loader.hide();
            _showErrorDialog(
              FlutterI18n.translate(context, "ERROR"),
              FlutterI18n.translate(context, "AT_LEAST_ONE_CODE_INCORRECT"),
            );
            return;
          }
          recoveryCode = await cryptoLibrary.recoveryCodeStripChecksums(
              _code1Controller.text + _code2Controller.text);
        } else {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, "SOMETHING_STRANGE_HAPPENED"),
          );
          return;
        }

        RecoveryEnable data;
        try {
          data = await user_service.recoveryEnable(
              username, recoveryCode, _serverUrlController.text);
        } on api_client.BadRequestException catch (e) {
          _showErrorDialog('ERROR', e.getFirst());
          return;
        } catch (e) {
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } finally {
          component.Loader.hide();
        }
        setState(() {
          _recoveryCode = recoveryCode;
          _recoveryData = data;
          _screen = _ScreenType.recoveryEnabledScreen;
        });
      },
      text: FlutterI18n.translate(context, "PASSWORD_RESET"),
    );

    final setNewPasswordButton = component.BtnPrimary(
      onPressed: () async {
        component.Loader.show(context);
        api_client.Info info;
        try {
          info = await api_client.info();
        } on api_client.ServiceUnavailableException {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        String? testError = helper.isValidPassword(
          _passwordController.text,
          _passwordRepeatController.text,
          info.complianceMinMasterPasswordLength,
          info.complianceMinMasterPasswordComplexity,
        );
        if (testError != null) {
          setState(() {
            _screen = _ScreenType.defaultScreen;
          });
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }
        String username = _usernameController.text + _domainSuffix;

        try {
          await user_service.setPassword(
            username,
            _recoveryCode,
            _passwordController.text,
            _recoveryData.userPrivateKey!,
            _recoveryData.userSecretKey,
            _recoveryData.userSauce!,
            _recoveryData.verifierPublicKey!,
          );
        } on api_client.ServiceUnavailableException {
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          _showErrorDialog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } on api_client.BadRequestException catch (e) {
          _showErrorDialog('ERROR', e.getFirst());
          return;
        } finally {
          component.Loader.hide();
        }
        setState(() {
          _screen = _ScreenType.successScreen;
        });
      },
      text: FlutterI18n.translate(context, "SET_NEW_PASSWORD"),
    );

    final server = component.CustomTextField(
      keyboardType: TextInputType.url,
      controller: _serverUrlController,
      autofocus: false,
      focusNode: serverFocus,
      labelText: FlutterI18n.translate(context, "SERVER_URL"),
    );

    if (_screen == _ScreenType.successScreen) {
      return const _SuccessScreen();
    } else if (_screen == _ScreenType.recoveryEnabledScreen) {
      return _RecoveryEnabledScreen(
          password: password,
          passwordRepeat: passwordRepeat,
          setNewPasswordButton: setNewPasswordButton);
    } else {
      return _DefaultScreen(
        username: username,
        code1: code1,
        code2: code2,
        words: words,
        requestPasswordResetButton: requestPasswordResetButton,
        server: server,
        serverFocus: serverFocus,
      );
    }
  }

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(context, title, content);
  }

  void _onTextFieldChange() {
    if (_usernameController.text.contains('@')) {
      _domainSuffix = '';
    } else {
      _domainSuffix = '@${helper.getDomain(_serverUrlController.text)!}';
    }

    setState(() {
      _domainSuffix = _domainSuffix;
    });
  }
}
