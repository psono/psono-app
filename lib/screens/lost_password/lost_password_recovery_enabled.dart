part of 'lost_password_screen.dart';

class _RecoveryEnabledScreen extends StatelessWidget {
  const _RecoveryEnabledScreen({
    super.key,
    required this.password,
    required this.passwordRepeat,
    required this.setNewPasswordButton,
  });

  final component.CustomTextField password;
  final component.CustomTextField passwordRepeat;
  final component.BtnPrimary setNewPasswordButton;

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.h),
              Align(
                alignment: Alignment.centerLeft,
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(
                    AppAssets.iconNavBack,
                    height: 24.h,
                    width: 24.h,
                  ),
                ),
              ),
              SizedBox(height: 24.h),
              component.TextDmSans(
                text: FlutterI18n.translate(context, "LOST_PASSWORD"),
                textAlign: TextAlign.center,
                color: Colors.white,
                fontSize: 32.sp,
                fontWeight: FontWeight.w500,
                height: 0,
              ),
              SizedBox(height: 24.h),
              password,
              SizedBox(height: 16.h),
              passwordRepeat,
              SizedBox(height: 40.h),
              setNewPasswordButton,
              const Spacer(),
              component.BtnText(
                onPressed: () {
                  helper.openUrl('https://www.psono.pw/privacy-policy.html');
                },
                text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                color: Colors.white.withOpacity(0.60),
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
