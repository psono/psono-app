part of 'app_onboarding.dart';

class _OnboardingItem extends StatelessWidget {
  _OnboardingItem({
    Key? key,
    required this.index,
  }) : super(key: key);

  final int index;
  static const List<String> headerTexts = [
    "INSTANTLY_GENERATE_PASSWORDS",
    "SECURE_ENCRYPTION",
    "SHARE_PASSWORDS",
  ];

  static const List<String> descriptionTexts = [
    "INSTANTLY_GENERATE_PASSWORDS_DESC",
    "SECURE_ENCRYPTION_DESC",
    "SHARE_PASSWORDS_DESC",
  ];
  final _bubbleWidgets = <Widget>[];

  @override
  Widget build(BuildContext context) {
    _buildBubbleWidget();
    return Column(
      children: [
        Stack(
          children: [
            Image.asset(
              AppAssets.onboardingIllustrations[index],
              height: 380.h,
              fit: BoxFit.fitHeight,
            ),
            ..._bubbleWidgets
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 18.w),
          child: Column(
            children: [
              components.TextDmSans(
                text: FlutterI18n.translate(
                  context,
                  headerTexts[index],
                ),
                textAlign: TextAlign.center,
                color: Colors.white,
                fontSize: 30.sp,
                fontWeight: FontWeight.w500,
              ),
              SizedBox(height: 24.h),
              components.TextDmSans(
                text: FlutterI18n.translate(
                  context,
                  descriptionTexts[index],
                ),
                textAlign: TextAlign.center,
                color: Colors.white.withOpacity(0.70),
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
              ),
            ],
          ),
        ),
        SizedBox(height: 24.h),
      ],
    );
  }

  _buildBubbleWidget() {
    _bubbleWidgets.clear();
    if (index == 0) {
      _bubbleWidgets.addAll(
        const [
          _RandomCharBubble0(),
          _HiddenPwdBubble0(),
          _UserNameBubble0(),
        ],
      );
    }
    if (index == 1) {
      _bubbleWidgets.addAll(
        const [
          _CardBubble1(),
          _PasswordBubble1(),
          _NoteBubble1(),
          _UserCommentBubble1(),
        ],
      );
    }
    if (index == 2) {
      _bubbleWidgets.addAll(
        [
          const _PasswordBubble2(),
          const _FolderBubble2(),
        ],
      );
    }
  }
}

class _FolderBubble2 extends StatelessWidget {
  const _FolderBubble2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      radius: 12.r,
      bottom: 60.h,
      left: 20.w,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 8.h),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              AppAssets.folder,
              height: 56.h,
              width: 62.h,
              fit: BoxFit.contain,
            ),
            components.TextDmSans(
              text: FlutterI18n.translate(context, "FOLDER"),
              fontSize: 12.sp,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
            components.TextDmSans(
              text: FlutterI18n.translate(context, "3_ITEMS"),
              fontSize: 9.sp,
              fontWeight: FontWeight.w400,
              color: Colors.white.withOpacity(0.50),
            ),
          ],
        ),
      ),
    );
  }
}

class _PasswordBubble2 extends StatelessWidget {
  const _PasswordBubble2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      radius: 12.r,
      top: 60.h,
      right: 20.w,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(
                AppAssets.iconPassword,
                height: 40.h,
                width: 40.h,
                fit: BoxFit.contain,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  components.TextDmSans(
                    text: FlutterI18n.translate(context, "TITLE"),
                    fontSize: 8.30.sp,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                  components.TextDmSans(
                    text: FlutterI18n.translate(context, "PASSWORD"),
                    fontSize: 12.36.sp,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: 15.w),
            child: components.TextDmSans(
              text: "******",
              fontSize: 22.sp,
              fontWeight: FontWeight.w700,
              color: C.springGreen,
              letterSpacing: 1.5,
            ),
          ),
        ],
      ),
    );
  }
}
