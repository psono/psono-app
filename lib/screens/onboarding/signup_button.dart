part of 'app_onboarding.dart';

class _SignupButton extends StatelessWidget {
  const _SignupButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, AppRoutes.register);
      },
      child: components.BtnPrimary(
        text: FlutterI18n.translate(
          context,
          "CREATE_ACCOUNT",
        ).toUpperCase(),
        onPressed: () {
          Navigator.pushNamed(context, AppRoutes.register);
        },
        width: 350.w,
        textColor: Colors.black,
      ),
    );
  }
}
