part of 'app_onboarding.dart';

class _SigninButton extends StatelessWidget {
  const _SigninButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return components.BtnText(
      text: FlutterI18n.translate(context, "SIGN_IN").toUpperCase(),
      onPressed: () {
        Navigator.pushNamed(context, AppRoutes.signin);
      },
    );
  }
}
