import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:onboarding/onboarding.dart';
import 'package:psono/components/_index.dart' as components;
import 'package:psono/routes/routes.dart';
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

part 'onboarding_item.dart';
part 'signin_button.dart';
part 'signup_button.dart';
part 'text_bubble.dart';
part 'text_bubbles_items.dart';

class AppOnboarding extends StatefulWidget {
  const AppOnboarding({Key? key}) : super(key: key);

  @override
  State<AppOnboarding> createState() => _AppOnboardingState();
}

class _AppOnboardingState extends State<AppOnboarding> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.transparent,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AppAssets.onboardingBackground),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: Column(
            children: [
              SizedBox(height: 40.h),
              Expanded(
                flex: 80,
                child: Onboarding(
                  animationInMilliseconds: 100,
                  swipeableBody: [
                    _OnboardingItem(index: 0),
                    _OnboardingItem(index: 1),
                    _OnboardingItem(index: 2),
                  ],
                  buildFooter: (context, netDragDistance, pagesLength,
                      currentIndex, setIndex, slideDirection) {
                    return AnimatedSmoothIndicator(
                      activeIndex: currentIndex,
                      count: 3,
                      onDotClicked: setIndex,
                      effect: ExpandingDotsEffect(
                        dotHeight: 4.h,
                        dotWidth: 10.w,
                        activeDotColor: Colors.white,
                        dotColor: Colors.white.withOpacity(0.30),
                      ),
                    );
                  },
                ),
              ),
              const Spacer(flex: 2),
              const _SignupButton(),
              SizedBox(height: 24.h),
              const _SigninButton(),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
