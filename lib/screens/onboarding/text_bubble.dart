part of 'app_onboarding.dart';

class _Bubble extends StatelessWidget {
  const _Bubble({
    Key? key,
    this.top,
    this.right,
    this.left,
    this.bottom,
    this.color,
    this.radius,
    required this.child,
  }) : super(key: key);
  final double? top;
  final double? right;
  final double? left;
  final double? bottom;

  final Widget child;
  final Color? color;
  final double? radius;
  @override
  Widget build(BuildContext context) {
    final r = radius ?? 100.r;
    return Positioned(
      right: right,
      top: top,
      left: left,
      bottom: bottom,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(r),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            decoration: BoxDecoration(
              color: color ?? C.aquaDeep.withOpacity(0.30),
              border: Border.all(
                color: const Color.fromRGBO(135, 255, 220, 0.26),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(r),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 7.h, horizontal: 14.w),
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
