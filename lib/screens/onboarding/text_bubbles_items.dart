part of "app_onboarding.dart";

class _UserNameBubble0 extends StatelessWidget {
  const _UserNameBubble0({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      bottom: 65.h,
      right: 10.w,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Image.asset(
            AppAssets.iconUser,
            width: 16.w,
            height: 16.w,
            fit: BoxFit.fill,
          ),
          SizedBox(width: 11.w),
          components.TextDmSans(
            text: FlutterI18n.translate(
              context,
              "USER_LOGIN",
            ),
            fontSize: 16.09.sp,
            fontWeight: FontWeight.w400,
            color: C.springGreen,
            height: 1,
          ),
        ],
      ),
    );
  }
}

class _RandomCharBubble0 extends StatelessWidget {
  const _RandomCharBubble0({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      top: 32.h,
      right: 42.w,
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(
              text: '14',
              style: GoogleFonts.dmSans(
                color: Colors.white,
                fontSize: 16.09.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '£]',
              style: GoogleFonts.dmSans(
                color: C.springGreen,
                fontSize: 16.09.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: 'Bt3',
              style: GoogleFonts.dmSans(
                color: Colors.white,
                fontSize: 16.09.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '&£',
              style: GoogleFonts.dmSans(
                color: C.springGreen,
                fontSize: 16.09.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: 'A',
              style: GoogleFonts.dmSans(
                color: Colors.white,
                fontSize: 16.09.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '@}',
              style: GoogleFonts.dmSans(
                color: C.springGreen,
                fontSize: 16.09.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}

class _HiddenPwdBubble0 extends StatelessWidget {
  const _HiddenPwdBubble0({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      top: 180.h,
      left: 25.w,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AppAssets.iconLock,
            width: 16.w,
            height: 16.w,
            fit: BoxFit.fill,
          ),
          SizedBox(width: 11.w),
          components.TextDmSans(
            text: "******",
            fontSize: 22.sp,
            fontWeight: FontWeight.w700,
            // color: _textColor,
            color: C.springGreen,
            letterSpacing: 1.5,
          ),
        ],
      ),
    );
  }
}

class _UserCommentBubble1 extends StatelessWidget {
  const _UserCommentBubble1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      radius: 12.r,
      bottom: 60.h,
      right: 10.w,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            AppAssets.avatar,
            height: 24.h,
            width: 24.h,
          ),
          SizedBox(height: 10.h),
          components.TextDmSans(
            text: "John S.",
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
          SizedBox(height: 5.h),
          SizedBox(
            width: 100.w,
            child: components.TextDmSans(
              text: FlutterI18n.translate(context, "USER_COMMENT"),
              fontSize: 10.sp,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

class _NoteBubble1 extends StatelessWidget {
  const _NoteBubble1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      bottom: 30.h,
      left: 70.w,
      color: C.deepSea.withOpacity(0.35),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            AppAssets.iconNote,
            height: 30.h,
            width: 30.h,
            fit: BoxFit.fill,
          ),
          SizedBox(width: 9.w),
          components.TextDmSans(
            text: FlutterI18n.translate(context, "NOTE"),
            color: Colors.white,
            fontSize: 12.36.sp,
            fontWeight: FontWeight.w700,
          )
        ],
      ),
    );
  }
}

class _PasswordBubble1 extends StatelessWidget {
  const _PasswordBubble1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      top: 130.h,
      left: 20.w,
      color: C.deepSea.withOpacity(0.35),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            AppAssets.iconPassword,
            height: 30.h,
            width: 30.w,
            fit: BoxFit.fill,
          ),
          SizedBox(width: 9.w),
          components.TextDmSans(
            text: FlutterI18n.translate(context, "PASSWORD"),
            color: Colors.white,
            fontSize: 12.36.sp,
            fontWeight: FontWeight.w700,
          )
        ],
      ),
    );
  }
}

class _CardBubble1 extends StatelessWidget {
  const _CardBubble1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Bubble(
      top: 80.h,
      left: 50.w,
      color: C.deepSea.withOpacity(0.35),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            AppAssets.iconCreditCard,
            height: 19.h,
            width: 25.w,
            fit: BoxFit.fitWidth,
          ),
          SizedBox(width: 9.w),
          components.TextDmSans(
            text: FlutterI18n.translate(context, "CARD"),
            color: Colors.white,
            fontSize: 12.36.sp,
            fontWeight: FontWeight.w700,
          )
        ],
      ),
    );
  }
}
