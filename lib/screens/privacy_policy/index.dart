import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:psono/components/_index.dart' as component;

class PrivacyPolicyScreen extends StatefulWidget {
  static String tag = 'datastore-screen';

  @override
  _PrivacyPolicyScreenState createState() => _PrivacyPolicyScreenState();
}

class _PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  final logo = Hero(
    tag: 'hero',
    child: SvgPicture.asset(
      'assets/images/logo.svg',
      semanticsLabel: 'Psono Logo',
      height: 70.0,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, 'PRIVACY_POLICY')),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          const SizedBox(height: 24.0),
          RichText(
            text: const TextSpan(
              text: 'Privacy Policy: ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(height: 16.0),
          RichText(
            text: const TextSpan(
              text: '''
Your privacy is important to us. Psono follows some fundamental principles:''',
            ),
          ),
          const SizedBox(height: 8.0),
          Padding(
            padding: const EdgeInsets.only(left: 24.0),
            child: RichText(
              text: const TextSpan(
                text: '''
- We do not ask for any information that we do not really need.
- We do not share your information with anyone except to comply with the German law, protect our rights and develop our product.
- No sensitive data like your passwords will ever leave your computer without being encrypted before.''',
              ),
            ),
          ),
          const SizedBox(height: 8.0),
          RichText(
            text: const TextSpan(
              text:
                  '''Your personal data is processed by us only in accordance with the German data privacy law. The following passages describes the type and purpose of the use of the gathered personal data.''',
            ),
          ),
          const SizedBox(height: 24.0),
          RichText(
            text: const TextSpan(
              text: 'Personal Data',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(height: 16.0),
          RichText(
            text: const TextSpan(
              text: '''
Any data (name, address, billing information, IP-address, ...) is only used to fulfill the service and without a legal basis or your explicit consent, not given to any third parties.''',
            ),
          ),
          const SizedBox(height: 8.0),
          RichText(
            text: const TextSpan(
              text: '''
Psono collects personal-identifying information like for example the IP address while using our service, to ensure the service availability and to protect your account. Psono may use that information for statistical analysis and may display this anonymized statistical analysis publicly or provide it to others.''',
            ),
          ),
          const SizedBox(height: 8.0),
          RichText(
            text: const TextSpan(
              text: '''
Account holders in addition must specify some additional information during registration, like for example their e-mail address. That information is used to prevent service disruptions and to inform users about security relevant information.''',
            ),
          ),
        ],
      ),
    );
  }
}
