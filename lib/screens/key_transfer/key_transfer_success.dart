part of 'key_transfer_screen.dart';

class _SuccessScreen extends StatelessWidget {
  const _SuccessScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            // logo,
            const SizedBox(height: 24.0),
            const Icon(
              component.FontAwesome.thumbs_o_up,
              color: Color(0xFFFFFFFF),
              size: 48.0,
            ),
            const SizedBox(height: 24.0),
            component.BtnPrimary(
              text: FlutterI18n.translate(context, "BACK_TO_HOME"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
