part of 'key_transfer_screen.dart';

class _DefaultScreen extends StatelessWidget {
  const _DefaultScreen({
    super.key,
    required this.password,
    required this.passwordRepeat,
    required this.approveButton,
    required this.formKey,
  });

  final component.CustomTextField password;
  final component.CustomTextField passwordRepeat;
  final component.BtnPrimary approveButton;
  final GlobalKey formKey;

  @override
  Widget build(BuildContext context) {
    bool serverSecretExists = reduxStore.state.serverSecretExists;

    return component.ScaffoldDark(
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom,
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.h),
                Align(
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    onTap: () {
                      user_service.logout();
                      if (context.mounted) {
                        Navigator.pushReplacementNamed(
                            context, AppRoutes.signin);
                      }
                    },
                    child: Image.asset(
                      AppAssets.iconNavBack,
                      height: 24.h,
                      width: 24.h,
                    ),
                  ),
                ),
                SizedBox(height: 24.h),
                component.AlertInfo(
                  text: FlutterI18n.translate(
                    context,
                    serverSecretExists
                        ? "ADMINISTRATOR_REQUIRES_ACCOUNT_SWITCH_TO_CLIENT_SIDE_ENCRYPTION"
                        : "ADMINISTRATOR_REQUIRES_ACCOUNT_SWITCH_TO_SERVER_SIDE_ENCRYPTION",
                  ),
                ),
                serverSecretExists ? SizedBox(height: 24.h) : const SizedBox(),
                serverSecretExists ? password : const SizedBox(),
                serverSecretExists ? SizedBox(height: 16.h) : const SizedBox(),
                serverSecretExists ? passwordRepeat : const SizedBox(),
                SizedBox(height: 40.h),
                approveButton,
                const Spacer(),
                component.BtnText(
                  onPressed: () {
                    helper.openUrl('https://www.psono.pw/privacy-policy.html');
                  },
                  text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                  color: Colors.white.withOpacity(0.60),
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w400,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
