import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/custom_text_field.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/server_secret.dart' as server_secret_service;
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';

part 'key_transfer_default.dart';
part 'key_transfer_success.dart';

enum _ScreenType {
  defaultScreen,
  successScreen,
}

class KeyTransferScreen extends StatefulWidget {
  static String tag = 'key-transfer-screen';
  @override
  _KeyTransferScreenState createState() => _KeyTransferScreenState();
}

class _KeyTransferScreenState extends State<KeyTransferScreen> {
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordRepeatController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );

  _ScreenType _screen = _ScreenType.defaultScreen;

  bool _obscurePassword = true;
  bool _obscureRepeatPassword = true;
  api_client.Info? info;
  final FocusNode serverFocus = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    _passwordController.dispose();
    _passwordRepeatController.dispose();
    _serverUrlController.dispose();
    super.dispose();
  }

  final formKey = GlobalKey<FormState>();

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(context, title, content);
  }

  @override
  Widget build(BuildContext context) {
    late api_client.Info info;
    final password = component.CustomTextField(
        controller: _passwordController,
        autofocus: true,
        obscureText: _obscurePassword,
        labelText: FlutterI18n.translate(context, "PASSWORD"),
        suffixIcon: ObscureIcon(
          obscure: _obscurePassword,
          onTap: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
        validator: (value) {
          if (!reduxStore.state.serverSecretExists) {
            return null;
          }
          String? testFailure = helper.isValidPassword(
            value ?? '',
            _passwordRepeatController.text,
            info.complianceMinMasterPasswordLength,
            info.complianceMinMasterPasswordComplexity,
          );
          if (testFailure == null) {
            return null;
          }
          return FlutterI18n.translate(
            context,
            testFailure,
          );
        });

    final passwordRepeat = component.CustomTextField(
      controller: _passwordRepeatController,
      autofocus: false,
      obscureText: _obscureRepeatPassword,
      labelText: FlutterI18n.translate(context, "PASSWORD_REPEAT"),
      suffixIcon: ObscureIcon(
        obscure: _obscureRepeatPassword,
        onTap: () {
          setState(() {
            _obscureRepeatPassword = !_obscureRepeatPassword;
          });
        },
      ),
      validator: (value) {
        if (value!.isNotEmpty && value != _passwordController.text) {
          return FlutterI18n.translate(
            context,
            'PASSWORDS_DONT_MATCH',
          );
        }
        return null;
      },
    );

    final approveButton = component.BtnPrimary(
      onPressed: () async {
        component.Loader.show(context);
        try {
          info = await api_client.info();
        } on api_client.ServiceUnavailableException {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          component.Loader.hide();
          _showErrorDialog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        if (!formKey.currentState!.validate()) {
          component.Loader.hide();
          return;
        }

        if (reduxStore.state.serverSecretExists) {
          try {
            await server_secret_service
                .deleteServerSecret(_passwordController.text);
          } on api_client.BadRequestException catch (e) {
            _showErrorDialog('ERROR', e.getFirst());
            return;
          } catch (e) {
            _showErrorDialog(
              FlutterI18n.translate(context, "SERVER_OFFLINE"),
              FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
            );
            return;
          } finally {
            component.Loader.hide();
          }

          reduxStore.dispatch(
            SetServerSecretExists(
              false,
            ),
          );
          if (mounted) {
            Navigator.pushReplacementNamed(context, AppRoutes.datastore);
          }
        } else {
          // create server secrets

          try {
            await server_secret_service.createServerSecret();
          } on api_client.BadRequestException catch (e) {
            _showErrorDialog('ERROR', e.getFirst());
            return;
          } catch (e) {
            _showErrorDialog(
              FlutterI18n.translate(context, "SERVER_OFFLINE"),
              FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
            );
            return;
          } finally {
            component.Loader.hide();
          }

          reduxStore.dispatch(
            SetServerSecretExists(
              true,
            ),
          );
          if (mounted) {
            Navigator.pushReplacementNamed(context, AppRoutes.datastore);
          }
        }
      },
      text: FlutterI18n.translate(context, "APPROVE"),
    );

    if (_screen == _ScreenType.successScreen) {
      return const _SuccessScreen();
    } else {
      return _DefaultScreen(
        password: password,
        passwordRepeat: passwordRepeat,
        approveButton: approveButton,
        formKey: formKey,
      );
    }
  }
}
