import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/components/text_dm_sans.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/routes/routes.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/storage.dart';
import 'package:psono/services/user.dart' as user_service;
import 'package:psono/utils/app_assets.dart';
import 'package:psono/utils/app_colors.dart';
import 'package:psono/utils/app_constants.dart';

part 'email_form_page.dart';
part 'password_form_page.dart';
part 'success.dart';

enum _ScreenType {
  defaultScreen,
  successScreen,
}

class RegisterScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _usernameController = TextEditingController(
    text: '',
  );
  final _emailController = TextEditingController(
    text: '',
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordRepeatController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );

  final FocusNode _serverFocus = FocusNode();
  _ScreenType _screen = _ScreenType.defaultScreen;
  late api_client.Info info;
  final PageController _pageController = PageController();

  String _domainSuffix = '@${helper.getDomain(reduxStore.state.serverUrl)!}';
  @override
  void initState() {
    _usernameController.addListener(onChangeTextField);
    _serverUrlController.addListener(onChangeTextField);
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _passwordRepeatController.dispose();
    _serverUrlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _emailForm = _EmailFormPage(
      usernameController: _usernameController,
      emailController: _emailController,
      serverUrlController: _serverUrlController,
      domainSuffix: _domainSuffix,
      serverFocus: _serverFocus,
      onNextTap: () {
        String? userNameError =
            helper.isValidUsername(_usernameController.text);
        if (userNameError != null) {
          helper.showErrorDialog(
            context,
            FlutterI18n.translate(context, "INVALID_USERNAME_FORMAT"),
            FlutterI18n.translate(context, userNameError),
          );
          return;
        }
        if (!helper.isValidEmail(_emailController.text)) {
          helper.showErrorDialog(
            context,
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, 'INVALID_EMAIL_FORMAT'),
          );
          return;
        }

        _pageController.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInOut,
        );
      },
    );
    final _passwordForm = _PasswordFormPage(
      passwordController: _passwordController,
      passwordRepeatController: _passwordRepeatController,
      onRegisterTap: _registerTap,
    );

    if (_screen == _ScreenType.successScreen) {
      return _Success(
        email: _emailController.text,
        password: _passwordController.text,
      );
    } else {
      return PopScope(
        canPop: false,
        onPopInvoked: (didPop) {
          if (didPop) return;
          if (_pageController.page == 0) {
            Navigator.pop(context);
          } else {
            _pageController.previousPage(
              duration: const Duration(milliseconds: 300),
              curve: Curves.easeInOut,
            );
          }
        },
        child: component.ScaffoldDark(
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top -
                  MediaQuery.of(context).padding.bottom,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(height: 20.h),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        if (_pageController.page!.toInt() == 0) {
                          Navigator.pop(context);
                        } else {
                          _pageController.previousPage(
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.easeInOut,
                          );
                        }
                      },
                      child: Image.asset(
                        AppAssets.iconNavBack,
                        height: 24.h,
                        width: 24.h,
                      ),
                    ),
                  ),
                  SizedBox(height: 24.h),
                  TextDmSans(
                    text: FlutterI18n.translate(context, "CREATE_AN_ACCOUNT"),
                    textAlign: TextAlign.center,
                    color: Colors.white,
                    fontSize: 32.sp,
                    fontWeight: FontWeight.w500,
                    height: 0,
                  ),
                  SizedBox(height: 32.h),
                  Flexible(
                    child: PageView(
                      physics: const NeverScrollableScrollPhysics(),
                      controller: _pageController,
                      children: [
                        _emailForm,
                        _passwordForm,
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }

  _registerTap() async {
    try {
      component.Loader.show(context);
      await storage.write(
        key: SK.serverUrl,
        value: _serverUrlController.text,
        iOptions: secureIOSOptions,
      );
      await storage.write(
        key: SK.username,
        value: _usernameController.text + _domainSuffix,
        iOptions: secureIOSOptions,
      );

      reduxStore.dispatch(
        InitiateLoginAction(
          _serverUrlController.text,
          _usernameController.text + _domainSuffix,
        ),
      );
      info = await api_client.info();
    } on api_client.ServiceUnavailableException {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "SERVER_OFFLINE"),
        FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
      );

      return;
    } on HandshakeException {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
        FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
      );
      return;
    } catch (e) {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "SERVER_OFFLINE"),
        FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
      );
      return;
    } finally {
      component.Loader.hide();
    }

    String? testError = helper.isValidPassword(
      _passwordController.text,
      _passwordRepeatController.text,
      info.complianceMinMasterPasswordLength,
      info.complianceMinMasterPasswordComplexity,
    );
    if (testError != null) {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "ERROR"),
        FlutterI18n.translate(context, testError),
      );
      return;
    }

    bool isValid = helper.isValidEmail(_emailController.text);
    if (!isValid) {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "ERROR"),
        FlutterI18n.translate(context, 'INVALID_EMAIL_FORMAT'),
      );
      return;
    }

    String username = _usernameController.text + _domainSuffix;

    testError = helper.isValidUsername(username);
    if (testError != null) {
      helper.showErrorDialog(
        context,
        FlutterI18n.translate(context, "ERROR"),
        FlutterI18n.translate(context, testError),
      );
      return;
    }

    initiateRegistration(
      _emailController.text,
      username,
      _passwordController.text,
    );
  }

  void onChangeTextField() {
    if (_usernameController.text.contains('@')) {
      _domainSuffix = '';
    } else {
      _domainSuffix = '@${helper.getDomain(_serverUrlController.text)!}';
    }

    setState(() {
      _domainSuffix = _domainSuffix;
    });
  }

  void initiateRegistration(
    String email,
    String username,
    String password,
  ) async {
    component.Loader.show(context);
    try {
      await user_service.register(
        email,
        username,
        password,
        helper.getDomain(_serverUrlController.text),
      );
    } on api_client.BadRequestException catch (e) {
      helper.showErrorDialog(context, 'ERROR', e.getFirst());
      return;
    } finally {
      component.Loader.hide();
    }
    setState(() {
      _screen = _ScreenType.successScreen;
    });
  }
}
