part of 'register_screen.dart';

class _EmailFormPage extends StatefulWidget {
  const _EmailFormPage(
      {Key? key,
      required this.usernameController,
      required this.emailController,
      required this.serverUrlController,
      required this.domainSuffix,
      required this.serverFocus,
      required this.onNextTap})
      : super(key: key);
  final TextEditingController usernameController;
  final TextEditingController emailController;
  final TextEditingController serverUrlController;
  final String domainSuffix;
  final FocusNode serverFocus;
  final Function() onNextTap;
  @override
  _EmailFormPageState createState() => _EmailFormPageState();
}

class _EmailFormPageState extends State<_EmailFormPage> {
  @override
  Widget build(BuildContext context) {
    final Widget server = component.CustomTextField(
      keyboardType: TextInputType.url,
      controller: widget.serverUrlController,
      labelText: FlutterI18n.translate(context, "SERVER_URL"),
      backgroundColor: Colors.white.withOpacity(0.10),
      focusNode: widget.serverFocus,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        component.CustomTextField(
          controller: widget.usernameController,
          keyboardType: TextInputType.emailAddress,
          labelText: FlutterI18n.translate(context, "USERNAME"),
          suffixIcon: Center(
            child: TextDmSans(
              text: widget.domainSuffix,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              color: Colors.white.withOpacity(0.50),
              fontSize: 16.sp,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        SizedBox(height: 16.h),
        component.CustomTextField(
          controller: widget.emailController,
          keyboardType: TextInputType.emailAddress,
          labelText: FlutterI18n.translate(context, "EMAIL"),
        ),
        SizedBox(height: 24.h),
        SizedBox(
          width: double.infinity,
          child: component.BtnPrimary(
            onPressed: widget.onNextTap,
            text: FlutterI18n.translate(context, "NEXT").toUpperCase(),
          ),
        ),
        SizedBox(height: 40.h),
        TextDmSans(
          text: FlutterI18n.translate(context, "ALREADY_HAVE_AN_ACCOUNT"),
          fontSize: 16.sp,
          fontWeight: FontWeight.w400,
          color: Colors.white.withOpacity(0.60),
        ),
        SizedBox(height: 8.h),
        component.BtnText(
          text: FlutterI18n.translate(context, "SIGN_IN"),
          fontSize: 16.sp,
          fontWeight: FontWeight.w400,
          color: Colors.white,
          onPressed: () {
            Navigator.pushReplacementNamed(context, AppRoutes.signin);
          },
        ),
        KeyboardVisibilityBuilder(
          builder: (context, isKeyboardVisible) {
            if (isKeyboardVisible && widget.serverFocus.hasFocus) {
              return const SizedBox();
            }
            return const Spacer();
          },
        ),
        server,
        const SizedBox(height: 24.0),
        Row(
          children: [
            const Spacer(flex: 4),
            component.BtnText(
              onPressed: () {
                helper.openUrl('https://www.psono.pw/privacy-policy.html');
              },
              text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
              color: Colors.white.withOpacity(0.60),
              fontSize: 16.sp,
              fontWeight: FontWeight.w400,
            ),
            const Spacer(flex: 4),
          ],
        ),
        SizedBox(height: 24.h),
      ],
    );
  }
}
