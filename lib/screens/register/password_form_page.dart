part of 'register_screen.dart';

class _PasswordFormPage extends StatefulWidget {
  const _PasswordFormPage({
    Key? key,
    required this.passwordController,
    required this.passwordRepeatController,
    required this.onRegisterTap,
  }) : super(key: key);
  final TextEditingController passwordController;
  final TextEditingController passwordRepeatController;
  final Function() onRegisterTap;
  @override
  _PasswordFormPageState createState() => _PasswordFormPageState();
}

class _PasswordFormPageState extends State<_PasswordFormPage> {
  bool _obscurePassword = true;
  bool _obscurePasswordRepeat = true;
  bool _isAccepted = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        component.CustomTextField(
          controller: widget.passwordController,
          labelText: FlutterI18n.translate(context, "PASSWORD"),
          obscureText: _obscurePassword,
          suffixIcon: component.ObscureIcon(
            onTap: () {
              setState(() {
                _obscurePassword = !_obscurePassword;
              });
            },
            obscure: _obscurePassword,
          ),
        ),
        SizedBox(height: 16.h),
        component.CustomTextField(
          controller: widget.passwordRepeatController,
          labelText: FlutterI18n.translate(context, "CONFIRM_PASSWORD"),
          obscureText: _obscurePasswordRepeat,
          suffixIcon: component.ObscureIcon(
            onTap: () {
              setState(() {
                _obscurePasswordRepeat = !_obscurePasswordRepeat;
              });
            },
            obscure: _obscurePasswordRepeat,
          ),
        ),
        SizedBox(height: 24.h),
        Row(
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  _isAccepted = !_isAccepted;
                });
              },
              child: Container(
                height: 24.h,
                width: 24.h,
                margin: EdgeInsets.all(4.h),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.10),
                  borderRadius: BorderRadius.circular(8.r),
                  border: Border.all(
                    color: C.mountainMeadow,
                    width: 1,
                  ),
                ),
                child: !_isAccepted
                    ? Container()
                    : const Icon(
                        Icons.check,
                        color: C.mountainMeadow,
                        size: 20,
                      ),
              ),
            ),
            SizedBox(width: 18.w),
            Expanded(
              child: Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(
                          context, "I_HAVE_READ_AND_AGREE_TO_THE"),
                      style: GoogleFonts.dmSans(
                        color: Colors.white.withOpacity(0.6),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        height: 0,
                      ),
                    ),
                    TextSpan(
                      text:
                          " ${FlutterI18n.translate(context, "TERMS_OF_USE")} ",
                      style: GoogleFonts.dmSans(
                        color: const Color(0xFF57C7A3),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        height: 0,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () async {
                          helper.openUrl(
                              'https://www.psono.pw/privacy-policy.html');
                        },
                    ),
                    TextSpan(
                      text: FlutterI18n.translate(context, "AND"),
                      style: GoogleFonts.dmSans(
                        color: Colors.white.withOpacity(0.60),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    TextSpan(
                      text:
                          " ${FlutterI18n.translate(context, "PRIVACY_POLICY")}.",
                      style: GoogleFonts.dmSans(
                        color: const Color(0xFF57C7A3),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        height: 0,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () async {
                          helper.openUrl(
                              'https://www.psono.pw/privacy-policy.html');
                        },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        KeyboardVisibilityBuilder(
          builder: (context, isKeyboardVisible) {
            if (isKeyboardVisible) {
              return SizedBox(height: 20.h);
            } else {
              return const Spacer();
            }
          },
        ),
        component.BtnPrimary(
          onPressed: widget.onRegisterTap,
          width: double.infinity,
          text: FlutterI18n.translate(context, "CREATE_ACCOUNT").toUpperCase(),
        ),
        SizedBox(height: 24.h),
      ],
    );
  }
}
