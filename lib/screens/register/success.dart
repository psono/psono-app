part of 'register_screen.dart';

class _Success extends StatefulWidget {
  const _Success({
    Key? key,
    required this.email,
    required this.password,
  }) : super(key: key);
  final String email;
  final String password;

  @override
  State<_Success> createState() => _SuccessState();
}

class _SuccessState extends State<_Success> {
  bool _obscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AppAssets.onboardingBackground),
            fit: BoxFit.fill,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                AppAssets.bigCheckMark,
                width: 451.w,
                height: 363.w,
                fit: BoxFit.fill,
              ),
              const Spacer(flex: 4),
              TextDmSans(
                text: FlutterI18n.translate(
                  context,
                  FlutterI18n.translate(
                      context, "ACCOUNT_CREATED_SUCCESSFULLY"),
                ),
                textAlign: TextAlign.center,
                fontSize: 40.sp,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
              const Spacer(),
              TextDmSans(
                text: widget.email,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: Colors.white.withOpacity(0.60),
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: TextDmSans(
                      text: _obscurePassword
                          ? widget.password.replaceAll(RegExp(r"."), "*")
                          : widget.password,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w400,
                      color: Colors.white.withOpacity(0.60),
                    ),
                  ),
                  SizedBox(width: 12.w),
                  component.ObscureIcon(
                      onTap: () {
                        setState(() {
                          _obscurePassword = !_obscurePassword;
                        });
                      },
                      obscure: _obscurePassword)
                ],
              ),
              const Spacer(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: TextDmSans(
                        text: FlutterI18n.translate(
                            context, "REMEMBER_YOUR_MASTER_PASSWORD"),
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w400,
                        color: Colors.white.withOpacity(0.60),
                      ),
                    ),
                    SizedBox(width: 12.w),
                    Icon(
                      Icons.info_outline,
                      color: Colors.white.withOpacity(0.60),
                    )
                  ],
                ),
              ),
              const Spacer(),
              component.BtnPrimary(
                text: FlutterI18n.translate(context, "START"),
                onPressed: () {
                  helper.makeFirstNamed(context, AppRoutes.datastore);
                },
              ),
              SizedBox(height: 24.h),
            ],
          ),
        ),
      ),
    );
  }
}
