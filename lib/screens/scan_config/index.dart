import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/scan_qr/index.dart';
import 'package:psono/services/api_client/index.dart' as api_client;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_host.dart' as managerHost;
import 'package:psono/services/storage.dart';
import 'package:psono/utils/app_constants.dart';

class ScanConfigScreen extends StatefulWidget {
  static String tag = 'scan-config-screen';
  @override
  _ScanConfigScreenState createState() => _ScanConfigScreenState();
}

class _ScanConfigScreenState extends State<ScanConfigScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  void _showErrorDialog(String title, String? content) {
    helper.showErrorDialog(context, title, content);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _initScanConfig();
    });
  }

  Future<void> _initScanConfig() async {
    String? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const ScanQR(
          backCount: 2,
        ),
      ),
    );

    Map? configMap;
    Config config;
    // v1
    // String result2 = """{
    //   "ConfigJson": {
    //     "backend_servers": [{
    //         "title": "Psono.pw",
    //         "domain": "example.com",
    //         "url": "https://example2.com"
    //       }
    //     ],
    //     "allow_custom_server": true,
    //     "allow_registration": true,
    //     "allow_lost_password": true,
    //     "authentication_methods": ["AUTHKEY", "LDAP"],
    //     "more_links": [{
    //         "href": "https://doc.psono.com/",
    //         "title": "DOCUMENTATION",
    //         "class": "fa-book"
    //       }, {
    //         "href": "privacy-policy.html",
    //         "title": "PRIVACY_POLICY",
    //         "class": "fa-user-secret"
    //       }, {
    //         "href": "https://www.psono.com",
    //         "title": "ABOUT_US",
    //         "class": "fa-info-circle"
    //       }
    //     ]
    //   }
    // }""";
    //
    // v2
    // String result2 = """{
    //   "version": 2,
    //   "config": {
    //     "verify_key": "7a04c60e944e5820da9ddf2e9310bec4b576242bd0a5751b0c473d4347620cab",
    //     "url": "https://example.com/server"
    //   }
    // }""";

    if (result == null) {
      return;
    }

    if (!mounted) {
      return;
    }
    component.Loader.show(context);

    try {
      configMap = jsonDecode(result);
      config = Config.fromJson(configMap as Map<String, dynamic>);
    } on FormatException {
      return;
    }

    if (config.version == 2) {
      await managerHost.approveHost(
          config.config!.url!, config.config!.verifyKey!);

      reduxStore.dispatch(
        InitiateLoginAction(
          config.config!.url!,
          "",
        ),
      );

      await storage.write(
        key: SK.serverUrl,
        value: config.config!.url!,
        iOptions: secureIOSOptions,
      );
      try {
        result = await managerHost.loadRemoteConfig(config.config!.url!);
        try {
          configMap = jsonDecode(result);
          config = Config(
              version: 1,
              configJson:
                  ConfigJson.fromJson(configMap as Map<String, dynamic>));
        } on FormatException {
          return;
        }
      } on api_client.ServiceUnavailableException {
        component.Loader.hide();
        if (!mounted) {
          return;
        }
        _showErrorDialog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
        return;
      } on HandshakeException {
        component.Loader.hide();
        if (!mounted) {
          return;
        }
        _showErrorDialog(
          FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
          FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
        );
        return;
      } catch (e) {
        component.Loader.hide();
        if (!mounted) {
          return;
        }
        _showErrorDialog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
        return;
      }
    }

    await storage.write(
      key: SK.configJson,
      value: jsonEncode(config),
      iOptions: secureIOSOptions,
    );

    reduxStore.dispatch(
      ConfigUpdatedAction(
        config,
      ),
    );
    if (config.configJson != null &&
        config.configJson!.backendServers != null &&
        config.configJson!.backendServers!.length > 0 &&
        config.configJson!.backendServers![0].url != null) {
      reduxStore.dispatch(
        InitiateLoginAction(
          config.configJson!.backendServers![0].url,
          "",
        ),
      );

      await storage.write(
        key: SK.serverUrl,
        value: config.configJson!.backendServers![0].url,
        iOptions: secureIOSOptions,
      );
    }
    component.Loader.hide();
    if (!mounted) {
      return;
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: component.Loading(),
    );
  }
}
