import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:psono/services/offline_cache.dart' as offline_cache;
import 'package:psono/utils/app_constants.dart';
import 'package:sentry/sentry.dart';

Future<void> main() async {
  await Sentry.init(
    (options) {
      options.dsn =
          'https://7b17edbedc9244da8c16f3c30db4b0fe@sentry.io/1527081';
    },
    appRunner: initApp, // Init your App.
  );
}

Future<void> initApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  await offline_cache.init();
  Timer(const Duration(milliseconds: 10000), () {
    offline_cache.incrementalUpdate(ratelimit: true);
  });
  var configuredApp = Builder(
    builder: (context) {
      final double ratio = MediaQuery.of(context).size.width /
          MediaQuery.of(context).size.height;
      const double designHeight = K.kDesignHeight;
      final double designWidth =
          ratio > 0.6 ? MediaQuery.of(context).size.width : K.kDesignWidth;
      final designSize = Size(designWidth, designHeight);
      return ScreenUtilInit(
        designSize: designSize,
        fontSizeResolver: FontSizeResolvers.width,
        builder: (context, _) {
          return AppConfig(
            appName: 'Psono',
            flavorName: 'production',
            defaultServerUrl: 'https://www.psono.pw/server',
            child: MyApp(),
          );
        },
      );
    },
  );
  runApp(configuredApp);
}
