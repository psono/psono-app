class AppAssets {
  static const String _base = 'assets/images';
  static const String onboardingBackground = '$_base/onboarding_bg.png';
  static const List<String> onboardingIllustrations = [
    "$_base/onboarding-il-1.png",
    "$_base/onboarding-il-2.png",
    "$_base/onboarding-il-3.png",
  ];

  static const avatar = '$_base/avatar.png';
  static const folder = '$_base/folder.png';
  static const folder2 = '$_base/folder_2.png';
  static const iconCreditCard = '$_base/icon_credit_card.png';
  static const iconNote = '$_base/icon_note.png';
  static const iconLock = '$_base/icon_lock.png';
  static const iconPassword = '$_base/icon_password.png';
  static const iconUser = '$_base/icon_user.png';
  static const iconNavBack = '$_base/icon_nav_back.png';
  static const darkScaffoldBg = '$_base/dark_scaffold_bg.png';
  static const iconEyeClose = '$_base/icon_eye_close.png';
  static const iconEyeOpen = '$_base/icon_eye_open.png';
  static const bigCheckMark = '$_base/big_check_mark.png';
  static const faceIDBg = "$_base/face_id_bg.png";
  static const faceLock = "$_base/face_lock.png";
  static const fingerLock = "$_base/finger_lock.png";
  static const faceIdSymbol = "$_base/face_id_symbol.png";
  static const fingerprint = "$_base/fingerprint.png";
  static const logoRound = "$_base/logo_round.png";
  static const warning = "$_base/warning.png";
  static const info = "$_base/info.png";
  static const alertDanger = "$_base/alert_danger.png";
  static const autofillLock = "$_base/autofill_lock.png";
  static const iconSettings = "$_base/iconSettings.png";
  static const iconPasswordsAccounts = "$_base/iconPasswordsAccounts.png";
  static const iconAutofill = "$_base/iconAutofill.png";
  static const iconOn = "$_base/iconOn.png";
  static const iconPsonoApp = "$_base/iconPsonoApp.png";
  static const homeScreenBg = "$_base/home_screen_bg.png";
  static const filterIcon = "$_base/filter_icon.png";
  static const listIcon = "$_base/list_icon.png";
  static const gridIcon = "$_base/grid_icon.png";
  static const psonoEmptyFolder = "$_base/psono_empty_folder.png";
  static const entityBookmarkIcon = "$_base/entity_bookmark_icon.png";
  static const entityCardIcon = "$_base/entity_card_icon.png";
  static const entityFolderIcon = "$_base/entity_folder_icon.png";
  static const entityNotesIcon = "$_base/entity_notes_icon.png";
  static const entityPasswordIcon = "$_base/entity_password_icon.png";
  static const bottomBarPerson = "$_base/bottom_bar_account.png";
  static const bottomBarSettings = "$_base/bottom_bar_settings.png";
  static const bottomBarHome = "$_base/bottom_bar_home.png";
  static const folderBg = "$_base/folder_bg.png";
  static const iconEdit = "$_base/icon_edit.png";
  static const iconBookmark = "$_base/icon_bookmark.png";
  static const iconTotp = "$_base/icon_totp.png";
  static const iconLock2 = "$_base/icon_lock_2.png";
  static const iconScan = "$_base/icon_scan.png";
  static const iconLock3 = "$_base/icon_lock_3.png";
  static const iconLock4 = "$_base/icon_lock_4.png";
  static const iconDatabases = "$_base/icon_databases.png";
  static const iconOfflineCache = "$_base/icon_offline_cache.png";
  static const iconSettings2 = "$_base/icon_settings_2.png";
  static const iconOverview = "$_base/icon_overview.png";
  static const iconChangeEmail = "$_base/icon_change_email.png";
  static const iconDeleteAccount = "$_base/icon_delete_account.png";
  static const iconUser2 = "$_base/icon_user_2.png";
  static const defaultAvatarImage = "$_base/default_avatar_image.png";
  static const iconCamera = "$_base/icon_camera.png";
}
