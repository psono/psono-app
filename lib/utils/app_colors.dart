import 'package:flutter/material.dart';

typedef C = AppColors;

class AppColors {
  static const Color aquaDeep = Color(0xff004C34);
  static const Color springGreen = Color(0xff0EFFB1);
  static const Color deepSea = Color(0xff008459);
  static const Color mountainMeadow = Color(0xFF1DA378);
}
