typedef K = AppConstants;
typedef SK = StorageKeys;

class AppConstants {
  static const double kDesignHeight = 844;
  static const double kDesignWidth = 390;
  static const String appFirstRunKey = "psono_app_first_run";
}

class StorageKeys {
  // storageKey
  static const String storageKey = 'storageKey';
  static const String serverUrl = 'serverUrl';
  static const String username = 'username';
  static const String token = 'token';
  static const String authentication = 'authentication';
  static const String sessionSecretKey = 'sessionSecretKey';
  static const String lastServerConnectionTimeSinceEpoch =
      'lastServerConnectionTimeSinceEpoch';
  static const String lastCacheTimeSinceEpoch = 'lastCacheTimeSinceEpoch';
  static const String complianceMaxOfflineCacheTimeValid =
      'complianceMaxOfflineCacheTimeValid';
  static const String secretKey = 'secretKey';
  static const String privateKey = 'privateKey';
  static const String publicKey = 'publicKey';
  static const String lockscreenPin = 'lockscreenPin';
  static const String complianceServerSecrets = 'complianceServerSecrets';
  static const String complianceDisableDeleteAccount =
      'complianceDisableDeleteAccount';
  static const String complianceDisableOfflineMode =
      'complianceDisableOfflineMode';
  static const String serverSecretExists = 'serverSecretExists';
  static const String userId = 'userId';
  static const String userEmail = 'userEmail';
  static const String userSauce = 'userSauce';
  static const String configJson = 'config';
  static const String passedAutofillOnboarding = 'passedAutofillOnboarding';
  //lastUnlockTime
  static const String lastUnlockTime = 'lastUnlockTime';
  //loginAttempts
  static const String loginAttempts = 'loginAttempts';
  //deviceFingerprint
  static const String deviceFingerprint = 'deviceFingerprint';
  //knownHosts
  static const String knownHosts = 'knownHosts';
}
