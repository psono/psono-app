//
//  OfflineCache.swift
//  AutoFillExtension
//
//  Created by Sascha Pfeiffer on 10.10.23.
//  Copyright © 2023 The Chromium Authors. All rights reserved.
//

import Foundation
import Sodium
import SQLite


class OfflineCacheService {
        
    private var db:Connection!
    private var storageKey:Bytes!
    
    public init() {
        
        self.storageKey = converter.fromHex(string: storage.read(key: "storageKey"))
        
        let directory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.psono.app")
        
        if directory != nil {
            do {
                self.db = try Connection("\(directory!.path)/psono_oc.db", readonly: true)
            } catch {
                print(error)
            }
        }
    }
    


    private func getLastServerConnectionTimeSinceEpoch () -> Int {
        let lastServerConnectionTimeSinceEpoch: String? = storage.read(key: "lastServerConnectionTimeSinceEpoch")
        if lastServerConnectionTimeSinceEpoch == nil {
            return 0
        }
        let lastServerConnectionTimeSinceEpochInt = Int(lastServerConnectionTimeSinceEpoch!)
        if lastServerConnectionTimeSinceEpochInt == nil {
            return 0
        }
        return lastServerConnectionTimeSinceEpochInt!
    }

    private func getComplianceMaxOfflineCacheTimeValid () -> Int {
        let complianceMaxOfflineCacheTimeValid: String? = storage.read(key: "complianceMaxOfflineCacheTimeValid")
        if complianceMaxOfflineCacheTimeValid == nil {
            return 0
        }
        let complianceMaxOfflineCacheTimeValidInt = Int(complianceMaxOfflineCacheTimeValid!)
        if complianceMaxOfflineCacheTimeValidInt == nil {
            return 0
        }
        return complianceMaxOfflineCacheTimeValidInt!
    }
    
    /**
     Returns the offline cached data for a specific connection type and endpoint
     
     - Parameter connectionType: The connection Type
     - Parameter endpoint: The endpoint

     - Returns: The corresponding hex value (represented as a String)
     */
    public func get(connectionType: String, endpoint: String) -> Data? {
        if getLastServerConnectionTimeSinceEpoch() + getComplianceMaxOfflineCacheTimeValid() < Int((Date().timeIntervalSince1970 * 1000.0).rounded()) {
            return nil
        }
        
        if self.db == nil || self.storageKey == nil {
            return nil
        }
        
        var data: Data? = nil
        var stmt: Statement? = nil
        
        do {
            stmt = try self.db.prepare("SELECT response, response_nonce FROM offline_cache WHERE connection_type = ? AND endpoint = ?")
        } catch {
            print(error)
            return nil
        }
        
        var dataString: String? = nil
        for row in stmt!.bind(connectionType.lowercased(), endpoint.lowercased()) {
            let text = converter.fromHex(string: row[0] as? String)!
            let nonce = converter.fromHex(string: row[1] as? String)!
            dataString = cryptoLibrary.decryptData(
                text: text,
                nonce: nonce,
                secretKey: self.storageKey
            )
            break
        }
        stmt!.reset()
        
        guard dataString != nil else {
            return nil
        }
        
        guard let cache = try? JSONSerialization.jsonObject(with: dataString!.data(using: .utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) else {
            return nil
        }
        
        guard let jsonDictionary: [String: Any] = cache as? [String: Any] else {
            return data
        }
        
        data = (jsonDictionary["body"] as! String).data(using: .utf8)
        
        return data
    }
    
}

let offlineCache = OfflineCacheService()

