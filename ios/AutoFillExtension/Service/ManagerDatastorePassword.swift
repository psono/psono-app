//
//  ManagerDatastorePassword.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class ManagerDatastorePasswordService {
    
    private var _allShareData: [String: Share] = [:]
    private var _shareIndex: [String: Bytes] = [:]
    
    public func findObject(folder: Folder, searchId: String) -> ([Folder]?, [Item]?, Int)? {
        if folder.folders != nil {
            for (n, f) in folder.folders!.enumerated() {
                if f.id == searchId {
                    return (folder.folders!, nil, n)
                }
            }
        }
        if folder.items != nil {
            for (n, item) in folder.items!.enumerated() {
                if item.id == searchId {
                    return (nil, folder.items!, n)
                }
            }
        }
        return nil
    }
    
    /**
     Go through the datastore recursive to find the object specified with the path
     */
    public func findInDatastore(path: [String], datastore: Folder) -> ([Folder]?, [Item]?, Int)? {
        var rest = path
        let toSearch: String = rest.removeFirst()
        
        if (rest.count == 0) {
            return findObject(folder: datastore, searchId: toSearch)
        }
        if datastore.folders != nil {

            for folder in datastore.folders! {
                if folder.id == toSearch {
                    return findInDatastore(path: rest, datastore: folder)
                }
            }
        }
        return nil
    }
    
    /**
     Updates some datastore folders or share folders with content
     Will calculate the delete property in the right object
     
     - Parameter datastore:
     */
    public func updatePathsWithData(datastore: Folder, path: [String], content: Share, parentShareRights: ShareRight?, parentShareId: String?, parentDatastoreId: String?) {
        let pathCopy = path
        let search = findInDatastore(path: pathCopy, datastore: datastore)
        if search == nil {
            return
        }
        
        if search!.0 != nil {
            let newFolderShare: Folder = search!.0![search!.2]
            if content.folder != nil {
                newFolderShare.name = content.folder!.name
                newFolderShare.folders = content.folder!.folders
                newFolderShare.items = content.folder!.items
                newFolderShare.shareIndex = content.folder!.shareIndex
            }
            // TODO add logic to update share rights
        }
        if search!.1 != nil {
            let newItemShare: Item = search!.1![search!.2]
            if content.item != nil {
                newItemShare.name = content.item!.name
                newItemShare.description = content.item!.description
                newItemShare.type = content.item!.type
                newItemShare.urlfilter = content.item!.urlfilter
                newItemShare.secretId = content.item!.secretId
                newItemShare.secretKey = content.item!.secretKey
            }
            // TODO add logic to update share rights
            
        }
    }
    
    /**
     Queries shares recursive. Will return the datastore with all the content in the completion handler
     
     - Parameter datastore: The datastore without recursive shares being loaded
     - Parameter shareRightsDict: The dictionary with the permissions for each share
     - Parameter completionHandler: The callback function with the content or the error
     */
    private func _readShares(datastore: Datastore?, shareRightsDict: [String: ReadShareRightsListEntry], completionHandler: @escaping (Datastore?, Error?) -> Void) {
        
        if datastore == nil || datastore!.data == nil || datastore!.data!.shareIndex == nil {
            completionHandler(datastore, nil)
            return
        }
        
        let shareIndex = datastore!.data!.shareIndex
        
        self._allShareData = [:]
        
        let parentShareRights = ShareRight.init(jsonDictionary: ["read" : true, "write" : true, "grant" : true, "delete" : true, ])!
        
        let taskGroup = DispatchGroup()
        
        
        func readSharesRecursive(datastore: Folder, shareRightsDict: [String: ReadShareRightsListEntry], shareIndex: [String: ShareLocation]?, parentShareRights: ShareRight?, parentShareId: String?, parentDatastoreId: String?, parentShareStack: [String]) {

            if shareIndex == nil {
                taskGroup.leave()
                return
            }
            
            for (shareId, shareLocation) in shareIndex! {
                _shareIndex[shareId] = shareLocation.secretKey
                var newParentShareStack = parentShareStack // create copy
                newParentShareStack.append(shareId)
                
                func readShareHelper(shareId: String, folderShare: Folder?, itemShare: Item?, path: [String], parentShareId: String?, parentDatastoreId: String?, parentShareStack: [String]) {
                    
                    managerShare.readShare(shareId: shareId, secretKey: _shareIndex[shareId]!, completionHandler: {
                        (content: Share?, error: Error?) -> Void in
                        
                        guard content != nil else {
                            taskGroup.leave() // leave readShareHelper
                            return
                        }
                        
                        self._allShareData[shareId] = content
                        
                        self.updatePathsWithData(datastore: datastore, path: path, content: content!, parentShareRights: parentShareRights, parentShareId: parentShareId, parentDatastoreId: parentDatastoreId)
                        
                        if folderShare != nil && content!.folder != nil {
                            taskGroup.enter() // enter readSharesRecursive
                            readSharesRecursive(datastore: folderShare!, shareRightsDict: shareRightsDict, shareIndex: content!.folder!.shareIndex, parentShareRights: content!.rights, parentShareId: shareId, parentDatastoreId: nil, parentShareStack: parentShareStack)
                            
                        }
                        taskGroup.leave() // leave readShareHelper
                    })
                }
                
                
                if shareIndex![shareId] != nil && shareIndex![shareId]!.paths != nil && shareIndex![shareId]!.paths != nil {
                    for path in shareIndex![shareId]!.paths! {
                        let pathCopy = path
                        
                        let search = self.findInDatastore(path: pathCopy, datastore: datastore)
                        if search == nil {
                            continue
                        }
                        var newFolderShare: Folder? = nil
                        var newItemShare: Item? = nil
                        if search!.0 != nil {
                            newFolderShare = search!.0![search!.2]
                        }
                        if search!.1 != nil {
                            newItemShare = search!.1![search!.2]
                        }
                        
                        // Break potential loop
                        if parentShareStack.contains(shareId) {
                            let content = Share.init(jsonDictionary: ["rights": ["read": false, "write": false, "grant": false]])!
                            self.updatePathsWithData(datastore: datastore, path: path, content: content, parentShareRights: parentShareRights, parentShareId: parentShareId, parentDatastoreId: nil)
                            continue
                        }
                        if self._allShareData[shareId] != nil {
                            self.updatePathsWithData(datastore: datastore, path: path, content: self._allShareData[shareId]!, parentShareRights: parentShareRights, parentShareId: parentShareId, parentDatastoreId: nil)
                            continue
                        }
                        // lets checjk if we have read writes fir this share and skip if we don't have read rights
                        if shareRightsDict[shareId] != nil && shareRightsDict[shareId]!.read != nil && !shareRightsDict[shareId]!.read! {
                            let content = Share.init(jsonDictionary: ["rights": ["read": shareRightsDict[shareId]!.read, "write": shareRightsDict[shareId]!.write, "grant": shareRightsDict[shareId]!.grant]])!
                            self.updatePathsWithData(datastore: datastore, path: path, content: content, parentShareRights: parentShareRights, parentShareId: parentShareId, parentDatastoreId: nil)
                            continue
                        }
                        // no specific share rights for this share, lets assume inherited rights and check if we have parent read rights
                        if shareRightsDict[shareId] != nil && parentShareRights != nil && parentShareRights!.read != nil && !parentShareRights!.read! {
                            let content = Share.init(jsonDictionary: ["rights": ["read": parentShareRights!.read, "write": parentShareRights!.write, "grant": parentShareRights!.grant]])!
                            self.updatePathsWithData(datastore: datastore, path: path, content: content, parentShareRights: parentShareRights, parentShareId: parentShareId, parentDatastoreId: nil)
                            continue
                        }
                        // no specific share rights for this share and datastaore as parent (no inheritance possible) we assume a share where we lost access rights
                        if shareRightsDict[shareId] == nil && parentDatastoreId != nil {
                            continue
                        }
                        taskGroup.enter() // enter readShareHelper
                        readShareHelper(shareId: shareId, folderShare: newFolderShare, itemShare: newItemShare, path: path, parentShareId: parentShareId, parentDatastoreId: parentDatastoreId, parentShareStack: newParentShareStack)
                    }
                }
            }
            taskGroup.leave() // leave readSharesRecursive
        }
        
        taskGroup.enter() // enter readSharesRecursive
        readSharesRecursive(datastore: datastore!.data!, shareRightsDict: shareRightsDict, shareIndex: shareIndex, parentShareRights: parentShareRights, parentShareId: nil, parentDatastoreId: datastore!.datastoreId, parentShareStack: [])
        
        taskGroup.notify(queue: .main) {
            // TODO call managerDatastore.updateShareRightsOfFoldersAndItems
            completionHandler(datastore, nil)
        }
    }
    
    
    /**
     Returns the password datastore and all shares filled recursive
     
     - Parameter datastoreId: the id of the datastore to return
     - Parameter completionHandler: The callback function with the content or the error
     */
    public func getPasswordDatastore(datastoreId: String?, completionHandler: @escaping (Datastore?, Error?) -> Void)  {
        
        let type: String = "password"
        
        managerDatastore.getDatastore(type: type, id: datastoreId) {
            (datastore: Datastore?, error: Error?) -> Void in
            
            managerShare.readShareRightsOverview(completionHandler: {
                (shareRightsOverview: ReadShareRightsList?, error: Error?) -> Void in
                
                var shareRightsDict: [String: ReadShareRightsListEntry] = [:]
                
                if shareRightsOverview != nil && shareRightsOverview!.shareRights != nil {
                    for shareRight in shareRightsOverview!.shareRights! {
                        shareRightsDict[shareRight.shareId!] = shareRight
                        
                    }
                }
                
                self._readShares(datastore: datastore, shareRightsDict: shareRightsDict, completionHandler: completionHandler)
                
            })
        }
        
        
    }
}

let managerDatastorePassword = ManagerDatastorePasswordService()
