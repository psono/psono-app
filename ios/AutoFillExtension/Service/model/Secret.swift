//
//  Secret.swift
//  AutoFillExtension
//

import Foundation
import Sodium


class KeyValue: JsonObject {
    public var key: String?
    public var value: String?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.key = jsonDictionary["key"] as? String
        self.value = jsonDictionary["value"] as? String
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.key != nil { jsonDict["key"] = self.key }
        if self.value != nil { jsonDict["value"] = self.value }

        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}


class CustomFields: JsonObject {
    public var name: String?
    public var type: String?
    public var value: String?

    required init() {}

    public required init?(jsonDictionary: [String: Any]){
        self.name = jsonDictionary["name"] as? String
        self.type = jsonDictionary["type"] as? String
        self.value = jsonDictionary["value"] as? String
    }

    public func toJsonDict() -> [String: Any] {

        var jsonDict: [String: Any] = [:]

        if self.name != nil { jsonDict["name"] = self.name }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.value != nil { jsonDict["value"] = self.value }

        return jsonDict
    }

    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}

class SecretData: JsonObject {
    public var type: String?
    public var customFields: Array<CustomFields>?

    public var applicationPasswordTitle: String?
    public var applicationPasswordUsername: String?
    public var applicationPasswordPassword: String?
    public var applicationPasswordNotes: String?

    public var totpTitle: String?
    public var totpCode: String?
    public var totpPeriod: String?
    public var totpDigits: String?
    public var totpAlgorithm: String?
    public var totpNotes: String?
    public var totpUrlFilter: String?

    public var websitePasswordTitle: String?
    public var websitePasswordUrl: String?
    public var websitePasswordUsername: String?
    public var websitePasswordPassword: String?
    public var websitePasswordNotes: String?
    public var websitePasswordAutoSubmit: Bool?
    public var websitePasswordUrlFilter: String?
    public var websitePasswordTotpCode: String?
    public var websitePasswordTotpPeriod: String?
    public var websitePasswordTotpDigits: String?
    public var websitePasswordTotpAlgorithm: String?
    
    public var noteTitle: String?
    public var noteNotes: String?
    
    public var environmentVariablesTitle: String?
    public var environmentVariablesVariables: Array<KeyValue>?
    public var environmentVariablesNotes: String?
    
    public var fileTitle: String?
    public var file: String?
    public var fileId: String?
    public var fileShareId: String?
    public var fileRepositoryId: String?
    public var fileDestination: String?
    public var fileSecretKey: String?
    public var fileSize: String?
    public var fileChunks: String?

    public var creditCardTitle: String?
    public var creditCardNumber: String?
    public var creditCardCVC: String?
    public var creditCardPIN: String?
    public var creditCardName: String?
    public var creditCardValidThrough: String?
    public var creditCardNotes: String?

    public var sshOwnKeyTitle: String?
    public var sshOwnKeyPublic: String?
    public var sshOwnKeyPrivate: String?
    public var sshOwnKeyNotes: String?

    public var elsterCertificateTitle: String?
    public var elsterCertificateFileContent: String?
    public var elsterCertificatePassword: String?
    public var elsterCertificateRetrievalCode: String?
    public var elsterCertificateNotes: String?

    public var mailGPGOwnKeyTitle: String?
    public var mailGPGOwnKeyEmail: String?
    public var mailGPGOwnKeyName: String?
    public var mailGPGOwnKeyPublic: String?
    public var mailGPGOwnKeyPrivate: String?
    public var mailGPGOwnKeyGenerateNew: String?
    public var mailGPGOwnKeyGenerateImportText: String?
    public var mailGPGOwnKeyEncryptMessage: String?
    public var mailGPGOwnKeyDecryptMessage: String?

    public var bookmarkTitle: String?
    public var bookmarkUrl: String?
    public var bookmarkNotes: String?
    public var bookmarkUrlFilter: String?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.type = jsonDictionary["type"] as? String
        self.customFields = jsonDictionary["custom_fields"] as? Array<CustomFields>

        self.applicationPasswordTitle = jsonDictionary["application_password_title"] as? String
        self.applicationPasswordUsername = jsonDictionary["application_password_username"] as? String
        self.applicationPasswordPassword = jsonDictionary["application_password_password"] as? String
        self.applicationPasswordNotes = jsonDictionary["application_password_notes"] as? String

        self.totpTitle = jsonDictionary["totp_title"] as? String
        self.totpCode = jsonDictionary["totp_code"] as? String
        self.totpPeriod = jsonDictionary["totp_period"] as? String
        self.totpDigits = jsonDictionary["totp_digits"] as? String
        self.totpAlgorithm = jsonDictionary["totp_algorithm"] as? String
        self.totpNotes = jsonDictionary["totp_notes"] as? String

        self.websitePasswordTitle = jsonDictionary["website_password_title"] as? String
        self.websitePasswordUrl = jsonDictionary["website_password_url"] as? String
        self.websitePasswordUsername = jsonDictionary["website_password_username"] as? String
        self.websitePasswordPassword = jsonDictionary["website_password_password"] as? String
        self.websitePasswordNotes = jsonDictionary["website_password_notes"] as? String
        self.websitePasswordAutoSubmit = jsonDictionary["website_password_auto_submit"] as? Bool
        self.websitePasswordUrlFilter = jsonDictionary["website_password_url_filter"] as? String
        self.websitePasswordTotpCode = jsonDictionary["website_password_totp_code"] as? String
        self.websitePasswordTotpPeriod = jsonDictionary["website_password_totp_period"] as? String
        self.websitePasswordTotpDigits = jsonDictionary["website_password_totp_digits"] as? String
        self.websitePasswordTotpAlgorithm = jsonDictionary["website_password_totp_algorithm"] as? String

        self.noteTitle = jsonDictionary["note_title"] as? String
        self.noteNotes = jsonDictionary["note_notes"] as? String

        self.environmentVariablesTitle = jsonDictionary["environment_variables_title"] as? String
        self.environmentVariablesVariables = jsonDictionary["environment_variables_variables"] as? Array<KeyValue>
        self.environmentVariablesNotes = jsonDictionary["environment_variables_notes"] as? String

        self.fileTitle = jsonDictionary["file_title"] as? String
        self.file = jsonDictionary["file"] as? String
        self.fileId = jsonDictionary["file_id"] as? String
        self.fileShareId = jsonDictionary["file_share_id"] as? String
        self.fileRepositoryId = jsonDictionary["file_repository_id"] as? String
        self.fileDestination = jsonDictionary["file_destination"] as? String
        self.fileSecretKey = jsonDictionary["file_secret_key"] as? String
        self.fileSize = jsonDictionary["file_size"] as? String
        self.fileChunks = jsonDictionary["file_chunks"] as? String

        self.creditCardTitle = jsonDictionary["credit_card_title"] as? String
        self.creditCardNumber = jsonDictionary["credit_card_number"] as? String
        self.creditCardCVC = jsonDictionary["credit_card_cvc"] as? String
        self.creditCardPIN = jsonDictionary["credit_card_pin"] as? String
        self.creditCardName = jsonDictionary["credit_card_name"] as? String
        self.creditCardValidThrough = jsonDictionary["credit_card_valid_through"] as? String
        self.creditCardNotes = jsonDictionary["credit_card_notes"] as? String

        self.sshOwnKeyTitle = jsonDictionary["ssh_own_key_title"] as? String
        self.sshOwnKeyPublic = jsonDictionary["ssh_own_key_public"] as? String
        self.sshOwnKeyPrivate = jsonDictionary["ssh_own_key_private"] as? String
        self.sshOwnKeyNotes = jsonDictionary["ssh_own_key_notes"] as? String

        self.elsterCertificateTitle = jsonDictionary["elster_certificate_title"] as? String
        self.elsterCertificateFileContent = jsonDictionary["elster_certificate_file_content"] as? String
        self.elsterCertificatePassword = jsonDictionary["elster_certificate_password"] as? String
        self.elsterCertificateRetrievalCode = jsonDictionary["elster_certificate_retrieval_code"] as? String
        self.elsterCertificateNotes = jsonDictionary["elster_certificate_notes"] as? String

        self.mailGPGOwnKeyTitle = jsonDictionary["mail_gpg_own_key_title"] as? String
        self.mailGPGOwnKeyEmail = jsonDictionary["mail_gpg_own_key_email"] as? String
        self.mailGPGOwnKeyName = jsonDictionary["mail_gpg_own_key_name"] as? String
        self.mailGPGOwnKeyPublic = jsonDictionary["mail_gpg_own_key_public"] as? String
        self.mailGPGOwnKeyPrivate = jsonDictionary["mail_gpg_own_key_private"] as? String
        self.mailGPGOwnKeyGenerateNew = jsonDictionary["mail_gpg_own_key_new"] as? String
        self.mailGPGOwnKeyGenerateImportText = jsonDictionary["mail_gpg_own_key_generate_import_text"] as? String
        self.mailGPGOwnKeyEncryptMessage = jsonDictionary["mail_gpg_own_key_encrypt_message"] as? String
        self.mailGPGOwnKeyDecryptMessage = jsonDictionary["mail_gpg_own_key_decrypt_message"] as? String

        self.bookmarkTitle = jsonDictionary["bookmark_title"] as? String
        self.bookmarkUrl = jsonDictionary["bookmark_url"] as? String
        self.bookmarkNotes = jsonDictionary["bookmark_notes"] as? String
        self.bookmarkUrlFilter = jsonDictionary["bookmark_url_filter"] as? String
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.type != nil { jsonDict["type"] = self.type }
        if self.customFields != nil { jsonDict["custom_fields"] = self.customFields }

        if self.applicationPasswordTitle != nil { jsonDict["application_password_title"] = self.applicationPasswordTitle }
        if self.applicationPasswordUsername != nil { jsonDict["application_password_username"] = self.applicationPasswordUsername }
        if self.applicationPasswordPassword != nil { jsonDict["application_password_password"] = self.applicationPasswordPassword }
        if self.applicationPasswordNotes != nil { jsonDict["application_password_notes"] = self.applicationPasswordNotes }

        if self.totpTitle != nil { jsonDict["totp_title"] = self.totpTitle }
        if self.totpCode != nil { jsonDict["totp_code"] = self.totpCode }
        if self.totpPeriod != nil { jsonDict["totp_period"] = self.totpPeriod }
        if self.totpDigits != nil { jsonDict["totp_digits"] = self.totpDigits }
        if self.totpAlgorithm != nil { jsonDict["totp_algorithm"] = self.totpAlgorithm }
        if self.totpNotes != nil { jsonDict["totp_notes"] = self.totpNotes }

        if self.websitePasswordTitle != nil { jsonDict["website_password_title"] = self.websitePasswordTitle }
        if self.websitePasswordUrl != nil { jsonDict["website_password_url"] = self.websitePasswordUrl }
        if self.websitePasswordUsername != nil { jsonDict["website_password_username"] = self.websitePasswordUsername }
        if self.websitePasswordPassword != nil { jsonDict["website_password_password"] = self.websitePasswordPassword }
        if self.websitePasswordNotes != nil { jsonDict["website_password_notes"] = self.websitePasswordNotes }
        if self.websitePasswordAutoSubmit != nil { jsonDict["website_password_auto_submit"] = self.websitePasswordAutoSubmit }
        if self.websitePasswordUrlFilter != nil { jsonDict["website_password_url_filter"] = self.websitePasswordUrlFilter }
        if self.websitePasswordTotpCode != nil { jsonDict["website_password_totp_code"] = self.websitePasswordTotpCode }
        if self.websitePasswordTotpPeriod != nil { jsonDict["website_password_totp_period"] = self.websitePasswordTotpPeriod }
        if self.websitePasswordTotpDigits != nil { jsonDict["website_password_totp_digits"] = self.websitePasswordTotpDigits }
        if self.websitePasswordTotpAlgorithm != nil { jsonDict["website_password_totp_algorithm"] = self.websitePasswordTotpAlgorithm }

        if self.noteTitle != nil { jsonDict["note_title"] = self.noteTitle }
        if self.noteNotes != nil { jsonDict["note_notes"] = self.noteNotes }

        if self.environmentVariablesTitle != nil { jsonDict["environment_variables_title"] = self.environmentVariablesTitle }
        if self.environmentVariablesVariables != nil { jsonDict["environment_variables_title"] = self.environmentVariablesVariables }
        if self.environmentVariablesNotes != nil { jsonDict["environment_variables_notes"] = self.environmentVariablesNotes }

        if self.fileTitle != nil { jsonDict["file_title"] = self.fileTitle }
        if self.file != nil { jsonDict["file"] = self.file }
        if self.fileId != nil { jsonDict["file_id"] = self.fileId }
        if self.fileShareId != nil { jsonDict["file_share_id"] = self.fileShareId }
        if self.fileRepositoryId != nil { jsonDict["file_repository_id"] = self.fileRepositoryId }
        if self.fileDestination != nil { jsonDict["file_destination"] = self.fileDestination }
        if self.fileSecretKey != nil { jsonDict["file_secret_key"] = self.fileSecretKey }
        if self.fileSize != nil { jsonDict["file_size"] = self.fileSize }
        if self.fileChunks != nil { jsonDict["file_chunks"] = self.fileChunks }

        if self.elsterCertificateTitle != nil { jsonDict["elster_certificate_title"] = self.elsterCertificateTitle }
        if self.elsterCertificateFileContent != nil { jsonDict["elster_certificate_file_content"] = self.elsterCertificateFileContent }
        if self.elsterCertificatePassword != nil { jsonDict["elster_certificate_password"] = self.elsterCertificatePassword }
        if self.elsterCertificateRetrievalCode != nil { jsonDict["elster_certificate_retrieval_code"] = self.elsterCertificateRetrievalCode }
        if self.elsterCertificateNotes != nil { jsonDict["elster_certificate_notes"] = self.elsterCertificateNotes }

        if self.sshOwnKeyTitle != nil { jsonDict["ssh_own_key_title"] = self.sshOwnKeyTitle }
        if self.sshOwnKeyPublic != nil { jsonDict["ssh_own_key_public"] = self.sshOwnKeyPublic }
        if self.sshOwnKeyPrivate != nil { jsonDict["ssh_own_key_private"] = self.sshOwnKeyPrivate }
        if self.sshOwnKeyNotes != nil { jsonDict["ssh_own_key_notes"] = self.sshOwnKeyNotes }

        if self.creditCardTitle != nil { jsonDict["credit_card_title"] = self.creditCardTitle }
        if self.creditCardNumber != nil { jsonDict["credit_card_number"] = self.creditCardNumber }
        if self.creditCardCVC != nil { jsonDict["credit_card_cvc"] = self.creditCardCVC }
        if self.creditCardPIN != nil { jsonDict["credit_card_pin"] = self.creditCardPIN }
        if self.creditCardName != nil { jsonDict["credit_card_name"] = self.creditCardName }
        if self.creditCardValidThrough != nil { jsonDict["credit_card_valid_through"] = self.creditCardValidThrough }
        if self.creditCardNotes != nil { jsonDict["credit_card_notes"] = self.creditCardNotes }

        if self.mailGPGOwnKeyTitle != nil { jsonDict["mail_gpg_own_key_title"] = self.mailGPGOwnKeyTitle }
        if self.mailGPGOwnKeyEmail != nil { jsonDict["mail_gpg_own_key_email"] = self.mailGPGOwnKeyEmail }
        if self.mailGPGOwnKeyName != nil { jsonDict["mail_gpg_own_key_name"] = self.mailGPGOwnKeyName }
        if self.mailGPGOwnKeyPublic != nil { jsonDict["mail_gpg_own_key_public"] = self.mailGPGOwnKeyPublic }
        if self.mailGPGOwnKeyPrivate != nil { jsonDict["mail_gpg_own_key_private"] = self.mailGPGOwnKeyPrivate }
        if self.mailGPGOwnKeyGenerateNew != nil { jsonDict["mail_gpg_own_key_new"] = self.mailGPGOwnKeyGenerateNew }
        if self.mailGPGOwnKeyGenerateImportText != nil { jsonDict["mail_gpg_own_key_generate_import_text"] = self.mailGPGOwnKeyGenerateImportText }
        if self.mailGPGOwnKeyEncryptMessage != nil { jsonDict["mail_gpg_own_key_encrypt_message"] = self.mailGPGOwnKeyEncryptMessage }
        if self.mailGPGOwnKeyDecryptMessage != nil { jsonDict["mail_gpg_own_key_decrypt_message"] = self.mailGPGOwnKeyDecryptMessage }

        if self.bookmarkTitle != nil { jsonDict["bookmark_title"] = self.bookmarkTitle }
        if self.bookmarkUrl != nil { jsonDict["bookmark_url"] = self.bookmarkUrl }
        if self.bookmarkNotes != nil { jsonDict["bookmark_notes"] = self.bookmarkNotes }
        if self.bookmarkUrlFilter != nil { jsonDict["bookmark_url_filter"] = self.bookmarkUrlFilter }

        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}


class Secret: JsonObject {
    public var createDate: String?
    public var writeDate: String?
    public var secretId: String?
    public var type: String?
    public var data: SecretData?
    public var callbackUrl: String?
    public var callbackUser: String?
    public var callbackPass: String?
    public var secretKey: Bytes?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.createDate = jsonDictionary["create_date"] as? String
        self.writeDate = jsonDictionary["write_date"] as? String
        self.secretId = jsonDictionary["secret_id"] as? String
        self.type = jsonDictionary["type"] as? String
        if jsonDictionary["data"] != nil {
            self.data = SecretData.init(jsonDictionary: jsonDictionary["data"] as! [String: Any])
        }
        self.callbackUrl = jsonDictionary["callback_url"] as? String
        self.callbackUser = jsonDictionary["callback_user"] as? String
        self.callbackPass = jsonDictionary["callback_pass"] as? String
        self.secretKey = converter.fromHex(string: jsonDictionary["secret_key"] as? String)
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.secretId != nil { jsonDict["secret_id"] = self.secretId }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.callbackUrl != nil { jsonDict["callback_url"] = self.callbackUrl }
        if self.callbackUser != nil { jsonDict["callback_user"] = self.callbackUser }
        if self.callbackPass != nil { jsonDict["callback_pass"] = self.callbackPass }
        if self.secretKey != nil { jsonDict["secret_key"] = converter.toHex(bytes: self.secretKey) }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
