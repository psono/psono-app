//
//  Datastore.swift
//  AutoFillExtension
//
//  Created by Sascha Pfeiffer on 03.01.21.
//  Copyright © 2021 The Chromium Authors. All rights reserved.
//

import Foundation
import Sodium


class Folder: JsonObject {
    public var id: String?
    public var datastoreId: String?
    public var folders: [Folder]?
    public var items: [Item]?
    public var name: String?
    public var deleted: Bool?
    public var shareId: String?
    public var shareSecretKey: Bytes?
    public var shareIndex: [String: ShareLocation]?
    public var shareRights: ShareRight?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.id = jsonDictionary["id"] as? String
        self.datastoreId = jsonDictionary["datastore_id"] as? String
        if let foldersJson = jsonDictionary["folders"] as? [[String: Any]] {
            self.folders = []
            for folderJson in foldersJson {
                let folder = Folder(jsonDictionary: folderJson)
                if folder != nil {
                    self.folders?.append(folder!)
                }
            }
        }
        if let itemsJson = jsonDictionary["items"] as? [[String: Any]] {
            self.items = []
            for itemJson in itemsJson {
                let item = Item(jsonDictionary: itemJson)
                if item != nil {
                    self.items?.append(item!)
                }
            }
        }
        self.name = jsonDictionary["name"] as? String
        self.deleted = jsonDictionary["deleted"] as? Bool
        self.shareId = jsonDictionary["share_id"] as? String
        if let shareSecretKey = jsonDictionary["share_secret_key"] as? String {
            self.shareSecretKey = converter.fromHex(string: shareSecretKey)
        }
        if let shareIndexJson = jsonDictionary["share_index"] as? [String: [String: Any]] {
            self.shareIndex = [:]
            for (key, value) in shareIndexJson {
                shareIndex![key] = ShareLocation.init(jsonDictionary: value)
            }
        }
        
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.id != nil { jsonDict["id"] = self.id }
        if self.datastoreId != nil { jsonDict["datastore_id"] = self.datastoreId }
        
        if self.folders != nil {
            var folders: [[String: Any]] = []
            for folder in self.folders! {
                folders.append(folder.toJsonDict())
            }
            jsonDict["folders"] = folders
        }
        if self.items != nil {
            var items: [[String: Any]] = []
            for item in self.items! {
                items.append(item.toJsonDict())
            }
            jsonDict["items"] = items
        }
        if self.name != nil { jsonDict["name"] = self.name }
        if self.deleted != nil { jsonDict["deleted"] = self.deleted }
        if self.shareId != nil { jsonDict["share_id"] = self.shareId }
        if self.shareSecretKey != nil { jsonDict["share_secret_key"] = converter.toHex(bytes: self.shareSecretKey) }
        if self.shareIndex != nil {
            var helperDict: [String: [String: Any]] = [:]
            for (key, value) in self.shareIndex!{
                helperDict[key] = value.toJsonDict()
            }
            jsonDict["share_index"] = helperDict
        }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}


class Item: JsonObject {
    public var id: String?
    public var name: String?
    public var description: String?
    public var deleted: Bool?
    public var type: String?
    public var urlfilter: String?
    public var secretId: String?
    public var fileId: String?
    public var secretKey: Bytes?
    public var shareId: String?
    public var shareSecretKey: Bytes?
    public var shareRights: ShareRight?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.id = jsonDictionary["id"] as? String
        self.name = jsonDictionary["name"] as? String
        self.description = jsonDictionary["description"] as? String
        self.deleted = jsonDictionary["deleted"] as? Bool
        self.type = jsonDictionary["type"] as? String
        self.urlfilter = jsonDictionary["urlfilter"] as? String
        self.secretId = jsonDictionary["secret_id"] as? String
        self.fileId = jsonDictionary["file_id"] as? String
        if let secretKey = jsonDictionary["secret_key"] as? String {
            self.secretKey = converter.fromHex(string: secretKey)
        }
        self.shareId = jsonDictionary["share_id"] as? String
        if let shareSecretKey = jsonDictionary["share_secret_key"] as? String {
            self.shareSecretKey = converter.fromHex(string: shareSecretKey)
        }
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.id != nil { jsonDict["id"] = self.id }
        if self.name != nil { jsonDict["name"] = self.name }
        if self.deleted != nil { jsonDict["deleted"] = self.deleted }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.urlfilter != nil { jsonDict["urlfilter"] = self.urlfilter }
        if self.secretId != nil { jsonDict["secret_id"] = self.secretId }
        if self.fileId != nil { jsonDict["file_id"] = self.fileId }
        if self.secretKey != nil { jsonDict["secret_key"] = converter.toHex(bytes: self.secretKey) }
        if self.shareId != nil { jsonDict["share_id"] = self.shareId }
        if self.shareSecretKey != nil { jsonDict["share_secret_key"] = converter.toHex(bytes: self.shareSecretKey) }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}



class ShareLocation: JsonObject {
    public var secretKey: Bytes?
    public var paths: [[String]]?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        if let secretKey = jsonDictionary["secret_key"] as? String {
            self.secretKey = converter.fromHex(string: secretKey)
        }
        self.paths = jsonDictionary["paths"] as? [[String]]
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.secretKey != nil { jsonDict["secret_key"] = converter.toHex(bytes: self.secretKey) }
        if self.paths != nil { jsonDict["paths"] = self.paths }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}



class ShareRight: JsonObject {
    public var read: Bool?
    public var write: Bool?
    public var grant: Bool?
    public var delete: Bool?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.read = jsonDictionary["read"] as? Bool
        self.write = jsonDictionary["write"] as? Bool
        self.grant = jsonDictionary["grant"] as? Bool
        self.delete = jsonDictionary["delete"] as? Bool
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.read != nil { jsonDict["read"] = self.read }
        if self.write != nil { jsonDict["write"] = self.write }
        if self.grant != nil { jsonDict["read"] = self.grant }
        if self.delete != nil { jsonDict["delete"] = self.delete }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}



class Datastore: JsonObject {
    public var datastoreId: String?
    public var type: String?
    public var description: String?
    public var secretKey: Bytes?
    public var isDefailt: Bool?
    public var data: Folder?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.datastoreId = jsonDictionary["datastore_id"] as? String
        self.type = jsonDictionary["type"] as? String
        self.description = jsonDictionary["description"] as? String
        if let secretKey = jsonDictionary["secret_key"] as? String {
            self.secretKey = converter.fromHex(string: secretKey)
        }
        self.isDefailt = jsonDictionary["is_default"] as? Bool
        if jsonDictionary["data"] != nil {
            self.data = Folder.init(jsonDictionary: jsonDictionary["data"] as! [String: Any])
        }
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.datastoreId != nil { jsonDict["datastore_id"] = self.datastoreId }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.description != nil { jsonDict["description"] = self.description }
        if self.secretKey != nil { jsonDict["secret_key"] = converter.toHex(bytes: self.secretKey) }
        if self.isDefailt != nil { jsonDict["is_default"] = self.isDefailt }
        if self.data != nil { jsonDict["data"] = self.data!.toJsonDict() }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
