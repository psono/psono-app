import 'package:flutter_test/flutter_test.dart';
import 'package:psono/services/manager_host.dart' as managerHost;

void main() {
  group('managerHost', () {
    // Existing Tests
    test('managerHost semverCompare "12.3.4" vs "2.4.5"', () {
      expect(managerHost.semverCompare('12.3.4', '2.4.5'), 1);
    });
    test('managerHost semverCompare  "2.4.5" vs "12.3.4"', () {
      expect(managerHost.semverCompare('2.4.5', '12.3.4'), -1);
    });

    // Testing with whitespace affix
    test('managerHost semverCompare "12.3.4" vs "2.4.5 test"', () {
      expect(managerHost.semverCompare('12.3.4', '2.4.5 test'), 1);
    });
    test('managerHost semverCompare  "2.4.5" vs "12.3.4 test"', () {
      expect(managerHost.semverCompare('2.4.5', '12.3.4 test'), -1);
    });
    test('managerHost semverCompare "12.3.4 test" vs "2.4.5"', () {
      expect(managerHost.semverCompare('12.3.4 test', '2.4.5'), 1);
    });
    test('managerHost semverCompare  "2.4.5 test" vs "12.3.4"', () {
      expect(managerHost.semverCompare('2.4.5 test', '12.3.4'), -1);
    });

    // Testing with +affix
    test('managerHost semverCompare "12.3.4" vs "2.4.5+test"', () {
      expect(managerHost.semverCompare('12.3.4', '2.4.5+test'), 1);
    });
    test('managerHost semverCompare  "2.4.5" vs "12.3.4+test"', () {
      expect(managerHost.semverCompare('2.4.5', '12.3.4+test'), -1);
    });
    test('managerHost semverCompare "12.3.4+test" vs "2.4.5"', () {
      expect(managerHost.semverCompare('12.3.4+test', '2.4.5'), 1);
    });
    test('managerHost semverCompare  "2.4.5+test" vs "12.3.4"', () {
      expect(managerHost.semverCompare('2.4.5+test', '12.3.4'), -1);
    });

    // New Tests for Expanded Coverage

    // Identical versions
    test('managerHost semverCompare "1.0.0" vs "1.0.0"', () {
      expect(managerHost.semverCompare('1.0.0', '1.0.0'), 0);
    });
    test('managerHost semverCompare "1.0.0+meta" vs "1.0.0"', () {
      expect(managerHost.semverCompare('1.0.0+meta', '1.0.0'), 0);
    });

    // Pre-release versions
    test('managerHost semverCompare "1.0.0-alpha" vs "1.0.0"', () {
      expect(managerHost.semverCompare('1.0.0-alpha', '1.0.0'), -1);
    });
    test('managerHost semverCompare "1.0.0" vs "1.0.0-alpha"', () {
      expect(managerHost.semverCompare('1.0.0', '1.0.0-alpha'), 1);
    });
    test('managerHost semverCompare "1.0.0-alpha" vs "1.0.0-beta"', () {
      expect(managerHost.semverCompare('1.0.0-alpha', '1.0.0-beta'), -1);
    });
    test('managerHost semverCompare "1.0.0-beta" vs "1.0.0-alpha"', () {
      expect(managerHost.semverCompare('1.0.0-beta', '1.0.0-alpha'), 1);
    });

    // Numeric edge cases
    test('managerHost semverCompare "1.0.0" vs "1.0"', () {
      expect(managerHost.semverCompare('1.0.0', '1.0'), 0);
    });
    test('managerHost semverCompare "1.0.0" vs "1"', () {
      expect(managerHost.semverCompare('1.0.0', '1'), 0);
    });
    test('managerHost semverCompare "1.0.10" vs "1.0.2"', () {
      expect(managerHost.semverCompare('1.0.10', '1.0.2'), 1);
    });
    test('managerHost semverCompare "1.0.2" vs "1.0.10"', () {
      expect(managerHost.semverCompare('1.0.2', '1.0.10'), -1);
    });

    // Leading zeros
    test('managerHost semverCompare "1.01.0" vs "1.1.0"', () {
      expect(managerHost.semverCompare('1.01.0', '1.1.0'), 0);
    });
    test('managerHost semverCompare "1.1.01" vs "1.1.1"', () {
      expect(managerHost.semverCompare('1.1.01', '1.1.1'), 0);
    });

    // Uneven version lengths
    test('managerHost semverCompare "1.2.3.4" vs "1.2.3"', () {
      expect(managerHost.semverCompare('1.2.3.4', '1.2.3'), 1);
    });
    test('managerHost semverCompare "1.2.3" vs "1.2.3.4"', () {
      expect(managerHost.semverCompare('1.2.3', '1.2.3.4'), -1);
    });

    // Empty or invalid inputs
    test('managerHost semverCompare "" vs "1.0.0"', () {
      expect(managerHost.semverCompare('', '1.0.0'), -1);
    });
    test('managerHost semverCompare "1.0.0" vs ""', () {
      expect(managerHost.semverCompare('1.0.0', ''), 1);
    });
    test('managerHost semverCompare "" vs ""', () {
      expect(managerHost.semverCompare('', ''), 0);
    });

    // Version strings with letters
    test('managerHost semverCompare "1.0.0-alpha" vs "1.0.0-beta"', () {
      expect(managerHost.semverCompare('1.0.0-alpha', '1.0.0-beta'), -1);
    });
    test('managerHost semverCompare "1.0.0-beta" vs "1.0.0-alpha"', () {
      expect(managerHost.semverCompare('1.0.0-beta', '1.0.0-alpha'), 1);
    });

    // Complex strings with both pre-release and build metadata
    test('managerHost semverCompare "1.0.0-alpha+001" vs "1.0.0-alpha+002"',
        () {
      expect(
          managerHost.semverCompare('1.0.0-alpha+001', '1.0.0-alpha+002'), 0);
    });
  });
}
