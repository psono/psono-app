import requests
import shutil
import os
import json
import time

POEDITOR_API_KEY = os.environ['POEDITOR_API_KEY']
POEDITOR_PROJECT_ID = os.environ['POEDITOR_PROJECT_ID']

DST_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'assets', 'locales')

LANGUAGE_CODE_MAPPING = {
    'en': ['en'],
    'ar': ['ar'],
    'bn': ['bn'],
    'ca': ['ca'],
    'cs': ['cs'],
    #'da': ['da'],
    'de': ['de'],
    'es': ['es'],
    'fi': ['fi'],
    'fr': ['fr'],
    'hi': ['hi'],
    'hu': ['hu'],
    'hr': ['hr'],
    'it': ['it'],
    'ja': ['ja'],
    'ko': ['ko'],
    'nl': ['nl'],
    #'no': ['nb', 'no'],
    'ru': ['ru'],
    'pl': ['pl'],
    'pt': ['pt'],
    'sv': ['sv'],
    'uk': ['uk'],
    'zh-cn': ['zh'],
}


def download_language(lang):
    data = [
        ('api_token', POEDITOR_API_KEY),
        ('action', 'export'),
        ('id', POEDITOR_PROJECT_ID),
        ('language', lang),
        ('type', 'key_value_json'),
    ]

    r = requests.post('https://poeditor.com/api/', data=data)

    if not r.ok:
        print("Error: download_language")
        print(r.text)
        exit(1)

    result = r.json()

    if 'item' not in result:
        print(f'{lang}: {result}')
        return

    r = requests.get(result['item'])

    if r.status_code == 200:
        for dst_lang in LANGUAGE_CODE_MAPPING[lang]:
            path = os.path.join(DST_PATH, f'{dst_lang}.json')
            with open(path, 'wb') as f:
                f.write(r.content)

            print(f"Success: download_language {lang} to {path}")


def main():

    # Download
    for lang in LANGUAGE_CODE_MAPPING:
        download_language(lang)

    print("Success")

if __name__ == "__main__":
    main()
