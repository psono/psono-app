## https://stackoverflow.com/a/76806805/4582775
# -keep class io.flutter.app.** { *; }
-keep class io.flutter.plugin.**  { *; }
-keep class io.flutter.util.**  { *; }
-keep class io.flutter.view.**  { *; }
# -keep class io.flutter.**  { *; }
-keep class io.flutter.plugins.**  { *; }
