package com.psono.psono

import android.app.Activity
//import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.net.Uri
//import android.os.Build
//import android.os.Bundle
import android.provider.Settings
import android.service.autofill.Dataset
import android.service.autofill.FillResponse
import android.util.Log
import android.view.WindowManager.LayoutParams
import android.view.autofill.AutofillManager
import android.view.autofill.AutofillValue
import android.widget.RemoteViews
//import androidx.core.app.ActivityCompat.startActivityForResult
//import androidx.core.content.ContextCompat.getSystemService
import com.psono.psono.autofill.ParsedStructure
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterFragmentActivity() {
    private val AUTOFILL_SERVICE_CHANNEL = "com.psono.psono/psono_autofill_service"
    private val SHARED_DATA_CHANNEL = "com.psono.psono/psono_shared_data"
    private val AUTOFILL_SERVICE_REQUEST_ACTIVITY_ID = 1
    private var enableAutofillRequestResult: MethodChannel.Result? = null
    private var sharedText: String? = null

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        val applicationInfo: ApplicationInfo = this.applicationInfo
        val isDebuggable = 0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE

        if (!isDebuggable) {
            window.addFlags(LayoutParams.FLAG_SECURE)
            Log.d("isDebuggable", "false")
        } else {
            Log.d("isDebuggable", "true")
        }

        val action = intent.action
        val type = intent.type

        if (Intent.ACTION_SEND == action && type != null) {
            if ("text/plain" == type) {
                handleSendText(intent) // Handle text being sent
            }
        }

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, AUTOFILL_SERVICE_CHANNEL)
            .setMethodCallHandler { call: MethodCall, result: MethodChannel.Result ->
                when (call.method) {
                    "isSupported" -> {
                        result.success(true)
                    }
                    "isEnabled" -> {
                        result.success(
                            getSystemService(AutofillManager::class.java)
                                .hasEnabledAutofillServices()
                        )
                    }
                    "enable" -> {
                        val setAutofillIntent =
                            Intent(Settings.ACTION_REQUEST_SET_AUTOFILL_SERVICE)
                        setAutofillIntent.data = Uri.parse("package:com.psono.psono")
                        enableAutofillRequestResult = result
                        startActivityForResult(
                            setAutofillIntent,
                            AUTOFILL_SERVICE_REQUEST_ACTIVITY_ID
                        )
                    }
                    "disable" -> {
                        getSystemService(AutofillManager::class.java).disableAutofillServices()
                        result.success(true)
                    }
                    "autofill" -> {
                        autofill(call, intent)
                    }
                    else -> {
                        result.notImplemented()
                    }
                }
            }


        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, SHARED_DATA_CHANNEL)
            .setMethodCallHandler { call: MethodCall, result: MethodChannel.Result ->
                if (call.method == "getSharedText") {
                    result.success(sharedText)
                } else {
                    result.notImplemented()
                }
            }
    }

    fun autofill(call: MethodCall, intent: Intent) {
        var title = ""
        var username = ""
        var password = ""

        if (title == "") {
            title = call.argument<String>("website_password_title") ?: ""
            username = call.argument<String>("website_password_username") ?: ""
            password = call.argument<String>("website_password_password") ?: ""
        }
        if (title == "") {
            title = call.argument<String>("application_password_title") ?: ""
            username = call.argument<String>("application_password_username") ?: ""
            password = call.argument<String>("application_password_password") ?: ""
        }

        val extras = intent.getBundleExtra("parsedStructure")
        val parsedStructure = extras?.getParcelable<ParsedStructure>("parsedStructure")
                ?: throw IllegalStateException("Unable to reconstruct parsedStructure")

        val response = buildFilteredFillResponse(title, username, password, parsedStructure)

        response?.let { setFillResponseAndFinish(it) } ?: cancelAndFinish()
    }

    private fun buildFilteredFillResponse(title: String, username: String, password: String, parsedStructure: ParsedStructure): FillResponse? {

        val datasetBuilder = Dataset.Builder()

        if (parsedStructure.usernameId != null) {
            val usernamePresentation = RemoteViews(this.packageName, R.layout.autofill_item)
            usernamePresentation.setTextViewText(android.R.id.text1, title)
            datasetBuilder.setValue(
                parsedStructure.usernameId,
                AutofillValue.forText(username),
                usernamePresentation
            )
        }

        if (parsedStructure.passwordId != null) {
            val passwordPresentation = RemoteViews(this.packageName, R.layout.autofill_item)
            passwordPresentation.setTextViewText(android.R.id.text1, title)
            datasetBuilder.setValue(
                parsedStructure.passwordId,
                AutofillValue.forText(password),
                passwordPresentation
            )
        }

        val dataset = datasetBuilder.build()

        // Add a dataset to the response
        val fillResponse: FillResponse = FillResponse.Builder()
                .addDataset(dataset)
                .build()

        return fillResponse
    }

    private fun cancelAndFinish() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    private fun setFillResponseAndFinish(fillResponse: FillResponse) {
        val results = Intent().putExtra(AutofillManager.EXTRA_AUTHENTICATION_RESULT, fillResponse)
        setResult(Activity.RESULT_OK, results)
        finish()
    }

    private fun handleSendText(intent: Intent) {
        sharedText = intent.getStringExtra(Intent.EXTRA_TEXT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOFILL_SERVICE_REQUEST_ACTIVITY_ID) {
            if (enableAutofillRequestResult != null) {
                if (resultCode == RESULT_OK) {
                    enableAutofillRequestResult?.success(true)
                } else {
                    enableAutofillRequestResult?.success(false)
                }
            } else {
                // received a result but we were not expecting a result as
                // enableAutofillRequestResult has been null
            }
        } else {
            // received a result not for one of our Intents
        }
    }

}
