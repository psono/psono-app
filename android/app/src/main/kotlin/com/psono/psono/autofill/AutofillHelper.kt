package com.psono.psono.autofill

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.os.Build
import android.os.Bundle
import android.service.autofill.FillResponse
import android.widget.RemoteViews
import com.psono.psono.MainActivity
import com.psono.psono.R

object AutofillHelper {
    fun buildFillResponse(context: Context, parsedStructure: ParsedStructure): FillResponse {
        val authIntentSender = buildAuthIntentSender(context, parsedStructure)
        val presentation = buildPresentation(
                context.packageName,
                context.getString(R.string.autofill_search_cta, "Psono")
        )
        return FillResponse.Builder()
                .setAuthentication(parsedStructure.autofillIds, authIntentSender, presentation)
                .build()
    }

    private fun buildPresentation(packageName: String, searchText: String): RemoteViews {
        return RemoteViews(packageName, R.layout.autofill_cta_presentation).also {
            it.setTextViewText(R.id.autofill_cta, searchText)
        }
    }

    private fun buildAuthIntentSender(context: Context, parsedStructure: ParsedStructure): IntentSender {
        val authIntent = Intent(context, MainActivity::class.java).apply {
            // Send any additional data required to complete the request.
            action = Intent.ACTION_SEND
            type = "text/plain"

            // pass the package name and web domain to the search function
            val packageName = parsedStructure.packageName
            val webDomain = parsedStructure.webDomain ?: ""
            putExtra(Intent.EXTRA_TEXT, "autofill::$packageName::$webDomain")

            // open the route /autofill/ in the app per default
            putExtra("route", "/autofill/")

            // add the parsedStructure to the intent so can later use it to fill out the request
            val extras = Bundle()
            extras.putParcelable("parsedStructure", parsedStructure)
            putExtra("parsedStructure", extras)
        }

        return PendingIntent.getActivity(
                context,
                1001,
                authIntent,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
                } else {
                    PendingIntent.FLAG_CANCEL_CURRENT
                }
        ).intentSender
    }
}